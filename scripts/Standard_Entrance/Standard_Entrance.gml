///Standard_Entrance
//Contains the standard actions for the entrance state.
//Players do an entrance animation until obj_game starts the match.
anim_sprite = my_sprites[?"Entrance"];
anim_speed = ani_speed_intro;
set_speed(0, 0, false, false);