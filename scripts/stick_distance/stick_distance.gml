///@func stick_distance(stick)
///@param stick
//Gets the distance of the stick from the origin
return argument[0]==Lstick ? control_distance_l : control_distance_r;