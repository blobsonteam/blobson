///@func stick_get_direction(stick)
///@param stick
return argument0==Lstick ? control_direction_l : control_direction_r;