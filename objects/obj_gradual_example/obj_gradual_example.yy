{
    "id": "df0c80fb-7488-4082-b207-a0c64251be63",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gradual_example",
    "eventList": [
        {
            "id": "764fec93-c767-4055-8286-b7720b831926",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df0c80fb-7488-4082-b207-a0c64251be63"
        },
        {
            "id": "e285f4c3-5813-4571-836d-8e4df17a1701",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "df0c80fb-7488-4082-b207-a0c64251be63"
        },
        {
            "id": "38842eaf-43a0-41d3-9695-228ec4972660",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "df0c80fb-7488-4082-b207-a0c64251be63"
        },
        {
            "id": "de9fa320-c510-458e-9196-a96342debf01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "df0c80fb-7488-4082-b207-a0c64251be63"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "2b4bae5d-3106-4d32-96cd-5f3be5a2d13e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 25,
            "y": 61
        },
        {
            "id": "a24107d1-9e76-4d98-9cc6-eda98512a6a2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 31,
            "y": 31
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
    "visible": true
}