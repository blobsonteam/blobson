/// @description
/*GENERAL HITBOX VARIBLES*/
visible=show_hitboxes;
image_blend=hitbox_draw_color;
lifetime=10;
owner=noone;
shape=HITBOX_SHAPE.rectangle;
overlay_sprite=sprite_index;
overlay_frame=0;
overlay_speed=image_speed;
has_hit=false;
priority=0;
list=ds_list_create();
snd_hit=noone;