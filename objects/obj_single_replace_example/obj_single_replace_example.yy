{
    "id": "26d6c146-44ba-4f36-bed2-e6c7dd29e562",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_single_replace_example",
    "eventList": [
        {
            "id": "020cb77c-3a0b-45af-a5b7-b9058a5b2a2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "26d6c146-44ba-4f36-bed2-e6c7dd29e562"
        },
        {
            "id": "b7dac513-6665-434b-affa-c3fd6dca1e41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "26d6c146-44ba-4f36-bed2-e6c7dd29e562"
        },
        {
            "id": "4157b260-cb2f-458a-bf89-d2d5b1adc924",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "26d6c146-44ba-4f36-bed2-e6c7dd29e562"
        },
        {
            "id": "f4883b76-37ac-4f53-923d-0847d3e135e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "26d6c146-44ba-4f36-bed2-e6c7dd29e562"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "011c9e9f-d61d-4124-a1d1-4f0aed78b7fb",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 25,
            "y": 61
        },
        {
            "id": "9882084a-e49e-46ca-9476-7f94eb706327",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 31,
            "y": 31
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
    "visible": true
}