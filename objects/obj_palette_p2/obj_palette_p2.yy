{
    "id": "bf5f09d6-1d3e-40fc-9b90-8fddd88dcadb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_palette_p2",
    "eventList": [
        {
            "id": "b659e579-0ddd-4c79-9c30-f51447be6e99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ee06a8de-191f-4463-90c5-aa6141e45354",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bf5f09d6-1d3e-40fc-9b90-8fddd88dcadb"
        },
        {
            "id": "05bf89db-0d2c-4135-9852-8e53e621cdaa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bf5f09d6-1d3e-40fc-9b90-8fddd88dcadb"
        },
        {
            "id": "d9579aba-cfe3-4b6d-b656-9bef2bf0acc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bf5f09d6-1d3e-40fc-9b90-8fddd88dcadb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bfcd3239-1de6-4e1a-9642-ead7a32bb57b",
    "visible": false
}