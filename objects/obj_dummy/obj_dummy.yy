{
    "id": "e1d79540-386f-4f74-956a-71b516b3875a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dummy",
    "eventList": [
        {
            "id": "cc612447-d9ad-4711-8d35-91797d5bd4a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e1d79540-386f-4f74-956a-71b516b3875a"
        },
        {
            "id": "c5772e7a-e33e-4893-9a6a-562891e8c448",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "e1d79540-386f-4f74-956a-71b516b3875a"
        },
        {
            "id": "672f1041-04d9-44d0-ada9-e2c65a5f7b09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e1d79540-386f-4f74-956a-71b516b3875a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "7502f3b0-288a-4515-aa40-6f5b55e3d5c0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 25,
            "y": 61
        },
        {
            "id": "e30db16a-3d0e-4a36-9c7b-b43fc7947bac",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 31,
            "y": 31
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
    "visible": true
}