{
    "id": "c7733968-8db6-481b-8e68-59ac15a431ce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_palette_p3",
    "eventList": [
        {
            "id": "7b1afcf2-681a-4511-98c5-d4e7dbb69a97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ee06a8de-191f-4463-90c5-aa6141e45354",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c7733968-8db6-481b-8e68-59ac15a431ce"
        },
        {
            "id": "f663c9f9-b385-449b-ba54-a5d49fdf25a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c7733968-8db6-481b-8e68-59ac15a431ce"
        },
        {
            "id": "66cc9571-58ec-43dc-8f40-bdb0489b2028",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c7733968-8db6-481b-8e68-59ac15a431ce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bfcd3239-1de6-4e1a-9642-ead7a32bb57b",
    "visible": false
}