{
    "id": "c512a125-088f-46ee-b68e-cecdea03b7b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_character_select_item",
    "eventList": [
        {
            "id": "25543529-3c14-42c3-999d-5f5ba2bf44a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c512a125-088f-46ee-b68e-cecdea03b7b3"
        },
        {
            "id": "35ffd97b-83fd-4d25-92f6-3f5300d6d649",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c512a125-088f-46ee-b68e-cecdea03b7b3"
        },
        {
            "id": "348b3216-3be7-4305-9352-1c048a052da8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c512a125-088f-46ee-b68e-cecdea03b7b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3cc08042-198c-4673-8204-ca261fad9f49",
    "visible": true
}