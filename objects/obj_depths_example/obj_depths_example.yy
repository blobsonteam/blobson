{
    "id": "67520ecf-7a6c-48d5-aa7a-092929e586f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_depths_example",
    "eventList": [
        {
            "id": "4f4f6c5e-ebd2-4f03-a8dd-33666933c72d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "67520ecf-7a6c-48d5-aa7a-092929e586f2"
        },
        {
            "id": "f3f31cc3-663e-4148-8529-d18bc87eb31b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "67520ecf-7a6c-48d5-aa7a-092929e586f2"
        },
        {
            "id": "efa9a699-60e7-4e25-b337-c20f2625c7fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "67520ecf-7a6c-48d5-aa7a-092929e586f2"
        },
        {
            "id": "299493d3-c4ff-4c8f-815d-b1b4c090c815",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "67520ecf-7a6c-48d5-aa7a-092929e586f2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}