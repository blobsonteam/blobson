{
    "id": "ebeb344b-e85d-4630-94c7-2c6249c4e94b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_layer_example",
    "eventList": [
        {
            "id": "ba4a3738-0f39-4637-bf6b-bd79510ed31d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ebeb344b-e85d-4630-94c7-2c6249c4e94b"
        },
        {
            "id": "1faaf6ad-48c2-4da8-99e7-b07c53ce1a5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ebeb344b-e85d-4630-94c7-2c6249c4e94b"
        },
        {
            "id": "75583265-03a8-4b65-a897-fe84e5c933c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "ebeb344b-e85d-4630-94c7-2c6249c4e94b"
        },
        {
            "id": "82ea1100-d889-4ff6-9c5c-623717da4f00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ebeb344b-e85d-4630-94c7-2c6249c4e94b"
        },
        {
            "id": "a9155c74-2fda-41e5-ac3a-78d021120f4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "ebeb344b-e85d-4630-94c7-2c6249c4e94b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}