{
    "id": "b81526b5-d983-4619-8081-5751f993ab15",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_stage_select_item",
    "eventList": [
        {
            "id": "ef604310-6391-4d79-b3e0-82dd97453aba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b81526b5-d983-4619-8081-5751f993ab15"
        },
        {
            "id": "9a5b5e67-b2d0-4a57-8fc9-3679aca215b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b81526b5-d983-4619-8081-5751f993ab15"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b871acda-0026-44b2-969d-16b2828681c3",
    "visible": true
}