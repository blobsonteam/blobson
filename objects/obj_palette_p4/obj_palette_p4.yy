{
    "id": "061b0526-f1d0-4612-8a56-f3261d1b1f87",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_palette_p4",
    "eventList": [
        {
            "id": "2bee8ad7-fe27-47d8-8b27-dcab34829c33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ee06a8de-191f-4463-90c5-aa6141e45354",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "061b0526-f1d0-4612-8a56-f3261d1b1f87"
        },
        {
            "id": "e908faf5-5dfd-4775-973a-58d11f61a1d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "061b0526-f1d0-4612-8a56-f3261d1b1f87"
        },
        {
            "id": "bc68e0a3-047f-4130-9df0-2fa085279591",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "061b0526-f1d0-4612-8a56-f3261d1b1f87"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bfcd3239-1de6-4e1a-9642-ead7a32bb57b",
    "visible": false
}