{
    "id": "4ad5ccc5-0a8a-4311-b6eb-e7ac48cb91a8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_palette_manager",
    "eventList": [
        {
            "id": "5c877deb-d048-44f3-84c1-d9de1cc4940b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4ad5ccc5-0a8a-4311-b6eb-e7ac48cb91a8"
        },
        {
            "id": "393eea5b-8fde-4256-8cea-709fd03b60f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4ad5ccc5-0a8a-4311-b6eb-e7ac48cb91a8"
        },
        {
            "id": "eec56130-7ea0-4c7d-928b-bdf92771a598",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "4ad5ccc5-0a8a-4311-b6eb-e7ac48cb91a8"
        },
        {
            "id": "de9e734c-1d21-4aaa-8013-e60d4df17b55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "4ad5ccc5-0a8a-4311-b6eb-e7ac48cb91a8"
        },
        {
            "id": "f3cc2e8a-8d2f-4350-8120-438595f05448",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "4ad5ccc5-0a8a-4311-b6eb-e7ac48cb91a8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}