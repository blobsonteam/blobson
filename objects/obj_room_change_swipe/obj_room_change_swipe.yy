{
    "id": "be1d6f7a-df8b-409b-be41-b78ba70f6c06",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_room_change_swipe",
    "eventList": [
        {
            "id": "79ad00c9-ca5a-4836-99aa-92f16d66bb1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "be1d6f7a-df8b-409b-be41-b78ba70f6c06"
        },
        {
            "id": "5af24645-d060-4c79-9d62-1cdb4bb5e25c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "be1d6f7a-df8b-409b-be41-b78ba70f6c06"
        },
        {
            "id": "e138b18c-b81b-43b5-878a-4ed58714c670",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "be1d6f7a-df8b-409b-be41-b78ba70f6c06"
        },
        {
            "id": "1960c010-239c-47bd-999d-acb15bfb1123",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "be1d6f7a-df8b-409b-be41-b78ba70f6c06"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "20516a31-a556-463f-9553-f05e3bddccab",
    "visible": true
}