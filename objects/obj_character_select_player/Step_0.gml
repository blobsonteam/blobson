/// @description Handle activity and character selection
if (!isActive)
{
	character = noone;
	bg_color = c_gray;
	return;
}