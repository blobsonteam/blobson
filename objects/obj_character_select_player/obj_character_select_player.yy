{
    "id": "cd553a24-2a70-49c7-bd9f-6794cf0e75fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_character_select_player",
    "eventList": [
        {
            "id": "1079c3d9-d849-4d56-b39d-8a1ec4b8718f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cd553a24-2a70-49c7-bd9f-6794cf0e75fd"
        },
        {
            "id": "6a5053af-8dda-4641-a462-ad64c24093fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cd553a24-2a70-49c7-bd9f-6794cf0e75fd"
        },
        {
            "id": "36ed20c0-54e2-4dd5-88b9-4b4f79e0050b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cd553a24-2a70-49c7-bd9f-6794cf0e75fd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}