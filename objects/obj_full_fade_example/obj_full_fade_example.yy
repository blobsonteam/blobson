{
    "id": "b50b390e-4db8-4d42-bc03-830f6398203e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_full_fade_example",
    "eventList": [
        {
            "id": "32dd736f-76c4-4d7c-9bc2-44b35c0c35a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b50b390e-4db8-4d42-bc03-830f6398203e"
        },
        {
            "id": "8ccfc4fb-0873-49f8-ba0b-bb4f90dd9d43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b50b390e-4db8-4d42-bc03-830f6398203e"
        },
        {
            "id": "93b01ca7-85df-4d05-a0c1-aa2de4bd1412",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b50b390e-4db8-4d42-bc03-830f6398203e"
        },
        {
            "id": "988f3310-50cd-4c85-afe4-2c0be27054e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b50b390e-4db8-4d42-bc03-830f6398203e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "1d0b2cd7-d32a-42fd-b9fc-021b0a69d4a5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 25,
            "y": 61
        },
        {
            "id": "a5bc3da3-641f-41d9-b2ff-3037cfff92d6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 31,
            "y": 31
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
    "visible": true
}