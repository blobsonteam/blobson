{
    "id": "371490bd-765f-468d-b2af-003e6a13073c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_desaturate_example",
    "eventList": [
        {
            "id": "357de731-fdd1-447d-9755-d413fc8d2234",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "371490bd-765f-468d-b2af-003e6a13073c"
        },
        {
            "id": "3f5b8af5-91e4-4306-9b6c-7f76726acbed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "371490bd-765f-468d-b2af-003e6a13073c"
        },
        {
            "id": "2945c9c9-f01b-47df-aa33-0f8efb3100af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "371490bd-765f-468d-b2af-003e6a13073c"
        },
        {
            "id": "5dba8073-d6b0-4607-921c-db355159688e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "371490bd-765f-468d-b2af-003e6a13073c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "bb8f457c-411c-4b9a-8b2a-70a7131d7917",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 25,
            "y": 61
        },
        {
            "id": "a4a9d186-5d38-4813-a424-5a2c14ad2342",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 31,
            "y": 31
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
    "visible": true
}