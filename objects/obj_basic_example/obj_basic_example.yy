{
    "id": "76efa62c-255a-4ae7-ae37-749865d37176",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_basic_example",
    "eventList": [
        {
            "id": "272c58a6-1c62-4528-9e65-5602a1f2664c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "76efa62c-255a-4ae7-ae37-749865d37176"
        },
        {
            "id": "cb757464-87a9-42b3-b6cf-e111633e0c10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "76efa62c-255a-4ae7-ae37-749865d37176"
        },
        {
            "id": "6115fe80-d2ce-42f2-89f6-3ae96db737de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "76efa62c-255a-4ae7-ae37-749865d37176"
        },
        {
            "id": "9a43cea6-6b49-496f-be27-483fe92b8472",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "76efa62c-255a-4ae7-ae37-749865d37176"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "9f91ca5d-7258-4aef-8fdc-ca14c4d8a351",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 25,
            "y": 61
        },
        {
            "id": "f69223a7-3e8a-4414-a025-d013fbfd966c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 31,
            "y": 31
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
    "visible": true
}