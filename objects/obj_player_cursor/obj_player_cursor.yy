{
    "id": "526c16a8-7221-47a2-b3df-915a5a32cab5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_cursor",
    "eventList": [
        {
            "id": "09070e7e-45ac-4616-9c94-73ddaef8c6d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "526c16a8-7221-47a2-b3df-915a5a32cab5"
        },
        {
            "id": "0b0c9708-ccde-46fa-a895-cd95fdd234ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "526c16a8-7221-47a2-b3df-915a5a32cab5"
        },
        {
            "id": "b1c19744-aa38-4d4e-9055-c7fba7a58738",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "526c16a8-7221-47a2-b3df-915a5a32cab5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}