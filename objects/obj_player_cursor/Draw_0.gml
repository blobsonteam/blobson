/// @description Draw cursor with player sprite
if (sprite_index == noone) { return; }

draw_sprite(sprite_index, 0, x, y);