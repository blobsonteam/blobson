{
    "id": "01fc668f-f426-4402-a00b-6840b40a3b11",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cursor",
    "eventList": [
        {
            "id": "0f1c1609-5ea0-4bb7-82e9-083ec01dacaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "01fc668f-f426-4402-a00b-6840b40a3b11"
        },
        {
            "id": "06045395-9f98-410c-b641-a92f1a86164f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "01fc668f-f426-4402-a00b-6840b40a3b11"
        },
        {
            "id": "ae40a01d-2b32-4cfa-b4c0-0d995bf0b44e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "01fc668f-f426-4402-a00b-6840b40a3b11"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}