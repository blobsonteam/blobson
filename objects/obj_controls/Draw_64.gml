/// @description
draw_set_font(fnt_menu);
draw_set_halign(fa_center);
draw_set_valign(fa_center);
draw_set_color(c_ltgray);
draw_text(room_width div 2,room_height * 0.7,"Press Start to begin on training stage!");
draw_text(room_width div 2,(room_height* 0.8)+32,"Press Select to reset controls!");
draw_text(room_width div 2,(room_height* 0.8)+64,"Press A for arena!");