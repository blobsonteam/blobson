{
    "id": "bf3d6b67-caa8-430c-8840-b13f629b7d41",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_title",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Braciola MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "329a705e-bc92-45d7-a7dd-efe4856efe0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f7b50068-60d1-44e7-8338-ce3c57b57bdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 56,
                "offset": 10,
                "shift": 30,
                "w": 6,
                "x": 468,
                "y": 234
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "2d00a292-2061-498b-9f8b-3a2a8aae5d03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 56,
                "offset": 8,
                "shift": 30,
                "w": 10,
                "x": 432,
                "y": 234
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5753f6ef-4a6d-4987-835f-32ba57bcf61f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 56,
                "offset": 1,
                "shift": 30,
                "w": 25,
                "x": 116,
                "y": 234
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0551ee32-a055-4047-965c-aa185d0d7b45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 321,
                "y": 118
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7818efb8-5d1e-4dfe-9cae-5f29da4a3dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 350,
                "y": 118
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b7e7ecf0-1a8a-4d12-98df-1f03fac20469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 379,
                "y": 118
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7c360189-b322-4180-ae73-d0a02a2900e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 56,
                "offset": 11,
                "shift": 30,
                "w": 5,
                "x": 484,
                "y": 234
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7d1e9d96-aee6-44a0-9ea2-bf602fe794ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 56,
                "offset": 6,
                "shift": 30,
                "w": 16,
                "x": 379,
                "y": 234
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1e0baf16-0789-475b-a986-00cbaa5d922a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 56,
                "offset": 6,
                "shift": 30,
                "w": 16,
                "x": 397,
                "y": 234
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "17db4230-99ae-4a3f-acd9-2676a9f7616e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 56,
                "offset": 2,
                "shift": 30,
                "w": 23,
                "x": 170,
                "y": 234
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6c9899e9-08ab-445d-9b74-35bcab79c41a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 56,
                "offset": 1,
                "shift": 30,
                "w": 25,
                "x": 89,
                "y": 234
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2f4b830a-b0bc-4e7c-bc5a-6ff9dd82131c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 56,
                "offset": 10,
                "shift": 30,
                "w": 6,
                "x": 460,
                "y": 234
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "061f26aa-e6cd-4b42-a8aa-761712fab702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 56,
                "offset": 1,
                "shift": 30,
                "w": 25,
                "x": 143,
                "y": 234
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "4547900a-3d16-4e67-9476-d881fea410ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 56,
                "offset": 10,
                "shift": 30,
                "w": 6,
                "x": 476,
                "y": 234
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d92ab807-4ee6-40eb-bb08-534f06f2f458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 56,
                "offset": 3,
                "shift": 30,
                "w": 20,
                "x": 219,
                "y": 234
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "54fcc5e6-676e-4c38-be84-ede0bf791574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 408,
                "y": 118
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "22b0d5bc-1022-4f04-a0ce-9b97face3a1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 437,
                "y": 118
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e290d19e-464a-48d9-b303-04a357d04eab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 466,
                "y": 118
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d8eff192-bfac-4fda-8f6d-64785f5926d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 2,
                "y": 176
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "91b6ac9f-0a21-4979-bdc0-90ed4b0995ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 31,
                "y": 176
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0ae4d702-f70c-4d84-b60e-12d7d297a4e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 60,
                "y": 176
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e8b22072-37f6-41e2-bef2-e3bbf284d5db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 205,
                "y": 176
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "692888ba-9295-4d44-af90-893f0e397a4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 118,
                "y": 176
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2f182e62-3f8d-45a0-a22e-fadb71c1926e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 147,
                "y": 176
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a3b44591-c26c-4e56-a1f1-e125feb9448d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 176,
                "y": 176
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2a9708e3-75ea-4cc0-a5bd-a2bb9baac745",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 56,
                "offset": 10,
                "shift": 30,
                "w": 6,
                "x": 452,
                "y": 234
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "19a144a0-f2f3-4694-9a59-5c6a1ad7c1b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 56,
                "offset": 10,
                "shift": 30,
                "w": 6,
                "x": 444,
                "y": 234
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "de37892d-bf21-49aa-97b9-f63ba21f103c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 56,
                "offset": 4,
                "shift": 30,
                "w": 17,
                "x": 322,
                "y": 234
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "96b05b09-261d-4d9d-a7db-157e79652aed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 60,
                "y": 234
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e2aa7d8e-eea4-43f8-a8bb-933126cff731",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 56,
                "offset": 4,
                "shift": 30,
                "w": 17,
                "x": 360,
                "y": 234
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "71ae805c-8694-4419-afcc-1e6241334f41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 408,
                "y": 176
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "394422d2-815b-4355-9f53-5e89c8c80036",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 379,
                "y": 176
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a549c846-da36-4571-be3e-ed7cf4ded812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 350,
                "y": 176
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f9b995fa-5aad-468e-a388-49e3a2e092a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 321,
                "y": 176
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "c78a8117-ffb9-450b-bea4-287df36438f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 292,
                "y": 176
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b25f9ae3-e78d-4711-9f8c-b44040463285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 263,
                "y": 176
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a88ce9ad-8cf4-4100-8825-f8938538be28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 437,
                "y": 176
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6b712db8-cf60-418f-ae3e-b6fc3b67610f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 466,
                "y": 176
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e7eb90a0-376f-4649-aa23-ed55d87ecda8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 2,
                "y": 234
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "40e4db56-d4dd-4198-ace3-fa8740e19d1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 31,
                "y": 234
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8fbd04a6-7a3e-4dd0-9060-da9fd65fe421",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 292,
                "y": 118
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e1b04009-3895-452f-83c3-52964f0e9634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 89,
                "y": 176
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "811828ef-8fd3-4796-b194-a43a5727ecb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 234,
                "y": 118
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7464b156-2142-446a-a676-c9ab3c51568b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 89,
                "y": 60
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "df8176b7-4549-4fbb-86ad-ec683c517f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 31,
                "y": 60
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e59f775b-6204-4592-a1de-188a029bb13a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 2,
                "y": 60
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "83ef9b39-dde4-420f-bc8d-cfd6ef9fd52f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 263,
                "y": 118
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "005cfac8-19dd-4a14-8e84-66bb808bd109",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 469,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6b9f35ec-6a89-4495-8c52-1d1860c87015",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 440,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "59b0ad78-4379-49e9-a577-4a6bbf15bd25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0f197c02-7870-42fe-a216-96525b0f83ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 382,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "4811f194-22f6-4164-937d-a04aa58348f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 353,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "cac06874-5f94-4081-a11e-de38e3a5bc31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 324,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "af843762-12b9-4368-8041-356a97512f40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 60,
                "y": 60
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "960cf6f0-3bf1-4268-b516-98164e85078b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 295,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "c5da88fa-22c4-48d5-b50d-47c9d7b936c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "4f5a2fc8-38d5-448a-a75e-0714c94a8e58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "05a913ec-166e-4730-8f3f-dd3290a40a8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "68443737-f877-4b38-93a7-c8b648628141",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 56,
                "offset": 6,
                "shift": 30,
                "w": 17,
                "x": 341,
                "y": 234
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "692eff60-77f7-4b00-a064-ae8cd7f5b47d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 56,
                "offset": 4,
                "shift": 30,
                "w": 20,
                "x": 241,
                "y": 234
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1dcf9105-9b5f-4544-b16c-cb1cc2dda594",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 56,
                "offset": 6,
                "shift": 30,
                "w": 17,
                "x": 303,
                "y": 234
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "35230c6e-eab2-4880-b1df-4718be9731f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 56,
                "offset": 2,
                "shift": 30,
                "w": 22,
                "x": 195,
                "y": 234
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a51a3df6-60eb-441d-8b79-ca0c53d44bd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "357e7050-2c73-42a3-90a6-0c764d4c8f6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 56,
                "offset": 1,
                "shift": 30,
                "w": 15,
                "x": 415,
                "y": 234
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2c3bd1c1-f9ae-4b51-9aeb-d90a088a19de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a0a1358f-76a4-4922-a2f7-8da5c6ff63ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5aa12a2f-0fdd-40cf-95c8-c585810be988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8c357675-fe34-48fb-bcba-328ba0e78b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "b2aba8c0-5158-4aee-a14d-41ea8c710333",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 266,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "05d42d8c-5fed-4a0a-af81-d6e35e6878e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 118,
                "y": 60
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "db04bb00-bca3-4224-8c14-ece6318338e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 408,
                "y": 60
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "8f449645-e918-4d85-a34b-5fc112c1306c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 147,
                "y": 60
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "cef854e3-8f85-40ea-9bc2-f3ab699f7f22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 176,
                "y": 118
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "82eeef38-4022-4168-a7aa-8aea6b853d99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 147,
                "y": 118
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "bfd52eb9-eb0d-47d7-9e23-2cb182e6ed53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 118,
                "y": 118
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7634015d-8af7-472e-9006-c00308c2b7a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 89,
                "y": 118
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4e047eb4-8a1c-492b-acc6-637622188e7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 60,
                "y": 118
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "bcd6e574-3bbb-4679-a4e6-c03b63820d4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 31,
                "y": 118
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "070308f4-fa9b-4d55-864a-e3c26f26e616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 2,
                "y": 118
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "279e8cd3-0acd-4d23-8dbf-bca19e81be19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 466,
                "y": 60
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f9c4c2c3-ebab-48c8-9286-3e650fa4db48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 205,
                "y": 118
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2591bca6-f377-491b-b16d-37f089717392",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 437,
                "y": 60
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8546c3a7-9345-49a1-9b5f-e054e6c0bebc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 379,
                "y": 60
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b3ad1b8c-6386-4538-8ddf-4095a35ad383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 350,
                "y": 60
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d3c4277b-5cec-4656-862f-4e3d0661052f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 321,
                "y": 60
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "273cd2c1-3c82-4c75-9968-08c887c164bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 292,
                "y": 60
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "90349589-8d96-4741-9966-7d6501a6df50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 263,
                "y": 60
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "fdded46d-a2a0-4fef-95a2-bf1399cfaecf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 234,
                "y": 60
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f322399b-4eec-41a1-afe9-be90d0cca6f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 205,
                "y": 60
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a9869643-b4cd-436f-9c08-71a31e81645b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 176,
                "y": 60
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c926defc-4746-4a1a-b31d-5855ed64e15e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 56,
                "offset": 4,
                "shift": 30,
                "w": 18,
                "x": 283,
                "y": 234
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9f6ea59b-1297-4a83-bc9d-c2d982eda567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 56,
                "offset": 11,
                "shift": 30,
                "w": 5,
                "x": 491,
                "y": 234
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d99b1923-a43b-4572-bbd3-2ae5a3c4f9f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 56,
                "offset": 6,
                "shift": 30,
                "w": 18,
                "x": 263,
                "y": 234
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "fd0805cc-c7aa-46b4-b49e-a4909e578f00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 234,
                "y": 176
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 40,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}