{
    "id": "6105c668-0a8d-43b2-8449-6083d192e076",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_instructions",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Open Sans",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "8f96cb5c-3ea8-4079-868f-7e459c0b076e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f90e9939-f701-4974-ad5e-961de781abe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 10,
                "y": 83
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5edebc3b-c3f1-4783-b413-3c1435586429",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 25,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "90a7b552-185f-4ecc-a179-ba62d1ab9b7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 235,
                "y": 56
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ee198cc6-de99-4ffe-af3d-dcade4d010d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 224,
                "y": 56
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2f8d7b20-a898-41d7-9d00-2f689871624c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 207,
                "y": 56
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7cf03a9c-6fab-49d3-b45c-f230663f7de4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 192,
                "y": 56
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e4e89f47-9619-4e8c-8c62-caec37c0eb79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 25,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 188,
                "y": 56
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d495e987-83f8-4c9f-aac4-3293822c5fd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 180,
                "y": 56
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5f4a4b5a-898e-42b8-aa8f-5ad7862ef741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 173,
                "y": 56
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "400ea559-35e8-4dbc-b4b5-83245df61d8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 15,
                "y": 83
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "00366855-1add-42fe-911d-bccdc83f5ae6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 161,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "218c6529-287d-48fd-9f15-d8397ed65fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 144,
                "y": 56
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "bdb79271-a997-4c77-ba06-7ff7d934ae2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 136,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ad8cf8b5-1bb7-4a35-9c02-c4cc0e559ef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 131,
                "y": 56
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "96407f08-5e53-4de7-9247-c0fa0944af0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 122,
                "y": 56
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "8e6d12c3-32e3-45e8-a3ca-c567cdfbaeae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 110,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6240f2cb-a1c2-4cc2-a582-45ad906a6d00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 6,
                "x": 102,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "df89c6ab-6553-4746-910c-482bdf9a658c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 90,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "448cf73a-fcd1-4e3c-ab5c-04b049e51f7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 78,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e5a54275-b002-4245-8bf2-dd82e06556e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 65,
                "y": 56
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b4298bdb-32e0-4843-997a-11f9f3298812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 150,
                "y": 56
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "60ffe808-1053-42b1-a7db-1eb51a4afb4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 41,
                "y": 83
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1c100850-14e4-4e38-b648-daa1084fad91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 165,
                "y": 83
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2f659d24-4306-442e-af8f-70cc4b47a9df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 52,
                "y": 83
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "cf19a563-1737-49db-b51c-a6395db8858d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 25,
                "y": 110
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1d1976c1-f7b4-4ed5-bec2-980aff66844f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 20,
                "y": 110
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c9ee640d-6435-415d-8750-7f30abf98b93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 14,
                "y": 110
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b0c99f32-fb22-42ec-8ba5-0273f99973d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "614b9fd6-f55a-475f-9e90-a9fb2366d157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 244,
                "y": 83
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "61191f27-9fae-431d-a57d-5d2080a9c273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 232,
                "y": 83
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9dcd6c2d-5799-406b-a540-e30eea711d23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 222,
                "y": 83
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b9d981fd-f877-4cda-8a8e-6b733f16630e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 205,
                "y": 83
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "65f3b1fa-d9cb-4287-8e5a-01132a32e78d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 190,
                "y": 83
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "534fb585-4633-4d25-bbd8-52862a19759a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 37,
                "y": 110
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fad916bf-39c0-452d-b03f-87d75dc6ceee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 177,
                "y": 83
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1463821b-cc4f-476f-bf26-cbd42e9d4338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 151,
                "y": 83
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "ae90f3ee-cf39-4889-808c-d918ddd15d9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 140,
                "y": 83
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "390b8174-206a-4234-a9ad-04de7721bdb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 129,
                "y": 83
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d5894417-e3b8-460e-bcf9-273b5c0caef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 115,
                "y": 83
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2af4a059-2f9c-4e69-b03c-a8278944c52c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 101,
                "y": 83
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "99afd0c3-f192-4307-976b-b5c5140bc90a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 96,
                "y": 83
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "08f134e1-159c-4141-8c78-8f08152b23d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": -2,
                "shift": 5,
                "w": 6,
                "x": 88,
                "y": 83
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "438ff904-31a4-45f0-92de-cdd531f875b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 75,
                "y": 83
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a905378b-8176-45d1-a107-c2950e85208e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 64,
                "y": 83
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "cd6649c5-a689-49c9-9e5c-b068e4a12601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 48,
                "y": 56
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "21d37118-9e95-4317-bfd2-cae4b56462dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 27,
                "y": 83
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9cebbeea-a5ce-400b-ac35-dcb8cf2760a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 33,
                "y": 56
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e2adcb06-9096-44b6-b9de-dd67e0b8fc41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 15,
                "y": 29
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "61a11d6e-faf4-49e3-b613-494068e5d0d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "de9a8a92-e395-4a3d-b3dd-48a3ea7c7655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c4e3da24-f88d-46db-bf5d-2565018d5ef2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e050b94e-449c-4936-993f-f339abbe2951",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ba08c3f1-9fd2-41dc-9a08-c64df9c01c11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "77c3bf3b-9620-40fc-94db-8e735c003bb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "bacb6dd1-2b00-4a0e-a5dd-ee73a639c949",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "209404ba-6adf-4e4f-9696-e69742f09cda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b9ada71c-638a-4ed9-aecc-a22cb6e01ec2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7b95bb2c-d7e6-4706-904a-8b4695bce8f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a03614bb-1641-4d35-aa56-f307154600f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "853b992a-dbc3-4b38-8aa8-548add97c488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5aca12e2-7142-4452-a0f9-31e5cabcdea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "29a8da65-4d2d-4084-b93b-4f6eabbffa81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4f2211aa-0442-46c8-bd2b-5e0eefc4ee8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 25,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9b4eeec9-67ce-4acd-8436-53f9d0bf2b37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "af18548b-f32d-4a6f-b30e-0eda5a86df6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "926dff9e-ea35-4365-a14d-f4364c86d1df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "bab1d004-f983-4145-aca7-60c104da7540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "9dec4287-4eed-4328-91f0-18969df91ce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "cde0a403-ad57-40bd-88b4-990b795f8c3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "290c4964-c408-4675-99eb-79fc1fa31944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 8,
                "x": 27,
                "y": 29
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "47c6ac79-352f-4da1-9941-0a2a32f16a4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 147,
                "y": 29
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0b4a8874-4017-4449-b576-607ea75f19e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 37,
                "y": 29
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1e8c5e74-ed99-48bc-ac48-88a5bb73732d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 10,
                "y": 56
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "20442d20-2112-4380-8069-f3e0a766714e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": -2,
                "shift": 5,
                "w": 6,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "532bfffa-28f4-4c6f-9b0f-a4398cd4fb5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 241,
                "y": 29
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "01849797-3fff-4461-ad01-7769b9c2630d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 236,
                "y": 29
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "2b392208-572f-460d-b953-f2ae0560f29d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 218,
                "y": 29
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "35e744b9-0be3-48c1-814e-d3bd7d372cea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 206,
                "y": 29
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "155723c1-d149-4904-8e9e-5f30b152f144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 194,
                "y": 29
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "99bc3cea-5349-4033-9495-33695ccf8f0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 182,
                "y": 29
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6936a648-cc3d-420c-9db9-6bc859182b4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 170,
                "y": 29
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d9cd3c03-a95c-4c7b-8ea3-190a00d41afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 15,
                "y": 56
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "86ff8967-99d4-45d4-96b9-5745d2d86603",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 159,
                "y": 29
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "81b75005-a594-47f6-97cf-6239d2ce54ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 138,
                "y": 29
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f3a04adc-aa9d-483b-94ba-8c5d62282b15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 126,
                "y": 29
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1116b9fa-04f3-4740-bcd5-75b09d6b3236",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 114,
                "y": 29
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b1f3d92e-5c35-4037-8b80-ed0ade2dc664",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 97,
                "y": 29
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "df87b8b7-794f-48a9-b852-dc2c42c3afcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 85,
                "y": 29
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "03518943-4a48-4b00-a4d4-e4cde006d4ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 73,
                "y": 29
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7f9121be-9e1b-47ce-9f9d-166251f05a5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 62,
                "y": 29
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "cedfd075-dfca-44ca-8019-7997524bbd94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 53,
                "y": 29
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "35bc16fe-e1c1-46ab-9333-c7d2b6ef2a65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 49,
                "y": 29
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5c028a36-657e-44c4-bd53-129508ed185c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 24,
                "y": 56
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a64787cc-a1be-4d45-9fcd-62e8916c82f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 110
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "97a4a970-6988-4194-9660-b2ddd33702af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 65
        },
        {
            "id": "6c5b07e7-fbed-4e6b-a5e0-952036307811",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 97
        },
        {
            "id": "d674924d-930c-4233-9c5f-bb8a4ff892a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 99
        },
        {
            "id": "b79569fa-579c-4784-a002-db9356e97b60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 100
        },
        {
            "id": "7bf0d47c-3881-4e33-b5f2-4a8773db3687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 101
        },
        {
            "id": "46e9f2f6-e20d-4eb7-bdc2-acaa0b4e79fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 111
        },
        {
            "id": "26227058-ef35-43e1-8521-fe5f0d070fcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 113
        },
        {
            "id": "e7e835b8-2236-42a0-a498-078c2eedcc0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 192
        },
        {
            "id": "6394dd24-a15b-4ec4-9f7e-97f7f9f468c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 193
        },
        {
            "id": "e27d9de1-173e-4282-acad-637aa2ea1609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 194
        },
        {
            "id": "b44add20-3b21-40eb-8b22-0f9a54bf0634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 195
        },
        {
            "id": "c35a8814-7dd2-4f92-8b6c-d146be8ffa7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 196
        },
        {
            "id": "e5186fb5-a269-445a-8618-aacfd02639a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 197
        },
        {
            "id": "b5df3233-a8aa-4463-97b5-c08f1e46becf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 224
        },
        {
            "id": "975b3505-1151-4c7d-b313-9b7c570a930a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 225
        },
        {
            "id": "41440bc2-e8f0-432a-b6fb-805f8a2ee446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 226
        },
        {
            "id": "42da2a77-1ae2-4e73-b4b8-c2fc687f22fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 227
        },
        {
            "id": "1933f8dc-2e35-4f96-bdff-cf70b9ad475c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 228
        },
        {
            "id": "cedc8f23-2f09-40f5-8f7d-57d6805ffc37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 229
        },
        {
            "id": "c8fdee8c-fe10-47b3-8794-a50e96ee01b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 230
        },
        {
            "id": "2ed5d24a-ec2a-4a06-b216-427b7500aede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 231
        },
        {
            "id": "306a252b-ecb7-4b56-a0af-4139e1e79c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 232
        },
        {
            "id": "33f02874-2201-4bcd-b8c2-00613907559e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 233
        },
        {
            "id": "25ae984e-00ff-40e7-b33e-4c15357ed9d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 234
        },
        {
            "id": "04049a8d-f271-4409-a981-0362484364f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 235
        },
        {
            "id": "13519e3f-6325-47b7-bbbe-47795bc27234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 242
        },
        {
            "id": "46d110ff-f539-4746-8638-d1117de6caee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 243
        },
        {
            "id": "560c9c66-b03d-48d7-861f-0269f0865d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 244
        },
        {
            "id": "6126e338-3c5d-4eab-88e7-047b6235f9c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 245
        },
        {
            "id": "8a53dff5-a95f-4045-a065-225fa472ac7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 246
        },
        {
            "id": "67227cff-4a85-4e8f-9c58-f3f1734ec6d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 248
        },
        {
            "id": "5385417d-78eb-4d0d-8046-6949c9afa9af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 256
        },
        {
            "id": "216fa525-84b5-47a1-ab25-07acb3c63182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 257
        },
        {
            "id": "3179cca4-8d7b-4f8a-8644-a19bdeeb432b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 258
        },
        {
            "id": "fdae78b8-6fcd-466f-99ef-346ca94aee10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 259
        },
        {
            "id": "5f4cec2a-44f3-4ced-9b9d-20c9a6f5ae44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 260
        },
        {
            "id": "3de5bcf7-5527-464e-9863-6333a6fb402f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 261
        },
        {
            "id": "f68d059a-1812-43ba-b857-e7292b4c46a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 263
        },
        {
            "id": "e93b4fa1-37b4-4edc-bfec-fb9b2b7ecda4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 265
        },
        {
            "id": "60673e1c-9494-4b4b-bb6b-fff3fe103957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 267
        },
        {
            "id": "7a70125e-081a-4583-b5ab-bc1f1d9e2f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 269
        },
        {
            "id": "557fc06d-c8dc-4bd8-94d4-49c4865113c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 271
        },
        {
            "id": "637c076c-4fc1-4d2f-96d9-87a46158f574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 273
        },
        {
            "id": "03fa88fe-df9b-4096-a81c-1c7d6b41d393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 275
        },
        {
            "id": "136b05c4-d40b-4af3-a101-6a8da7738425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 277
        },
        {
            "id": "f1b66185-6866-473e-8c31-bccb9d24c7c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 279
        },
        {
            "id": "0e10d58e-3ea7-413b-826c-cb0c6dc75719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 281
        },
        {
            "id": "e8ed4135-0d88-4557-b66e-e4e93c305ee4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 283
        },
        {
            "id": "3f59b87d-00cd-40b5-aad6-8c43478083a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 333
        },
        {
            "id": "d063c765-ad37-4846-8fc9-f69c652c33b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 335
        },
        {
            "id": "630291b5-5f03-4747-909b-bd59a685a304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 337
        },
        {
            "id": "1c2f963a-16bd-4c73-9e5f-9a25c038e165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 339
        },
        {
            "id": "9890060e-8bb2-4c16-b852-b82ac2ecba91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 417
        },
        {
            "id": "2f7a6642-da3c-447d-960a-65e8aa78eca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 506
        },
        {
            "id": "684a080c-6f0a-4a92-bb2f-3f0f3e1a769e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 507
        },
        {
            "id": "029bb3a3-4572-4930-8925-994bfec04338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 509
        },
        {
            "id": "b23ea990-d283-4159-97e4-1ea716e1ef49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 511
        },
        {
            "id": "02ab2341-5731-44c5-8282-d62816828f1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 902
        },
        {
            "id": "4ba88b11-402b-4b05-9c5a-76c387bcf665",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 913
        },
        {
            "id": "80ec8d03-75f6-4369-a4bb-77b493b135e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 916
        },
        {
            "id": "af032929-f6d4-4e54-85bf-024f61a0a265",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 923
        },
        {
            "id": "ffa18285-eca2-40ce-a4f6-1dfc57e31641",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 940
        },
        {
            "id": "33d78569-4620-418e-9312-d13ae4590ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 945
        },
        {
            "id": "bf0262ac-269c-4315-8017-89002a7e6ba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 959
        },
        {
            "id": "e6b3163f-27b2-493b-9eb4-1bd2171bfb68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 962
        },
        {
            "id": "b3502d48-5a74-47e4-86f8-49eccd65eea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 963
        },
        {
            "id": "1198074e-081c-475a-b596-1e36293bdfe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 966
        },
        {
            "id": "bc54ba51-f397-4444-81fc-5c9eda42d0c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 972
        },
        {
            "id": "84505106-07bd-47cf-9b1d-5cd92ba5480c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1033
        },
        {
            "id": "1774738f-045d-4a95-a6b6-57c3d5978b71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1040
        },
        {
            "id": "949daace-8733-41dc-920f-d458c3bd2861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1044
        },
        {
            "id": "830d9500-6437-486b-8d3b-c3fa971666da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1051
        },
        {
            "id": "834adace-93dd-4b65-b1f4-41ed3e708368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1076
        },
        {
            "id": "6fd377d9-6c1a-4e9d-b8a9-0f30b4f99d2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1077
        },
        {
            "id": "f7061d93-4725-4036-84b0-90aae7000f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1083
        },
        {
            "id": "6267b72b-a17b-4b36-8e17-cd1ad1970c8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1086
        },
        {
            "id": "8689114f-4edb-4bb5-a2f3-adb9941ad733",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1089
        },
        {
            "id": "7a1c9754-7595-4f30-a10e-a5bc142deb02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1092
        },
        {
            "id": "0559839d-ba13-4b4b-8c0a-ae9f1d07ab46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1104
        },
        {
            "id": "bf595ffd-f0ab-46cb-9831-0257eac487ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1105
        },
        {
            "id": "260f0801-e012-4027-803d-3f76305ed6ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1108
        },
        {
            "id": "cca347b9-a6d4-4d69-90ed-e5cc57757899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1113
        },
        {
            "id": "49914a72-17c0-4c6e-84be-170c841f4d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1126
        },
        {
            "id": "649438f1-cc8d-4dea-9364-abcb745426b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1127
        },
        {
            "id": "32fd5e57-7628-4832-977f-affe51612fe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1139
        },
        {
            "id": "0248c149-e4f6-43a0-8c6d-ef2b243d5ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1145
        },
        {
            "id": "562ea211-dc65-4b03-abe0-bf1294770bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1147
        },
        {
            "id": "381c9b04-34fa-4887-ac5b-597c8133fa61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1149
        },
        {
            "id": "e5e2425d-2d43-4463-b58c-303a5da0e3bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1153
        },
        {
            "id": "feb10a9e-426f-462e-9f7d-5df08f486a7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1193
        },
        {
            "id": "feefb8d8-b55c-4e84-9c23-8d6b139c1b69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1195
        },
        {
            "id": "bddcd5fa-6e5d-4d5f-84df-b92d7fca5052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1221
        },
        {
            "id": "eef5bbb0-9d2e-4676-9f09-060f4c31db73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1222
        },
        {
            "id": "4a12f268-90ef-4c93-add1-7deac378ceac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1232
        },
        {
            "id": "fd506e80-1e4d-4688-8b5b-b2f1f0cc2122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1234
        },
        {
            "id": "b46f4c07-63ce-43b5-bb8f-2cfecfe9717f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1236
        },
        {
            "id": "9674bb6d-4a67-4687-9c69-35708face5b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1239
        },
        {
            "id": "7dc1044e-2eb8-43a1-a8d6-331e95692df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1255
        },
        {
            "id": "e5dd4eba-aa9e-44e3-990c-895e0ee8db27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1257
        },
        {
            "id": "e3c5facd-6d27-490a-80ec-f0ea8a50942b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1259
        },
        {
            "id": "e74055c4-7075-4915-af7f-bd658483a703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1280
        },
        {
            "id": "03293b3a-5c48-4465-bd45-91747d761368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1281
        },
        {
            "id": "dd787ade-8b8f-4ee2-bbab-b21c92136c3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1282
        },
        {
            "id": "1149ac82-b9c1-4a9d-99d7-809c1f153d94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1283
        },
        {
            "id": "b165e0d5-df9f-4067-8c7c-ee269c1477d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1288
        },
        {
            "id": "5fbd2960-71c4-4dbb-a08a-842ed5caecbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1289
        },
        {
            "id": "2d0530f6-e8a7-4dad-9ccb-42efef14e672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1293
        },
        {
            "id": "98029237-2acb-4f38-83cd-771a79618f60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1297
        },
        {
            "id": "ba69382e-e4f3-4a86-a525-2c9f06229d94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1298
        },
        {
            "id": "c9f0732b-3a5b-47a6-acef-ca76d045df82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1299
        },
        {
            "id": "20920c36-5544-4521-8996-d4eb46dee328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7680
        },
        {
            "id": "8a813887-47f3-4f16-b53d-a9f789cbee50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7681
        },
        {
            "id": "4b91d3ff-7216-47ec-bef6-0da6adde3c08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7840
        },
        {
            "id": "9ee2fd47-0b2c-4217-b7ef-c9ba8e950c38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7841
        },
        {
            "id": "631951eb-f44f-4afa-bee7-629b9cc38007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7842
        },
        {
            "id": "a07f19c8-6691-40fd-af1c-f231460d0c28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7843
        },
        {
            "id": "0f751cff-61bc-4ffe-a169-09fb18806b3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7844
        },
        {
            "id": "c110cdae-70f0-4ee4-80b4-c5d4600998d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7845
        },
        {
            "id": "53e6074d-0ddc-436e-aa48-993b45d811fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7846
        },
        {
            "id": "85e063b9-4340-4bce-be46-d3407d1405c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7848
        },
        {
            "id": "b7e8b608-afbb-42d5-b381-924ee220b35b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7849
        },
        {
            "id": "bea345a7-8844-451f-8d46-882019255de2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7850
        },
        {
            "id": "9e11d012-2151-4867-abc6-ce29789c8e52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7851
        },
        {
            "id": "bc577247-668e-480e-a238-13ec794965cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7852
        },
        {
            "id": "f50b99d4-3036-4309-a707-df6fc78a7744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7853
        },
        {
            "id": "52ebb4c3-e2bd-4c01-9739-16acc9a0d0cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7854
        },
        {
            "id": "fd1886f7-8085-41f2-9b43-bd782d2143ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7855
        },
        {
            "id": "e269da4b-8907-4360-a2f9-470381726798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7856
        },
        {
            "id": "f733a2e2-d45d-42ea-ba85-ee36c8595914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7857
        },
        {
            "id": "7c1f21a3-e1a8-4f47-a8be-d0e59ed2945e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7858
        },
        {
            "id": "aa0fa36c-1e6e-46d9-b9d4-31ed8c78c1b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7859
        },
        {
            "id": "5e5f5d9e-ebf4-4a4c-8852-8a4730cf9934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7860
        },
        {
            "id": "cde98906-f563-4b9d-8923-73f61a6033eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7861
        },
        {
            "id": "eafd249e-b62c-442a-a954-d343340f17b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7862
        },
        {
            "id": "12aa7905-c78f-41b2-aa14-c964bf85a6ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7863
        },
        {
            "id": "a1743664-13b5-4adb-9fb0-d97faa0b5b46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7865
        },
        {
            "id": "d22c7bb9-ba7e-4433-abab-229a32ea1739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7867
        },
        {
            "id": "b29b2be2-9ed4-4310-b9f9-2369ae18ce0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7869
        },
        {
            "id": "1a83c465-cd50-4c70-89e5-3d0e42e150b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7871
        },
        {
            "id": "68f47d68-9b12-4abc-85b7-2f1a80b5e1ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7875
        },
        {
            "id": "2d754564-9b69-4e4c-abd7-805918b6c2b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7877
        },
        {
            "id": "598507cd-f06a-4fb0-aad7-0d46b083167e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7879
        },
        {
            "id": "d374b2e1-f20b-4f7e-b95b-1fd714a6e66f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7885
        },
        {
            "id": "0214e14a-a31e-4bd6-bdc6-d14187d56f07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7887
        },
        {
            "id": "7adf60ff-9ece-4707-8b78-f2ca8e333a9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7889
        },
        {
            "id": "8eed3b2a-33ea-49f4-a910-ec754f3f9716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7893
        },
        {
            "id": "b1fffa1f-b332-441f-a5ba-0ab4ad73745b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7895
        },
        {
            "id": "45b37cdb-b9a8-4f0b-8b8e-76daf6b92572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7897
        },
        {
            "id": "a0723f14-ac1b-4140-9785-551fb3e74460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7899
        },
        {
            "id": "2f4be75c-60d8-49ab-8094-3a4148e869e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7901
        },
        {
            "id": "d085757d-27f7-4795-9307-e5cd435a326f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7903
        },
        {
            "id": "9bab5dd8-c362-454a-8feb-ff12eadd8fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7905
        },
        {
            "id": "7251938c-4743-4ac5-b5ca-cb3141ac1d25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7907
        },
        {
            "id": "e8dae2a8-4a63-49c6-849b-2d06687a60e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 65
        },
        {
            "id": "2e6e1dc2-9c65-45e7-82a5-23b53010589d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 97
        },
        {
            "id": "1da09115-488e-476d-8c99-e2f1a50f6753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 99
        },
        {
            "id": "671d1161-efef-4146-916c-d910a5d520d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 100
        },
        {
            "id": "f97d2ae6-6388-4c15-9e8e-cfdeb8798a5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 101
        },
        {
            "id": "27eed994-8092-4891-bfb1-ba0ae39093db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 111
        },
        {
            "id": "7fec661f-b2de-498d-a38f-95c4ed9a8257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 113
        },
        {
            "id": "df336001-dc88-49f8-8cb9-967be13aaee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 192
        },
        {
            "id": "21043bba-da59-4861-acf3-67d7c3f8f81e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 193
        },
        {
            "id": "efdc66a7-44c6-4972-8b5a-033a90e1bd23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 194
        },
        {
            "id": "0ecdf945-97bf-4855-815e-4e982e2f97d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 195
        },
        {
            "id": "7c88ecf8-3587-4271-b744-4ae10c13279e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 196
        },
        {
            "id": "0ae28661-317f-48e6-be8e-1ec47fcdd731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 197
        },
        {
            "id": "8718f11d-072c-4122-a670-e2c01012cba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 224
        },
        {
            "id": "252be41f-34d7-494d-a16a-837190bab5c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 225
        },
        {
            "id": "3405d653-4316-4e1a-ad79-046028e78667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 226
        },
        {
            "id": "5b9c37e9-a6cd-42fa-8969-510bf2195bfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 227
        },
        {
            "id": "d1093919-e50c-4cb9-9a8c-8b0b0be26a4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 228
        },
        {
            "id": "693a5928-f0dc-4095-87a1-4ba3492aab19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 229
        },
        {
            "id": "4c8339fa-7bd1-4659-9f96-d803c6d378d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 230
        },
        {
            "id": "8eca9825-b266-4a63-982f-79f3acdacc0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 231
        },
        {
            "id": "6f8cce8c-37bf-4a0b-a9be-65ec68936a3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 232
        },
        {
            "id": "795479a2-139b-4954-9a74-01b2babc6076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 233
        },
        {
            "id": "09eeb42e-03a8-4e0a-b2dc-a97cc983bea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 234
        },
        {
            "id": "e123b23c-3a8a-4119-835d-6443ce79fde5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 235
        },
        {
            "id": "07452823-0a62-4319-9246-7f2563345cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 242
        },
        {
            "id": "e8aa50d2-1b59-44c2-a480-918d717f5e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 243
        },
        {
            "id": "4c99af7c-3590-4d9e-9aab-b1d41499aca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 244
        },
        {
            "id": "963bb7c0-57d4-4144-b942-e4059986a8a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 245
        },
        {
            "id": "cf439879-7387-4700-935d-d6484f6e08b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 246
        },
        {
            "id": "56e352aa-1c85-4469-aaf1-b7387d14ccfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 248
        },
        {
            "id": "670543de-6fb4-4664-a4e5-bbc4fcb094b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 256
        },
        {
            "id": "16161c6d-8da7-44d5-b9c9-e30787e5fa55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 257
        },
        {
            "id": "cbb4dcf5-d6ef-43ba-a4b3-9b6d99fb2bec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 258
        },
        {
            "id": "524a9e1c-8481-4996-bbae-9d0b6870d525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 259
        },
        {
            "id": "5748943d-99d4-4e17-951c-de54593dea8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 260
        },
        {
            "id": "0d0ca383-a013-4b12-ac95-7e6f833989c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 261
        },
        {
            "id": "84f2143f-cf42-4303-9a8b-b7a086e9244a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 263
        },
        {
            "id": "7813ef43-3c18-4992-a7e1-50eb05fe4637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 265
        },
        {
            "id": "15a6524c-7483-4260-8c92-32da7a8272fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 267
        },
        {
            "id": "c4570934-1277-4bac-8c70-8acadd8f5df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 269
        },
        {
            "id": "b701e1f5-f376-45e7-8c58-6586532d1873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 271
        },
        {
            "id": "6a507afc-50a1-4a62-b90b-29c3d7e012cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 273
        },
        {
            "id": "cf71be43-adac-4b7a-9876-d7bc9f271029",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 275
        },
        {
            "id": "29a55387-3ccd-4230-aa71-a4c79c09a246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 277
        },
        {
            "id": "f18c9bfd-fd69-4cf2-a2ab-c9d1c2b107e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 279
        },
        {
            "id": "a7104a12-a5e8-4f9d-93d4-c57d347a4860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 281
        },
        {
            "id": "6107e6f7-ca37-4fc2-90ec-2d8231252766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 283
        },
        {
            "id": "4060c8df-7f70-409c-b2ca-64987b987784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 333
        },
        {
            "id": "06d6f154-2558-43f7-bfc4-b93875aa0778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 335
        },
        {
            "id": "ebe6a058-d4f1-4b2d-8ace-31b23eecb6a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 337
        },
        {
            "id": "767855a2-f535-409c-bec1-56338f463afc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 339
        },
        {
            "id": "07182ded-12b2-486e-945b-9225b25fd3f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 417
        },
        {
            "id": "342d6463-782e-4ab7-aedc-e790c10365c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 506
        },
        {
            "id": "1f485b60-4939-4388-9b24-66cd3a859af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 507
        },
        {
            "id": "74744b55-1d2a-4967-adaf-4778ffb0f4b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 509
        },
        {
            "id": "2b68066a-b088-4618-a4f0-5d40d9395d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 511
        },
        {
            "id": "097be7a4-1ff0-4725-9258-167ee44b3093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 902
        },
        {
            "id": "9c7333ef-2cf4-43c8-be60-7ba686707457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 913
        },
        {
            "id": "6f9b5ab2-5317-48e3-8c40-4c92a2c504e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 916
        },
        {
            "id": "47cbb9e2-6777-454a-951e-421d08dfb657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 923
        },
        {
            "id": "37c1dad7-b87d-4478-a2ea-fe5752294f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 940
        },
        {
            "id": "c4c18dff-bb63-4d9d-9b5c-dd31cdaaed0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 945
        },
        {
            "id": "2c8f1d85-ff50-4c2e-8154-c5f3667cd748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 959
        },
        {
            "id": "056b257e-8b8c-4057-b443-36b8c3938f4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 962
        },
        {
            "id": "1d5f1ce6-4332-48c9-aeea-46b06dc73c4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 963
        },
        {
            "id": "a9417289-c825-4c5e-b4d6-db49524e8234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 966
        },
        {
            "id": "5b5e9f0e-4cc6-4084-8675-4d8614a8ca98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 972
        },
        {
            "id": "ee7c1e68-2464-491a-8dce-14b1c1dfb95c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1033
        },
        {
            "id": "d4d8907c-a4aa-4491-8960-3a87e92f96e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1040
        },
        {
            "id": "d072a628-bdeb-45c3-a58a-841e6f96a5c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1044
        },
        {
            "id": "5d49d1b2-8364-4233-9c7e-ab7c03b3eb40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1051
        },
        {
            "id": "90825cad-ba0d-483a-80e1-0e5dfd554387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1076
        },
        {
            "id": "8cee81ee-e9f2-4982-acb5-478507d7158e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1077
        },
        {
            "id": "73d20a47-c9ed-4e61-9a77-61c2b833851f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1083
        },
        {
            "id": "e23d8ed5-57d8-423e-b30b-b5f40f99a33f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1086
        },
        {
            "id": "a3a5661b-0c58-44ac-8982-40c7a0918b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1089
        },
        {
            "id": "f37018ea-e33f-440a-933f-f7e4f6d30004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1092
        },
        {
            "id": "640798ed-61fa-4a08-99a8-d38818b7dab3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1104
        },
        {
            "id": "7b326a34-fc8b-43ff-abf8-e8a425ef59ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1105
        },
        {
            "id": "d74dbf4c-159f-4d0c-a165-4d4d67526fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1108
        },
        {
            "id": "db8231b5-4343-4ad5-970d-aee46609cbd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1113
        },
        {
            "id": "fbd1d963-6320-4f2e-8941-0c293989b6b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1126
        },
        {
            "id": "e69cb262-d751-4d64-9f48-3932dc4fde4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1127
        },
        {
            "id": "c6c2b814-309b-4b35-a96a-9328ea34e86e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1139
        },
        {
            "id": "c54c64dc-bca2-4219-bae3-29a2f5a59588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1145
        },
        {
            "id": "1c183ee4-95da-4c2b-8512-59d22281c612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1147
        },
        {
            "id": "9db33201-c71b-40e3-a0d8-d2e3a0c0c699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1149
        },
        {
            "id": "88003634-af4d-4c07-b377-143ee69aac84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1153
        },
        {
            "id": "020b12ea-8ec4-444b-a11e-a1b8a5844d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1193
        },
        {
            "id": "1a5d543d-5aa0-4fa4-ac0d-195d2d8e2fa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1195
        },
        {
            "id": "5ea61ebd-8c86-44b5-8f2c-f5bb7823ea4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1221
        },
        {
            "id": "4af3e20d-81f1-4f51-a7a9-8787a4e2f6e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1222
        },
        {
            "id": "4b8eb341-f62d-4d73-a3ae-723edb9863bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1232
        },
        {
            "id": "cc55eaa3-e038-4232-8cea-9547582e78c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1234
        },
        {
            "id": "41b95faa-c69c-44ac-b68e-6a8131fc5da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1236
        },
        {
            "id": "6a8b1c95-76fb-4cf8-95b6-061de9ec1551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1239
        },
        {
            "id": "746b1296-c7b4-4ab6-b059-361279972138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1255
        },
        {
            "id": "3aa81a9e-3d68-4172-bed1-7afb326098bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1257
        },
        {
            "id": "19b44772-f14d-431f-b8b3-60b1aa7a06a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1259
        },
        {
            "id": "251437a7-1db1-4cf4-8bb8-9254d57c627c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1280
        },
        {
            "id": "baa12e41-53d5-4454-b990-cf8be3c1499a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1281
        },
        {
            "id": "072a563e-f1d2-4b9b-b6bc-32da351597f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1282
        },
        {
            "id": "69bc81d1-e8c6-49e9-9953-d432ee55f244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1283
        },
        {
            "id": "24ce41bf-9615-429a-83b7-66af23ac8ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1288
        },
        {
            "id": "760391ae-124d-4767-aa53-5fdf28fb0810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1289
        },
        {
            "id": "fdb79090-af5b-452a-aee3-619739ec7065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1293
        },
        {
            "id": "0bbb383f-20b5-4599-a158-93b953d68b01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1297
        },
        {
            "id": "c2974576-d853-4206-bc43-97403334ed9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1298
        },
        {
            "id": "4f3a8c66-d309-4ec8-bb82-22b436ae845a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1299
        },
        {
            "id": "fedfaaa7-5d18-43fa-90c6-34a81db2c8f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7680
        },
        {
            "id": "a8c4677a-99cc-4cad-a824-b50afd4c65ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7681
        },
        {
            "id": "15dabe33-9f6b-4e59-a874-765fcf236cfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7840
        },
        {
            "id": "641d4193-9266-41de-bea2-31fd6b1f538f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7841
        },
        {
            "id": "1f9e84ae-61d7-4413-8a9a-c862605ef37a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7842
        },
        {
            "id": "91d48801-ab30-4f9e-9ed9-00ab8dd01f87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7843
        },
        {
            "id": "a7d33fff-b0d0-452a-a9b1-006884c18912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7844
        },
        {
            "id": "46dd7f2b-d857-4577-a9ce-3ed8db047fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7845
        },
        {
            "id": "3436819a-586b-48b1-8e8b-3b07c412cef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7846
        },
        {
            "id": "867b54a2-7c66-4b0b-b43a-deb0cda237c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7848
        },
        {
            "id": "d1055327-baa0-4bfe-bbe5-d6b6df434dbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7849
        },
        {
            "id": "a6d5c4f6-97e5-4082-b427-56f499c786e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7850
        },
        {
            "id": "f25a438f-a6e9-4ffa-8663-53e6b66f6608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7851
        },
        {
            "id": "240c5a95-2a30-4f2f-a01b-c67ee77c07c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7852
        },
        {
            "id": "ea74a0b5-ed2f-44a7-a545-211172f9484e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7853
        },
        {
            "id": "3709b455-61ef-44b8-8739-a3a378a5ed7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7854
        },
        {
            "id": "b905b39d-5af9-4e01-9ea9-d255aedfe436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7855
        },
        {
            "id": "b75f95f1-9e1f-4eaf-817d-3e9acaa409fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7856
        },
        {
            "id": "a521735a-5a58-4e12-bfb6-5c9dd812e67a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7857
        },
        {
            "id": "4e13abdb-8197-4f94-aa90-11ba9b2abbe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7858
        },
        {
            "id": "fecda65e-9311-4747-8d7a-9ede18b43322",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7859
        },
        {
            "id": "5dca1e06-ccec-48ad-9428-9ecce6af8fc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7860
        },
        {
            "id": "7e94f9aa-384f-4713-9ee6-194c8efbd5eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7861
        },
        {
            "id": "d7c6e8e3-7a7f-4e7a-bb33-bcd05ed61f00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7862
        },
        {
            "id": "86a075c2-79cd-4273-bc99-6ec3abccedce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7863
        },
        {
            "id": "3aae0bbc-7626-465e-9e70-7dfef3180ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7865
        },
        {
            "id": "d81a5728-9f8f-4cd0-bf2a-94dceaa3118d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7867
        },
        {
            "id": "0273ab50-2f55-4a84-84eb-8dbf77354398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7869
        },
        {
            "id": "4d513b35-6640-4135-8b20-c0001242d3c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7871
        },
        {
            "id": "9c636409-1395-4913-a871-c22db0b416b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7875
        },
        {
            "id": "60d0ab7c-7816-44c0-9c22-8cc7377d8454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7877
        },
        {
            "id": "7868a9ea-136a-4aa7-af6a-548abfdff937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7879
        },
        {
            "id": "0d6928bb-67e8-4e69-9728-d748323ab978",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7885
        },
        {
            "id": "0057109d-c335-48c1-a5d3-960691fb2ba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7887
        },
        {
            "id": "ac085cd9-772c-4184-b6ab-413220b8616f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7889
        },
        {
            "id": "cb76d10c-6780-4b56-af8f-b14d376841b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7893
        },
        {
            "id": "747c1f09-e943-4037-9207-1c2705c45d4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7895
        },
        {
            "id": "34ac64c1-44ba-486a-8cae-8132a6e270c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7897
        },
        {
            "id": "8b1faa69-aade-4b5e-ae36-c214d54568dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7899
        },
        {
            "id": "6cd355f9-1e08-48d5-8acb-344cc8037156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7901
        },
        {
            "id": "00e5b99c-9abd-4d41-9727-e7e631db32a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7903
        },
        {
            "id": "517b381c-b630-4304-ba4e-ce1924f5848e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7905
        },
        {
            "id": "a10e305e-04b3-4e4c-810d-786244d515c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7907
        },
        {
            "id": "bd95867f-8adf-44c8-80f4-3d802a3f965d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 40,
            "second": 74
        },
        {
            "id": "aa88d8db-77cf-490c-b69b-9a214690c000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 67
        },
        {
            "id": "9b61a12a-f6cb-43c5-8323-0d99b2952ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 71
        },
        {
            "id": "0f7a5f5f-b9f1-4785-8682-9d20da0256a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 79
        },
        {
            "id": "65a3b07d-005f-4af6-968c-66a3b28cb087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 81
        },
        {
            "id": "813a661b-14e2-4ca0-be0e-27283a7fa8d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 84
        },
        {
            "id": "33535f6a-1b8b-49a2-9852-130df8fa18cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 86
        },
        {
            "id": "999fdc72-46cd-4150-8319-532bb44b3543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 87
        },
        {
            "id": "3424f5a2-451c-42db-83d5-56bf09b14912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 89
        },
        {
            "id": "dedb5dad-0344-4fbf-8669-ce6a7b62fa5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 199
        },
        {
            "id": "7a6dc06e-86b2-4f04-bddc-71053a8cc1f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 210
        },
        {
            "id": "d29e3050-051f-47b6-a728-698c482d2570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 211
        },
        {
            "id": "0a21549d-5598-4302-978e-fa0d0dd75be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 212
        },
        {
            "id": "4c5344b5-a1b0-4acb-8fa8-05fd40e845ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 213
        },
        {
            "id": "e9dd220c-6901-4aff-ba6a-a5a022ef00a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 214
        },
        {
            "id": "6fe58c3e-b38c-4c9b-9931-0dae9db0b456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 216
        },
        {
            "id": "782fc69a-1f84-4964-8b5c-6679fc264275",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 221
        },
        {
            "id": "f8117260-aaa4-4fe7-868e-0b98cdca06e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 262
        },
        {
            "id": "0b057ec0-ef7e-4a46-94fa-eab23a4a4a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 264
        },
        {
            "id": "0ee997b1-779f-4ee0-a845-7af875d636e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 266
        },
        {
            "id": "80b4e907-e668-4d2e-aedb-4c9e8c2d150f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 268
        },
        {
            "id": "21a41602-b02a-4f6d-bfa3-6181d0db2675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 284
        },
        {
            "id": "ff8c77bf-991e-4e95-9906-54a918b4830f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 286
        },
        {
            "id": "5caf6dc5-ac3f-4380-8cde-1f93349ffcc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 288
        },
        {
            "id": "06214447-8567-472e-9fda-3aa575fd1654",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 290
        },
        {
            "id": "041f0c44-2008-4442-962c-1bf26df1fcff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 332
        },
        {
            "id": "dfbf3fd7-bcbc-4529-8d08-cbdf565e4801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 334
        },
        {
            "id": "6e0324dd-378a-45a1-8934-2daf9c4a4897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 336
        },
        {
            "id": "b8f3a046-2482-4d55-92e3-24745b8dbd5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 338
        },
        {
            "id": "e78bb4fe-8e36-434e-9106-50b782f3a148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 354
        },
        {
            "id": "eaf84672-aeda-4ee8-9551-4f847b234ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 356
        },
        {
            "id": "fe7e5619-ea51-4f72-bfd7-2ab5c1bf32ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 372
        },
        {
            "id": "4b28a0b6-50b1-4084-9f3a-00b7c0addf47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 374
        },
        {
            "id": "8a42864e-ad8f-4482-be87-7417aaec02f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 376
        },
        {
            "id": "89e47df4-d9bc-4c70-b40e-37fef2d25e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 416
        },
        {
            "id": "644fb901-10d6-4a6d-9cec-cc5648b6559c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 510
        },
        {
            "id": "1a91c384-481b-4389-baf8-d3f8257a9e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 538
        },
        {
            "id": "58b523e6-9ae4-4155-ac7b-fc3dd01116c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 920
        },
        {
            "id": "8bcac353-d063-4868-aa62-73d16fa0408a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 927
        },
        {
            "id": "5830193d-40ab-4750-816a-1b160313f2c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 932
        },
        {
            "id": "56d7b610-0e61-40d7-8ee8-44230ef49272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 933
        },
        {
            "id": "afbd01e7-6898-445f-a27a-f95c84c9621b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 934
        },
        {
            "id": "c0cb1792-f71b-4300-9637-ff16706af077",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 936
        },
        {
            "id": "385bb916-1785-4f30-b594-2c648392c635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 939
        },
        {
            "id": "8de9c2ed-4a1d-4072-b7e0-b70e27a1b6f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 978
        },
        {
            "id": "06ac59d3-c7a9-4dc9-a7fb-e57e62ee7b85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1026
        },
        {
            "id": "c4417fd5-b387-41e1-af41-5062801023e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1028
        },
        {
            "id": "179a0c80-4721-400c-aeeb-d39732fd1e3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1035
        },
        {
            "id": "3796023c-ad6e-4fc5-a8bf-8dcb851198c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1054
        },
        {
            "id": "4a87a733-abd2-4659-b36e-2202287b32a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1057
        },
        {
            "id": "0d24bd45-8e9b-4e32-b630-c68ea8bbf4e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1058
        },
        {
            "id": "c88199c0-7564-4d44-b748-fcc4a952eeed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1060
        },
        {
            "id": "7ca7cbf5-5ff0-46e0-b3a4-df36a5149395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1063
        },
        {
            "id": "f69cf8a8-8363-498d-bf60-444fdb8dd83d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1066
        },
        {
            "id": "411fea3a-eadf-4acb-845b-ecf9101f8c7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1090
        },
        {
            "id": "73ae79cd-b9e4-43f5-bac9-a898fdbc0a52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1095
        },
        {
            "id": "5705f3e7-5158-48fb-86bf-b6aed6549d7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1098
        },
        {
            "id": "c2c420b0-bea6-4a0e-b904-2a9800fc0074",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1120
        },
        {
            "id": "c3f0e7db-57a2-4c75-9e90-e5b638b6ace9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1136
        },
        {
            "id": "0934637f-3a21-43db-87b0-4579a3916c86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1138
        },
        {
            "id": "0a1823e0-3fa6-4cb6-aa66-600dd3676a47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1140
        },
        {
            "id": "eee47c55-f1c7-47f9-8f6d-97e1233efead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1142
        },
        {
            "id": "b9ce3a61-c654-4897-b50b-23ef1c08e309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1144
        },
        {
            "id": "961c368b-2652-453b-afe4-9307d68beb2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1146
        },
        {
            "id": "f96eee45-72f9-4aeb-b414-16c9d750ada1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1148
        },
        {
            "id": "8f0f3e40-6c1a-4443-b6ba-c7e4cee7dd20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1150
        },
        {
            "id": "8b518ce3-0a5d-4e05-ab9e-610a1feb6c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1152
        },
        {
            "id": "669f4b77-5d66-45aa-b0ce-2c8a762765b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1184
        },
        {
            "id": "d21236f6-5901-4d5d-96c8-2e9fa2d38a4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1185
        },
        {
            "id": "16532a2b-555b-41fb-a86d-01e2753842b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1192
        },
        {
            "id": "fe551164-be6d-49fe-a33a-3ecddf0fdcf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1194
        },
        {
            "id": "71a2a1a9-8b3d-4011-a1c9-4f3f5934c6bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1196
        },
        {
            "id": "9c964499-7874-458f-9679-2f06abf69791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1197
        },
        {
            "id": "4a5903d6-06a6-4aea-b7be-f030821c3e71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1198
        },
        {
            "id": "0ba793ef-2335-49ae-a56d-a08fd47960b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1200
        },
        {
            "id": "026bdb51-1adf-44ae-9bde-147f913ca38f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1204
        },
        {
            "id": "0667f77a-ac2c-4285-9e16-d4e73f79d61a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1205
        },
        {
            "id": "a9082ac1-953d-4db5-8a34-dc4e39ca14cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1206
        },
        {
            "id": "d299fb4a-b96d-4d79-bb31-1d616a31ade3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1207
        },
        {
            "id": "b054467f-0b20-44e2-9ca1-5fe3adb44a94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1208
        },
        {
            "id": "e199da85-c5d3-4a7a-8a18-aa5179486888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1209
        },
        {
            "id": "80dc0698-4f3f-48b9-9e11-b832b522fa05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1212
        },
        {
            "id": "a0879155-912e-4e32-8a81-5e8aa62803c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1214
        },
        {
            "id": "9287cd37-3a3b-4523-b569-c1a880cc8320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1227
        },
        {
            "id": "61b92dcc-28d7-40d8-8787-f16aff580acf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1228
        },
        {
            "id": "21bdab36-feb3-4a12-a79e-113ab374ce8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1254
        },
        {
            "id": "fbe7509f-8a05-44dc-b4fd-e9a1df34d8a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1256
        },
        {
            "id": "ea032b47-a38a-4c8a-b06b-28e99a0e6988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1258
        },
        {
            "id": "9bfcc3c4-541c-4f58-ad86-2461208e1d69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1268
        },
        {
            "id": "391ea9b4-ec59-44f2-9677-059d269083c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1269
        },
        {
            "id": "5ad596dd-3a61-4e88-89ae-9fb264e0ed81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1284
        },
        {
            "id": "209e8608-1d8f-41b5-ba29-f8b82e31ed30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1285
        },
        {
            "id": "ed3be102-dc1e-4a2c-b5c3-581b1624fa8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1286
        },
        {
            "id": "ab81580f-3e5c-4913-9770-6eaa03e4b78d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1287
        },
        {
            "id": "42b852e9-4730-49f3-a792-c6c4df1af64d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1292
        },
        {
            "id": "ab225a1f-a562-41fc-9e87-3bdf595bb884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1294
        },
        {
            "id": "15cf3860-0e83-4110-b2fa-60a1808dec2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1295
        },
        {
            "id": "56494c7b-3730-4e13-bee5-f140c6fcf7e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7808
        },
        {
            "id": "a7994ab2-2106-46cf-9e7c-7b77381ac19e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7810
        },
        {
            "id": "a5fd43b7-0657-45c1-8f37-e9b8d6d37b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7812
        },
        {
            "id": "f9b8f326-817e-421e-95cd-ddff176c751d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7884
        },
        {
            "id": "018b26ee-8b25-417d-8abf-4c21fbf6d670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7886
        },
        {
            "id": "900c08ca-bba6-497c-a6ec-664774942050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7888
        },
        {
            "id": "9aefa7ba-c512-4b9e-be68-efcf2c1889b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7890
        },
        {
            "id": "d8fcc4f3-e529-49a7-87cc-690db0a96f66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7892
        },
        {
            "id": "eba42aff-5d56-4ad2-be22-50ad51738cf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7894
        },
        {
            "id": "db8e0e8a-2544-48c3-84dd-1716db3600cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7896
        },
        {
            "id": "73359f4b-eca2-43ce-a565-885931e67b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7898
        },
        {
            "id": "1c86e988-ef8f-4c18-8c62-262f95bcfeb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7900
        },
        {
            "id": "14540dd5-3334-4cd5-889c-5a5a61f84379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7902
        },
        {
            "id": "f4c3a78f-c556-463e-b5e9-4b0fca7af8ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7904
        },
        {
            "id": "81973515-148c-4595-b31f-b63f0d7c5149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7906
        },
        {
            "id": "a4d82f20-c2bc-4833-958f-597897e82187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7922
        },
        {
            "id": "985ab4aa-d4b1-455e-83ad-6c1c2b35d587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7924
        },
        {
            "id": "18339172-3f9a-4940-938d-bdf80712cd15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7926
        },
        {
            "id": "65b01377-152f-4541-9721-807e974c2cd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7928
        },
        {
            "id": "a61230aa-b521-4174-88dc-3ed609523861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 84
        },
        {
            "id": "0e24f9b5-0944-4803-84b1-79c9ee749c33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 354
        },
        {
            "id": "819b2171-c0e6-4995-9996-bc2734b6c002",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 356
        },
        {
            "id": "5ce69f0a-47f2-42e0-b88b-8e0d05f1754d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 538
        },
        {
            "id": "3296530c-23d5-495a-a41d-5143c9c25c11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 932
        },
        {
            "id": "077d79f5-91e9-4fbc-a28a-23746406c2de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1026
        },
        {
            "id": "ccbb0d1c-723d-4924-9e4a-8cbbd7cba158",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1035
        },
        {
            "id": "f10c6299-13c9-4e1e-91f2-586f5c64802c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1058
        },
        {
            "id": "13085903-0d7d-45dc-8e91-334a14e77a9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1066
        },
        {
            "id": "b9eb6419-9bdf-4388-b3c7-2a2555ccb55e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1184
        },
        {
            "id": "1e031721-4c3f-454b-93f4-df673fbc2238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1196
        },
        {
            "id": "0886ba9c-0767-4917-bcbc-08700ad00601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1204
        },
        {
            "id": "cc4cdb22-5435-4240-89b5-185f1ae64e32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1294
        },
        {
            "id": "e6993e33-8d02-4bb4-bfa9-91884f67462b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 67
        },
        {
            "id": "26fbcff9-d188-47bb-a541-8c8e2d942675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 71
        },
        {
            "id": "370f3212-8ba3-4d1b-8e2d-539eba77a93c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 79
        },
        {
            "id": "3c9200d5-cbc5-4947-9b5f-0539c9143610",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 81
        },
        {
            "id": "d82e5d3e-dd22-43f9-9609-06a353457181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 84
        },
        {
            "id": "6b053a8f-e1ff-4574-8561-981d1ca1405a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 86
        },
        {
            "id": "288ddd8a-efb0-4559-9403-0c14fea252b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 87
        },
        {
            "id": "529ae9bc-76d3-4a47-8065-4c103e687f1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 89
        },
        {
            "id": "60e9dcd9-8b42-4935-83b7-812423a53c34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 199
        },
        {
            "id": "9e47ef68-a87f-4eb1-a762-ecd7521eaeaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 210
        },
        {
            "id": "94d0b7ce-5a29-473f-8658-41fe0aa74fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 211
        },
        {
            "id": "5567aa46-fa5e-4e76-a982-a46fd98b16bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 212
        },
        {
            "id": "871e995c-b08f-4204-9adf-ff2e7de587a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 213
        },
        {
            "id": "e5e725dd-0540-47fe-8185-469a2c9cb86c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 214
        },
        {
            "id": "d490f67e-eb13-4db5-a346-c5c3931da4f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 216
        },
        {
            "id": "21dd829f-3410-414e-8075-78c3b14c5773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 221
        },
        {
            "id": "3a0fa1e4-a984-4f90-ae9b-8bfed3ceb9e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 262
        },
        {
            "id": "5822d03a-a280-404e-b430-4298a73095c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 264
        },
        {
            "id": "a11d4ed6-798b-4bcf-989a-43ac0295ccb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 266
        },
        {
            "id": "05ceac72-3d14-413e-ae20-6e0d87df2f22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 268
        },
        {
            "id": "62de2ce9-6b14-4852-8625-450b9b5b9ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 284
        },
        {
            "id": "07cc951e-ea3f-46de-85a6-43a493796297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 286
        },
        {
            "id": "425223c9-3499-4bc2-b0ca-e68e67f30381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 288
        },
        {
            "id": "0926e36c-c99f-43c2-ab35-339d67766f16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 290
        },
        {
            "id": "4069a2cf-2f53-4053-9946-a6610595a48f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 332
        },
        {
            "id": "c9c037f3-b266-42a9-bfb9-c0640cfce9a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 334
        },
        {
            "id": "04e3fc2f-4351-431e-ba57-4e65c59d59cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 336
        },
        {
            "id": "e9d5aa64-f695-4319-8cbc-f9e6d7ca301f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 338
        },
        {
            "id": "96f29047-a9f1-4afd-8ae3-71b9e74a21f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 354
        },
        {
            "id": "43841b99-ed5d-4f9c-a032-7f7155d31c89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 356
        },
        {
            "id": "3c23a84a-5a43-4911-9f66-60339a7514db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 372
        },
        {
            "id": "7121e4ec-1a8f-4784-b4b3-c0971cf627ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 374
        },
        {
            "id": "d1bedfac-8c39-44ba-9cf5-b59a3a75c643",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 376
        },
        {
            "id": "f2c3c747-1d4f-4695-a793-013f0b170669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 416
        },
        {
            "id": "38a17382-8f60-4a16-8919-a4792927489e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 510
        },
        {
            "id": "59465009-7a12-461a-95a6-14c5d76d66f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 538
        },
        {
            "id": "8da175f0-d236-4056-b5c0-44e4ee16c8d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 920
        },
        {
            "id": "7dcdacb9-d972-4465-8814-70bd76bafa24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 927
        },
        {
            "id": "925a682d-e934-4952-b52e-98e91b3a067f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 932
        },
        {
            "id": "352a8553-27ae-4dbd-ac8e-866f0918116a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 933
        },
        {
            "id": "68a114a7-27b8-4c72-be12-b02b0e81dc2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 934
        },
        {
            "id": "b1e29dff-bbed-45fb-8f7e-76a6446d606d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 936
        },
        {
            "id": "7988119f-9690-4fcf-acde-17b869662496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 939
        },
        {
            "id": "5551c186-41c1-403a-8647-52a03309e04a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 978
        },
        {
            "id": "838a5c83-353e-46da-a337-ec4ecf6ee5fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1026
        },
        {
            "id": "26376618-e2cc-45d0-8d9c-4131642e3b1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1028
        },
        {
            "id": "e31031a0-a3f9-4457-b227-f72ad0899f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1035
        },
        {
            "id": "83a97c00-3593-41c4-a0ce-0bf78280548c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1054
        },
        {
            "id": "7b91b5dd-934c-4f11-b66a-b99fe45d0cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1057
        },
        {
            "id": "fdfc9951-7622-481d-96cb-159f360aa6ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1058
        },
        {
            "id": "2ccab100-fdc0-47ec-b988-aa6bea120552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1060
        },
        {
            "id": "33b04afc-bc97-4974-8f8c-0b8ea5040b01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1063
        },
        {
            "id": "0e10b9da-b60b-46ba-9e6e-ee79e8c38916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1066
        },
        {
            "id": "bf51b3b7-f51a-4ba4-99c6-10dbf8ff704d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1090
        },
        {
            "id": "7490aaf1-35da-4c00-adbb-45f37e03a67d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1095
        },
        {
            "id": "1d2b375b-e05a-4959-989a-a4d634573740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1098
        },
        {
            "id": "1e0110de-e072-48a3-9bf1-4bfcf9bd356d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1120
        },
        {
            "id": "9d90c91b-3852-4c4a-82f1-2d85a195b057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1136
        },
        {
            "id": "662aa762-1eb5-4050-a217-890002c868f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1138
        },
        {
            "id": "9e2032bd-3059-4af5-acdd-8755d9aac8e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1140
        },
        {
            "id": "bd788c0e-472a-4cdc-a998-452a6e21a0f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1142
        },
        {
            "id": "e243235d-9540-47fa-8ea3-270f82b25c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1144
        },
        {
            "id": "5c87cbb4-9bac-4503-851b-0c9a57695a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1146
        },
        {
            "id": "3d0f1a0d-d2b1-424b-a603-9df824ec57cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1148
        },
        {
            "id": "10d12dea-40f9-48aa-9ecb-59d974bffac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1150
        },
        {
            "id": "846aab6f-3ca8-4472-b9ee-fb33eb3572de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1152
        },
        {
            "id": "429e7ef2-1e03-448a-a0db-94f63af8bcf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1184
        },
        {
            "id": "74fed300-7118-4eb6-b4ae-b60e03431fab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1185
        },
        {
            "id": "da6987be-5a34-4292-8615-787fef9bcd8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1192
        },
        {
            "id": "e4156095-7d54-43ee-9dbb-84047bb0161d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1194
        },
        {
            "id": "e7798d39-78c0-4cf9-9dc9-02d67931fd4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1196
        },
        {
            "id": "36d3b8f4-2e31-42b2-91b8-cfffe1f3f751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1197
        },
        {
            "id": "460762e3-ad82-47ca-b392-0d82ac631768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1198
        },
        {
            "id": "0ec27784-a286-482b-9767-c8a2e0fecfd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1200
        },
        {
            "id": "28ebb8ea-b277-4692-904e-88877de9b959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1204
        },
        {
            "id": "0036c64e-1e61-4f19-8069-abeafbc74ac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1205
        },
        {
            "id": "12120737-528d-4fbf-a8d6-8003ef35d969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1206
        },
        {
            "id": "44db0516-944e-4724-b84f-add2a4f3f365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1207
        },
        {
            "id": "548ce1d0-b3ec-4c62-a8a0-7340c0f289f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1208
        },
        {
            "id": "d84710b0-bf10-4815-8841-8609d0c621b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1209
        },
        {
            "id": "2d3ebd8b-5553-4e04-a60c-5710614d0136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1212
        },
        {
            "id": "9d54f8db-90ff-4e01-8101-7527a69eb8c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1214
        },
        {
            "id": "47d2a054-ef1e-448b-9e5e-ad212d86abdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1227
        },
        {
            "id": "779f4b77-63e8-420e-9da9-d347c2b4cfe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1228
        },
        {
            "id": "7b575936-2e05-40a1-b183-27919dc9ba3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1254
        },
        {
            "id": "4be46503-bae1-48a5-8207-6e3c246eff3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1256
        },
        {
            "id": "b8e4ac60-aba8-4727-ba7e-e5837e78e743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1258
        },
        {
            "id": "6c0c9ac3-57cb-4aee-9b7a-32d9d2115890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1268
        },
        {
            "id": "2e33a8e4-40b6-4565-90d5-aca121b150c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1269
        },
        {
            "id": "c27f2902-b8f3-4dc8-9f3d-5efc96388f2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1284
        },
        {
            "id": "74fe86f5-c5e1-4cb0-b74e-fe3f2ed653cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1285
        },
        {
            "id": "52f4e92f-3176-4e14-a6e3-5c0faf2e5d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1286
        },
        {
            "id": "73c422f2-49b1-40b4-a5b7-3c99a77fe34e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1287
        },
        {
            "id": "2d30a00c-80af-4df0-be99-400e02e6c289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1292
        },
        {
            "id": "1a13e19c-1d16-4726-bc70-113e585441fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1294
        },
        {
            "id": "6baad9b7-b7a8-435c-8b67-d6f5e6c0c7a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1295
        },
        {
            "id": "3d190771-7a44-4bc7-9208-246ca0fe7f20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7808
        },
        {
            "id": "ae5853f8-f4ce-4b98-b8c9-fcdbe6112143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7810
        },
        {
            "id": "f4e6a48f-fdb1-4fff-92cd-ebaf66eb2d1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7812
        },
        {
            "id": "1adab0df-fe43-4d0f-af70-28252f80a8b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7884
        },
        {
            "id": "a4f18b7c-fdb4-4ddc-bb0e-04133569e033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7886
        },
        {
            "id": "79b01456-e8a8-40c1-b37b-62bfc5327ccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7888
        },
        {
            "id": "f3312142-e858-415c-ae17-20c770783806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7890
        },
        {
            "id": "43211c7b-3601-4fce-ac6c-22c00c4fac6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7892
        },
        {
            "id": "0334b4e7-bb83-4ef1-8195-e9e0599201b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7894
        },
        {
            "id": "01294b5d-8517-4cc8-bd80-0e6b001479c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7896
        },
        {
            "id": "de1bf432-d2fb-4265-a105-87b8657f9374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7898
        },
        {
            "id": "41f42d24-f6ea-423d-b720-ea4bc7a5ed56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7900
        },
        {
            "id": "5efbc81e-890b-4fcb-a332-037628ddde5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7902
        },
        {
            "id": "cbb15ce0-595c-4524-89dd-c5fd895d517f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7904
        },
        {
            "id": "01a74355-01c2-4bde-bb53-8c2a425d3bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7906
        },
        {
            "id": "e4a1a03c-aad4-4f86-9d1c-864f2adcac65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7922
        },
        {
            "id": "817bd8fa-1142-4c2b-a507-ae825987880d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7924
        },
        {
            "id": "4226c7a5-62f4-4220-9569-b48d7487e3c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7926
        },
        {
            "id": "7d931577-cd10-45b1-8ef1-e1d4512a5116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7928
        },
        {
            "id": "449b576a-3332-4d4b-9502-c33fa59ed8f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 34
        },
        {
            "id": "9e139c44-0f6c-439c-889c-3be1a488039e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 39
        },
        {
            "id": "10904da8-b904-4763-ab8d-90d443136a07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 65,
            "second": 74
        },
        {
            "id": "f2ea692c-e312-48fb-acd0-3a5d8aa796f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "43c3e32b-1eee-4233-8717-7faa32cd44e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "2879e535-125e-43ef-b6b0-7c0624af18e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "3cfc3307-4791-4d5b-847d-ba6bdfb44b63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "83665de6-0fa2-4ae9-9ad4-2bde29e9db8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 221
        },
        {
            "id": "39b232fc-6135-4df7-8f52-60abf3d7b25f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 354
        },
        {
            "id": "29e1b33e-8faa-426a-b42f-67a1df36059d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 356
        },
        {
            "id": "30a402f4-b348-4aef-b4fe-249bf0e4b4f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 372
        },
        {
            "id": "790dd36e-40c7-43a3-acb3-620e2f8e65bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 374
        },
        {
            "id": "4da364d0-7ec9-47c2-a5ed-36c96459b983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 376
        },
        {
            "id": "0aae3e4f-bdbd-408f-8c5a-01b5a1c77f17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 538
        },
        {
            "id": "4d8fa919-5cc6-4a7c-8df9-51a38b48554c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7808
        },
        {
            "id": "62f02681-3f7f-416e-9a17-116d5ac1f90e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7810
        },
        {
            "id": "212ceac2-fe09-4665-8267-d9cc66b92686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7812
        },
        {
            "id": "59e64ba7-40a5-4c9e-8e08-9a90bbe62bc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7922
        },
        {
            "id": "40ede070-12c6-4eba-8880-784ed549322d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7924
        },
        {
            "id": "47bf90c6-2c11-40e6-9dd9-a283fae1c585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7926
        },
        {
            "id": "9146bd90-3880-44dc-9ff8-e91a423e7364",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7928
        },
        {
            "id": "cf8adc79-a078-4dfd-b403-2fbdb3b64af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "3a34d27b-1d7c-4c62-984c-b8a5df4088fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "5b233382-1929-4f30-9712-6f9561600335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 44
        },
        {
            "id": "24f9e89f-644d-44d3-81e8-85ba46ff4fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 46
        },
        {
            "id": "0ddea5d5-5b20-4bed-831f-69f18738e4b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8218
        },
        {
            "id": "84b6d25b-340b-484a-bf86-1788ad262091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8222
        },
        {
            "id": "8506eb5c-47c4-4bbe-834f-33818e4a8e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "a41a35c5-75f7-482e-8b20-b4bedec8f9d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "115789e0-71de-4769-adfa-b15af60ec0f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8218
        },
        {
            "id": "0a2cc7b7-6e29-4a45-8604-017e52cf9ef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8222
        },
        {
            "id": "239da3c0-d9c6-4f5d-984d-982b1ebd36a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 74
        },
        {
            "id": "af2fce95-07b0-4622-b875-af8532773541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "03faf989-7c84-46d5-919d-f8a1acaadc9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "e2a25076-e1ad-4c30-b241-ae30072396c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8218
        },
        {
            "id": "e16eea91-18cf-411e-be1e-09bd3c49c17a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8222
        },
        {
            "id": "5313c38f-078c-483f-bfa9-6943a21cd2a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 34
        },
        {
            "id": "778705cf-6696-4768-a17a-89736e201744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 39
        },
        {
            "id": "a87af53c-c219-427c-95fd-e4f2869d1a9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "b10bbb42-deac-4514-ba75-48b0da3a6015",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "a40f6c3b-0d10-414b-8d82-cf06f0f790e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "2fadab7b-ec27-4c9f-930b-c59181e44191",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "fd599920-76a0-430a-b27c-c5eb65dd7388",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8218
        },
        {
            "id": "00bd5dba-d6ce-488b-a8d7-3df3d905b907",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8222
        },
        {
            "id": "0eb71d65-c3f5-481e-9721-53604ac3d80a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "b744a965-0771-4f05-a8cf-abc66a106c84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "923c7f53-ba25-4e89-97b1-07036cdaabb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "ef1bb577-3ede-412e-8f18-2d8408546555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 192
        },
        {
            "id": "bc9baca0-9e93-4497-86c6-b31b8fadbbef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 193
        },
        {
            "id": "94c9862a-b6fb-46d3-9c6c-598616d3b523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 194
        },
        {
            "id": "ffab68a7-a4c3-474c-8135-3673e6ba56cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 195
        },
        {
            "id": "4c15f772-fad0-46f4-9f05-fc3c821a1528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 196
        },
        {
            "id": "b9450785-c6f0-4371-8133-499c53bdd74f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 197
        },
        {
            "id": "d2b965d8-2280-4124-bab2-c584d35e975e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 256
        },
        {
            "id": "da04d1ad-131a-41cb-885e-88f202b6d21e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 258
        },
        {
            "id": "39cb3dff-fa16-4a6b-ac8c-e318f800dbe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 260
        },
        {
            "id": "e3fc5793-96a0-4de9-ab12-1045e0f085e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 506
        },
        {
            "id": "374bfc44-33ed-4f90-9ec7-dcf57e5dacd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7680
        },
        {
            "id": "3575a1ed-b53e-4f4c-b988-343430e56d05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7840
        },
        {
            "id": "6c14a50e-0e24-49db-b653-0a01746bda11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7842
        },
        {
            "id": "667f6d7c-3239-40cf-8656-247c11ee1ec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7844
        },
        {
            "id": "98490ac1-8081-42d4-9ff0-f2459125a6d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7846
        },
        {
            "id": "7ae995ab-6544-41fc-a8c7-8c9853e91f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7848
        },
        {
            "id": "a95e1750-737c-48b0-945d-b70d035759d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7850
        },
        {
            "id": "1719c480-e7c9-473d-931b-468d950aa803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7852
        },
        {
            "id": "34994bd5-f293-4372-9cb3-64ea8d11bf1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7854
        },
        {
            "id": "00d7094e-6e9f-4cec-9032-78f235334430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7856
        },
        {
            "id": "0125dee8-1cc2-4e6f-826f-4d73b26df001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7858
        },
        {
            "id": "fc37a9e8-a436-4645-8d10-cae3f0437bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7860
        },
        {
            "id": "df6c755c-40eb-434c-8949-a195be737604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7862
        },
        {
            "id": "3dcdcc22-14c4-41c2-b55d-4fd7346aa223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 8218
        },
        {
            "id": "47e0a998-481e-4620-a02c-796dcf18c46e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 8222
        },
        {
            "id": "d61e6d92-a688-4035-9e4d-ea04ded3ed3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 44
        },
        {
            "id": "814f568f-b96e-440e-98dc-3030577a70b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 46
        },
        {
            "id": "e7b70201-8afe-4f1b-8d69-ba08121e7ec0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8218
        },
        {
            "id": "e46a3c92-c696-48dd-9a05-83a7c90ac859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8222
        },
        {
            "id": "2d049806-a29b-4d76-b9c5-73d026b7ca12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "8970b25a-904d-4619-a753-649f2f2ed243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "b47b4367-61b1-41d0-89a3-bcaa0637a8ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "2ee9ed14-9c75-4d20-8cb9-81e612443702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "9289bca7-3174-47ab-ac90-785350df2a5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "79584c05-4fcb-48f5-a646-0289bdc41107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "410df433-acf2-4e74-9d88-2ade06e5fd77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "2f283214-981c-4574-b777-4397998ce95b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "9cdbed08-eadd-436e-a8af-443e608df858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "68d9d822-4e81-4628-8e2b-d0bbca523cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "60b80ece-d7e5-42a1-8560-b5fbfa7b526f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "2f090773-d4cc-444a-be10-3d447fd4512c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "28b01c53-d8a5-42f0-969e-3c8378cffe16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "bc900402-a027-4077-a803-4ddd41f4de56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 113
        },
        {
            "id": "ab11061c-cdee-4adc-a7c1-9d13ff0656d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "d18f6985-39aa-4520-8f53-db080aa18f5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "32d1fa58-69d1-4c9a-a070-fb490e574370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "b04c6108-efdb-49ca-ae8e-07e1bae4ecf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "98761516-bb53-45c9-bdb1-fbce5c123b26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 192
        },
        {
            "id": "9d004fbd-1201-4194-afac-0783223f4e7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 193
        },
        {
            "id": "3fa03986-89fd-4ff7-ae70-1df11eb49e7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 194
        },
        {
            "id": "ed2157c9-d4f7-42e8-a945-c4d6b9a7031f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 195
        },
        {
            "id": "0b9413bb-7267-4fdd-9fb4-afa97949ee3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "f3da47f6-a9fc-47ba-a41f-542fac9b3ac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 197
        },
        {
            "id": "ddda4bea-9b37-4816-847c-fb7393bd51f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 224
        },
        {
            "id": "5b0b1363-f7db-4225-84e6-31b956eaff24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 225
        },
        {
            "id": "5620d0bc-3b2a-427d-89ee-f66f98dc7e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 226
        },
        {
            "id": "61aac929-45cc-4ed8-b9e5-c4c02b9bcd2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 227
        },
        {
            "id": "b15ee458-48c3-4654-9147-52cd2acaf337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 228
        },
        {
            "id": "032f913f-9b9e-4084-b414-85162548a25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 229
        },
        {
            "id": "a931b39a-e6e7-43f8-80b4-0b4f69721c08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 230
        },
        {
            "id": "e712c814-5060-4a9a-b225-3d1c8e7ac118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 231
        },
        {
            "id": "3f588858-9798-4975-a8ca-6d8243538aca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 232
        },
        {
            "id": "da878293-5131-47a1-bbf1-a0104ab5fda4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 233
        },
        {
            "id": "a08a68dc-3663-4b8c-8e75-a967c039b8b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 234
        },
        {
            "id": "0f37b07c-87e4-4664-b7a6-9ec101b3b418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 235
        },
        {
            "id": "c6b55f6c-b411-4047-8818-06baa20156a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 242
        },
        {
            "id": "9ac12fa1-9173-4e02-8045-8474b24748a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 243
        },
        {
            "id": "27f27a01-70f8-4c0f-b415-7ae8e9e32631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 244
        },
        {
            "id": "7568c7bf-a110-43c7-8f14-1d8c11e44ccb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 245
        },
        {
            "id": "725d4804-a3f7-4bc8-8c4e-9e6a0167014e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 246
        },
        {
            "id": "19655187-63c9-4ce8-b892-a7ba40ab01e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 248
        },
        {
            "id": "53b114aa-c899-446e-954f-93e7576a96ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 249
        },
        {
            "id": "bcd3ad22-1f5b-4f21-8a2c-149db5a6da88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 250
        },
        {
            "id": "0ec4d98f-a9ca-40df-a575-115293bf3a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 251
        },
        {
            "id": "f908f8cb-fdce-4654-ab73-7b61de7fe70e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 252
        },
        {
            "id": "54062727-1d25-4dff-a76a-8bbdbcb77c2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 256
        },
        {
            "id": "e97dda13-10b1-4ffb-94c9-430026f5fc0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 257
        },
        {
            "id": "54b2b3dd-2b16-4453-82c6-b8497ecaee34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 258
        },
        {
            "id": "8b3c2a0e-3bf6-424c-876a-34a06001fe75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 259
        },
        {
            "id": "559db52e-a9ec-41ff-8bf2-46252ca2ec28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 260
        },
        {
            "id": "1a091a8b-cfae-4d0f-bb9b-282018b51a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 261
        },
        {
            "id": "4e463c45-b33d-46ea-8047-3d45ff4529b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 263
        },
        {
            "id": "c3a96697-14d7-4ccc-be3c-bd54feb712b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 265
        },
        {
            "id": "b3f18c90-548a-48e9-aad3-0129196cef60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 267
        },
        {
            "id": "5625adb7-387a-4899-8c2b-27e475c02b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 269
        },
        {
            "id": "2be46f52-433b-401a-89f1-29e6dc021f0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 271
        },
        {
            "id": "345fee69-f183-4413-b283-dd4074350594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 273
        },
        {
            "id": "71989097-5f75-4af5-9801-76f9cd47f65b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 275
        },
        {
            "id": "a79815a6-c5d6-4b9b-b57e-e51a8bf3eac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 277
        },
        {
            "id": "3f9acdef-f6fc-4f99-8696-fd83f81e0bc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 279
        },
        {
            "id": "aaf05400-7948-404b-ba0d-45d1f74b1f7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 281
        },
        {
            "id": "26da9fd5-20b4-415e-85cc-dfb145a11f89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 283
        },
        {
            "id": "6bc7a052-444c-4a74-8e7c-8b87f9c57f67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 285
        },
        {
            "id": "65cc005b-26ed-4011-a484-4ffea44538b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 287
        },
        {
            "id": "3ab7286f-c814-459b-ba2b-55ab3909728b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 289
        },
        {
            "id": "45fb9179-c9c9-4754-9fd9-63bef8d91e3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 291
        },
        {
            "id": "d687499c-6e89-425d-8083-7d7ca6980793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 312
        },
        {
            "id": "bf8ce461-9f14-4ae4-b8f6-54990e1a85ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 324
        },
        {
            "id": "9018b530-4861-456f-afba-d7c93327778e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 326
        },
        {
            "id": "9c687e68-13c6-452e-aff5-5e7dbdcdeb52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 331
        },
        {
            "id": "4deae2cb-33dd-4222-a66e-588f8edc5bd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 333
        },
        {
            "id": "7c06115e-b899-48e7-90ee-7cc85df79f07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 335
        },
        {
            "id": "15c55150-dfbe-4860-a114-62444a4a63f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 337
        },
        {
            "id": "5b1471ec-ff73-4764-bd42-fefc07c4b3f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 339
        },
        {
            "id": "9fb3be44-5b74-42d8-bb58-665375975a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 341
        },
        {
            "id": "0235ffb7-5bfb-4a92-a6e6-0fc960accbca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 343
        },
        {
            "id": "b3c8aa36-3bb8-4ba5-81bc-48b538d414c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 347
        },
        {
            "id": "91a7a857-78fa-42dc-b262-32a0f68de5d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 351
        },
        {
            "id": "2d85c05a-ab6a-4864-8de6-df1f3ccc3442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 361
        },
        {
            "id": "46d4dd08-402d-4c59-8b42-732df0d2171b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 363
        },
        {
            "id": "ac75ec70-e878-4178-baf6-4bd2cae95454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 365
        },
        {
            "id": "5ada9192-303a-40f9-9368-9fc814299a90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 367
        },
        {
            "id": "43a947fb-219a-4e40-a0d0-ab5b292fea8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 369
        },
        {
            "id": "d6e342c3-ff05-4fe5-9ec0-58687a07b0eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 371
        },
        {
            "id": "a1615aef-4c3b-45dd-97aa-82086ae6ced9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 378
        },
        {
            "id": "42388cf0-bd4c-49f2-86bb-fee90a79963f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 380
        },
        {
            "id": "3cc61a7f-c793-4018-a683-8867a9336c20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 382
        },
        {
            "id": "54d90a8c-b90f-4b53-8486-a00ce51bb588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 417
        },
        {
            "id": "f968a62e-626f-41c2-8949-96e12f4cb1a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 432
        },
        {
            "id": "39f6a083-1bf1-4d3b-952b-64fe11775538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 506
        },
        {
            "id": "3e50d834-7953-4731-a5bd-1a2d882f9959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 507
        },
        {
            "id": "951100b3-edb5-42d9-aa2b-419ef818023c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 509
        },
        {
            "id": "4a88bd14-ef63-4d92-972e-d67cc424cd33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 511
        },
        {
            "id": "e3f44e1d-c246-4bb3-b53b-b5defc1648e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 537
        },
        {
            "id": "e3c1c8ea-364e-4162-9fdd-fcc60260c794",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7680
        },
        {
            "id": "4dd3ed1d-8418-4409-b70e-7313668d50c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7681
        },
        {
            "id": "4e97f31b-0c34-4970-9dd8-8a930ccd2535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7743
        },
        {
            "id": "327d150f-5c90-4c78-bffa-b25d10eefe1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7840
        },
        {
            "id": "486a5b19-8bf5-481b-a787-5e28333c1f26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7841
        },
        {
            "id": "3f379ded-afc3-4237-839f-2e1be5ad260a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7842
        },
        {
            "id": "74b94055-5bf7-432d-aa91-4ed080f98632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7843
        },
        {
            "id": "b17b532e-d7b6-4d60-84ce-ccbe42abefb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7844
        },
        {
            "id": "6a713fba-693c-448e-9c7e-05b35bffbced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7845
        },
        {
            "id": "2050fd16-e90f-4bf8-8ffd-7bc98c897f52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7846
        },
        {
            "id": "1fa12bc5-327a-4ec4-82a9-3767fe71e3af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7848
        },
        {
            "id": "932339f6-bdab-434e-8ab6-e9eb09b8cfe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7849
        },
        {
            "id": "e8241c34-fa11-4df6-9aae-75b6593a666e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7850
        },
        {
            "id": "cc894d91-be8e-4f1e-a61c-69060e5c88ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7851
        },
        {
            "id": "7a03d706-e7c1-42eb-8e0f-13fe7998fd2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7852
        },
        {
            "id": "90edf1be-7f42-43ef-a1da-8a0f877abccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7853
        },
        {
            "id": "c3802808-6494-44bb-8fa9-cecf71e0b143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7854
        },
        {
            "id": "d8fd587c-0c45-4200-8992-b2ee99793b88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7855
        },
        {
            "id": "45ce4c90-8c6f-4727-aebf-c34dff873575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7856
        },
        {
            "id": "3ec17770-8479-4218-873f-49c6a55db896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7857
        },
        {
            "id": "16a270fe-a1c3-4293-9ef3-2fe8734b5946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7858
        },
        {
            "id": "f254254c-0d3b-4290-9186-b28b82f9c198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7859
        },
        {
            "id": "ef2f89e1-0ef1-4432-99b7-1cb6bca899df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7860
        },
        {
            "id": "5c819c9d-21b2-45ea-81e2-b62020e375dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7861
        },
        {
            "id": "082adccb-8094-4ad7-bdb6-b2735d82bddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7862
        },
        {
            "id": "2494060a-ee88-4cd4-8ac3-618f4d368d21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7863
        },
        {
            "id": "6935fd4c-1544-4387-84b2-7878aac8e8b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7865
        },
        {
            "id": "5778223e-cdf6-4d1a-92dd-9439d9f10ace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7867
        },
        {
            "id": "220df6f3-f36f-4296-8931-4e48d41c82c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7869
        },
        {
            "id": "b27a10cc-b4cd-4745-b6d1-a03a577bc139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7871
        },
        {
            "id": "5ee37ad5-39c7-4848-89a6-1ddec878fb25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7875
        },
        {
            "id": "4118d53f-97c5-4eed-a03b-90c4651896af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7877
        },
        {
            "id": "90286d34-6081-4c44-83f7-2d49a6a19ec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7879
        },
        {
            "id": "1a8055b0-43b5-4721-b690-d904ea52cbb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7885
        },
        {
            "id": "335a77b9-7d96-4d35-915f-4ae97c90958b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7887
        },
        {
            "id": "2506fa26-717d-4a2f-af23-9128f9be8700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7889
        },
        {
            "id": "76aca6e2-efa2-41cb-a89b-0892e26f4348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7893
        },
        {
            "id": "1a5405d5-a4c3-4c75-8406-9f0e8698761d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7895
        },
        {
            "id": "e0b7254d-0746-4bd9-97a3-b5211d969ff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7897
        },
        {
            "id": "d7869a3d-cba5-46e7-be3b-f544769cf14a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7899
        },
        {
            "id": "8df4900d-db99-4860-826b-5ecab3410e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7901
        },
        {
            "id": "53698fb6-9abb-4e85-983e-f1811ab27fc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7903
        },
        {
            "id": "fe5f4d59-d189-477e-a53a-05c4df5fa8ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7905
        },
        {
            "id": "d4073439-830d-4d44-aa37-b6b2016952ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7907
        },
        {
            "id": "39d8866f-ea65-4e2b-8141-55e3b802862a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7909
        },
        {
            "id": "b112ee07-9470-4d97-8d23-5210eb4d5170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7911
        },
        {
            "id": "6a63ad30-3a07-495c-a7ce-e35343ec7bd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7913
        },
        {
            "id": "ee604c4f-0a77-4a73-8cbb-9476922c706d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7915
        },
        {
            "id": "55d0c9e1-54dd-4fa4-9c2b-89580c758ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7917
        },
        {
            "id": "844b8a78-a711-4161-9637-da35a91d8ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7919
        },
        {
            "id": "a31ee23a-e4f5-4741-8a6b-036c434a0234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7921
        },
        {
            "id": "dd93b2de-c7bf-43ae-b146-8e3082fa240b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8211
        },
        {
            "id": "d6431d33-489a-4885-ba49-05a7b409ba89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8212
        },
        {
            "id": "b4b4fd09-f197-4e09-a30a-caaa8f17df73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8213
        },
        {
            "id": "8836da30-694a-4e29-ae68-044a01106021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8218
        },
        {
            "id": "4c1f3e84-a652-4ace-a199-55c11c99b29a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8222
        },
        {
            "id": "214219e4-b5d0-4535-89f6-70e2913654ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "ffaf547d-d863-4666-b3d2-2c646b5a0979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "a21d042b-6f0d-489a-a8a0-546278c6e5b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "badf696e-596f-4b67-a4d9-909a05791440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 192
        },
        {
            "id": "7e9e3c10-00c2-419c-9d2d-b2b0d24b1382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 193
        },
        {
            "id": "8e8d5b4d-a815-4042-a6bf-82009661d55b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 194
        },
        {
            "id": "a247c425-d18d-4fce-8ed3-688d01757aab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 195
        },
        {
            "id": "f9d92ea6-e606-47b7-a168-1281697ed67b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 196
        },
        {
            "id": "9ea0046c-f60c-4ebb-9d52-158458755f4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 197
        },
        {
            "id": "028b10a2-9122-4f98-b94e-493b5423bc7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 256
        },
        {
            "id": "e3416524-888d-4abc-882e-53effd936cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 258
        },
        {
            "id": "60962d14-edb3-42cd-9639-309e67015f47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 260
        },
        {
            "id": "81cf223a-397f-4f0b-b263-2656b0c6b117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 506
        },
        {
            "id": "6f26788c-e03a-4e1f-ac83-6299e33f0aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7680
        },
        {
            "id": "4c02480a-6cf7-44b8-9712-12bd9393d0d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7840
        },
        {
            "id": "3e325fec-1524-4c10-93b9-55e91b5c3735",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7842
        },
        {
            "id": "dd0f74e2-5205-45f2-9c53-f87723f8e8c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7844
        },
        {
            "id": "792ba85f-20ae-43bc-8d59-a96cd2a8465f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7846
        },
        {
            "id": "2d334600-a0f6-43ac-8ebc-933991d4d8cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7848
        },
        {
            "id": "941b911e-2a0e-491c-aa56-c1b72aaaadc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7850
        },
        {
            "id": "b6c4a5e6-fdd1-4cef-bb52-933d3bd99bb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7852
        },
        {
            "id": "7c933746-6404-4db7-bbae-af2882eb8d5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7854
        },
        {
            "id": "733a5acb-f54d-4f1e-ab90-e61f71186815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7856
        },
        {
            "id": "b1dbd3f3-fc5a-4fc7-9ff7-d8a029b85303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7858
        },
        {
            "id": "1869eb62-3cc6-4e94-97f2-7b8be4fa09b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7860
        },
        {
            "id": "78a2d3d5-54e0-43be-9483-5659df37a78a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7862
        },
        {
            "id": "243730b8-fc97-42da-9d54-37c941fe8c55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8218
        },
        {
            "id": "75866691-ee56-4329-a239-b282e721f170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8222
        },
        {
            "id": "8a480b00-b451-448d-96ed-4ce278df9d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "9ebf48fb-22bf-4e49-8f4e-2eee693f5f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "c9bd6ee6-3b7d-4741-8033-9bd67c5dd7ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "15b41916-394d-4b97-9218-667970785b4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 192
        },
        {
            "id": "072c5833-1f21-4858-a1d3-bfd2490d260c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 193
        },
        {
            "id": "8e2fbbb4-b650-4cae-8ac1-4c7ad80e4aeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 194
        },
        {
            "id": "0d2d1bb8-7528-4ef9-9861-50dd191ee2b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 195
        },
        {
            "id": "f5e564d1-b28f-489f-aadc-d3bca4764bd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 196
        },
        {
            "id": "d04b3b70-132c-48ce-828e-0a9141eb006f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 197
        },
        {
            "id": "15b06b06-9cf8-4ccb-b002-0546f32bb7ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 256
        },
        {
            "id": "7cb0e9e3-48e5-40e9-a325-4a9a1a281053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 258
        },
        {
            "id": "b0ce725f-0865-4341-8c3f-fcce42fb7ca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 260
        },
        {
            "id": "c2341082-155b-4afd-905b-9c0d520f820d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 506
        },
        {
            "id": "af4a1ea2-e884-4e98-9de1-5a4a2ddbd3bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7680
        },
        {
            "id": "c6139aed-1f94-424b-9736-76ee53923fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7840
        },
        {
            "id": "9037cef9-3a9e-4da2-a191-261c7beb4ec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7842
        },
        {
            "id": "b42a4805-83cf-47c7-acd8-f66475211063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7844
        },
        {
            "id": "fc5b3ffc-e1a5-4d5d-b987-acf3ab1bffdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7846
        },
        {
            "id": "0546e831-22fb-46d0-99ea-b2be5f1268e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7848
        },
        {
            "id": "a69c6f8e-1499-4964-a803-0a26d7681331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7850
        },
        {
            "id": "f6a47ca6-7b04-4f63-bc0e-74108a7e8d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7852
        },
        {
            "id": "f8e093d3-bf55-4b14-ab94-e0403c370f01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7854
        },
        {
            "id": "0de401d9-c854-4761-94d8-e2b0530db3a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7856
        },
        {
            "id": "01ab09ea-7a3a-4c24-af27-52a584856e00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7858
        },
        {
            "id": "3060ac93-f5c5-4a53-9045-3808694fa6bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7860
        },
        {
            "id": "99a013d5-39da-4c4e-a67e-a46064080379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7862
        },
        {
            "id": "959f8b88-575b-4110-b9bc-1c3b5b5c3052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8218
        },
        {
            "id": "a2ce1bf2-3a2e-4b39-a844-f6e56fce89a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8222
        },
        {
            "id": "8952a35e-3066-4ae5-8dda-2e5cb18fde51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "484ad5da-3e87-4a65-992b-ae67a48ce164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "673d985f-0be8-451d-b841-afef8e66ebfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "f893f60f-6736-464d-be5b-4b722160d192",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "7292e5eb-5335-4b4e-a267-3a7ae10650b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 99
        },
        {
            "id": "d53ea8f8-72cb-4a7a-87bc-3c29c9930bf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "fcff3f5b-ea0d-4c1b-88af-839247b5de8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "9ae9eb3d-da79-4ba1-9566-365796e52c0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "09016623-3c9b-4098-bafe-89881afa5d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "8b1ddcd6-2162-4f71-b297-9ecadf4807c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "25062704-79b4-4ae1-ad48-aa17bb8d35ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 192
        },
        {
            "id": "f4608249-bc9e-4b3d-b356-d35d35f71180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 193
        },
        {
            "id": "b9f58221-57ec-4c5b-9a15-b623c4ff2f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 194
        },
        {
            "id": "82065974-3907-4d11-9a25-0d82592fd4e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 195
        },
        {
            "id": "855c7efe-6501-4046-a49d-8dbb172d2687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 196
        },
        {
            "id": "07d5a781-d9d5-46d5-89db-0f35aa4707d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 197
        },
        {
            "id": "343c93fa-7ac5-4049-afd4-7ae7890a596e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 224
        },
        {
            "id": "bf9428bc-ba26-4b97-80f1-ad390c6576e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 225
        },
        {
            "id": "d703c53c-9643-456f-82d8-46c99cd3e3a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 226
        },
        {
            "id": "9431d129-48d2-41b6-aa2d-92bbd2074cf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 227
        },
        {
            "id": "0bc10aef-7610-43d6-82b8-fee5a9c47a43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 228
        },
        {
            "id": "8d0b03fc-f3e3-4ae5-bb2a-fbf04075c234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 229
        },
        {
            "id": "c846b8ad-82f0-4b63-9897-5bbae9157a38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 230
        },
        {
            "id": "9a1c06e9-99ce-4b33-815a-c5311a86eab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 231
        },
        {
            "id": "788fc003-70fd-4db7-b5b3-ccaee949239d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 232
        },
        {
            "id": "549c5487-bf1f-4094-8434-a456bb9674c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 233
        },
        {
            "id": "d69bb587-c9a8-4d23-8c8f-27e4ba2746b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 234
        },
        {
            "id": "f20ba7d7-e33f-46a2-8b1f-c127d64dccca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 235
        },
        {
            "id": "0f861ec8-bc79-440c-b834-36bfedc752d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 242
        },
        {
            "id": "bb7c9c48-420b-47b0-8c53-2bb826219eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 243
        },
        {
            "id": "c1b9eeab-ce79-4315-be95-1b9a688f668c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 244
        },
        {
            "id": "5ff031f7-653b-418f-a13d-26236e432175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 245
        },
        {
            "id": "e2f3cf71-3bde-4855-80f4-20baccfdef88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 246
        },
        {
            "id": "2afce897-5dac-4014-af3f-7bc8cc3787cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 248
        },
        {
            "id": "f0dc0b3c-58fa-48ac-8c83-dc0ba5076808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 256
        },
        {
            "id": "6761a39a-c4e1-4925-9647-793cced04e00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 257
        },
        {
            "id": "79ec3f3d-6a7f-4037-beee-41c8bff59131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 258
        },
        {
            "id": "dbfd9eb0-a5b5-4282-8b83-d6680a1f0c67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 259
        },
        {
            "id": "4f2ac327-6270-4e4c-9883-fc0b8aab964c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 260
        },
        {
            "id": "e675c6ef-e47e-49b9-8467-6eccf76ee0be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 261
        },
        {
            "id": "932662c8-7465-4c05-a642-863f8ec8459a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 263
        },
        {
            "id": "a8e13f08-e6eb-46f1-8977-14070ff54969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 265
        },
        {
            "id": "f67fd0bc-b0be-4203-9352-8fdf21b53560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 267
        },
        {
            "id": "67afddbb-46c7-44bf-8956-d63c22010608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 269
        },
        {
            "id": "0547fb52-f818-48cb-906e-57e183e4e6f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 271
        },
        {
            "id": "de9bc7b0-95bd-4c16-8378-f05843e5d360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 273
        },
        {
            "id": "309e96dd-6542-41b6-80bc-676edd46784d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 275
        },
        {
            "id": "93a36f74-e471-41be-a3dd-8f5134a1b2ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 277
        },
        {
            "id": "562221ba-1a75-4eef-b88e-6d29982a8558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 279
        },
        {
            "id": "ac946a6f-53fa-48d0-b878-935c900389a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 281
        },
        {
            "id": "5253e2ff-a6b1-4e3b-b2c0-2054390d0de1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 283
        },
        {
            "id": "c9a93503-d4a9-408d-8f79-6d277451ecd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 333
        },
        {
            "id": "850eefcd-20da-4d75-a53f-05f314b04b67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 335
        },
        {
            "id": "f1acccc9-7c57-4e7f-aa2c-fdc6230180cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 337
        },
        {
            "id": "639617d8-5d4c-4385-8eb3-8d2d5be80988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 339
        },
        {
            "id": "76adaa30-48dd-4c26-ada4-a0f4d4a57abe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 347
        },
        {
            "id": "766aac36-e840-43f9-a126-cb6a114d92ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 351
        },
        {
            "id": "600b3575-78d5-4223-bf2e-6eb86267d6a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 417
        },
        {
            "id": "c23fad07-6cb9-4d28-b697-356216699504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 506
        },
        {
            "id": "a3a9a195-57d2-4db8-8554-211488a7f210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 507
        },
        {
            "id": "ab6b3bf2-210e-413b-a76b-a684c46f44ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 509
        },
        {
            "id": "80b81e29-9c4c-44cb-81b0-60cd2728dc78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 511
        },
        {
            "id": "faaeda07-7a8b-431b-a988-cbde1d6188e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 537
        },
        {
            "id": "09f84163-2fc5-40e9-bf5d-4099f614bfe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7680
        },
        {
            "id": "3c114be9-3fb4-4c67-b43c-5e3f31be169d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7681
        },
        {
            "id": "be0cfaec-9c0f-4076-a1f6-d8711bb90601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7840
        },
        {
            "id": "3a00c05c-196f-43a5-b19c-b45413f613b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7841
        },
        {
            "id": "6ecfbe37-a2b9-455d-aa73-12898ff881a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7842
        },
        {
            "id": "2629f38f-fd20-4392-bee4-d9235f22d8cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7843
        },
        {
            "id": "94e44785-533e-41ae-86ba-9d1a342fb17f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7844
        },
        {
            "id": "72ea5246-46cb-4ea3-aaf4-f89ca95b1c07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7845
        },
        {
            "id": "982ce7ae-5b79-47e1-b474-c8e2cf348fc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7846
        },
        {
            "id": "853380b6-b280-47e9-b049-26871b2f2e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7848
        },
        {
            "id": "8702f5e4-2706-43af-b0a7-a9f530b7bdda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7849
        },
        {
            "id": "129f8b3b-6d89-472c-b444-8635dcc3eafc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7850
        },
        {
            "id": "da2c4665-02b7-4736-b819-39ee79a8936c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7851
        },
        {
            "id": "d5aee9a7-6269-487b-b8be-8b043cd4b8d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7852
        },
        {
            "id": "a8d6a00e-4681-460a-80d3-eca940496473",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7853
        },
        {
            "id": "ae56753c-901a-40cb-a7df-e7d4890863b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7854
        },
        {
            "id": "e4c77bc9-fefc-4c66-9622-aaf88df60de7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7855
        },
        {
            "id": "3edbb07a-893d-4633-92c8-daa177bd0fcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7856
        },
        {
            "id": "98985858-12e4-403e-8ab6-b9d620256297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7857
        },
        {
            "id": "52ca90b0-2607-4d5f-8905-1fae446c0038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7858
        },
        {
            "id": "85bee1f0-44af-4aa7-b0a2-ec3ae48e17df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7859
        },
        {
            "id": "31634be6-7f0a-4a55-a2c2-f570122b1c67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7860
        },
        {
            "id": "866c4738-b576-42cd-8eb0-2bbd86981a8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7861
        },
        {
            "id": "ebbff698-1a4f-4c43-8be0-c90f9f52ab24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7862
        },
        {
            "id": "1d52a68b-4c82-4cf9-8303-646d7628d6f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7863
        },
        {
            "id": "54bf772a-811e-4e73-9efb-e34e66486a43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7865
        },
        {
            "id": "a14022db-b8b9-4c8a-854f-46d66d2089af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7867
        },
        {
            "id": "62689a2d-685c-4f45-bee5-767def025744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7869
        },
        {
            "id": "a8563e71-0946-4269-a12d-cb78146f2335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7871
        },
        {
            "id": "61580106-e04e-4dd0-93a1-068ca9786d71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7875
        },
        {
            "id": "8bfccac3-4e26-4ff1-8852-f88128f072f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7877
        },
        {
            "id": "fa66d87f-a8b2-4076-abee-22a56a9bc2c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7879
        },
        {
            "id": "cfee8865-c791-4fe7-bf12-33d020c20d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7885
        },
        {
            "id": "54d911b0-dd45-4692-b662-e36934f1c200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7887
        },
        {
            "id": "d6835ab6-9d0c-4880-a238-4e1b082ff610",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7889
        },
        {
            "id": "b956446c-2867-4ce5-ad16-6c849e9eff7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7893
        },
        {
            "id": "40bcafd0-67e9-4fbc-80cf-8a3e8f953863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7895
        },
        {
            "id": "a9b307a7-cbd9-400d-ab18-0153745f2da7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7897
        },
        {
            "id": "2ffb31ca-2b17-4043-b4f4-c2a224750bbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7899
        },
        {
            "id": "7f525d1b-4569-4514-8143-8c5a3c8016af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7901
        },
        {
            "id": "db37cba1-b2b1-47e0-b351-2e7af302dc64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7903
        },
        {
            "id": "d7cdfb86-d391-4a45-b93f-789202a40a3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7905
        },
        {
            "id": "593ac3ab-1bca-4be4-942f-eadc1287ad16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7907
        },
        {
            "id": "f28533e3-f4d4-4f77-b6f6-4eb4c052a365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8218
        },
        {
            "id": "7ab2662a-cef8-435d-95ba-f2f20c796d58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8222
        },
        {
            "id": "90fa5fea-7622-4fc9-ac44-6f4badc17c7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 91,
            "second": 74
        },
        {
            "id": "fc67ee68-f15e-4d96-8754-ac389507e382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 34
        },
        {
            "id": "560577f9-4f78-47c4-82c1-59300a9d2fd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 39
        },
        {
            "id": "db1d398d-c8d0-4f22-b4ae-e25291c76104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "d4fe4e7a-8a7f-4af1-9335-f664d8eca938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8221
        },
        {
            "id": "3568b432-6501-402e-807e-9ff7bc0709ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 34
        },
        {
            "id": "c57fe492-dffa-4e2c-8f67-4671bd0efb89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 39
        },
        {
            "id": "0d2378db-0342-47e8-8d50-bb9d5e1a26f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "89fb6bd6-a800-4617-ab0b-29b15136c400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8221
        },
        {
            "id": "507301c7-f001-4ce3-871c-a36791ff94fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 34
        },
        {
            "id": "f6a4d4f3-776c-40ee-8ffc-d283b683705a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 39
        },
        {
            "id": "3831baef-0655-4df9-ab6a-d17ff70112dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "c117fad7-5f95-4532-b729-543091c4de7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "dac01f2a-53d5-4f13-8018-0aeca492bafe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 8217
        },
        {
            "id": "0d4ff03b-c38e-4406-91d7-ee4260fb6305",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8218
        },
        {
            "id": "b1332009-78a6-4462-bf18-2eb7f396a0b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 8221
        },
        {
            "id": "03f47f76-e25c-48f2-b928-a3d05e332305",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8222
        },
        {
            "id": "311055e7-1fcd-4eaa-86f7-ae439f974dc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 34
        },
        {
            "id": "7749d8eb-7152-498a-ae83-9d3014da31fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 39
        },
        {
            "id": "593ef2ee-567f-4b3e-bead-24ccd9311a8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "65efebae-e5ca-47b3-8ba6-bac4ce57f7c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "0abc8a3c-223e-44dd-9e60-a881b145b222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 8217
        },
        {
            "id": "5aa73672-0810-499a-a293-0fd7915bb969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8218
        },
        {
            "id": "5454d4b9-1311-433e-a1a4-07d02e23b786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 8221
        },
        {
            "id": "eb095c0d-bb7d-4368-8888-df8af5d09f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8222
        },
        {
            "id": "d5e4f012-bccc-4544-b1c5-c873182d4752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 34
        },
        {
            "id": "9a8d7183-210d-440f-9752-db234e39f48a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 39
        },
        {
            "id": "67e7c0da-3a55-43c1-9ce1-8e450f4531bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "5bdf5fca-b361-41bd-b600-ecf322f0a1a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "b8ca0ad9-0864-4813-a67a-45d5cda89d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 8217
        },
        {
            "id": "1a94bdc3-f0c9-45bb-93a7-9e06ea372142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8218
        },
        {
            "id": "38e4342e-84f0-4596-a45a-26acb0b30913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 8221
        },
        {
            "id": "97319003-1405-472d-9450-ba0084e7e2a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8222
        },
        {
            "id": "cd7d918b-5a3a-4707-bc20-be9a8178d3f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 123,
            "second": 74
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}