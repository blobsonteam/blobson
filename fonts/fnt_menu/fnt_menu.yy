{
    "id": "4bdc3bb1-fa99-42e7-bc97-65d7b0442e09",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_menu",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e3626f65-7be2-494d-bab3-e352e6fe4c3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 49,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b22be8f8-449f-4566-ba01-fe8e83dcb5aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 49,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 47,
                "y": 155
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5072edf8-7211-48cc-b05c-400f00e0cff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 49,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 28,
                "y": 155
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cbf52d48-62b1-4df2-a016-4dd0e8ae0ecb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 2,
                "y": 155
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "c433cabb-2e88-4b56-a537-aad897d67236",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 469,
                "y": 104
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "113edf5e-a245-4428-af93-32cbfb373741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 49,
                "offset": 1,
                "shift": 38,
                "w": 36,
                "x": 431,
                "y": 104
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "0692f691-847b-47dc-97ff-47857f5ea670",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 49,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 399,
                "y": 104
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "48d17ba2-f486-479a-a1e0-5343c8d05acb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 49,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 389,
                "y": 104
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "22ff1445-1d3e-4990-9dc8-ce19600387b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 49,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 376,
                "y": 104
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5f4b922f-8a5c-4b43-b919-89ba627f056b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 49,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 362,
                "y": 104
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a59e9cb9-2ece-40cd-85f5-dc819a2bb870",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 49,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 57,
                "y": 155
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "acee2a58-dac7-4965-bf6c-187a8c01af66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 49,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 337,
                "y": 104
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "cd236d28-0f0d-4730-b351-9b3eefc711e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 49,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 304,
                "y": 104
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ea74589b-78b1-4a45-9f13-93a8ead67b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 49,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 290,
                "y": 104
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "49a8f7d4-35f4-4b01-acb9-5b91cbe60f4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 49,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 282,
                "y": 104
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e8f8b9be-beb9-49e1-986e-4d43e467c4d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 49,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 267,
                "y": 104
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d1e56178-0a16-4ea7-9454-c496c4323310",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 244,
                "y": 104
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0dd393f7-fe34-437e-bcb6-8ada416e3802",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 49,
                "offset": 3,
                "shift": 24,
                "w": 14,
                "x": 228,
                "y": 104
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e325b755-bc99-4131-82d7-c0b385572cc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 205,
                "y": 104
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "0d7f081a-4194-4c85-a419-415fcc720b21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 181,
                "y": 104
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "efd80789-0369-4266-a809-0b5cc2ef6762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 156,
                "y": 104
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "02b660fb-b7af-4b92-bf92-62cad775fcf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 313,
                "y": 104
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "237e5f47-2854-44ce-a5df-d1fa28866c03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 75,
                "y": 155
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "03902f29-9c3d-4d03-bac8-763bf444ca88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 99,
                "y": 155
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "e8210007-359c-4259-b3f4-c6c165cd3bac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 122,
                "y": 155
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a5beb1d9-4271-4c5d-ad7e-8d381fbcf1a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 161,
                "y": 206
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "33593486-05ba-40c1-9e67-af835ecc09ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 49,
                "offset": 4,
                "shift": 14,
                "w": 7,
                "x": 152,
                "y": 206
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ef643bdd-14df-4994-8247-750ad6400ac7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 49,
                "offset": 3,
                "shift": 14,
                "w": 7,
                "x": 143,
                "y": 206
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9443d4cc-0bd7-499c-99d2-5721dd8960eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 49,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 119,
                "y": 206
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0d18f199-9d5f-4781-9ca6-e3061dba4d2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 49,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 94,
                "y": 206
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "dab69c11-6b41-4004-bb3b-ce058029f7c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 49,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 70,
                "y": 206
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1650e0fa-dd48-4b14-b48b-ea520384c51a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 49,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 45,
                "y": 206
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "cd1757a7-15fd-490c-81cf-da20aeae2b2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 49,
                "offset": 1,
                "shift": 42,
                "w": 41,
                "x": 2,
                "y": 206
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d74720ca-4915-4089-830d-3575d645a2dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 49,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 457,
                "y": 155
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "24ca8e9e-3a81-4fa8-997b-2c47076fb4a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 49,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 429,
                "y": 155
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "744b0c2c-4cb7-499f-bf26-a50b6f92bb7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 49,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 400,
                "y": 155
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "17b2b024-decd-4adf-aaab-507fe58a0927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 49,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 372,
                "y": 155
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "13b07e39-6b14-4f25-a9c7-eab8fddd16a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 49,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 346,
                "y": 155
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "af1610fb-d7e5-4edc-8b2c-e1212288fc43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 49,
                "offset": 3,
                "shift": 26,
                "w": 22,
                "x": 322,
                "y": 155
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "db3ca203-ca10-49ee-af23-d33296ee04ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 49,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 291,
                "y": 155
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "287b776a-c0ce-4188-a2de-0842cc3d1261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 49,
                "offset": 3,
                "shift": 31,
                "w": 25,
                "x": 264,
                "y": 155
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3b7ff399-a895-492d-89f5-fe51de4af3b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 49,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 254,
                "y": 155
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "663c9c5a-746c-4888-9e33-a8b0039f6bee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 21,
                "x": 231,
                "y": 155
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "422ccf66-16a4-4476-827d-fc4d62861db5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 49,
                "offset": 3,
                "shift": 31,
                "w": 28,
                "x": 201,
                "y": 155
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "16b0918a-4383-4259-b405-d6c306992c25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 49,
                "offset": 3,
                "shift": 26,
                "w": 22,
                "x": 177,
                "y": 155
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "99daa103-2fc4-4624-b1f2-c1429ca5bd78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 49,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 145,
                "y": 155
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4b6ac8d6-86b2-4d6d-8987-567c12c08fa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 49,
                "offset": 3,
                "shift": 31,
                "w": 25,
                "x": 129,
                "y": 104
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f5cb84e5-016d-4ac4-822e-b150bdf2b33d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 49,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 96,
                "y": 104
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b91adff2-a9f2-4451-91fb-d9a85ccbb425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 49,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 70,
                "y": 104
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5102743a-7ea1-48ac-b985-a8668f3a61ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 49,
                "offset": 1,
                "shift": 33,
                "w": 32,
                "x": 45,
                "y": 53
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ba6cb3aa-5782-4f7b-ac41-b5ea7d0eb152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 49,
                "offset": 3,
                "shift": 31,
                "w": 28,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "eba3384b-1e20-40c5-ac71-49926a6ba389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 49,
                "offset": 1,
                "shift": 29,
                "w": 26,
                "x": 470,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "66e9bdf9-dfc2-47a2-aa07-d1e4768fa0fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 49,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f0a422be-6bf0-46d8-960c-e4ab8f62ed64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 49,
                "offset": 3,
                "shift": 31,
                "w": 25,
                "x": 415,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5089b9ea-fa9e-4156-a51b-a0417477a955",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 49,
                "offset": -1,
                "shift": 29,
                "w": 30,
                "x": 383,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d43b059e-a919-4e2b-a824-a17abb628b1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 49,
                "offset": 0,
                "shift": 41,
                "w": 41,
                "x": 340,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b5c15733-a484-4cb3-9a7b-d34766d2a72b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 49,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 309,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "36ae1b7c-36f0-4e8a-bed7-129780308cbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 49,
                "offset": -1,
                "shift": 29,
                "w": 30,
                "x": 277,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b12a7e25-67ca-400b-ae1c-a92f67bf9620",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 49,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 249,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "e1b8e952-803e-4b6e-828f-81ec21634b8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 49,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 32,
                "y": 53
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1ad4cea3-fccc-43a4-ba6f-95690a625a32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 49,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c3d5138b-4f5a-42b0-bea7-13621dfb9d1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 49,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "047eb590-49ea-4b7d-aa90-42999a6dbaa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 49,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "28116e93-e67c-4c05-bfe8-8a5b8aae10ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 49,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f05016ec-0ffa-4fef-a2a3-b74bf52c3013",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 49,
                "offset": 0,
                "shift": 14,
                "w": 11,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "75fafdfe-825d-4922-baaa-5e5c24e99a46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f990034c-c03c-49d0-8ddd-d02a2a0474b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 49,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "81ad9a03-707e-4974-9314-5045aff39d7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "97f430c0-3115-4fd6-9590-d42217febe7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8d68b188-3c5d-4f63-8d34-8302eeed0108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b80d3186-2f84-4ac1-be03-637e8213a9d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 49,
                "offset": 0,
                "shift": 14,
                "w": 16,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "64d50bfe-ea1d-48b2-90af-742fa801ff29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 79,
                "y": 53
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "42b8eae6-908e-4e6e-bece-1be563ec2ab9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 49,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 316,
                "y": 53
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a42be7da-c6c5-4e90-bdf6-453fc2216577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 49,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 104,
                "y": 53
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a9f87fd1-75c9-4aa6-bfe8-dfe16ea4612a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 49,
                "offset": -2,
                "shift": 12,
                "w": 11,
                "x": 34,
                "y": 104
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "722710ed-6649-4e33-9112-455ab0350e60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 49,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 10,
                "y": 104
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "84745fe4-3cc4-4e9d-ad17-09aaa7884b79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 49,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c1c6f5a9-e9da-478f-827f-88880857d65a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 49,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 472,
                "y": 53
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "f2c5efea-b677-43a4-9c51-e362035e5224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 49,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 449,
                "y": 53
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "bd51921e-5e78-4b39-878b-c7e2bac70680",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 423,
                "y": 53
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e4ea89f0-ca1a-4785-9065-69415d01b465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 49,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 398,
                "y": 53
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "640d5a82-ef95-43eb-bdb3-abe332c1aad8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 373,
                "y": 53
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "5345acc8-1057-4eef-952e-f4311663e933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 49,
                "offset": 2,
                "shift": 17,
                "w": 16,
                "x": 355,
                "y": 53
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "839623f3-c903-4a35-a1c7-9140b645ecf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 47,
                "y": 104
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "4cbd7a03-ba3d-4b44-b024-e8b4ddbdf7b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 49,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 339,
                "y": 53
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "bdba1fd7-3de4-43d0-a184-bd6bf0ef5256",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 49,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 292,
                "y": 53
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e699e481-7a77-4fed-a04d-decb14c9f335",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 266,
                "y": 53
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "672aaf49-4669-4fbc-a8a5-edae95a24053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 49,
                "offset": 0,
                "shift": 33,
                "w": 34,
                "x": 230,
                "y": 53
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d56df66f-10ab-49ab-8c90-a42a768795ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 204,
                "y": 53
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8f104c44-fe29-43c5-8801-ef2e452103e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 178,
                "y": 53
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "fb3711dc-3ec4-41dd-87ac-d93ad276743b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 49,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 155,
                "y": 53
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b59037ef-e9b4-4b3a-8428-3a4154ad18fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 49,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 138,
                "y": 53
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3a2232bd-ccec-4e71-a1b7-ca183eff7ef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 49,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 130,
                "y": 53
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f8ed0d78-d4f5-44a5-bd12-d6934190ec85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 49,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 112,
                "y": 53
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "e807b4d0-ff94-4b85-bcf8-2e9c422f9c06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 49,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 184,
                "y": 206
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4a03712c-0baf-4c68-9049-58db6183737d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 49,
                "offset": 8,
                "shift": 42,
                "w": 26,
                "x": 209,
                "y": 206
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "1cf7a5c1-7245-46d0-ade3-95153267d07c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 65
        },
        {
            "id": "cd5f3946-972d-4a7d-b40d-217cc86e423e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "12768b6c-d538-48e5-a6c2-71d6307899e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 902
        },
        {
            "id": "4c0fce70-58a8-454d-9c24-25933d2d9e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 913
        },
        {
            "id": "aaa37e34-f845-4b1c-af39-b136f0b18fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 916
        },
        {
            "id": "542927e8-73de-48c9-be83-027fd91030a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 923
        },
        {
            "id": "85331be3-5c83-490f-bbfa-381bf418784c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "32820183-9401-412b-8fcc-dc6a8a15ecc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "ef04e29a-128b-45f0-a72d-dc7570182653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 49
        },
        {
            "id": "61978c0e-6117-410f-8d3a-7bc2f8ba1e51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 32
        },
        {
            "id": "3867eca5-8856-496b-94e0-be5b598ca14c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "106b6693-11a3-4cde-8980-b651680c8cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 86
        },
        {
            "id": "c0828b9c-6796-4f0b-bac4-1d23f0e8d0de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "4f08ed53-e614-4010-8b6d-4b1c22bb439f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "3dbaf8fa-b0c1-4765-b21f-1d4de52f3b25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 118
        },
        {
            "id": "2f0a3859-8d53-4bae-b0b4-605d17f676e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "dd3fc211-0051-4099-adae-fd8ffd5730a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 121
        },
        {
            "id": "8bf1e278-08f5-45db-82d5-76d76f45d4e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 160
        },
        {
            "id": "848856e0-8b23-4872-a853-b1c65dcf44c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "93b539aa-87c4-430a-9eca-c193fb8b77ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 44
        },
        {
            "id": "1c0db7ec-d323-44b7-a9c3-66037d3a3a68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 46
        },
        {
            "id": "02ce48b9-89a2-4c6f-ae23-c341f442d797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "02c7ce95-811e-4bef-b46c-d618c5878c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "cc5adf59-06ec-4fa7-800e-aea340890d51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "a4bf4b76-04d4-434f-9a6c-8006aec1147c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 86
        },
        {
            "id": "d4bd437e-2ae5-40d3-8302-04a2b14ee025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "7958d5f4-e584-41ab-919e-ca22d653fe69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 89
        },
        {
            "id": "f2e7d056-4b7a-4edb-a370-f6664c4fe309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "3453378d-b3a2-41ae-a196-d2f7edc41727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "adf8bcfa-e527-454c-8f91-72a95f122843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "d41a99bc-15df-4342-8709-4acf1030b6c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "309f593f-856d-4442-bfdd-4294b85ee2c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 44
        },
        {
            "id": "c92a4fed-7fda-4d2e-afe7-d2a21e12b347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "7cd40454-189e-4e78-bd85-e030352053be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 65
        },
        {
            "id": "3bc89cdf-d716-46c4-b7b0-c7b2c1633424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "104d1b51-8c9c-4d31-9aa7-eb164c135d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "775b20f4-1c9d-41a2-8d24-556e1d714a9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "9860ab40-358d-4c5e-9ca8-8a49d4dd8fc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "1e6260fb-69a6-4c4d-a6c3-b60580b41fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 44
        },
        {
            "id": "0217af21-8abe-4faa-87c3-f9f1ef151ec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "d9d067db-130b-48fa-bbdc-4d98686b6b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 46
        },
        {
            "id": "471aa80a-75e8-4ec3-844f-bedcb05336f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 58
        },
        {
            "id": "335ecfb7-0532-4e7e-a187-d5e46ecf9d58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 59
        },
        {
            "id": "9de52577-eac3-4cdf-9e6d-b97416964370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "d19035e8-3334-4ea2-8dfc-49640ff37747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "9d84c9ca-d44b-4a62-8f30-65304e00810e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "2129e055-abda-46b5-a025-35fe6512ef08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "62778848-77b4-4871-809b-af00a44fd795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "36c372e6-e590-40a4-b05b-c27ef0fc0c7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "dddb873c-f871-4ef9-88f9-16aefbeca7d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "3cfbcdcd-fe80-4c52-ade0-0af37bf2d5a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "561e5edb-30da-4277-a3f9-31bd369ea93d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "4f17e1cf-d87b-4c0d-b45b-4c2b391b37c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 117
        },
        {
            "id": "288e180c-5994-4dd4-a2db-2f1afca53ce7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 119
        },
        {
            "id": "252ce2cf-386b-4279-8a42-5077a4cf7b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 121
        },
        {
            "id": "453cb6c1-1a47-4abc-83d4-09f334267118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "3719a432-b8d1-4bb5-8a3c-88e1ec7c6930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 894
        },
        {
            "id": "96419d82-94fd-4e02-b894-6069fba705a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 44
        },
        {
            "id": "a1252f09-ae1b-431c-8bf3-82129713425d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "7ba0b91b-6d01-47e3-8d78-b957e2f2c7f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 46
        },
        {
            "id": "3561f13a-65c2-48e8-a7de-caa72821e910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "ce1498b4-13f5-474c-b5d0-4c65bd42241b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "471666d4-ca99-496e-8ab8-61fcbaca7056",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "4808b6ae-d7a9-4d73-a4a5-d4677dd8e80f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "ceec3781-492c-4b3a-a479-c505a9f632b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "3e4f0cfb-97d4-4e5f-aa8f-71a20bfc0d7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "f2eeeeee-7b9d-4f31-9f04-1ed4526f6545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 111
        },
        {
            "id": "0e8f79f1-0cd1-44a9-9abe-f91d999bd4c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "d522a3bd-cb05-4681-97e6-11db91a7bc1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "1bbad4b9-a1e8-4b1d-a658-f3c1c98c3bd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "4ec1d04b-ad10-4fbf-ab39-c616cd83675d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 173
        },
        {
            "id": "831759a3-f41c-4149-b167-bc1572798522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 894
        },
        {
            "id": "77c05b4b-41e2-4e92-a2d0-1d6826bd4719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "09d9fb92-2b9b-4a52-b93b-3b9479776aa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "49b67401-6654-42d0-9a2d-1bb6b0396484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "f56ab745-a80b-4ee0-9ddd-1fb89d39a5c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "6fed0b53-47dc-4d6c-b69d-daa340c6baa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "f5faf8f3-0a21-41e9-b9d2-e7277183e8ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "2f47c671-e7a4-4971-af5b-e055db492fd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "2d39797a-c47d-4c2b-9044-91d0ee1c0db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "2d1248d3-d9fb-4eb7-b91e-2d48c652fa1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "35555aa1-b783-4832-95a5-e6205fadf299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "d3ab23a8-1aef-402e-a4df-6c7604456b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "fd018bbc-0fdf-43ff-a451-ef0a7a19b926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "4604847a-ce72-4268-a70c-42e3bf0bbd62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "652c6439-a960-4508-8ebf-86460c5c0785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "05464279-2ad0-4f10-aa6b-49aa169095d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "f6f6246a-d15d-4462-88bc-71c4ba5d3c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 44
        },
        {
            "id": "27d4b44c-067a-4406-8e75-30dd4e050f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "ca6257e2-8449-4bfe-ad3f-aa801f4afb80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 46
        },
        {
            "id": "7f090e75-8daf-42e5-86fe-4aa74324e931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 58
        },
        {
            "id": "9d366e9f-e51f-485b-a3de-8ca3ed461151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 59
        },
        {
            "id": "a1a74e52-ba61-4bd7-a9c9-74882d52193d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "5d662070-7267-4fb2-ac5d-24a822dd8f34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "dd59f0ea-3f4d-4df4-894d-f208ce4f2f3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "5b1b7939-2cb7-405a-89e7-1f4b0dbe0778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "42db04d2-a04e-4882-98e1-5d32622df569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "9d84daf2-e5ae-43b2-97d1-947987469c20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "66fd29ab-f922-4dc5-934a-ce2620c0e31d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "480942d9-163d-4056-8979-5e06fd61c12b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "cb5fe782-a0cf-46f5-b667-4d7eac422135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "9fe819be-7bc6-4e3f-b0df-18a878e64833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "a3d63c99-4bdf-481b-8502-81705fcf96cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "438a58b0-4816-4f83-9a55-e7aefb5b230b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 894
        },
        {
            "id": "6494ea4b-c56b-47c4-a077-121ef58f5af4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "c010494b-d1fb-4240-a6ac-6f7559e43ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "3ce6027f-8479-4080-99f4-6c893a04db60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "9a32b0b7-addf-4fc8-8c60-2c41101982f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "f4edcc27-78ec-4ca6-9609-5e1972b15d74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 44
        },
        {
            "id": "453a9ceb-da87-4dbc-9dfb-02fb2c020ad8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 46
        },
        {
            "id": "e0421af7-8376-4554-b1b5-ed26d1058467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "1c6498ef-14d9-4b8b-b0d0-7453f2c269ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "06f9c80d-4863-4f8f-ad05-fe6d1c079202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 44
        },
        {
            "id": "732f8e86-dee6-4de3-95ad-de508724be11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 32,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}