{
    "id": "bed51a19-fb9a-40cd-bd15-cf272fc0abde",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_consolas",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6154ba55-9cb7-457c-9a91-6df1a75e4096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8d0963f7-0928-439d-ad0a-d3c72a3b9508",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 16,
                "offset": 3,
                "shift": 7,
                "w": 1,
                "x": 17,
                "y": 74
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e9c3dbcc-887d-4fad-8613-cab990882ff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 16,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 11,
                "y": 74
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "9e28507f-e38d-461d-807d-c00b8149322a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "daec0f28-8a12-4b72-a624-46ff28d03a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 118,
                "y": 56
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ddaac680-616c-4b80-a0a3-2440809b59f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 109,
                "y": 56
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4d50be9a-d7c1-475c-a15b-bdb8dd41d2c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 100,
                "y": 56
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "82b42070-72a8-4e40-93e3-b24876a1f61d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 16,
                "offset": 3,
                "shift": 7,
                "w": 1,
                "x": 97,
                "y": 56
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d3b127de-9576-4818-8673-0f5f9769f5be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 16,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 91,
                "y": 56
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "67e4d3bd-d0d4-47e0-9ddf-18928cf2246d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 16,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 86,
                "y": 56
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9683dfde-4913-4728-b5cd-506faf9dde6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 20,
                "y": 74
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b9d5b6bb-e88e-4e90-8125-1c9454d02f55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 78,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "cadbc18d-bc16-4d35-acc2-625de780c1e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 16,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 66,
                "y": 56
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4fa0b609-8342-4cb4-949f-3812473d52d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 16,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 61,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "fb8b5d23-ef00-477e-b99e-8bcfe9e9547c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 16,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 56,
                "y": 56
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "3a2a025a-9822-4f1c-ac2e-61c11e77569c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 49,
                "y": 56
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "673dfde8-9892-4a17-94f5-eed4ffa88c31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 41,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "276f5eb1-d184-4b61-a0df-fa6a81646dc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 34,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "5fe49487-f06f-489f-ad6f-c910eadb1494",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 27,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a82be968-bc77-40d6-bea5-3cfbe1cb7f7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 20,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9b2deccd-d7d8-4093-b8aa-13c7d2ab4978",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 56
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1cd3e69a-a4a5-41a1-b865-707e2b5e45ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 71,
                "y": 56
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d0b60ad2-b8bf-4094-82b8-90219239f1c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 34,
                "y": 74
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1d10e841-5651-4c3f-a2b6-7ba73dfcbbe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 115,
                "y": 74
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "fa6631d1-b043-4826-8c24-b7db104d1524",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 42,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e6d9c800-b886-4a80-9616-a5e12d6bda65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 63,
                "y": 92
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "5a053592-fe18-47a9-87f4-c9c2d4328bb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 16,
                "offset": 3,
                "shift": 7,
                "w": 2,
                "x": 59,
                "y": 92
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "93d8e7b0-c567-457c-a26f-d237440750f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 16,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 54,
                "y": 92
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3a0f3690-fbbd-4198-ba08-e752674df756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 47,
                "y": 92
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "20c831d0-c4f5-4f8e-a101-dcff14d0595f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 40,
                "y": 92
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a1f97aad-bf9d-4e8c-a898-7cb00a35176e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 33,
                "y": 92
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1a5c59fb-b025-4f6d-bb2b-559387e35a90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 16,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 27,
                "y": 92
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8e40247c-862a-41ea-84d9-33689b644acd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 18,
                "y": 92
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "11e97ce5-c12e-43ea-bd63-4ce75b6d46df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 9,
                "y": 92
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "020fcca5-dc33-4f3d-972d-bfd85f7ae9bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 70,
                "y": 92
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "aabd63e6-ec42-4a8c-b450-3b944a985158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "df3998ab-336b-453e-8c8b-2c4209b77016",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 107,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "581fb549-843b-4725-9c60-1412e762ac52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 100,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c1c37e7e-5b5e-45ec-801e-c3d08a7e122f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 93,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c2babb72-074a-4a25-97f1-a06852cfb0c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 85,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "cc2ba5ba-357b-4520-80c7-0d33faa917ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 78,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "af8bfed9-6303-4d86-aaef-0c15c80a16e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 71,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d0933411-dcd7-4f02-a390-3f09a94d67ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 64,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "51c1474b-1159-428b-a96c-ab003d55126e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 56,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "32dd1687-bdd6-424d-931e-467499d9a91b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 49,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "eb4b4266-11ce-49ee-b648-cfceda056190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7842f6c5-5eae-4639-9dff-8486523f89fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 27,
                "y": 74
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4b8c2b34-81ef-4ad9-a71d-ee80340ce449",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 113,
                "y": 38
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "fa42423b-a3f1-4de1-b160-4bd0328e6672",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 48,
                "y": 20
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "190e6db7-de5a-4f6d-aed8-6d75ea1c5b5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 32,
                "y": 20
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8c2cd81c-8db1-4c0d-9b31-f757fc4aaa0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 24,
                "y": 20
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c6606c82-9444-4f27-af6b-661049c23d37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 17,
                "y": 20
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c816f24a-a2d2-4550-9405-ce690bcb7f78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 9,
                "y": 20
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f27f5ebb-aee9-4670-a226-0feea9429a41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b79234bf-b561-4faa-a84c-2d8cd9551d6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9bd9962e-3081-4e59-8f23-af7d42b57e33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "73f9530b-80c3-4a02-80c7-8695ac759213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b600436e-e0cd-4cc2-9983-89de7dceed36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ee3ecc66-29e4-4a2d-932e-315679c7b066",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 41,
                "y": 20
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3dbbe044-b9b9-4032-b2c3-790a7de1a64a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 16,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ec3304f1-d167-42c7-9dae-deca3ad2090e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "781ce839-c874-44d8-8ae4-07faaaa5f951",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 16,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c24cf99c-be10-4581-8b92-ef5867a333bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5a8e5df0-3ee8-44e6-b906-b52629927f05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6d8b6bbb-007c-4933-8c50-77cff0fb8654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 4,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b7d130f6-de19-47db-9008-cddcacc9f9d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "cad93c21-2692-48e2-a5ee-b5dfc4212ce9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "84836618-852f-4719-81f5-bc879cab27a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "06c3a03a-a1f3-449f-9dfb-e836504a1c0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ac94208b-ab2d-41ee-aca2-5d4d978ba780",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "fdb9b2d2-2e55-4901-890f-4a577eefd114",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 55,
                "y": 20
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1e24c729-82f9-4f38-b9d4-dd69a858ce39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 17,
                "y": 38
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "364fccd7-53e2-4f2e-a12f-ba736a10a883",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 64,
                "y": 20
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "11313ee2-b19d-450c-92c4-a8126830a6c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 91,
                "y": 38
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a68a3259-f12e-4d04-aef5-43beb474f0b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 84,
                "y": 38
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "fa7c9f20-655e-4157-b0af-7e2344ff4765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 76,
                "y": 38
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "030d21ee-d797-4318-9151-358a69b49a59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 69,
                "y": 38
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9690ab79-7ec1-4038-907c-4ebddf6e71d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 61,
                "y": 38
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a8a22989-0ab5-4419-bfb8-2e975ddcbd13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 54,
                "y": 38
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3653997f-78a5-42af-b666-6006a1cda19a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 46,
                "y": 38
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "32f2d330-a2dc-4196-a4c0-7f79e2d2a78b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 39,
                "y": 38
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "35374884-d170-4d0f-b11b-f4f6ca56722e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 32,
                "y": 38
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4a3bc5b6-c67b-4d7f-9836-a1a8ba61ac4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 98,
                "y": 38
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2f55fb19-942f-4538-85a7-1ac3993ce5d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 25,
                "y": 38
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5cea1f62-fa8e-4ca4-858e-13d0f835b5ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 9,
                "y": 38
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6be29e8b-e798-4743-a562-d4eee8bc5fdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "32e9e404-bdbc-45e0-a0f8-0b7f2578d2f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 115,
                "y": 20
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "35540a5c-9f50-4f09-bff2-2272f9415387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 106,
                "y": 20
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "863f33ea-a5cd-4fbb-a233-ea88486cfec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 97,
                "y": 20
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c8035bc3-bd0e-4113-be47-17069a6cd672",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 88,
                "y": 20
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4e2b39f6-1de4-44a8-aa34-4acd31e4a405",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 81,
                "y": 20
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ebe6c8e6-4e68-4fa0-a2d9-51f9a10bd372",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 74,
                "y": 20
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0f142dc0-90d1-46a6-b1fa-4e6d62fa2abc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 16,
                "offset": 3,
                "shift": 7,
                "w": 1,
                "x": 71,
                "y": 20
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "fc36609e-98eb-4895-9aeb-f861708f48ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 106,
                "y": 38
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "67828c4c-2bd1-4568-a9b2-0584cec98afe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 77,
                "y": 92
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 10,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}