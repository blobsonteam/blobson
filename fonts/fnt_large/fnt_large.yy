{
    "id": "d3a9acb7-03e7-4e40-86c5-c5bbb8f38ccc",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_large",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Courier New",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "733a940d-4408-426b-a09a-e02cb197fb7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b345d7de-5ec2-4572-a7e9-5171e02e3183",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 24,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 57,
                "y": 80
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b5db22f6-6861-431e-804a-1d218802f9ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 24,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 48,
                "y": 80
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "94c84856-a445-4623-b186-d0d1f8d0a83c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 36,
                "y": 80
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "dacdf4c5-9a17-4b2a-bf14-a23887f35327",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 25,
                "y": 80
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "bf812767-6ab5-46df-8cdb-e05fb40aed67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 14,
                "y": 80
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ddaf091a-c760-4745-8fd0-71ec8fef73e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "76e25561-8fd6-4229-974e-feac63e4c9c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 24,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 246,
                "y": 54
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "704516bf-2de3-4ca8-a33d-43db2fee50fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 24,
                "offset": 6,
                "shift": 13,
                "w": 4,
                "x": 240,
                "y": 54
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7947b5e0-2b4b-49ee-824f-a0f511015b3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 5,
                "x": 233,
                "y": 54
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "7c1e55a2-2b38-456c-8fef-958b40e59d6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 62,
                "y": 80
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "3d01f98c-c8ca-4e14-b6b1-5d7c42180bf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 220,
                "y": 54
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e3143a77-663b-4260-af9c-ed4e3dc57d2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 24,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 201,
                "y": 54
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "01c37541-2a84-47a2-b9ca-01faf83e58ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 190,
                "y": 54
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "feec2fc4-8351-4641-bd5a-2ae19d80b636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 24,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 185,
                "y": 54
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "25407296-d35a-4931-be0c-12301e196507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 174,
                "y": 54
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d2da87da-b2c7-4616-9804-76aa3ef49cb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 163,
                "y": 54
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "59291ba5-bffa-418f-93fe-595ef0773538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 152,
                "y": 54
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c20adb8b-044c-4544-9b42-706e969cfacf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 140,
                "y": 54
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "22ef53b8-0d83-4533-90b3-2fffa4cee950",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 128,
                "y": 54
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ea4688f1-1ada-43b4-adaf-7d2572beaaf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 117,
                "y": 54
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9b8e25f4-993c-46f6-aa19-ab577922fa54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 208,
                "y": 54
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0159e355-104a-4187-be11-4deae401989e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 73,
                "y": 80
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "59c1be37-9c5a-474b-98c3-f7b1e9787fae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 84,
                "y": 80
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a21868ad-7b70-45be-83f7-ff9369a11ec6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 95,
                "y": 80
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3278d749-46d6-4d60-8a0c-299acd0a2ca7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 106,
                "y": 106
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2c9cd00f-caaf-427c-a131-a45537bd9bd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 24,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 101,
                "y": 106
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f4eee5a4-3409-4e65-84f1-aa86a432990c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 24,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 94,
                "y": 106
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e446c6c8-ee8f-484e-a253-0d872a0d8950",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 80,
                "y": 106
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5d6e9bfd-99ec-4346-86e5-a18b297d9560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 66,
                "y": 106
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5e4bcfac-ac1f-443a-ba26-82b992f6376e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 52,
                "y": 106
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "00b23f65-cd37-4d66-80ae-5933c7bd2755",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 41,
                "y": 106
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "bbb5a935-5731-49a5-bb10-9d3beaf94eb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 31,
                "y": 106
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c18e3b02-0ec7-41d0-b46e-1070864d6059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 16,
                "y": 106
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1c33b9e3-5f8b-416b-aed4-718fa9594a06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b94502e5-fd2f-4ff6-87d4-7d196d480b96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 242,
                "y": 80
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "14ce4cc4-428b-4e8a-bcb7-bb9e17de6d91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 228,
                "y": 80
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "ee9d2173-2fc5-47b5-b15f-6f8a08f1541a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 214,
                "y": 80
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f0b7d98f-7e44-42d2-af09-020e7cafc04a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 201,
                "y": 80
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f6afe892-b148-4a27-b606-4b81cfd90043",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 188,
                "y": 80
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "af8d69ac-28ce-4a2c-a4cd-3b92385776d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 174,
                "y": 80
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0361ff23-746f-49f2-bd43-e0afdf4538d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 163,
                "y": 80
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "08e97ec3-456d-442d-9c35-e125d72c44ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 149,
                "y": 80
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1e403d2a-0812-4c03-9fe2-eb53c5f6dfb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 134,
                "y": 80
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "76fe663e-b0a5-4f49-b62a-6a54d51dfef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 121,
                "y": 80
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b822d876-3efc-44d7-86c9-234bd7a8ad63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 106,
                "y": 80
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "465e75f1-c985-4cf7-9426-627848a44fe9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 103,
                "y": 54
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "433ff175-c351-43d5-ae23-8ee7b263cd1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 89,
                "y": 54
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6db292a3-b338-4d1b-a80d-ad5d77f3ee7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 76,
                "y": 54
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "073d08a0-1ead-4342-ae56-8bf761089f54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 24,
                "y": 28
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ca8ad927-7721-496e-a264-c1acf74c477b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "63a40dc3-c16f-42bb-98e6-13a11cc7de6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0732f3cf-2cd8-4624-9d6e-6b9dc8ef0ff7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "52ca1517-8e96-4bab-8805-e4934adaa34c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "050b4c3d-67e8-4118-a34b-b3270e4b3600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "868ceb7e-33b6-4851-b3f2-b6c5f675641f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "bd219ba1-a995-4400-a077-387ec231207f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d6cde5bb-9ef1-4aba-acdf-a6b62d42a11b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "690de22d-d34c-48eb-a64a-9b15104e4221",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "32a55630-03ee-459c-a59f-2e72d306c85c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 24,
                "offset": 5,
                "shift": 13,
                "w": 5,
                "x": 17,
                "y": 28
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "0aeb623f-758b-484b-a48b-0b59c6587968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "78553ad0-2fb1-411c-9c45-57f6d07f6c37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 24,
                "offset": 3,
                "shift": 13,
                "w": 4,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "26c7c3ad-65e4-442d-b694-a3cd8e3f9320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "12292dd4-dc88-4845-85ab-91b0b789f187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 24,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6344adc1-5f7d-4681-ae25-00cce6871ffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 24,
                "offset": 4,
                "shift": 13,
                "w": 4,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "010a782e-28c6-4e15-a50b-63d36b200cbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "083bf613-406f-4004-8637-647258a3ab43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "f9a6af1d-e2de-45b0-9f6a-0d8e7357a1e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "da9473f0-c317-4185-bf95-9e5ddec02f8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0118b8de-5ab5-4eda-8dbe-5edfdb8f0e57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e9d3ad03-8b14-4b46-a3aa-77e5cd7bc4b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1b0ef2ef-dfeb-4f35-a2ca-dfe393ec561d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 38,
                "y": 28
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b2599096-4f2b-49cf-acc7-3cd9c6ef1c55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 165,
                "y": 28
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "65cac7a5-9533-4cc5-96c4-dc0a0c0c826a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 51,
                "y": 28
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "0e198d19-11b7-4fa0-94bf-af6613a71edc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 55,
                "y": 54
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f0ecf09f-3d4d-4305-a4ad-80ad04d86047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 42,
                "y": 54
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d4caf3e3-cc4f-4dd5-ae21-2a426a82f184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 30,
                "y": 54
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4c4b2191-5af3-4b9a-93da-0c488702f5ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 15,
                "y": 54
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "9c3b4e1a-f4d8-4756-b1e7-8c98cf9faad3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "80f6b7c2-b7f6-4e8d-b593-011459e9c9c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 232,
                "y": 28
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "de3d6c7f-5a3f-43d6-9eaa-ee01f0016b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 218,
                "y": 28
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "7e1793d6-76c3-4531-817e-872a71eeeb34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 204,
                "y": 28
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b43957ba-55d2-431a-b8c0-67a2c60fa3c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 191,
                "y": 28
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a02ae2ca-ac8b-49cc-bcf2-a1053e910d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 65,
                "y": 54
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c8557815-7533-4243-b45b-18d862bbe8f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 179,
                "y": 28
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9db1a52a-633d-4146-a84e-f2b1bcc130a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 151,
                "y": 28
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "416355a6-471d-4abf-bacc-924a26c237b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 136,
                "y": 28
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ce37b851-f4d1-42bf-be5a-79884dd95f2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 121,
                "y": 28
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8193f90f-7879-4f81-9926-7dd62f716cad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 107,
                "y": 28
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7c640e78-5d74-47f9-b883-550b58ddf523",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 93,
                "y": 28
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "270b3f7b-0e44-41fe-a81d-dccb85fcaba4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 24,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 82,
                "y": 28
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b42b9576-27c7-4abb-a687-54e6d5967dba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 24,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 75,
                "y": 28
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1af4ea9b-e5b3-4be7-9521-c6f07d0ed9bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 5,
                "shift": 13,
                "w": 2,
                "x": 71,
                "y": 28
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2852369e-cfc3-4d25-9320-f7b1d3f1f5c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 24,
                "offset": 3,
                "shift": 13,
                "w": 6,
                "x": 63,
                "y": 28
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c9e32826-4d6f-4b0a-9f37-852a19501a40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 117,
                "y": 106
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "b16d72e6-d83a-403a-ba3b-62972a39b0b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 24,
                "offset": 4,
                "shift": 20,
                "w": 12,
                "x": 129,
                "y": 106
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}