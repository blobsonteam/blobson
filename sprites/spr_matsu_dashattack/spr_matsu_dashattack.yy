{
    "id": "5ababbda-ddd7-46c4-b23f-a58392a99c99",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_dashattack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 0,
    "bbox_right": 107,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0f775d6-16fb-47a7-83d4-a2966493d6e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ababbda-ddd7-46c4-b23f-a58392a99c99",
            "compositeImage": {
                "id": "3b089328-b128-4570-9371-ec305b54a582",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0f775d6-16fb-47a7-83d4-a2966493d6e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d62e5a94-d349-4e85-a6d2-a51464c7c304",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0f775d6-16fb-47a7-83d4-a2966493d6e4",
                    "LayerId": "5d33245b-6f17-4a2b-bd04-af8034a684e6"
                }
            ]
        },
        {
            "id": "31b73182-a87d-4847-9425-b7c577c68d30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ababbda-ddd7-46c4-b23f-a58392a99c99",
            "compositeImage": {
                "id": "ff7ff36a-a7f2-4ef6-a611-3c5272b7bd1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31b73182-a87d-4847-9425-b7c577c68d30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f9a5e54-c091-4bc3-be56-918e135ecd1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31b73182-a87d-4847-9425-b7c577c68d30",
                    "LayerId": "5d33245b-6f17-4a2b-bd04-af8034a684e6"
                }
            ]
        },
        {
            "id": "6c5e9e0c-ac71-46e9-8939-73ccd8b4e45c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ababbda-ddd7-46c4-b23f-a58392a99c99",
            "compositeImage": {
                "id": "77a1676f-2dda-427b-bf6b-2ec33f212da5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c5e9e0c-ac71-46e9-8939-73ccd8b4e45c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "031a6213-402a-4971-bd02-dd62a719cdcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c5e9e0c-ac71-46e9-8939-73ccd8b4e45c",
                    "LayerId": "5d33245b-6f17-4a2b-bd04-af8034a684e6"
                }
            ]
        },
        {
            "id": "2f8e7409-78f1-4d54-9bff-02be63a2f4c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ababbda-ddd7-46c4-b23f-a58392a99c99",
            "compositeImage": {
                "id": "4c35ac25-ae38-43ec-98b2-7b8095c14a42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f8e7409-78f1-4d54-9bff-02be63a2f4c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e37d21af-a66e-48cc-89ef-bb7ad0fd1250",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f8e7409-78f1-4d54-9bff-02be63a2f4c1",
                    "LayerId": "5d33245b-6f17-4a2b-bd04-af8034a684e6"
                }
            ]
        },
        {
            "id": "fb5a3294-2cac-47ee-8faa-b78f8ece587a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ababbda-ddd7-46c4-b23f-a58392a99c99",
            "compositeImage": {
                "id": "96ebaf24-1f6c-4670-8618-75e9b91f4dc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb5a3294-2cac-47ee-8faa-b78f8ece587a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a967147e-c947-461d-a9da-8a1e38cad843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb5a3294-2cac-47ee-8faa-b78f8ece587a",
                    "LayerId": "5d33245b-6f17-4a2b-bd04-af8034a684e6"
                }
            ]
        },
        {
            "id": "e77027f9-230e-4e9a-b9ee-96b6bf204e35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ababbda-ddd7-46c4-b23f-a58392a99c99",
            "compositeImage": {
                "id": "928344a9-ffe2-4c33-a6bf-f398718abc40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e77027f9-230e-4e9a-b9ee-96b6bf204e35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0690f38b-3eab-425d-9fc3-aa0703e7b5a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e77027f9-230e-4e9a-b9ee-96b6bf204e35",
                    "LayerId": "5d33245b-6f17-4a2b-bd04-af8034a684e6"
                }
            ]
        },
        {
            "id": "3f2a9c08-11e3-42be-a890-742020ba2f6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ababbda-ddd7-46c4-b23f-a58392a99c99",
            "compositeImage": {
                "id": "4d3ee1da-765f-4e3a-a783-3a52ea2afb7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f2a9c08-11e3-42be-a890-742020ba2f6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2c6c6c7-0812-4a2f-a4a6-3c48d495e14c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f2a9c08-11e3-42be-a890-742020ba2f6f",
                    "LayerId": "5d33245b-6f17-4a2b-bd04-af8034a684e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 91,
    "layers": [
        {
            "id": "5d33245b-6f17-4a2b-bd04-af8034a684e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ababbda-ddd7-46c4-b23f-a58392a99c99",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 35,
    "yorig": 90
}