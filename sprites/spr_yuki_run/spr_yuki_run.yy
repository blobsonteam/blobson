{
    "id": "6114da75-1933-435d-8510-41ababdebd1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yuki_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 0,
    "bbox_right": 132,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e398d99-b1b8-4e15-b296-121f55699026",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6114da75-1933-435d-8510-41ababdebd1d",
            "compositeImage": {
                "id": "a629c289-9ccd-4dac-9fdb-76ea12c34655",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e398d99-b1b8-4e15-b296-121f55699026",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7606623d-6193-460f-9e23-02d52bfa81b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e398d99-b1b8-4e15-b296-121f55699026",
                    "LayerId": "9d919636-1daa-40c8-aa9a-5f07fb1d085e"
                }
            ]
        },
        {
            "id": "d57878ef-97fa-4a81-94b3-5bfb214587ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6114da75-1933-435d-8510-41ababdebd1d",
            "compositeImage": {
                "id": "6b44f03d-a5f9-47cf-abf4-6922dd18a735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d57878ef-97fa-4a81-94b3-5bfb214587ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9106671e-6a7b-419a-85ef-9d490b90248c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d57878ef-97fa-4a81-94b3-5bfb214587ab",
                    "LayerId": "9d919636-1daa-40c8-aa9a-5f07fb1d085e"
                }
            ]
        },
        {
            "id": "5718d9f5-8f0d-414f-874f-f4e752caa470",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6114da75-1933-435d-8510-41ababdebd1d",
            "compositeImage": {
                "id": "a4434420-a12b-49c2-9b77-ac2d181527a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5718d9f5-8f0d-414f-874f-f4e752caa470",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aac9523a-a9f3-4e76-b183-a6984ad4ebdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5718d9f5-8f0d-414f-874f-f4e752caa470",
                    "LayerId": "9d919636-1daa-40c8-aa9a-5f07fb1d085e"
                }
            ]
        },
        {
            "id": "92423c06-4b53-4cd3-a1bc-1c1cbc60b741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6114da75-1933-435d-8510-41ababdebd1d",
            "compositeImage": {
                "id": "a8c6add8-c860-45a7-92ab-a98d1e55425f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92423c06-4b53-4cd3-a1bc-1c1cbc60b741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb0ae269-65af-4ff8-991d-e7c1b87949e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92423c06-4b53-4cd3-a1bc-1c1cbc60b741",
                    "LayerId": "9d919636-1daa-40c8-aa9a-5f07fb1d085e"
                }
            ]
        },
        {
            "id": "d4909405-30f6-4824-899a-16e28a72addb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6114da75-1933-435d-8510-41ababdebd1d",
            "compositeImage": {
                "id": "8eaf2b3b-c5e6-4535-b7e1-3888ed97a3ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4909405-30f6-4824-899a-16e28a72addb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f292b3f9-f221-43b9-b6d7-ed967bf5044a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4909405-30f6-4824-899a-16e28a72addb",
                    "LayerId": "9d919636-1daa-40c8-aa9a-5f07fb1d085e"
                }
            ]
        },
        {
            "id": "fddab44b-16c4-4366-be3a-705cc7d5b469",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6114da75-1933-435d-8510-41ababdebd1d",
            "compositeImage": {
                "id": "bb0665e2-b3ac-4202-a829-6d6f686dc42f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fddab44b-16c4-4366-be3a-705cc7d5b469",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f57d9c4c-35dd-403d-b032-348876f28ee7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fddab44b-16c4-4366-be3a-705cc7d5b469",
                    "LayerId": "9d919636-1daa-40c8-aa9a-5f07fb1d085e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "9d919636-1daa-40c8-aa9a-5f07fb1d085e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6114da75-1933-435d-8510-41ababdebd1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 133,
    "xorig": 92,
    "yorig": 89
}