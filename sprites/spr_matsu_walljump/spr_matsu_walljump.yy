{
    "id": "51b4ad8b-fa4e-4a59-8b01-5e9a23a8d73a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_walljump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 0,
    "bbox_right": 81,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8b81fac-2b83-4f4c-8fe9-7ef0d2352f79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51b4ad8b-fa4e-4a59-8b01-5e9a23a8d73a",
            "compositeImage": {
                "id": "812c1401-ace6-438c-a262-67f06977d37c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8b81fac-2b83-4f4c-8fe9-7ef0d2352f79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ab80d4c-8aa4-4b3e-8690-a8b11bd7e05f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8b81fac-2b83-4f4c-8fe9-7ef0d2352f79",
                    "LayerId": "6920c840-1152-4724-b60c-c53f1461be65"
                }
            ]
        },
        {
            "id": "7991655c-f153-4a67-8e3b-f8b70cca4823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51b4ad8b-fa4e-4a59-8b01-5e9a23a8d73a",
            "compositeImage": {
                "id": "05540f7e-0308-42f1-95c1-cf7f72837d3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7991655c-f153-4a67-8e3b-f8b70cca4823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "391eac50-a260-42cd-986e-af8e451d8fce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7991655c-f153-4a67-8e3b-f8b70cca4823",
                    "LayerId": "6920c840-1152-4724-b60c-c53f1461be65"
                }
            ]
        },
        {
            "id": "0bf66d22-3e7e-4746-94d6-d7ba4ebd64b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51b4ad8b-fa4e-4a59-8b01-5e9a23a8d73a",
            "compositeImage": {
                "id": "17b2c83c-5e89-4dd1-a4fe-6b3e454103f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bf66d22-3e7e-4746-94d6-d7ba4ebd64b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a091b3a6-620f-4010-b794-0feae7285c45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bf66d22-3e7e-4746-94d6-d7ba4ebd64b0",
                    "LayerId": "6920c840-1152-4724-b60c-c53f1461be65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 98,
    "layers": [
        {
            "id": "6920c840-1152-4724-b60c-c53f1461be65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51b4ad8b-fa4e-4a59-8b01-5e9a23a8d73a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 82,
    "xorig": 0,
    "yorig": 0
}