{
    "id": "e331e3c1-2e06-4e08-9d6e-0300248caaf6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_hitstun2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 109,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4037ec61-02ed-45b0-8d1c-94f0941e2d58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e331e3c1-2e06-4e08-9d6e-0300248caaf6",
            "compositeImage": {
                "id": "62fa620c-1ee3-43ad-983e-79c34438f5ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4037ec61-02ed-45b0-8d1c-94f0941e2d58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8975b8b-ec0b-4ff9-aea4-76716dc48a3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4037ec61-02ed-45b0-8d1c-94f0941e2d58",
                    "LayerId": "002b1169-22b7-4e8e-ad1d-150445c7c6fc"
                }
            ]
        },
        {
            "id": "a2e74e7e-8651-42a9-94f0-1dbde1630d0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e331e3c1-2e06-4e08-9d6e-0300248caaf6",
            "compositeImage": {
                "id": "73d61abc-4791-471a-a493-1ac45147c563",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2e74e7e-8651-42a9-94f0-1dbde1630d0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c712f5f0-e62f-4f54-8c12-0a72ea77c6df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2e74e7e-8651-42a9-94f0-1dbde1630d0e",
                    "LayerId": "002b1169-22b7-4e8e-ad1d-150445c7c6fc"
                }
            ]
        },
        {
            "id": "74d98170-f615-4cbb-ad9b-f70c379f35e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e331e3c1-2e06-4e08-9d6e-0300248caaf6",
            "compositeImage": {
                "id": "7cab6980-ddbd-418a-8c43-82385b6671ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74d98170-f615-4cbb-ad9b-f70c379f35e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "601e0311-fd3a-4bc1-97a3-676c81fbc1ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74d98170-f615-4cbb-ad9b-f70c379f35e8",
                    "LayerId": "002b1169-22b7-4e8e-ad1d-150445c7c6fc"
                }
            ]
        },
        {
            "id": "9b32157a-edb5-4913-bc82-f97ee4974273",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e331e3c1-2e06-4e08-9d6e-0300248caaf6",
            "compositeImage": {
                "id": "f30f2d5a-02f3-47f6-93ed-cf80460af499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b32157a-edb5-4913-bc82-f97ee4974273",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a299b4e-96ac-4127-823f-b7eeb6261c36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b32157a-edb5-4913-bc82-f97ee4974273",
                    "LayerId": "002b1169-22b7-4e8e-ad1d-150445c7c6fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "002b1169-22b7-4e8e-ad1d-150445c7c6fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e331e3c1-2e06-4e08-9d6e-0300248caaf6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 110,
    "xorig": 36,
    "yorig": 89
}