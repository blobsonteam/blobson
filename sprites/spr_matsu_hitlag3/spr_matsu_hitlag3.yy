{
    "id": "77754287-0ba5-4103-9faa-6230d31016d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_hitlag3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 0,
    "bbox_right": 64,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a302209c-1eea-4d83-ba26-2ab05b25bc6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77754287-0ba5-4103-9faa-6230d31016d6",
            "compositeImage": {
                "id": "48ffd55a-e792-42cc-950f-7506a974f311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a302209c-1eea-4d83-ba26-2ab05b25bc6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1de7e3dd-bc8d-4cb9-a56f-611aebfbc38a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a302209c-1eea-4d83-ba26-2ab05b25bc6c",
                    "LayerId": "cc4e5cff-3c16-4588-ac00-d791b856c9f9"
                }
            ]
        },
        {
            "id": "cfac8950-646e-4792-b4ab-6479cc704523",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77754287-0ba5-4103-9faa-6230d31016d6",
            "compositeImage": {
                "id": "97ab8962-6d89-45c3-ad61-0fc260a190a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfac8950-646e-4792-b4ab-6479cc704523",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68dbbf65-f583-4677-86c2-d56d4fc89c87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfac8950-646e-4792-b4ab-6479cc704523",
                    "LayerId": "cc4e5cff-3c16-4588-ac00-d791b856c9f9"
                }
            ]
        },
        {
            "id": "2763d164-e8cb-4e16-acea-3ff318373a9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77754287-0ba5-4103-9faa-6230d31016d6",
            "compositeImage": {
                "id": "1c01ee51-3331-41db-b858-656304bac0b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2763d164-e8cb-4e16-acea-3ff318373a9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb6715a1-e7d1-41c3-a3ea-7308369d46d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2763d164-e8cb-4e16-acea-3ff318373a9d",
                    "LayerId": "cc4e5cff-3c16-4588-ac00-d791b856c9f9"
                }
            ]
        },
        {
            "id": "17b59f41-186b-4391-b35b-8ac10256ff1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77754287-0ba5-4103-9faa-6230d31016d6",
            "compositeImage": {
                "id": "e2b27799-dcd0-4b71-8c6d-a952b74f7e91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17b59f41-186b-4391-b35b-8ac10256ff1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb966a88-50ea-4cb7-9592-9d0a264b624e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17b59f41-186b-4391-b35b-8ac10256ff1c",
                    "LayerId": "cc4e5cff-3c16-4588-ac00-d791b856c9f9"
                }
            ]
        },
        {
            "id": "9e657879-8066-4a0c-a2a2-207aeaf68f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77754287-0ba5-4103-9faa-6230d31016d6",
            "compositeImage": {
                "id": "113b208b-2bd8-4bcd-a40c-28d90c023f60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e657879-8066-4a0c-a2a2-207aeaf68f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d2189fd-0379-4ac9-84bd-2282f2fd58a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e657879-8066-4a0c-a2a2-207aeaf68f28",
                    "LayerId": "cc4e5cff-3c16-4588-ac00-d791b856c9f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 78,
    "layers": [
        {
            "id": "cc4e5cff-3c16-4588-ac00-d791b856c9f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77754287-0ba5-4103-9faa-6230d31016d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0.2,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 27,
    "yorig": 77
}