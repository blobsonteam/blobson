{
    "id": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_training_colors",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76e93436-703a-4fa2-babe-6425511e8568",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "037413fe-1065-422a-8651-ca80667ae8b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76e93436-703a-4fa2-babe-6425511e8568",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d77cb12c-9962-4308-b4b3-c3989d4530cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76e93436-703a-4fa2-babe-6425511e8568",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "efcf8448-6151-4cd1-90f1-46a27947dc4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "c49d7aef-ab34-4851-971d-12e936a7ad0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efcf8448-6151-4cd1-90f1-46a27947dc4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c089aa4-d77d-42a1-a352-24836717692b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efcf8448-6151-4cd1-90f1-46a27947dc4e",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "497ffda6-7e80-4796-9283-bab7d944636e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "11902b9b-d7db-42ac-961d-b148e81fce70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "497ffda6-7e80-4796-9283-bab7d944636e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc2cb314-2a20-4083-94eb-3292135fd33f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "497ffda6-7e80-4796-9283-bab7d944636e",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "3646ed2c-4fef-40ed-8210-395d4ce182ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "a9a3a844-4e05-4659-93d4-13d151f46c44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3646ed2c-4fef-40ed-8210-395d4ce182ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cf691c9-d073-4c35-a2f1-f55611081cec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3646ed2c-4fef-40ed-8210-395d4ce182ab",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "cedf8d56-d034-45dd-9a0d-ae37da436a6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "78d37907-4ed9-4d41-afa7-247ca37bc9ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cedf8d56-d034-45dd-9a0d-ae37da436a6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61f34eaf-aeb4-48f2-9836-925fc0e15205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cedf8d56-d034-45dd-9a0d-ae37da436a6c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "e459e380-73f8-451f-9c29-499786bf34df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "aa16a4de-7a2a-49a4-8858-c16cc46b05d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e459e380-73f8-451f-9c29-499786bf34df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fed3b58e-beb0-4297-a8f4-647de6d3a8b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e459e380-73f8-451f-9c29-499786bf34df",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "96d4db9a-7877-4eff-801f-5f7abee229da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "6e045fc3-6f02-4f00-b5cb-eafa0da91f84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96d4db9a-7877-4eff-801f-5f7abee229da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8042d277-0574-41a9-bb9a-6ebcbbb11ee4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96d4db9a-7877-4eff-801f-5f7abee229da",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "4c4ac6ef-5bea-421c-8922-b8ee631a6980",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "fbeafb0a-ed9f-485c-af08-45c121b6525f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c4ac6ef-5bea-421c-8922-b8ee631a6980",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b60b89f-e679-426d-af72-dce92b336800",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c4ac6ef-5bea-421c-8922-b8ee631a6980",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "4d890c4c-0a8b-44a4-a013-f14192aaa022",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "2add1aef-9c2e-482a-8011-5a2d7f915d20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d890c4c-0a8b-44a4-a013-f14192aaa022",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73ec32db-d230-4ddc-a6e2-26a22ada9260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d890c4c-0a8b-44a4-a013-f14192aaa022",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "68aad57b-2e18-493c-b310-1d5a7de057fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "737b8412-4e79-4162-b7f4-2a23e93fd22e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68aad57b-2e18-493c-b310-1d5a7de057fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a273fb6-67d1-41ff-8241-098656d87f52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68aad57b-2e18-493c-b310-1d5a7de057fd",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "2ee9979a-36b4-424f-a501-e77e82b7f63c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "1344521d-1354-44ab-8dab-9962281226f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ee9979a-36b4-424f-a501-e77e82b7f63c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8a114ed-a787-4c2d-952a-331e9644fe81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ee9979a-36b4-424f-a501-e77e82b7f63c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "37a00d8f-79d5-44c0-8a81-d234c36e39ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "52a1c371-ce2b-4222-834b-051eec633f0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37a00d8f-79d5-44c0-8a81-d234c36e39ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbd379f4-b08d-4c33-8c99-d6d97d57ec64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37a00d8f-79d5-44c0-8a81-d234c36e39ef",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "e0c01d35-bfa7-408e-82dc-4d61ee30f843",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "041910f3-e890-4eb1-9c34-3c2e032b37d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0c01d35-bfa7-408e-82dc-4d61ee30f843",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50608239-ee17-4581-bfdd-9bb10af0e39d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0c01d35-bfa7-408e-82dc-4d61ee30f843",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "768c5b08-f33a-4c6e-8563-534240a50c30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "087ca1ca-ecc8-43dc-81a5-7e4d205c0229",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "768c5b08-f33a-4c6e-8563-534240a50c30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a13cea17-ccf5-4bc7-aaa3-9d50f56290cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "768c5b08-f33a-4c6e-8563-534240a50c30",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "84f54653-78bf-44c0-9cca-4b5617222740",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "b46f7ccd-5dd8-4c39-8a74-9003a266a1c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84f54653-78bf-44c0-9cca-4b5617222740",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c6b6250-fd24-4478-993d-54bc93e2c857",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84f54653-78bf-44c0-9cca-4b5617222740",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "75d93a42-bff6-4792-858d-2fb020d30fa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "a8eb976b-0a20-461a-a34e-239b9757f19f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75d93a42-bff6-4792-858d-2fb020d30fa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02d81970-8412-4661-893e-caa44642e5bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75d93a42-bff6-4792-858d-2fb020d30fa4",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "af434979-cb00-4cc5-b0f7-77f93853f03c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "aaa2e923-15ea-40c7-b861-298a1d881edc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af434979-cb00-4cc5-b0f7-77f93853f03c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "015c088e-8be5-4082-84e4-9fa72127b4a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af434979-cb00-4cc5-b0f7-77f93853f03c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "dd8a3947-27fd-48c8-9f8c-a853b6ca1548",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "23031625-507e-4ab4-8814-e664aabfcb8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd8a3947-27fd-48c8-9f8c-a853b6ca1548",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b43daa5-656a-4415-b284-77f4f4f6f92f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd8a3947-27fd-48c8-9f8c-a853b6ca1548",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "6e20e8f9-559b-4376-b701-cd60cbbb9999",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "548633d0-3002-45c6-a977-6d3d4b3bb02d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e20e8f9-559b-4376-b701-cd60cbbb9999",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6490319-3d1b-4a7d-a568-412c87e0c513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e20e8f9-559b-4376-b701-cd60cbbb9999",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "a4ff5926-bd22-4d89-aa81-9e5b873cdcd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "ae8691cf-9fd7-43d9-81bd-ab09cb9c25d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4ff5926-bd22-4d89-aa81-9e5b873cdcd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02ca3b9e-1802-49ba-80a6-420e42bff73b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4ff5926-bd22-4d89-aa81-9e5b873cdcd3",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "947b386b-15df-4734-9466-531de8604b82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "d53fafae-bf7d-49bd-8572-39a849e00e07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "947b386b-15df-4734-9466-531de8604b82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a0279d0-77d7-48fc-87f4-41a29b864680",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "947b386b-15df-4734-9466-531de8604b82",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "3a751916-3286-436c-a3ab-406f7fc68386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "ec703507-3c0c-42e1-9ca1-b349d08407bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a751916-3286-436c-a3ab-406f7fc68386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a1c7869-230e-4ab4-b42f-d1852a59a1f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a751916-3286-436c-a3ab-406f7fc68386",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "6ccdbd5c-f5cd-4872-affe-bc835fc87d52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "6f482e53-57b5-42c6-b67f-93b6a8f5d7af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ccdbd5c-f5cd-4872-affe-bc835fc87d52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e73c250-ac93-4061-b413-5959018dad6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ccdbd5c-f5cd-4872-affe-bc835fc87d52",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "4775a73f-8b23-41d9-9fc4-23fd0aaf7443",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "df715ff1-7907-4816-9bab-bfbf2d58501c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4775a73f-8b23-41d9-9fc4-23fd0aaf7443",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2414086c-b9ed-421b-974b-ca0448c58f16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4775a73f-8b23-41d9-9fc4-23fd0aaf7443",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "9d7d53d2-a263-4c7b-9ea5-2746dfbdd070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "09f1a450-60a8-4f94-bea2-bf0c76de9165",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d7d53d2-a263-4c7b-9ea5-2746dfbdd070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af476d46-08dc-49dc-ad76-3704cc8052bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d7d53d2-a263-4c7b-9ea5-2746dfbdd070",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "7ce23069-bdf4-4f99-9943-dab16df3aace",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "49d1ac42-3865-476e-a0b4-de24b49f4d91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ce23069-bdf4-4f99-9943-dab16df3aace",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "725eba6a-dac4-47d8-853f-eb64a7819f1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ce23069-bdf4-4f99-9943-dab16df3aace",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "0b417a53-6fdf-44a9-9c99-5989772fe5e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "2fe43b84-4b65-41ec-b303-ecb4dfa33403",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b417a53-6fdf-44a9-9c99-5989772fe5e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff711a3d-38c6-40fd-8d14-59b9bbffa244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b417a53-6fdf-44a9-9c99-5989772fe5e9",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "e9868f31-dd7a-4e2f-b137-9fe497b24fce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "f8b53d62-8bbf-4023-aca4-1fef95c963eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9868f31-dd7a-4e2f-b137-9fe497b24fce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e89734f7-74b0-45e6-b8ce-a45ac8ca9b41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9868f31-dd7a-4e2f-b137-9fe497b24fce",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "9ca8e09a-4d67-41ca-80a9-99dd448b3373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "26569425-dbca-4933-a882-ef42143f2c22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ca8e09a-4d67-41ca-80a9-99dd448b3373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4b855d0-d420-4c37-84f2-7f9bec2578c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ca8e09a-4d67-41ca-80a9-99dd448b3373",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "ba3c1ec6-f883-430a-bbb1-76ce28240d0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "d0ac0eb6-f3cd-4b44-9b7c-d696f48b27e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba3c1ec6-f883-430a-bbb1-76ce28240d0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dabef2d1-d0d1-4361-90c3-3bd29c611fac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba3c1ec6-f883-430a-bbb1-76ce28240d0b",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "ff71735d-711b-4c59-b665-d202d48941e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "45e9b922-22a7-483c-a3f3-5c5d5168cd25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff71735d-711b-4c59-b665-d202d48941e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90adcfe1-ebf1-41e5-acca-0baeef36d06b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff71735d-711b-4c59-b665-d202d48941e0",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "4d3bfa3d-2b00-490e-aea0-78ed64c169e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "56ad8699-16f6-4718-955d-3caca0cc9032",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d3bfa3d-2b00-490e-aea0-78ed64c169e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bf281c2-9b84-4c9a-bb67-397d75cc2d2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d3bfa3d-2b00-490e-aea0-78ed64c169e9",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "a6bb79d8-5a84-4ea5-95d6-c586e4f28104",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "124873df-b6c5-4721-8696-d13f82693bd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6bb79d8-5a84-4ea5-95d6-c586e4f28104",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b354f72-b805-4013-a3e9-507e774f29a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6bb79d8-5a84-4ea5-95d6-c586e4f28104",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "f72465d8-35d9-49cc-9f0a-75368049dff5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "43f5fb6f-3b6e-4a2b-8239-76172269d6d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f72465d8-35d9-49cc-9f0a-75368049dff5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e18b7e3-e5d9-423b-acf1-af3ec4b03430",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f72465d8-35d9-49cc-9f0a-75368049dff5",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "9a04dc42-663d-4971-8767-1739b62ad51d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "fd99d559-467c-4ffd-af4c-e37257341f0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a04dc42-663d-4971-8767-1739b62ad51d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0434a4d-34a4-491b-afc7-736119f78e5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a04dc42-663d-4971-8767-1739b62ad51d",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "599cf6e6-562c-4b59-b265-8b3e1c4d8650",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "7549dcbf-7c43-4431-b07b-8256eb646c55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "599cf6e6-562c-4b59-b265-8b3e1c4d8650",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d4208ad-de35-46a3-a8e9-44b0a253e78a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "599cf6e6-562c-4b59-b265-8b3e1c4d8650",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "53a93c29-b810-4daa-96fb-a478a4aa9154",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "ced9efd6-4f1f-42df-b9da-55e941f1f64b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53a93c29-b810-4daa-96fb-a478a4aa9154",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9e51539-335f-4483-99b8-96485624036f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53a93c29-b810-4daa-96fb-a478a4aa9154",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "bcb0d34c-3833-4bb2-b5c8-825509f751fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "db7d4e7f-4acf-40d2-a21e-5d7ae4eed03a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcb0d34c-3833-4bb2-b5c8-825509f751fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d58bd239-4654-495a-bdbf-72deb0eaa876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcb0d34c-3833-4bb2-b5c8-825509f751fb",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "dbdcd93e-c081-46f2-bcbf-fdba7432a603",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "0d20bfbc-2d3e-4129-a469-e96686ed7fef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbdcd93e-c081-46f2-bcbf-fdba7432a603",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1eb7b35-2bda-49a5-9afb-46b8ce55dd36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbdcd93e-c081-46f2-bcbf-fdba7432a603",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "17b93ad4-10ed-4d09-8a7f-783063bb696d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "b7d6bd8c-82a4-4251-9334-ec974cc2ada8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17b93ad4-10ed-4d09-8a7f-783063bb696d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a13c811d-5c7d-473b-a608-5edfe8dbbd2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17b93ad4-10ed-4d09-8a7f-783063bb696d",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "eaad251f-dccc-4db7-a840-3c5534953e47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "491040af-810f-4bd1-a12a-5c382b91c7c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaad251f-dccc-4db7-a840-3c5534953e47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fffe411a-a2f4-4346-83a6-fd6da935e524",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaad251f-dccc-4db7-a840-3c5534953e47",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "61d51d71-2439-485a-8893-86073d3d8096",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "b36eb95c-f7d2-45ac-8e11-2f67c4960640",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61d51d71-2439-485a-8893-86073d3d8096",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fea56771-e848-413f-861a-76df9479cd13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61d51d71-2439-485a-8893-86073d3d8096",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "8ac16260-c294-49af-bc8d-24dcb78bf077",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "8776e7ae-b5db-4396-800c-9ec93cb6c477",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ac16260-c294-49af-bc8d-24dcb78bf077",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91b352cf-5a93-446a-b575-f1888eb79de5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ac16260-c294-49af-bc8d-24dcb78bf077",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5b518eb0-9446-479a-b0a8-85e037cd2146",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "219c6ce0-a1d0-4dfb-8500-a6802a827d6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b518eb0-9446-479a-b0a8-85e037cd2146",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef344a70-2d8f-4e9d-8c07-a64ae9f2e9df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b518eb0-9446-479a-b0a8-85e037cd2146",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "d57526b8-95e8-4980-b757-28693c73f2c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "76283149-288e-4980-9219-8e182d6c1022",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d57526b8-95e8-4980-b757-28693c73f2c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ac25da2-492c-4aa7-972b-6659c82d16ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d57526b8-95e8-4980-b757-28693c73f2c3",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "48910968-d13e-472e-9aef-2d7ea50070d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "50ec8fa4-fda9-4c2d-bd77-e986ec2f3d52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48910968-d13e-472e-9aef-2d7ea50070d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcc9a060-72ea-4bc4-bcce-b6adea17e03a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48910968-d13e-472e-9aef-2d7ea50070d1",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "6c3f8656-0519-4708-96e1-2d08638ec3f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "d2c65622-9339-4e19-9ff7-2f171cc06b07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c3f8656-0519-4708-96e1-2d08638ec3f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91b70dc1-14b0-4098-a990-6dd6b15b2a19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c3f8656-0519-4708-96e1-2d08638ec3f3",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "4a147ab5-d43d-46c0-a6f8-f28a2b2c2c30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "b361d8cf-ba0b-48d6-8fc4-d6c3730b1b95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a147ab5-d43d-46c0-a6f8-f28a2b2c2c30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "919e148f-dba4-4354-9b77-4e459666edca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a147ab5-d43d-46c0-a6f8-f28a2b2c2c30",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "c576e999-ef44-4b8d-b187-150e1c623df0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "66c339eb-b265-4b44-bd61-900a35787753",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c576e999-ef44-4b8d-b187-150e1c623df0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2acde58f-b114-456b-88af-61b7c57d4396",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c576e999-ef44-4b8d-b187-150e1c623df0",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "f419f425-1444-4619-a163-c47de6711215",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "490adcca-4dbc-40e3-83cb-03b94222bc97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f419f425-1444-4619-a163-c47de6711215",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2e98088-d60d-400e-9974-0447f8365b83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f419f425-1444-4619-a163-c47de6711215",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "dc413836-01e0-4155-93cf-bb36231bde21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "4cbb0e95-b4eb-4fca-81be-85dc1e8f39cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc413836-01e0-4155-93cf-bb36231bde21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49015524-f097-483b-a74d-50966fdf35db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc413836-01e0-4155-93cf-bb36231bde21",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "7ab3709e-8063-4443-bebe-88b9deaa331c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "29be23f7-eb5c-473f-a5ca-d22c039cde45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ab3709e-8063-4443-bebe-88b9deaa331c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ba33cc9-a9bf-481b-bc72-07f613bf1df1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ab3709e-8063-4443-bebe-88b9deaa331c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "ea727b66-40be-4b2b-a249-69eeaabcc205",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "2d3c5c4c-2942-481b-8196-34e3611f09cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea727b66-40be-4b2b-a249-69eeaabcc205",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59726f87-26da-44b7-9405-6c8a68e05cd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea727b66-40be-4b2b-a249-69eeaabcc205",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "766bc5aa-a77f-4d49-b668-f52887df9e74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "26911bb6-7c06-4be0-8899-643bf9dcbba1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "766bc5aa-a77f-4d49-b668-f52887df9e74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11b84eaa-7f55-4ee5-a7c1-938a97b93dd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "766bc5aa-a77f-4d49-b668-f52887df9e74",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5d73ecc7-a165-4697-99e0-ae183099ffe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "defba986-888c-4a8a-9913-6bfb11a19025",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d73ecc7-a165-4697-99e0-ae183099ffe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54545eea-7770-4553-8840-32e7ebdbccfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d73ecc7-a165-4697-99e0-ae183099ffe1",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "9cfe209d-29f5-4597-a270-2db9a2c01312",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "b049d91a-6990-4b0a-80e1-0089260645fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cfe209d-29f5-4597-a270-2db9a2c01312",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4381b626-f2f0-4a41-aa26-4d91ae25856d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cfe209d-29f5-4597-a270-2db9a2c01312",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "f7030a4c-70ee-41be-aecf-b4f25ec7f51e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "48479712-f2a5-4ec7-ac49-0caf52fd6f80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7030a4c-70ee-41be-aecf-b4f25ec7f51e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6df3069-8fc9-4a19-b828-f93ccac8401b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7030a4c-70ee-41be-aecf-b4f25ec7f51e",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "6c90c7a4-a235-4854-a33c-7853c9a605bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "047d93bf-f965-4507-a32b-5cc2cbb8501b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c90c7a4-a235-4854-a33c-7853c9a605bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9b53c4c-69d5-471a-984e-ce1c43a6c215",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c90c7a4-a235-4854-a33c-7853c9a605bb",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "08ee1543-d386-4251-9354-d747bf3afa8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "3c3d1b72-2f53-4830-89ca-e5579349aa45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08ee1543-d386-4251-9354-d747bf3afa8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08f24bb2-6a50-43b5-82e1-b4d53d83b235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08ee1543-d386-4251-9354-d747bf3afa8b",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "c2fa824f-e621-4489-950a-72bac4794fe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "f87d3184-c124-445b-8e19-821813b1ae5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2fa824f-e621-4489-950a-72bac4794fe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cb7ef1f-8c7b-4b19-9cc7-6bb45d71be98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2fa824f-e621-4489-950a-72bac4794fe4",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "e94fdfb9-149a-4a01-8aa4-db37368f2df8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "f8c3b0f6-3ce4-49dc-93b0-8202f3b9f221",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e94fdfb9-149a-4a01-8aa4-db37368f2df8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4743313-1377-4961-93e7-3831fdaed317",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e94fdfb9-149a-4a01-8aa4-db37368f2df8",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "3af28dd6-bdce-4032-bb17-6ab3a2de98c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "721e6b08-14fb-4795-b17d-c4d0506c13ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3af28dd6-bdce-4032-bb17-6ab3a2de98c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be48c8b5-ab66-4c41-9bd3-824dbd6fdba6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3af28dd6-bdce-4032-bb17-6ab3a2de98c7",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5e47f1a8-9201-4ff9-9302-a5f1da934a8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "ea9cef53-66f9-4899-b648-c989863f5b8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e47f1a8-9201-4ff9-9302-a5f1da934a8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e36a388-1532-440a-ba44-90834eea0396",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e47f1a8-9201-4ff9-9302-a5f1da934a8b",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "c7849ba7-98b6-4c21-ad00-65b6b6adc35c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "627d1713-be32-402c-a9ae-9009a886051e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7849ba7-98b6-4c21-ad00-65b6b6adc35c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec9c1364-5368-48a5-83a0-f165e16c0310",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7849ba7-98b6-4c21-ad00-65b6b6adc35c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "559e55c3-2edb-43f3-bc2f-332b26f7c373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "84183de6-91c0-4a0e-bc25-19da71dca525",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "559e55c3-2edb-43f3-bc2f-332b26f7c373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "101ebfbb-182b-4a9c-b9cb-2ed2492848d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "559e55c3-2edb-43f3-bc2f-332b26f7c373",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "c7a349a9-b2ee-4552-95cb-8354bec32971",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "3d262844-4675-4b07-90f7-85bbb561013c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7a349a9-b2ee-4552-95cb-8354bec32971",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bb57319-4c71-4ef8-94f6-84cda8bb33c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7a349a9-b2ee-4552-95cb-8354bec32971",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "b30de1f3-48fe-4a48-aadc-7c5df1e17232",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "84036afa-187f-4d30-b105-9024c6f64c3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b30de1f3-48fe-4a48-aadc-7c5df1e17232",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b908fe6-6d9b-47f2-b166-3621544f6811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b30de1f3-48fe-4a48-aadc-7c5df1e17232",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "43748641-faa6-4d59-8bb6-92d5ac862e69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "e6464e3f-7889-40a2-819f-307e700d864b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43748641-faa6-4d59-8bb6-92d5ac862e69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36e85af1-30c0-4869-b3d6-0139f2ac9aff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43748641-faa6-4d59-8bb6-92d5ac862e69",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "406f776e-a01f-42ec-ad52-56279754db56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "24342ef2-aac8-4300-810f-3d1a57e17be6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "406f776e-a01f-42ec-ad52-56279754db56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd12804c-1d49-4d99-9618-8d39fb987958",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "406f776e-a01f-42ec-ad52-56279754db56",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "abcd3950-0aad-4c32-bbe8-e976fc4c078a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "9213c256-75a6-45cd-954e-af443428e330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abcd3950-0aad-4c32-bbe8-e976fc4c078a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eca6808a-2c43-4282-8b28-b8b639daa18e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abcd3950-0aad-4c32-bbe8-e976fc4c078a",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "8584bd41-8694-42d3-99d6-dc6f7478c254",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "e65580f5-ab0e-430c-99cd-e90058572b1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8584bd41-8694-42d3-99d6-dc6f7478c254",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0d9c12b-a9ae-4892-9bd6-299a2d750298",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8584bd41-8694-42d3-99d6-dc6f7478c254",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "b6a605a2-1554-4070-8675-9f5307f5690c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "9d7d69d7-5114-482a-8efc-78dfe47b5ba9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6a605a2-1554-4070-8675-9f5307f5690c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "956c57ec-0932-4a23-bb1f-5ab18466655f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6a605a2-1554-4070-8675-9f5307f5690c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5f794fef-5a71-40da-9a87-9017beefca5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "abf9a4e0-aa3f-4905-b66f-d204d6098845",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f794fef-5a71-40da-9a87-9017beefca5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5915dfcb-711f-47bf-8089-27e7ac1cc13e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f794fef-5a71-40da-9a87-9017beefca5d",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "48484744-9908-427d-9597-511713dc1348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "eec1ca75-885b-4ef6-999e-f062ae879920",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48484744-9908-427d-9597-511713dc1348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb52eab4-87a8-4780-b5c5-c6938a260af6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48484744-9908-427d-9597-511713dc1348",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "574ce717-24e2-40a0-b5a8-aa20aa86a73a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "30879ac3-88f4-4e6a-a6d7-87f982b0598d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "574ce717-24e2-40a0-b5a8-aa20aa86a73a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d331d80a-e469-41a3-a2f1-a1a45c33c2cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "574ce717-24e2-40a0-b5a8-aa20aa86a73a",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "874de0cc-bbc4-41dd-b94e-4e5021abddbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "7c070300-2539-448b-9759-c78d1a0f3566",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "874de0cc-bbc4-41dd-b94e-4e5021abddbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be43cde7-4204-48f6-a2b2-682759aa6523",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "874de0cc-bbc4-41dd-b94e-4e5021abddbb",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "bfbce47a-d453-40d1-92f6-a137a1a8de58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "b9d812b8-e290-4223-b0f2-80b554531f63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfbce47a-d453-40d1-92f6-a137a1a8de58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0918631-6a66-4175-960f-2c233f689315",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfbce47a-d453-40d1-92f6-a137a1a8de58",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "f68efee1-7c1b-4a94-b3c5-1fbd2ad0b9ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "d1767550-2d71-4c2c-9244-2649fdf9199b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f68efee1-7c1b-4a94-b3c5-1fbd2ad0b9ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12583f67-5de7-4647-8d42-4fac3a0425f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f68efee1-7c1b-4a94-b3c5-1fbd2ad0b9ce",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "26d332b3-f51c-4d45-a1fa-96b83d591071",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "87fa4a4a-995c-40f8-b321-2f3ec91200e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26d332b3-f51c-4d45-a1fa-96b83d591071",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "159295a0-ee2d-4066-a629-3cadd413c4b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26d332b3-f51c-4d45-a1fa-96b83d591071",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "3e4cf134-127f-4bc8-a7d2-a116e9c8cea0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "63d4bb03-6d2a-4b8c-b9a8-0d054e06ab15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e4cf134-127f-4bc8-a7d2-a116e9c8cea0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebc89ae5-cc11-4273-80d3-a60e4177adbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e4cf134-127f-4bc8-a7d2-a116e9c8cea0",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "08c9396d-004e-48b5-a1c3-16e69f7ae44b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "dc47d71a-ce64-45e1-9ed4-4ab09f340f2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08c9396d-004e-48b5-a1c3-16e69f7ae44b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7561754a-c46e-4907-8211-494e5abfb790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08c9396d-004e-48b5-a1c3-16e69f7ae44b",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "ba64c9bb-9d42-427c-bf66-45f032f1e464",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "6a0847da-faeb-43d6-9c17-57c0a48722a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba64c9bb-9d42-427c-bf66-45f032f1e464",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e35eba4-7701-4654-8605-52680d537468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba64c9bb-9d42-427c-bf66-45f032f1e464",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "d6ff1a47-5429-4595-b436-e37724bba576",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "a7540846-9a3f-4a33-a370-24d052c5622a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6ff1a47-5429-4595-b436-e37724bba576",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6a7473c-c60a-45a3-a41f-8b70033dae3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6ff1a47-5429-4595-b436-e37724bba576",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "6a5c0584-0f76-4c8e-98b8-cbd1f9b6814b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "16627fdb-d785-4916-b7b7-8f02851b5bdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a5c0584-0f76-4c8e-98b8-cbd1f9b6814b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e18f9ef-efeb-4c77-afb9-440fb4ec2438",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a5c0584-0f76-4c8e-98b8-cbd1f9b6814b",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "d5070892-dc55-4077-9123-44d801ab16dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "d42df0d6-cb9c-44f7-be9c-8cbe782710a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5070892-dc55-4077-9123-44d801ab16dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f447c0c7-72f9-4a3d-b8fe-9f3eecb017da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5070892-dc55-4077-9123-44d801ab16dc",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "6b59f434-fc0c-4422-a66b-02fe2b345241",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "44eec58c-62e5-4ba6-b6d5-810b532e5178",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b59f434-fc0c-4422-a66b-02fe2b345241",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22140066-c65b-492e-bab6-424834947831",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b59f434-fc0c-4422-a66b-02fe2b345241",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "c0926967-2f84-4324-aae4-ff8932337a8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "bdcbd5ee-5134-4694-8223-f623f23df353",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0926967-2f84-4324-aae4-ff8932337a8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5999b479-eaca-40d4-8072-3aa2af634196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0926967-2f84-4324-aae4-ff8932337a8a",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "8f35e42b-23a2-4581-a5d8-2aaf8765fb3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "382a08a2-8526-4262-ad1d-8304457366ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f35e42b-23a2-4581-a5d8-2aaf8765fb3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88d83a56-6d49-4a6e-8dca-85f974854a37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f35e42b-23a2-4581-a5d8-2aaf8765fb3b",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "f5b075e9-d936-4542-9572-0cda0388c1fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "f1502184-b3ea-449f-b838-c3de2b030518",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5b075e9-d936-4542-9572-0cda0388c1fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cf38b34-aee5-4afa-af1a-64f938898573",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5b075e9-d936-4542-9572-0cda0388c1fe",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "1a6d0a21-0000-4ce8-b911-b30a4267d5da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "138f4d5b-0374-426a-8881-a7788792b9c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a6d0a21-0000-4ce8-b911-b30a4267d5da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f47a822b-2aa5-4b6c-b41c-573da1ba5b46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a6d0a21-0000-4ce8-b911-b30a4267d5da",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "59fed596-fe0d-4519-ad79-6f378b5b92e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "446a84c4-f5f6-4544-bf2d-38a199a84650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59fed596-fe0d-4519-ad79-6f378b5b92e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4dbd190-d3dd-4901-a64f-92e88ecf32a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59fed596-fe0d-4519-ad79-6f378b5b92e9",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "daf2f450-ee55-437c-98c4-6d0bd5113673",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "541e0fca-2dc1-4922-bdcf-fc2c2238d61b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daf2f450-ee55-437c-98c4-6d0bd5113673",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "708e5c95-a6c6-474c-bfdb-ec89f16ea6e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daf2f450-ee55-437c-98c4-6d0bd5113673",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "b62eea0e-dce9-405b-9c6d-0e37bbdc1c5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "55b36fb3-69a0-43fe-891f-6a453a35db0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b62eea0e-dce9-405b-9c6d-0e37bbdc1c5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55d2ec4c-ac67-4cd0-920a-b244e32993d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b62eea0e-dce9-405b-9c6d-0e37bbdc1c5f",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "d1dd2146-d446-4dfb-b842-0d141eb26e7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "fe8297ec-93c1-470c-9aff-b42d558c602b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1dd2146-d446-4dfb-b842-0d141eb26e7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "889de0d9-5eee-48a9-9c65-22cd06c34e3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1dd2146-d446-4dfb-b842-0d141eb26e7a",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5909c22d-2386-4122-b2ff-46f9e842cd30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "3b39cbc4-afc4-401e-b493-49f84b1d8bfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5909c22d-2386-4122-b2ff-46f9e842cd30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ba0fbb5-4d27-4614-a9a4-91b7362e7c99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5909c22d-2386-4122-b2ff-46f9e842cd30",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "596eef2c-0f54-4f3d-97c9-089d8292d46c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "a06350b7-ac06-48d0-baa7-0a7b1d879eda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "596eef2c-0f54-4f3d-97c9-089d8292d46c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8a919bf-ba7c-4e0b-b968-f5a5eea169dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "596eef2c-0f54-4f3d-97c9-089d8292d46c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "d9bc59cc-d9f3-4022-bdb6-4f0cd77bf9e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "5ff721cc-8e47-4442-b0d6-d2b9fb7130e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9bc59cc-d9f3-4022-bdb6-4f0cd77bf9e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ead5d512-4d8b-4166-b587-590ba4faa78b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9bc59cc-d9f3-4022-bdb6-4f0cd77bf9e9",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "30997d3c-1c9a-4a00-b62a-b5f1183f032c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "24d4b998-f09e-458b-9257-60c9385c7beb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30997d3c-1c9a-4a00-b62a-b5f1183f032c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "836c77b9-4e61-46a8-a27a-66dc632a001a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30997d3c-1c9a-4a00-b62a-b5f1183f032c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "2c6e1d2b-b56b-403f-83ba-32257589b844",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "2f089365-8938-4550-8d2f-a66f3fdde2f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c6e1d2b-b56b-403f-83ba-32257589b844",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "750d2367-59d2-4ffd-b101-5d15f7265275",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c6e1d2b-b56b-403f-83ba-32257589b844",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "d9ad6d5a-57c0-4290-8925-94430dc7de7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "44dc87f8-400e-4838-8127-40f43e95a2e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9ad6d5a-57c0-4290-8925-94430dc7de7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9841a33-a05c-4c1f-a521-68bf62839a73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9ad6d5a-57c0-4290-8925-94430dc7de7a",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "b237f9c9-1569-4bd4-9c52-539a30b62768",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "f7220bf2-4054-46b2-9b81-b1a4a88719de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b237f9c9-1569-4bd4-9c52-539a30b62768",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d223f3e-26b4-4dc2-add2-876e7e803a6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b237f9c9-1569-4bd4-9c52-539a30b62768",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "de527678-c87c-488a-bd6f-8069c57324cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "c4a6c894-daba-454b-9ef4-d8d6496ca31f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de527678-c87c-488a-bd6f-8069c57324cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a685c1a3-6d55-408d-905d-d31167a0d78d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de527678-c87c-488a-bd6f-8069c57324cc",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "1bec9e6c-fd38-4a46-bbb4-8f9958f72ca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "d9fa3817-eee0-4a63-a4bd-7f54d8a4046e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bec9e6c-fd38-4a46-bbb4-8f9958f72ca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7625d476-6b0f-4cab-b4e9-a55ab052a693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bec9e6c-fd38-4a46-bbb4-8f9958f72ca8",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "37b91dc0-b7c8-44a5-bdd9-0bbcea913137",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "6d5b9fe8-8aa5-4f1b-9825-cdb4f1e31074",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37b91dc0-b7c8-44a5-bdd9-0bbcea913137",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d77fc44-4bd9-464c-917a-b2941ba3ae32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37b91dc0-b7c8-44a5-bdd9-0bbcea913137",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "81248dd5-fde2-4248-9265-7ecf2d424339",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "7f2ee1f0-73e6-4e66-8d7e-f10f66792f05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81248dd5-fde2-4248-9265-7ecf2d424339",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f640e5ff-3981-4362-8071-a18be69edfd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81248dd5-fde2-4248-9265-7ecf2d424339",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "ffbbbefd-3b11-4c9c-a3c7-9484e3202354",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "a5a8e941-b359-4f74-8433-7bef43b5cb90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffbbbefd-3b11-4c9c-a3c7-9484e3202354",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a75db7f-eb82-4a36-8ea3-66cb81fd9b1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffbbbefd-3b11-4c9c-a3c7-9484e3202354",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "35c34874-1713-4951-ae3f-4a5987cf3e9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "f0eb1bda-9e20-4ba4-88d1-ed71568f508e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35c34874-1713-4951-ae3f-4a5987cf3e9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96703498-b90e-4d58-a9f0-5ecacd1efbf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35c34874-1713-4951-ae3f-4a5987cf3e9b",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "590e8181-12ac-4cca-94a5-859c68f4ed6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "03e9a669-f700-48c8-9f63-c189a1d0ae96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "590e8181-12ac-4cca-94a5-859c68f4ed6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e71093ba-0e57-495e-bbf2-7cf60b4e1d0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "590e8181-12ac-4cca-94a5-859c68f4ed6a",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "0ebe7832-0e75-4dad-b1f7-770413af2dbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "360508e4-3057-42da-bcfd-e3ee816535f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ebe7832-0e75-4dad-b1f7-770413af2dbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac176c67-50f1-4b5f-bd34-8dc475913a37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ebe7832-0e75-4dad-b1f7-770413af2dbb",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "f225aba5-efbd-4bfd-9012-1497a206753b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "ad5ac890-d584-4ccb-b5b1-43cae96a9aea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f225aba5-efbd-4bfd-9012-1497a206753b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6eca7da-abac-4f21-8089-2430aaee8bea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f225aba5-efbd-4bfd-9012-1497a206753b",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "ca6155de-f69d-4cd4-a923-af6729e56609",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "ba27c8a7-284c-4f91-b4db-39b3b0fe2aef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca6155de-f69d-4cd4-a923-af6729e56609",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01143228-2709-46d8-832e-988891636896",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca6155de-f69d-4cd4-a923-af6729e56609",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5bbfd144-a6a8-4ba3-bb02-b94c6ccafcf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "2c3733bb-ffba-4057-8405-034b4adcaefd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bbfd144-a6a8-4ba3-bb02-b94c6ccafcf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85272bbc-db4c-4447-94fa-29afcdc1de0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bbfd144-a6a8-4ba3-bb02-b94c6ccafcf7",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "f4f1a0a4-2b6a-4cde-b70d-ff803eae1ea1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "86f1553a-dfbe-43d5-a22a-379ef9050efb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4f1a0a4-2b6a-4cde-b70d-ff803eae1ea1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aefd4050-2190-4809-89f5-68fd41331cb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4f1a0a4-2b6a-4cde-b70d-ff803eae1ea1",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "66fc9dfb-2b92-4743-b377-b7b088c3cd87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "f6ceebf7-97e1-43d1-994b-dde40e5eecab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66fc9dfb-2b92-4743-b377-b7b088c3cd87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14411f6f-bf79-44cb-85f5-2a0d1b1aa072",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66fc9dfb-2b92-4743-b377-b7b088c3cd87",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "923e188e-c4f6-42cc-a29c-408f721e0402",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "de6bab26-d516-4564-9b86-82d5343dae67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "923e188e-c4f6-42cc-a29c-408f721e0402",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b832ab02-4df2-46b3-9457-58f8ac82e4e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "923e188e-c4f6-42cc-a29c-408f721e0402",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "f155fb59-28d9-4ab9-9eb3-7f9d321e79aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "b3509f82-0875-478c-962b-2352bac66344",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f155fb59-28d9-4ab9-9eb3-7f9d321e79aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "092ee318-8bc1-4c3a-8f70-ea0a6481478b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f155fb59-28d9-4ab9-9eb3-7f9d321e79aa",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "e5797d1f-14b7-4937-84d6-2a1d006bd0fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "8b19fb0b-b0f3-4ec7-8162-c0592233bc01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5797d1f-14b7-4937-84d6-2a1d006bd0fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7593f1ce-fe05-485b-8560-230f2d9ad98c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5797d1f-14b7-4937-84d6-2a1d006bd0fc",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "20ac7850-3d31-435a-9539-e195c1baac2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "2ab70326-1c63-4ee0-912b-a5c3c1142cca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20ac7850-3d31-435a-9539-e195c1baac2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c900add8-452e-4aec-a7b8-587e2fbffba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20ac7850-3d31-435a-9539-e195c1baac2d",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "13d5297e-d56b-4ad3-8830-656d82478a0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "4cce39d1-5d63-4d9c-8952-eb9666ac725e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13d5297e-d56b-4ad3-8830-656d82478a0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00ea3025-1a26-4fba-8887-2f92a1c1c34a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13d5297e-d56b-4ad3-8830-656d82478a0c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "55363b77-8a56-40a1-bde7-938f032a9f1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "6a520957-05cb-4fce-a10d-370a6702f9b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55363b77-8a56-40a1-bde7-938f032a9f1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f623009-017b-4bfb-acf9-495d2fb58382",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55363b77-8a56-40a1-bde7-938f032a9f1a",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "89c64aae-8124-46e9-bc07-c2825ff890be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "b699eb88-b04b-49b3-9445-e40da9a5b661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89c64aae-8124-46e9-bc07-c2825ff890be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9887701e-35ea-4974-8112-f76009b44dc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89c64aae-8124-46e9-bc07-c2825ff890be",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "9800fe60-c11c-48b4-b464-8ed205bb2625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "76fe92fa-5c37-4bc1-9b44-0c20bc18fab3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9800fe60-c11c-48b4-b464-8ed205bb2625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9ff54d5-e47c-4448-9024-fc778ac90849",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9800fe60-c11c-48b4-b464-8ed205bb2625",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "93fd692c-7503-4b28-98f9-22d19c6cc696",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "8d479540-9115-4ecc-85be-4a76c0d6cfca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93fd692c-7503-4b28-98f9-22d19c6cc696",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95c3222f-391f-4dde-90d6-ea3d0fbac18e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93fd692c-7503-4b28-98f9-22d19c6cc696",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "72ccd802-c12a-4cbb-8265-4984bcc5f4c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "53ced9d3-d433-42dd-909c-131238cb975e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72ccd802-c12a-4cbb-8265-4984bcc5f4c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "693e2eb7-6594-4861-8aba-637209e29648",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72ccd802-c12a-4cbb-8265-4984bcc5f4c6",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "b21c7849-cc38-4f4b-8a21-fd82e7e37ff6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "151accad-8a86-48ad-8e88-1d9f8f9ff06a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b21c7849-cc38-4f4b-8a21-fd82e7e37ff6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24f868d1-44e5-44e2-85fd-b4d265371a34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b21c7849-cc38-4f4b-8a21-fd82e7e37ff6",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "3f01575e-cb71-4ece-84ab-fb48e21b2b62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "65db54a9-c965-4539-830f-b961696a7d1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f01575e-cb71-4ece-84ab-fb48e21b2b62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "096784f9-4586-4638-a5aa-b313baf856b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f01575e-cb71-4ece-84ab-fb48e21b2b62",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5cb6890b-b501-4a51-ac3c-38196dca45bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "1f4c1f7c-191d-4d11-b549-eab339a9e5e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cb6890b-b501-4a51-ac3c-38196dca45bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b78682fc-7fe3-475b-a80b-bd32836c08f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cb6890b-b501-4a51-ac3c-38196dca45bb",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "2a706b90-c412-4230-98e6-6a289965eeac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "9ed04362-5b97-4e85-93db-b984fbb59b15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a706b90-c412-4230-98e6-6a289965eeac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "375dd966-626a-441d-b9e9-f124d8149e1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a706b90-c412-4230-98e6-6a289965eeac",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "b1df378b-abbd-4c6b-91d9-0793a4884617",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "a78e6508-e1e2-4969-a7bb-acd33f656d10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1df378b-abbd-4c6b-91d9-0793a4884617",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3227d943-0c5d-4f1e-84df-5794c7d242d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1df378b-abbd-4c6b-91d9-0793a4884617",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "9e5abde8-ac69-4fd5-b42a-6b5bf69b8cd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "7ddddf35-c447-4a8a-b5be-c082fd959933",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e5abde8-ac69-4fd5-b42a-6b5bf69b8cd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d6a86d0-aa76-4969-b1d1-c3dd377a4aeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e5abde8-ac69-4fd5-b42a-6b5bf69b8cd7",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "bc327bad-e149-4c05-ae31-bab2613aa2b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "58ec913c-bbc4-4df7-96c1-2dffc7907074",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc327bad-e149-4c05-ae31-bab2613aa2b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "289ca080-3fbb-42d6-bd5c-ca49c020380c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc327bad-e149-4c05-ae31-bab2613aa2b3",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "fde3375b-8719-4cf9-bd6b-811434607c61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "3898489d-40c7-4e99-98d3-25165078a9de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fde3375b-8719-4cf9-bd6b-811434607c61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34d8796f-e508-4182-ac1a-a70484f016f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fde3375b-8719-4cf9-bd6b-811434607c61",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "61cdc6d4-5169-41f0-b18c-fff2d183a394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "bbca60d3-7493-4d59-bf39-78a7fb236120",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61cdc6d4-5169-41f0-b18c-fff2d183a394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d38e58cb-8e56-41e4-bd48-ef2630477fe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61cdc6d4-5169-41f0-b18c-fff2d183a394",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "c66b7f76-7a4e-4fa4-a22d-ca7ffaa48e40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "74db2271-db2e-4a01-b7a1-8d82e9033426",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c66b7f76-7a4e-4fa4-a22d-ca7ffaa48e40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2aa40b47-eed8-4d42-82a5-058104acc341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c66b7f76-7a4e-4fa4-a22d-ca7ffaa48e40",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "d71eb495-bf1b-4fa0-ba28-fd18a19e245f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "089e297e-a552-46af-b61c-c9436bacadd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d71eb495-bf1b-4fa0-ba28-fd18a19e245f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c3f500c-752e-4a5d-b2bf-a7b29486cbc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d71eb495-bf1b-4fa0-ba28-fd18a19e245f",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "3db20ad1-e424-45c6-ac3d-f23d11622aa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "48191401-36d0-45a9-bb1e-3bd18492387c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3db20ad1-e424-45c6-ac3d-f23d11622aa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20080b8c-9dae-4da7-ab77-90ed01ee6bb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3db20ad1-e424-45c6-ac3d-f23d11622aa2",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "328f2e8c-0847-40cb-8bde-2d7b4b277755",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "28a3afb8-09f0-4966-839b-8d7a899fdf46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "328f2e8c-0847-40cb-8bde-2d7b4b277755",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bc0a019-8ab6-4672-930d-60c5201ceffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "328f2e8c-0847-40cb-8bde-2d7b4b277755",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "f9b32497-e0c2-4ff0-9b21-b4f083232839",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "a92de303-73bd-4fce-86e0-f49c27d82e7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9b32497-e0c2-4ff0-9b21-b4f083232839",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "662c85a8-3de9-49f0-bf01-f4362e7b8816",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9b32497-e0c2-4ff0-9b21-b4f083232839",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5c5562df-eb58-457c-a915-e19ab583b3a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "d57867e4-44b2-4c34-8336-c3c7476526fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c5562df-eb58-457c-a915-e19ab583b3a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78070c87-8c82-4d0a-8098-acec45aa52e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c5562df-eb58-457c-a915-e19ab583b3a6",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "b74dc122-b8da-41b7-96ae-c40d6776a01a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "d189273d-737f-4456-a304-bab08e204087",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b74dc122-b8da-41b7-96ae-c40d6776a01a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "961816b3-7a1b-4b02-afdd-b58ed96d7fdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b74dc122-b8da-41b7-96ae-c40d6776a01a",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "fd98b0be-1be8-4880-aa4a-1aea775920f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "ed7e2742-fa83-4d0d-ba80-75a1fad15b0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd98b0be-1be8-4880-aa4a-1aea775920f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "447fabba-3df9-477b-8153-87ecf8fe000f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd98b0be-1be8-4880-aa4a-1aea775920f1",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "311ddad6-7583-484b-ac8b-6e440a3ba002",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "fa15b8be-65de-4c86-a233-f0f4835a4415",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "311ddad6-7583-484b-ac8b-6e440a3ba002",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6b920f3-a7b8-473d-9e6c-fa27289b110d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "311ddad6-7583-484b-ac8b-6e440a3ba002",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "374273a0-fe88-4495-b3f7-16fe0fc45f7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "44bc77e7-a540-4d04-af99-24d863c8c50f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "374273a0-fe88-4495-b3f7-16fe0fc45f7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7affb502-62b5-4fcd-80ce-5a382783810d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "374273a0-fe88-4495-b3f7-16fe0fc45f7b",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "9e8d44a4-cf43-4345-a647-be8fa697a669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "aeaed350-a7fd-4cac-bc22-a219dd5dff61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e8d44a4-cf43-4345-a647-be8fa697a669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f8195f2-ae0d-4fa4-8fef-d5ce726850dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e8d44a4-cf43-4345-a647-be8fa697a669",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "845a569f-febb-41b5-81c8-3a9a802322c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "c710c564-1b0b-4234-93d4-7a14984f1062",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "845a569f-febb-41b5-81c8-3a9a802322c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "251f78af-b086-4f00-8b25-0c152f9d2e98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "845a569f-febb-41b5-81c8-3a9a802322c4",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "a68e7591-949d-4043-8909-c2aeaa277a1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "1d7874b2-d534-478c-96ad-dd9e7826722f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a68e7591-949d-4043-8909-c2aeaa277a1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f45e6533-7e6c-4bcc-bd74-b44e06d8e572",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a68e7591-949d-4043-8909-c2aeaa277a1b",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "b832f99f-c37a-411a-9368-0bc97c04ab25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "c984d19e-ab83-4833-988e-6789f9d2dd8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b832f99f-c37a-411a-9368-0bc97c04ab25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dfb8a83-20c5-430f-8194-06e372fc7b4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b832f99f-c37a-411a-9368-0bc97c04ab25",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "b3455578-c24b-497e-99f1-0b38314c67e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "7a6f7dd5-a91c-4078-93cf-acb267ff0985",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3455578-c24b-497e-99f1-0b38314c67e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "835a0655-0ee3-462e-9abe-222e7cabff49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3455578-c24b-497e-99f1-0b38314c67e9",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "b2692c34-2e3c-43a2-a68f-61d1ef074bd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "7fd70b70-b486-4bb3-a11f-2ab0fd227c57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2692c34-2e3c-43a2-a68f-61d1ef074bd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cec3a9c4-c512-4a49-880b-9d7309daf18d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2692c34-2e3c-43a2-a68f-61d1ef074bd6",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "928eb05d-8b84-438f-8e4d-7ff202b45322",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "4db6e8f4-dee6-4967-a177-0c382b904a9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "928eb05d-8b84-438f-8e4d-7ff202b45322",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc2b5552-d4eb-4b90-a895-fb868bd066c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "928eb05d-8b84-438f-8e4d-7ff202b45322",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "f5794ff1-a789-4894-8a0d-858ffefab9d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "71a07af3-d43b-48af-905d-ca2cec994a78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5794ff1-a789-4894-8a0d-858ffefab9d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68fb7ff6-382f-44f0-9576-b87ca4eaca8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5794ff1-a789-4894-8a0d-858ffefab9d0",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "9dca5169-49e0-4cd7-8bc5-9b482c3c2f8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "6da22ed1-c5b7-48df-9bcc-a46d0411ba04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dca5169-49e0-4cd7-8bc5-9b482c3c2f8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57a0cc72-55e1-4053-9db3-4f012ac57ee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dca5169-49e0-4cd7-8bc5-9b482c3c2f8e",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "4cf19db7-3ef6-4351-af5f-f3985d0e6362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "23abbe93-5757-4083-8228-86cffc73d583",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cf19db7-3ef6-4351-af5f-f3985d0e6362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e1759d4-83d9-4b8f-96ab-4cd0c620ace8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cf19db7-3ef6-4351-af5f-f3985d0e6362",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "f4e7cb18-bc07-47cf-8bb9-8cca33cc54d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "a166975c-b219-42f1-8a8e-c0ac72865ad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4e7cb18-bc07-47cf-8bb9-8cca33cc54d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c8d58f7-6722-4223-922b-575b7e22276d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4e7cb18-bc07-47cf-8bb9-8cca33cc54d9",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "77204ad9-1669-4721-b0c9-7a9406838cb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "dce34c4b-acdb-4ede-94ba-6d5a32940be6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77204ad9-1669-4721-b0c9-7a9406838cb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "debe0cab-9f70-4c0b-81cb-26d05e294d62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77204ad9-1669-4721-b0c9-7a9406838cb7",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "bb960fb2-4277-4be1-af99-90ff48b8f6ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "0fa57a62-a0c1-4db0-a293-ee05db606f21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb960fb2-4277-4be1-af99-90ff48b8f6ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a11ce8b6-315e-4894-b521-86a4a7c50441",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb960fb2-4277-4be1-af99-90ff48b8f6ae",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "e8aef863-8aeb-4d08-8655-64b426ed6553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "011e37b7-dae2-487c-8326-3948f774a688",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8aef863-8aeb-4d08-8655-64b426ed6553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6f04c74-59f4-42dc-b9c8-a10e61818e96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8aef863-8aeb-4d08-8655-64b426ed6553",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "d1c7bcf2-4f9e-4c94-98eb-380517a2ec87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "25a80727-3a08-400d-901d-dc26d8ed6f95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1c7bcf2-4f9e-4c94-98eb-380517a2ec87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd25ddb5-2a2a-4c12-bab9-8cce4f460b8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1c7bcf2-4f9e-4c94-98eb-380517a2ec87",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "2b9a079d-bb84-4e12-91d6-c2f964a08381",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "d1f4ea28-c81e-4df2-bac2-c1c8dfea8317",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b9a079d-bb84-4e12-91d6-c2f964a08381",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04a1ad97-d479-4257-8d67-090bc9adc651",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b9a079d-bb84-4e12-91d6-c2f964a08381",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "efc07c12-420f-45f3-b64f-87e6a88c9027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "bb726c5b-7dad-442b-92c6-4ca3c3b81fec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efc07c12-420f-45f3-b64f-87e6a88c9027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1def8e0b-82b3-48b3-a4fb-c62be2ed524f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efc07c12-420f-45f3-b64f-87e6a88c9027",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "92019c7d-9915-45c7-918d-598f2fe21d9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "ef1c7df7-011e-45b0-8f7b-5d0abf265e1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92019c7d-9915-45c7-918d-598f2fe21d9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9309692e-7799-4d6f-9cb9-0ce533f4820b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92019c7d-9915-45c7-918d-598f2fe21d9b",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "05040b00-0816-43ca-960d-1c36ffb4a3b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "e0bb0d40-3a2d-4927-958c-b7e8b22cc4af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05040b00-0816-43ca-960d-1c36ffb4a3b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d6c6c07-e8c0-4b9c-bd01-ebbd4a18888b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05040b00-0816-43ca-960d-1c36ffb4a3b4",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "a8a363fc-b59e-41ea-a35b-c74a30c1c40a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "7d53a547-3bb3-4147-a1a4-55e825c84ff8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8a363fc-b59e-41ea-a35b-c74a30c1c40a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a55564e-abbd-4481-8839-4be52c99af88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8a363fc-b59e-41ea-a35b-c74a30c1c40a",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "7a1e8274-cd67-41f4-a9c5-dbf4c5d7b449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "69c0eacb-16bf-4d51-a399-bd030577da7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a1e8274-cd67-41f4-a9c5-dbf4c5d7b449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a495470a-7a14-4978-90b7-334b6dbc8d78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a1e8274-cd67-41f4-a9c5-dbf4c5d7b449",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "d940392c-8da1-449a-9b8d-fed45702d69c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "4ee064b2-effa-46fb-9e39-3f86e811b963",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d940392c-8da1-449a-9b8d-fed45702d69c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8740e3c-2879-458e-bb77-0ae0ae0bd1a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d940392c-8da1-449a-9b8d-fed45702d69c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "3f95a084-37ca-46ee-b291-494cdb12823a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "7d52d158-7d6d-48b3-91a1-72692ceece2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f95a084-37ca-46ee-b291-494cdb12823a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5ec8728-2d1c-4dc5-b413-7523232dffd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f95a084-37ca-46ee-b291-494cdb12823a",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "b01d054d-613e-4195-9fa9-52a50d27c47c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "f3dec5ad-7ced-42b0-a795-55fbd36f4f81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b01d054d-613e-4195-9fa9-52a50d27c47c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d68eb8d4-6b73-4818-9453-054e73aa41a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b01d054d-613e-4195-9fa9-52a50d27c47c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "88458094-f049-4502-93d5-9afa44f79bc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "508c1b4f-0d66-4654-a4dd-a1e7dac39a30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88458094-f049-4502-93d5-9afa44f79bc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f2f9deb-7723-4286-b2d7-91996ac6f66c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88458094-f049-4502-93d5-9afa44f79bc8",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5e7b15f1-cc55-4662-81a6-8b3fe4874f68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "2bcaa7dc-8e79-4d92-905a-932a0f41cd3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e7b15f1-cc55-4662-81a6-8b3fe4874f68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8788de0-5785-4476-8144-9d9415a7e173",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e7b15f1-cc55-4662-81a6-8b3fe4874f68",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "4c4e6b38-09c2-4296-b19d-0af6939c4da8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "c4cc0c28-97fe-42eb-b79c-8f857d6ff61a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c4e6b38-09c2-4296-b19d-0af6939c4da8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8952e6ef-3d7d-42fc-b791-172c4bc0404e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c4e6b38-09c2-4296-b19d-0af6939c4da8",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "26cf1fde-3237-4455-b5fd-89bee0a5bf95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "ead430bd-98e4-482b-a6bc-b71cb62ec00f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26cf1fde-3237-4455-b5fd-89bee0a5bf95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb345ad4-24df-4d62-af4d-2d91b33aa176",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26cf1fde-3237-4455-b5fd-89bee0a5bf95",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "57a54522-8c4e-4399-a6bd-080a71a4f7b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "8c5f43f5-b5aa-49df-9f14-eac77144778a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57a54522-8c4e-4399-a6bd-080a71a4f7b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4596600-9e82-4390-a129-7e835981f2a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57a54522-8c4e-4399-a6bd-080a71a4f7b7",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "d7c1e7c8-d678-4807-8415-adc8dc73f68d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "b78b9f12-b2e9-4e95-a457-18b0ac5ed664",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7c1e7c8-d678-4807-8415-adc8dc73f68d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "504d0580-cdf8-4951-92bf-72cb6ef7df84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7c1e7c8-d678-4807-8415-adc8dc73f68d",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "21d6eafe-f31a-43ea-9f20-1ec2c476d452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "216a0dbf-c2de-4396-aacc-05d511888de7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21d6eafe-f31a-43ea-9f20-1ec2c476d452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbc7ec51-32ff-4f76-9648-697ab2b79267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21d6eafe-f31a-43ea-9f20-1ec2c476d452",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "859c39f5-09e6-49c3-9c02-8a4cf2c58acd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "cf0c2069-c76b-48e2-bf15-b62d8ea24718",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "859c39f5-09e6-49c3-9c02-8a4cf2c58acd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9250db2-de97-456f-a384-15ccd73822d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "859c39f5-09e6-49c3-9c02-8a4cf2c58acd",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "ba19cc4c-3b2f-41fd-81a2-a283e01b3746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "32c73c91-e989-4a10-89b6-4d2bfb244bcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba19cc4c-3b2f-41fd-81a2-a283e01b3746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9281cc30-ee3d-4cc6-b0bb-180f0d6aeca5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba19cc4c-3b2f-41fd-81a2-a283e01b3746",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5a75c89c-90cc-419f-9ce4-14ce5617ead8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "f1c81f94-070b-42de-b11a-7f966ac82461",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a75c89c-90cc-419f-9ce4-14ce5617ead8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b995338-75e2-41c1-9916-c78d541df53b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a75c89c-90cc-419f-9ce4-14ce5617ead8",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "ee211ee6-527a-4ce5-a8cd-bfab9a66cc22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "8d370409-0c37-4d3e-9dc3-aa0d57fef15f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee211ee6-527a-4ce5-a8cd-bfab9a66cc22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "463e210c-5847-4d31-bc35-c6291aa9bdee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee211ee6-527a-4ce5-a8cd-bfab9a66cc22",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "fed2f4f4-8e89-4fc1-84f7-5edf926b4cb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "e1f81960-9886-4dad-b3b6-b953ef1fa050",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fed2f4f4-8e89-4fc1-84f7-5edf926b4cb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d12e550-3ee2-4a1b-b7c9-62641cf795a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fed2f4f4-8e89-4fc1-84f7-5edf926b4cb7",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "d0d5ec33-355c-42be-9b10-742bc7a2dd53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "cf9c5cf8-1f56-48f7-98ab-59cb9a65142e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0d5ec33-355c-42be-9b10-742bc7a2dd53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ac77fc9-6d22-4ee8-a183-eb9b38f68880",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0d5ec33-355c-42be-9b10-742bc7a2dd53",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "1d63c704-391f-4f5e-acaa-9ed14afffed3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "17ce140a-f4d1-4202-939e-01a0b356ee32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d63c704-391f-4f5e-acaa-9ed14afffed3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf032cf6-c26b-49d4-94ca-8456c78760c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d63c704-391f-4f5e-acaa-9ed14afffed3",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "14c9ca4a-1175-43e2-92b0-ecd9c1a16478",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "6eb30afe-9090-462f-bf2d-2bbe075442bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14c9ca4a-1175-43e2-92b0-ecd9c1a16478",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11e062b5-526f-47b5-b3f1-5f02a3ef6bd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14c9ca4a-1175-43e2-92b0-ecd9c1a16478",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "612788da-f19f-4793-a615-f1e0f0010351",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "86fd0176-6854-4cc9-b5d8-e68a2b8a9989",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "612788da-f19f-4793-a615-f1e0f0010351",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac8b4136-422a-457d-a29e-13e54a3d13ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "612788da-f19f-4793-a615-f1e0f0010351",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "da97adec-bca9-46fe-9b00-d2b09b93e0ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "4f5b9055-dc82-4e67-9caa-60b957018aaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da97adec-bca9-46fe-9b00-d2b09b93e0ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "584027f6-b9b1-4b7c-b2ed-58efc27d4016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da97adec-bca9-46fe-9b00-d2b09b93e0ad",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "c694a176-8e48-4c43-a1bb-3f407b7e931e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "249ec986-f6d0-496f-8403-961f56b01057",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c694a176-8e48-4c43-a1bb-3f407b7e931e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed0c0490-bfdd-4bcd-b7d4-aed41b643e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c694a176-8e48-4c43-a1bb-3f407b7e931e",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "768d5364-47ee-48c9-a683-473b90f6efad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "e60550be-7faa-4a20-9f66-2be8070d8df1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "768d5364-47ee-48c9-a683-473b90f6efad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d231ee78-e7e5-4693-a620-e33b020dc1f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "768d5364-47ee-48c9-a683-473b90f6efad",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "927bb9c6-8c65-4588-99f2-a024a9ba98fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "aae5ca6f-449d-4a23-9082-ce9621243d9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "927bb9c6-8c65-4588-99f2-a024a9ba98fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83573bf0-09e8-4d72-9029-a303e5ce1fcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "927bb9c6-8c65-4588-99f2-a024a9ba98fd",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "e74b47dd-e3ad-4646-90e6-2d6765457d2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "a595bc52-8779-4796-bb4e-53a5a3e019fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e74b47dd-e3ad-4646-90e6-2d6765457d2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27069854-eb7b-4e5e-af30-8e0b4ed2ab50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e74b47dd-e3ad-4646-90e6-2d6765457d2e",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "e621c057-e63a-4c7c-b505-89554f01b761",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "342dbbcf-472a-4da2-b954-88b0010922f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e621c057-e63a-4c7c-b505-89554f01b761",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "645220e9-b99a-4895-bd7c-605e15dc63fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e621c057-e63a-4c7c-b505-89554f01b761",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "8248e05f-ed45-4abb-91da-05aca88ea8c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "3a0efbf0-2f52-4863-9d9b-dc270e9f1543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8248e05f-ed45-4abb-91da-05aca88ea8c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "585014c1-7ae7-42f4-9163-99106a9f38f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8248e05f-ed45-4abb-91da-05aca88ea8c3",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "4c5e6141-1c82-4a1d-ae80-a465364b3ebf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "735244c2-4884-4f12-8dbb-c1b275f233d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c5e6141-1c82-4a1d-ae80-a465364b3ebf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "490e9054-7261-437e-be4c-ed5833084669",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c5e6141-1c82-4a1d-ae80-a465364b3ebf",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "d74e5e87-81a0-4692-b223-4257cc1417a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "721b52ae-c203-4c26-b89a-6f238748f3c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d74e5e87-81a0-4692-b223-4257cc1417a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3373df7e-faea-467c-9aa4-a238ac921b75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d74e5e87-81a0-4692-b223-4257cc1417a1",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "9352f135-596a-494f-8e04-db7948c24d3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "bd60970c-f5e5-4ca0-9651-c07016928305",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9352f135-596a-494f-8e04-db7948c24d3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc0d0608-eb5c-4797-a5c0-59354db3fb75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9352f135-596a-494f-8e04-db7948c24d3d",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5be8cc1b-cc0f-4fcc-b9d8-81802420b8aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "480251ac-d247-4809-97b9-e3ad1d8e0cd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5be8cc1b-cc0f-4fcc-b9d8-81802420b8aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04102aed-f40b-43e3-a622-a5bce3eec110",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5be8cc1b-cc0f-4fcc-b9d8-81802420b8aa",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "30ba2e3c-551e-42e5-8134-b6850265f005",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "ecb1f499-a98c-4027-b498-3d671cd5620c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30ba2e3c-551e-42e5-8134-b6850265f005",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35b18b12-0803-4fc4-983a-826ae3606b53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30ba2e3c-551e-42e5-8134-b6850265f005",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "bcb36643-6f1a-446f-9d72-7223de69aaed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "c665726b-0759-430a-931b-14308d6a08e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcb36643-6f1a-446f-9d72-7223de69aaed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39320ee0-b93d-4931-8c69-c77b8913502e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcb36643-6f1a-446f-9d72-7223de69aaed",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "a597e688-a872-4c13-adb3-4205c798fe50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "1364cbec-4b15-437a-a435-4cf78f2221ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a597e688-a872-4c13-adb3-4205c798fe50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7b22a16-eb49-4376-a1c4-c432df0d63f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a597e688-a872-4c13-adb3-4205c798fe50",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "158e6105-2e16-4466-b13e-136332628979",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "dff3bf74-3b0c-459b-b5ba-7c2cb66f8294",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "158e6105-2e16-4466-b13e-136332628979",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbda92eb-b2f5-43ee-8866-c090808dfc3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "158e6105-2e16-4466-b13e-136332628979",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5937383c-d254-4c86-aa8e-3b3c4b0c12ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "22f9ae53-8d64-42d5-85ac-90ab576cfb6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5937383c-d254-4c86-aa8e-3b3c4b0c12ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89078bbe-2958-4f4b-9509-28f11e96aba9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5937383c-d254-4c86-aa8e-3b3c4b0c12ac",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "338dfd7e-fb36-4735-b68c-098d9adb98c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "09170b84-e0ce-4f62-8f22-c25c99605ce8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "338dfd7e-fb36-4735-b68c-098d9adb98c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ace10b5c-2c86-4944-a125-ff5a7fadaf76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "338dfd7e-fb36-4735-b68c-098d9adb98c3",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "299b8a72-fcc2-4127-bcb2-f0dadce03939",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "a90ad40c-c200-4347-a582-24dffc5d629e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "299b8a72-fcc2-4127-bcb2-f0dadce03939",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba9d11a9-896b-414c-a289-e9fd2a0d3fd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "299b8a72-fcc2-4127-bcb2-f0dadce03939",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "b44c2505-d391-4074-8bb8-ff62dab2bc45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "f15d11ef-f7b7-4a74-95ed-ad021a0ea400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b44c2505-d391-4074-8bb8-ff62dab2bc45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b36eacbd-8ac0-4cef-bf9e-d08b996b7ab9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b44c2505-d391-4074-8bb8-ff62dab2bc45",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "270093d3-605b-486b-8fa5-a0641ddcb817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "2d9b2ad5-279d-4b06-9d49-92156fe71457",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "270093d3-605b-486b-8fa5-a0641ddcb817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a70e22a-c963-41b5-aec4-b7e64b085841",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "270093d3-605b-486b-8fa5-a0641ddcb817",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "41e55b28-b103-4bce-ad4d-476864523cc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "64369a95-ce51-461b-b741-d38f6d21b526",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41e55b28-b103-4bce-ad4d-476864523cc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5481ed4-d798-4cec-8fe3-58f09e5acaa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41e55b28-b103-4bce-ad4d-476864523cc5",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "6e04bbbd-e748-4780-a28b-f68a1fdef6b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "ad62ff74-d951-402f-a79a-360224724378",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e04bbbd-e748-4780-a28b-f68a1fdef6b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "831a8932-ec72-4895-a0d8-10439ee3f98b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e04bbbd-e748-4780-a28b-f68a1fdef6b7",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "03fd0910-cc07-4d31-99e1-4bae1f4bacda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "8ebd4e11-a70c-4964-b3e3-5ca6bef66e66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03fd0910-cc07-4d31-99e1-4bae1f4bacda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "846203fe-1ed4-461f-87d1-b01d8216ac46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03fd0910-cc07-4d31-99e1-4bae1f4bacda",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "ecefd773-5f42-4f23-8985-c4183abb9cf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "ffd1babc-6811-4c05-8776-8e9c7274d68c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecefd773-5f42-4f23-8985-c4183abb9cf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a54a514e-d451-439b-889e-67c717969425",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecefd773-5f42-4f23-8985-c4183abb9cf0",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5cc010d9-ea69-4987-8c9c-05586be32707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "046281f1-1726-45ee-8e52-cf02c88a00a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cc010d9-ea69-4987-8c9c-05586be32707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9944c4c-d6fb-44a5-a62f-e4ee4fe10afe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cc010d9-ea69-4987-8c9c-05586be32707",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "eff87357-9332-4083-857a-ebb450ceeff1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "8bcc692e-bf78-4428-b6b8-820ae1c19b12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eff87357-9332-4083-857a-ebb450ceeff1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "228d6900-46ed-49c5-82c3-cec63955f9b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eff87357-9332-4083-857a-ebb450ceeff1",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "84c33787-fe6c-489d-bf5d-6aa055bf612c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "09a85a46-70ad-48af-ba15-be0d61ef259b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84c33787-fe6c-489d-bf5d-6aa055bf612c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f75b30cf-3c72-4df9-8f74-0c38f9b78f05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84c33787-fe6c-489d-bf5d-6aa055bf612c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "53322814-dae5-4a68-b380-accd6db6e51c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "71434a63-2dfc-4c48-8021-980da36309c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53322814-dae5-4a68-b380-accd6db6e51c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a7d3716-72f8-46e2-b2e4-a50633255349",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53322814-dae5-4a68-b380-accd6db6e51c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "956ea35f-256c-4662-b72b-63afef54c293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "640cf0d9-bc78-45d7-b6ab-c86b260c3302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "956ea35f-256c-4662-b72b-63afef54c293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0670a5d-4cea-45e1-a5e4-80d7f17c89d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "956ea35f-256c-4662-b72b-63afef54c293",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "61f63371-f9d3-41a8-8fe9-8893d832c5ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "43f26c6d-5997-495e-b54d-af5da0ebf4f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61f63371-f9d3-41a8-8fe9-8893d832c5ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6807084-0ad8-48ad-819a-8adf68ff3b01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61f63371-f9d3-41a8-8fe9-8893d832c5ce",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "09e5334d-f6f8-4449-9663-b3e4b69914a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "db985030-8b58-4892-9d06-ac973024e38b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09e5334d-f6f8-4449-9663-b3e4b69914a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9553520d-5d9a-41f1-99b7-3b7c4886b200",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09e5334d-f6f8-4449-9663-b3e4b69914a5",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "c3458612-2971-4020-8e73-b29e2378c1b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "f16cb769-2b78-4ffa-b1de-4732a0caecb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3458612-2971-4020-8e73-b29e2378c1b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5895a49d-6e4c-458d-b354-6d8b8bb35f3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3458612-2971-4020-8e73-b29e2378c1b4",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "81d224b7-9115-42a8-8fd3-17782a12b9b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "a48aad15-7e16-48e1-917b-c177d6a86f64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81d224b7-9115-42a8-8fd3-17782a12b9b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c46ca78-0001-4218-bdcf-2465fec0efef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81d224b7-9115-42a8-8fd3-17782a12b9b5",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "a2e85db7-6fb4-4299-bc01-356304f0730c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "b58965a1-9c25-47d1-a014-4300f514fb77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2e85db7-6fb4-4299-bc01-356304f0730c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32ac6451-0e6b-42e8-96ac-a54be7b6cd88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2e85db7-6fb4-4299-bc01-356304f0730c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "0ec20c37-a982-48ba-979e-e683f1f4965b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "2b69b99b-f4e5-4e29-972a-b3f1f821bc4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ec20c37-a982-48ba-979e-e683f1f4965b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cae487e2-2cff-43ef-81af-d4ee60c48bb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ec20c37-a982-48ba-979e-e683f1f4965b",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "74167499-4a99-4333-87a7-4ba8d03c4795",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "82e954a4-8536-475d-9117-124deab6e549",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74167499-4a99-4333-87a7-4ba8d03c4795",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7be1dd21-25d8-41f1-b0b4-8582076ef562",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74167499-4a99-4333-87a7-4ba8d03c4795",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5062c591-0fd3-467a-a122-39550863d310",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "20499f1b-0c77-4ac4-9edc-6478dde05ab1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5062c591-0fd3-467a-a122-39550863d310",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19324bbd-1d4b-4026-a349-1847aa60fd09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5062c591-0fd3-467a-a122-39550863d310",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "9e784ae2-5204-4b05-b5a7-bb03cbb5a6a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "354f4bce-ac7f-40f2-a66f-04ed0adcce3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e784ae2-5204-4b05-b5a7-bb03cbb5a6a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4534068-f69d-422e-970f-b0bb9c31fd0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e784ae2-5204-4b05-b5a7-bb03cbb5a6a2",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "b7ee9646-da92-4c85-814d-04eb6d1f2a40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "a7411024-f71a-4092-b587-9f6e6ae06703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7ee9646-da92-4c85-814d-04eb6d1f2a40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "412f7c2a-a08b-4a58-9c03-475d59b12796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7ee9646-da92-4c85-814d-04eb6d1f2a40",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "ac1b5141-cb7b-4cb7-8eab-c02fdd9e0c76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "e5bc071d-6ea2-4109-8e70-1ec4670fbd0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac1b5141-cb7b-4cb7-8eab-c02fdd9e0c76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0af8efa6-06e4-4856-9f51-07b6df7ce984",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac1b5141-cb7b-4cb7-8eab-c02fdd9e0c76",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "7e8ad125-d443-4b24-b018-0577609c73ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "0dcb131d-d1e6-442e-93a2-ade9e1bc3208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e8ad125-d443-4b24-b018-0577609c73ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b6aeb4a-c9b3-4919-a9d5-68b403f2127b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e8ad125-d443-4b24-b018-0577609c73ed",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "39c19232-9dd3-4a45-819f-34fa503fa682",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "5f405d5a-a701-41bf-931e-684d5c285916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39c19232-9dd3-4a45-819f-34fa503fa682",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9737778e-5e58-4b85-8031-2771ca78af18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39c19232-9dd3-4a45-819f-34fa503fa682",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "784293bb-88ff-4331-ba8c-17fa76e5be24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "883e694d-be97-47ce-9c8c-5eb8c1cb8517",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "784293bb-88ff-4331-ba8c-17fa76e5be24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a6b73f9-c126-4e12-b2ed-eb1cd9f1ba73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "784293bb-88ff-4331-ba8c-17fa76e5be24",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "8e2e065a-78ec-4356-9e41-0dbb72ba676f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "82d25dd4-c7cd-4df5-91c2-c4e227af8ec8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e2e065a-78ec-4356-9e41-0dbb72ba676f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d78e6edf-31a8-4d3a-a2e8-57952b21149b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e2e065a-78ec-4356-9e41-0dbb72ba676f",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "244c3ce0-d155-44f1-9ac7-428445da482f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "d987cf41-00a5-4d18-b9dc-c3c273ad30d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "244c3ce0-d155-44f1-9ac7-428445da482f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ce88c73-c69b-496a-8a94-05483fd2cd3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "244c3ce0-d155-44f1-9ac7-428445da482f",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "3d2303d3-58da-4b6b-80a6-ee79864d6291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "d1f64b63-3148-416f-b2f2-b601a0b95eb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d2303d3-58da-4b6b-80a6-ee79864d6291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dcfe8d2-100b-4bbd-9d82-28e0fcbb7158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d2303d3-58da-4b6b-80a6-ee79864d6291",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "dd7ed6ea-1355-4fee-988f-9016689a5c65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "2144a67f-178a-4cb5-a595-c399e0f25f8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd7ed6ea-1355-4fee-988f-9016689a5c65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c1d5b4d-44fe-4a82-bab8-cdeba82b9131",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd7ed6ea-1355-4fee-988f-9016689a5c65",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "e7a844d7-e196-4237-b0bf-f172b30bc047",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "4b3da272-b85e-4d8f-9995-aa24afafc96e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7a844d7-e196-4237-b0bf-f172b30bc047",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d45ad8f4-97dc-449b-a582-dc88c601fb34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7a844d7-e196-4237-b0bf-f172b30bc047",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "2ccf702b-a24c-48ba-a0c3-414d7267a65c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "d11de618-5795-4753-b9a3-0762ae6b34c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ccf702b-a24c-48ba-a0c3-414d7267a65c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d13d6753-81b5-478c-bb95-474746a03e52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ccf702b-a24c-48ba-a0c3-414d7267a65c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "2d98fc13-c866-4c3f-a968-172d48dac77c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "02ad6799-50f0-45b7-a2db-77e959f592e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d98fc13-c866-4c3f-a968-172d48dac77c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9217cf8-f2db-48b0-9ef0-9acc90e2511d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d98fc13-c866-4c3f-a968-172d48dac77c",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "3b9aae41-6fe6-43e8-9e04-c9af99e29cd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "d7a163e5-eae4-42d4-8f18-8c05ca5b6b10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b9aae41-6fe6-43e8-9e04-c9af99e29cd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1acb6ae-6b43-435e-8259-248e46ccda6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b9aae41-6fe6-43e8-9e04-c9af99e29cd0",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "fd14e73f-b57b-4589-9a47-870c0e783a4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "a8691986-9b8b-4a86-a682-f659c4a1171b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd14e73f-b57b-4589-9a47-870c0e783a4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5e7b0f6-069c-4165-9ac1-227333b0a0f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd14e73f-b57b-4589-9a47-870c0e783a4e",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "7bf7f777-c474-4366-98cb-27e8d131ef04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "e344dc03-d6d0-496b-a3fe-adad30686d5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bf7f777-c474-4366-98cb-27e8d131ef04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc83cd43-af74-46ce-abab-0b937642c005",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bf7f777-c474-4366-98cb-27e8d131ef04",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "cbdb18b6-00ba-4065-a64e-748b116b5006",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "983d6c9d-504d-4343-8612-3b6b780b15af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbdb18b6-00ba-4065-a64e-748b116b5006",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0a17409-8852-48d4-8585-a4c76123b1c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbdb18b6-00ba-4065-a64e-748b116b5006",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "7f76b849-c140-416c-b154-cd6d5c11e574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "fe3c3f86-3845-45f2-96c6-74912efed6c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f76b849-c140-416c-b154-cd6d5c11e574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b2aa5a1-32be-4182-870c-1f6dd8c0feff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f76b849-c140-416c-b154-cd6d5c11e574",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "ad07ecee-1f8d-4ed9-8464-c2df9eeb5dbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "8edb4557-cec1-4646-877f-4943141cb37a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad07ecee-1f8d-4ed9-8464-c2df9eeb5dbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b4ab735-4372-4dc6-8e88-420b49d6c048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad07ecee-1f8d-4ed9-8464-c2df9eeb5dbd",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "ee66eccf-b172-4fbf-89ce-1339ecfd81b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "2129aeae-2e76-4273-ae04-98cd299c2ca9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee66eccf-b172-4fbf-89ce-1339ecfd81b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df420d02-35c9-4e47-af3c-4fcc620213d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee66eccf-b172-4fbf-89ce-1339ecfd81b9",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "3e0e9d2c-fdca-4a23-9c32-a04571805695",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "5bedc6bb-1afb-4797-af8a-bccb692ecdbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e0e9d2c-fdca-4a23-9c32-a04571805695",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b1ea6ac-808b-4152-87b4-89f5a0add6df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e0e9d2c-fdca-4a23-9c32-a04571805695",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5f726806-e49d-4477-8c22-2d121b850c44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "b3abc59e-a857-4420-b9af-84ba0a783792",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f726806-e49d-4477-8c22-2d121b850c44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b95875de-b5a9-4fa8-b86c-971cd8bfcbfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f726806-e49d-4477-8c22-2d121b850c44",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "1783d633-d801-4bc2-93bc-024a2c83b760",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "337190a4-51e2-443a-9deb-e0ffdd24531c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1783d633-d801-4bc2-93bc-024a2c83b760",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39c7bd02-614a-47c7-bbbd-84a94a35a312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1783d633-d801-4bc2-93bc-024a2c83b760",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "b732af1a-ec9a-4fbf-9f8f-61a32bc3c4c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "b499df17-450d-4ed2-bd69-d02aa482d471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b732af1a-ec9a-4fbf-9f8f-61a32bc3c4c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93753042-7dcc-4088-a451-3660f46db856",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b732af1a-ec9a-4fbf-9f8f-61a32bc3c4c7",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "b46e6b7c-9a79-44cb-9718-715eb971d897",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "97e48b5a-5aa4-4653-a669-01c9d0051dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b46e6b7c-9a79-44cb-9718-715eb971d897",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39b7e623-c14b-42de-a6f5-89f95bdd9235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b46e6b7c-9a79-44cb-9718-715eb971d897",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "3e4069b5-0f1e-4a20-a755-91a15afd1a70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "8b785a5d-c65b-4718-8212-a9748bed9aea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e4069b5-0f1e-4a20-a755-91a15afd1a70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae73a029-bfbf-4614-b40a-d63d0c01309f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e4069b5-0f1e-4a20-a755-91a15afd1a70",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "2bdaeb1d-a5e2-4084-be1a-939094517341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "dff95f47-042f-46d3-8ce3-1c5620e558e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bdaeb1d-a5e2-4084-be1a-939094517341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05b321f6-e9e8-4db7-8d5e-69f3054ee6dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bdaeb1d-a5e2-4084-be1a-939094517341",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "336d44ff-df59-4060-a199-0123d7583b31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "159b1ff9-a5c4-407e-94e4-c97515211b31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "336d44ff-df59-4060-a199-0123d7583b31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bca0dcd-a32b-4e6c-bc09-c2b213f9490d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "336d44ff-df59-4060-a199-0123d7583b31",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "a8257f41-89fc-4319-9b20-826d8086a21e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "1efc03d5-783a-40eb-8ec9-0917cf1b6cc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8257f41-89fc-4319-9b20-826d8086a21e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb799703-bf4f-4ff1-834b-0f95df3e0258",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8257f41-89fc-4319-9b20-826d8086a21e",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "0505ece8-6eca-4b59-beaa-e73033c29d67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "c05bea34-3e63-4ac3-9d6b-924a26fb2e30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0505ece8-6eca-4b59-beaa-e73033c29d67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8ebedf7-ddc4-4be8-b5ee-e72a13941571",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0505ece8-6eca-4b59-beaa-e73033c29d67",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "5bcf89ce-74f8-4ac6-a1bc-70826dcd7e45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "571d3a37-fa39-4071-98c1-2a5fdc5670f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bcf89ce-74f8-4ac6-a1bc-70826dcd7e45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db5c82b7-1813-4879-8c07-4f2b4c721202",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bcf89ce-74f8-4ac6-a1bc-70826dcd7e45",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "51ef939c-44e6-4a0a-9178-3b076068c729",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "7ee76e7f-6f61-4320-84a8-17c10ad11686",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51ef939c-44e6-4a0a-9178-3b076068c729",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e79799a1-0604-475a-8dce-dbde3539267e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51ef939c-44e6-4a0a-9178-3b076068c729",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "a36bc341-cea0-4961-a36a-084c2da41322",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "c69a960c-7968-4e36-89ab-79f9857ea43e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a36bc341-cea0-4961-a36a-084c2da41322",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e90bf52-8960-4d16-b0d5-321d1c775118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a36bc341-cea0-4961-a36a-084c2da41322",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "42c1425b-fff4-452d-b5c7-637d4fbd3820",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "4c561a61-4c78-454f-b192-6f629c6f15b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42c1425b-fff4-452d-b5c7-637d4fbd3820",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67bd2b81-7f07-4297-aaa1-4f9e1a3bda60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42c1425b-fff4-452d-b5c7-637d4fbd3820",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "1311042e-8cf1-446d-a591-f2cd8887f5dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "6c8cdadd-d945-4b62-83ce-08aee7438c79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1311042e-8cf1-446d-a591-f2cd8887f5dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e36172fd-8450-4931-9adf-665cbb8ca601",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1311042e-8cf1-446d-a591-f2cd8887f5dd",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        },
        {
            "id": "01cba21b-c235-4cb0-9ee2-63169b06bc67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "compositeImage": {
                "id": "d686f5ef-bd1e-4b36-965c-d7b3f841a1a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01cba21b-c235-4cb0-9ee2-63169b06bc67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6102fec6-8c40-481f-ac6c-a038889a2b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01cba21b-c235-4cb0-9ee2-63169b06bc67",
                    "LayerId": "81031490-a915-44af-9109-5caf8193e937"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "81031490-a915-44af-9109-5caf8193e937",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be98cc7c-15a9-404e-bc43-2eabf7f8a43f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}