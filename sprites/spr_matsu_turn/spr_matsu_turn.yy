{
    "id": "291b0091-305a-41e8-ab86-e8e1a45f1256",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_turn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 0,
    "bbox_right": 66,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "787290cd-9c3e-431f-ae37-cd03961ead5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "291b0091-305a-41e8-ab86-e8e1a45f1256",
            "compositeImage": {
                "id": "217b3536-a394-48c2-bfc2-4a567f9ba2a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "787290cd-9c3e-431f-ae37-cd03961ead5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9030378-5bf0-48e2-94c7-e29624c9b449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "787290cd-9c3e-431f-ae37-cd03961ead5d",
                    "LayerId": "2487bc51-8e46-40c9-a4f2-d4050c3956e3"
                }
            ]
        },
        {
            "id": "29f24c46-2cc2-4cf7-a4bf-19c6e91f2f88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "291b0091-305a-41e8-ab86-e8e1a45f1256",
            "compositeImage": {
                "id": "8ced44d8-6fdd-4409-b1e5-93823716246a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29f24c46-2cc2-4cf7-a4bf-19c6e91f2f88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46fcb1d1-dbd0-4da7-a53e-aee78da5ba28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29f24c46-2cc2-4cf7-a4bf-19c6e91f2f88",
                    "LayerId": "2487bc51-8e46-40c9-a4f2-d4050c3956e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 94,
    "layers": [
        {
            "id": "2487bc51-8e46-40c9-a4f2-d4050c3956e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "291b0091-305a-41e8-ab86-e8e1a45f1256",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 67,
    "xorig": 0,
    "yorig": 0
}