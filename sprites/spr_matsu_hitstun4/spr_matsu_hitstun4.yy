{
    "id": "ba9582d1-535f-4e8e-a7e1-892c671ba6d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_hitstun4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 109,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91923790-ddbd-4096-a25a-6be90c856e2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba9582d1-535f-4e8e-a7e1-892c671ba6d9",
            "compositeImage": {
                "id": "513daf36-a1f5-4d64-ba9d-56a2c61fbae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91923790-ddbd-4096-a25a-6be90c856e2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b17a26ee-59a5-4c18-b46d-4064994f15dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91923790-ddbd-4096-a25a-6be90c856e2b",
                    "LayerId": "b4f59adf-5617-4af9-8035-edae38eacfb3"
                }
            ]
        },
        {
            "id": "e3afa1a7-bf33-40d5-ab12-7986568342ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba9582d1-535f-4e8e-a7e1-892c671ba6d9",
            "compositeImage": {
                "id": "a21589d1-27ce-40e0-9686-bca1f92c2071",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3afa1a7-bf33-40d5-ab12-7986568342ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7afcf583-6c14-4d6c-a0ee-b1805f910bf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3afa1a7-bf33-40d5-ab12-7986568342ce",
                    "LayerId": "b4f59adf-5617-4af9-8035-edae38eacfb3"
                }
            ]
        },
        {
            "id": "f3a9cf7b-fafc-4efe-be68-824aa3914ce9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba9582d1-535f-4e8e-a7e1-892c671ba6d9",
            "compositeImage": {
                "id": "6b99ef03-9b9f-43c5-aef8-c2f16b5c45ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3a9cf7b-fafc-4efe-be68-824aa3914ce9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4d925fa-495f-4119-86d9-8903325721a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3a9cf7b-fafc-4efe-be68-824aa3914ce9",
                    "LayerId": "b4f59adf-5617-4af9-8035-edae38eacfb3"
                }
            ]
        },
        {
            "id": "ab0d58c0-4886-43b9-a5a8-07ecc5975ae9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba9582d1-535f-4e8e-a7e1-892c671ba6d9",
            "compositeImage": {
                "id": "3672745b-bd2c-4e5e-9cbd-405cb9fcc532",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab0d58c0-4886-43b9-a5a8-07ecc5975ae9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38f54a28-a442-4557-9032-ff1022fb8bf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab0d58c0-4886-43b9-a5a8-07ecc5975ae9",
                    "LayerId": "b4f59adf-5617-4af9-8035-edae38eacfb3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "b4f59adf-5617-4af9-8035-edae38eacfb3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba9582d1-535f-4e8e-a7e1-892c671ba6d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 110,
    "xorig": 36,
    "yorig": 89
}