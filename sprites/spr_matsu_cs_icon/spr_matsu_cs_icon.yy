{
    "id": "112eec46-fe94-4e9a-9d54-bccbf0211c0e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_cs_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 85,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a483e815-efcf-4797-8ee4-efe772679eec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "112eec46-fe94-4e9a-9d54-bccbf0211c0e",
            "compositeImage": {
                "id": "51bc4315-e0cf-46f4-9380-871659f7d894",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a483e815-efcf-4797-8ee4-efe772679eec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70679714-d86d-4c41-976b-8e8209a5b3f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a483e815-efcf-4797-8ee4-efe772679eec",
                    "LayerId": "c0db31ca-bf91-4e67-9caf-9be5bcb9d717"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "c0db31ca-bf91-4e67-9caf-9be5bcb9d717",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "112eec46-fe94-4e9a-9d54-bccbf0211c0e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}