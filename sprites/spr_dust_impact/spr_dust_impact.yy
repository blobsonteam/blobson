{
    "id": "c8c0c118-ea76-4cb3-b746-5a226e7a18d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dust_impact",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a99c8ba4-0787-407c-ab09-59d466070d43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8c0c118-ea76-4cb3-b746-5a226e7a18d8",
            "compositeImage": {
                "id": "6b7ed87a-65db-4872-96c8-d68ea09629f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a99c8ba4-0787-407c-ab09-59d466070d43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29fdaa5e-7d9d-4245-ae4b-0e21a5b96b8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a99c8ba4-0787-407c-ab09-59d466070d43",
                    "LayerId": "f724c34d-b2cc-412f-b24a-05e19290f3f4"
                }
            ]
        },
        {
            "id": "f2104522-5e2f-4411-b82c-a3a1838bec4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8c0c118-ea76-4cb3-b746-5a226e7a18d8",
            "compositeImage": {
                "id": "74637e9a-f243-40b4-ab10-dc3063fa31f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2104522-5e2f-4411-b82c-a3a1838bec4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5589d8c0-0cc7-4c7a-b2ab-348b303165d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2104522-5e2f-4411-b82c-a3a1838bec4d",
                    "LayerId": "f724c34d-b2cc-412f-b24a-05e19290f3f4"
                }
            ]
        },
        {
            "id": "e08c5375-c877-4862-b850-9b0f6e765241",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8c0c118-ea76-4cb3-b746-5a226e7a18d8",
            "compositeImage": {
                "id": "bc9d77d9-0aca-410d-a83b-ebfec6cac693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e08c5375-c877-4862-b850-9b0f6e765241",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a74803f6-1026-4577-846d-a139827aa538",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e08c5375-c877-4862-b850-9b0f6e765241",
                    "LayerId": "f724c34d-b2cc-412f-b24a-05e19290f3f4"
                }
            ]
        },
        {
            "id": "1f4ca653-ce23-4be7-9d0f-6bad961f41bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8c0c118-ea76-4cb3-b746-5a226e7a18d8",
            "compositeImage": {
                "id": "faf27b28-8d36-4d5b-9c9b-f521d3b36617",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f4ca653-ce23-4be7-9d0f-6bad961f41bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31846281-2f43-4932-abe7-797f2d76f45f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f4ca653-ce23-4be7-9d0f-6bad961f41bc",
                    "LayerId": "f724c34d-b2cc-412f-b24a-05e19290f3f4"
                }
            ]
        },
        {
            "id": "92382e4b-a49f-41d6-a096-39b2e8cd1fc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8c0c118-ea76-4cb3-b746-5a226e7a18d8",
            "compositeImage": {
                "id": "44507d4f-88ee-4c48-9da9-fbddd42d9d6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92382e4b-a49f-41d6-a096-39b2e8cd1fc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce27d0eb-5182-4d46-9bac-3459d46706c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92382e4b-a49f-41d6-a096-39b2e8cd1fc0",
                    "LayerId": "f724c34d-b2cc-412f-b24a-05e19290f3f4"
                }
            ]
        },
        {
            "id": "06e58184-5af1-428b-902e-5d2768356d12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8c0c118-ea76-4cb3-b746-5a226e7a18d8",
            "compositeImage": {
                "id": "e852bd16-32fa-4595-b428-33f7740ec4d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06e58184-5af1-428b-902e-5d2768356d12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe2e007a-88bf-4fd1-b321-92eeda50458a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06e58184-5af1-428b-902e-5d2768356d12",
                    "LayerId": "f724c34d-b2cc-412f-b24a-05e19290f3f4"
                }
            ]
        },
        {
            "id": "5e77b42b-9769-4c4a-b453-f98a96efa10f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8c0c118-ea76-4cb3-b746-5a226e7a18d8",
            "compositeImage": {
                "id": "52a828b5-9953-41b8-bb21-7b7dfc4f09f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e77b42b-9769-4c4a-b453-f98a96efa10f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1860ad8-d10d-46a0-937e-362643cbe4d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e77b42b-9769-4c4a-b453-f98a96efa10f",
                    "LayerId": "f724c34d-b2cc-412f-b24a-05e19290f3f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f724c34d-b2cc-412f-b24a-05e19290f3f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8c0c118-ea76-4cb3-b746-5a226e7a18d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 32
}