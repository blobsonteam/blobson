{
    "id": "0b4141f3-8252-45d9-8d0e-eea1b024b069",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite240",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f31c3af-cf9d-4184-b1ff-779faabedcdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b4141f3-8252-45d9-8d0e-eea1b024b069",
            "compositeImage": {
                "id": "429414f2-64ae-4151-8435-f3cb19612489",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f31c3af-cf9d-4184-b1ff-779faabedcdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cafba71-c507-40e7-b397-85b4d88c6660",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f31c3af-cf9d-4184-b1ff-779faabedcdc",
                    "LayerId": "2364f6df-9868-4bc2-88d3-676ba0a43d55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2364f6df-9868-4bc2-88d3-676ba0a43d55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b4141f3-8252-45d9-8d0e-eea1b024b069",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}