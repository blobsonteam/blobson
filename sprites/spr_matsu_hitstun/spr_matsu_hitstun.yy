{
    "id": "0da9dc44-b310-4fbe-973f-c06ff1932d91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_hitstun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 9,
    "bbox_right": 83,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75d74687-4443-49b2-bfbf-e21cd281b0bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0da9dc44-b310-4fbe-973f-c06ff1932d91",
            "compositeImage": {
                "id": "4e9c426f-b5bf-4611-a1ba-456b57189e39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75d74687-4443-49b2-bfbf-e21cd281b0bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42e3b780-516d-4df8-b29b-f81ca09031c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75d74687-4443-49b2-bfbf-e21cd281b0bd",
                    "LayerId": "9a4bb065-6c78-42e0-a1e3-3ce8011bb739"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "9a4bb065-6c78-42e0-a1e3-3ce8011bb739",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0da9dc44-b310-4fbe-973f-c06ff1932d91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 110,
    "xorig": 36,
    "yorig": 89
}