{
    "id": "56ffa879-add3-4f21-8cd6-2e97e9235011",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_airdodge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e317df79-b03a-435a-9bdb-17dec61936e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56ffa879-add3-4f21-8cd6-2e97e9235011",
            "compositeImage": {
                "id": "11f9f9e0-5517-4238-ae38-f40d182a134e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e317df79-b03a-435a-9bdb-17dec61936e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e89e3b3a-8d4a-4dcd-9183-0d2449004aee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e317df79-b03a-435a-9bdb-17dec61936e5",
                    "LayerId": "48dd3f97-7da0-4013-bf6c-71fc8a98a706"
                }
            ]
        },
        {
            "id": "9b2910c4-aa8c-4cd5-af11-3fc99cb77883",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56ffa879-add3-4f21-8cd6-2e97e9235011",
            "compositeImage": {
                "id": "99995c98-8b55-4bb7-99fd-fe7304fe83a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b2910c4-aa8c-4cd5-af11-3fc99cb77883",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3c622f8-3364-4da0-93ee-54d1267ca183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b2910c4-aa8c-4cd5-af11-3fc99cb77883",
                    "LayerId": "48dd3f97-7da0-4013-bf6c-71fc8a98a706"
                }
            ]
        },
        {
            "id": "50969612-32ff-4848-bdc1-0e4d5b599468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56ffa879-add3-4f21-8cd6-2e97e9235011",
            "compositeImage": {
                "id": "0ea91497-132d-4274-8e47-7b3d0d472cf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50969612-32ff-4848-bdc1-0e4d5b599468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "992399b5-6074-4be2-b575-fc366444a7d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50969612-32ff-4848-bdc1-0e4d5b599468",
                    "LayerId": "48dd3f97-7da0-4013-bf6c-71fc8a98a706"
                }
            ]
        },
        {
            "id": "26de621a-d6aa-4feb-9721-d03a0b85aeec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56ffa879-add3-4f21-8cd6-2e97e9235011",
            "compositeImage": {
                "id": "c7ab5595-cb09-40b1-9edf-33322e1e8e28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26de621a-d6aa-4feb-9721-d03a0b85aeec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dd9e607-f156-4bb0-ae3f-1be0718f253e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26de621a-d6aa-4feb-9721-d03a0b85aeec",
                    "LayerId": "48dd3f97-7da0-4013-bf6c-71fc8a98a706"
                }
            ]
        },
        {
            "id": "e671c15a-62ec-4d74-b127-4c31b838b700",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56ffa879-add3-4f21-8cd6-2e97e9235011",
            "compositeImage": {
                "id": "6fc2f3b0-d213-41f5-92fb-5f81da14ca18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e671c15a-62ec-4d74-b127-4c31b838b700",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e72f7aa-8e3d-48e6-bfaf-8e924c35f325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e671c15a-62ec-4d74-b127-4c31b838b700",
                    "LayerId": "48dd3f97-7da0-4013-bf6c-71fc8a98a706"
                }
            ]
        },
        {
            "id": "6d2eb4d7-63cb-4948-9090-b2bffae33268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56ffa879-add3-4f21-8cd6-2e97e9235011",
            "compositeImage": {
                "id": "c8bdeb9c-60d4-49b2-8296-5cf50f0f9a00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d2eb4d7-63cb-4948-9090-b2bffae33268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10b13381-ca3a-4f3f-a2bc-88703cedb9d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d2eb4d7-63cb-4948-9090-b2bffae33268",
                    "LayerId": "48dd3f97-7da0-4013-bf6c-71fc8a98a706"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "48dd3f97-7da0-4013-bf6c-71fc8a98a706",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56ffa879-add3-4f21-8cd6-2e97e9235011",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 29,
    "yorig": 95
}