{
    "id": "0cc7de9e-2738-4c1b-90aa-620a591b139a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fighter_pal_select_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 54,
    "bbox_right": 181,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca75207a-76d7-4d4e-a811-6b54e7341b1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cc7de9e-2738-4c1b-90aa-620a591b139a",
            "compositeImage": {
                "id": "e95c984e-d386-40de-b9a7-7bd8f9946a7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca75207a-76d7-4d4e-a811-6b54e7341b1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9e1a0e5-98b0-4e11-878c-dc832d4f377f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca75207a-76d7-4d4e-a811-6b54e7341b1d",
                    "LayerId": "bcd22e26-b8c1-4660-8ec2-4cafac272ceb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "bcd22e26-b8c1-4660-8ec2-4cafac272ceb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0cc7de9e-2738-4c1b-90aa-620a591b139a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 204,
    "xorig": 0,
    "yorig": 0
}