{
    "id": "a4ec2b91-167a-413b-9697-dcab79d6d811",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_jumpsquat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 145,
    "bbox_left": 7,
    "bbox_right": 61,
    "bbox_top": 65,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b0c4bf4-b4b4-4b1a-8dd3-76fa2e165951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4ec2b91-167a-413b-9697-dcab79d6d811",
            "compositeImage": {
                "id": "8c3a5357-70f1-4679-8963-90f346f18bbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b0c4bf4-b4b4-4b1a-8dd3-76fa2e165951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f35f186a-b50e-4cf8-bcbd-a3ee2a1e061d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b0c4bf4-b4b4-4b1a-8dd3-76fa2e165951",
                    "LayerId": "a851c214-3044-4d7f-adb3-2ba31af69cae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 146,
    "layers": [
        {
            "id": "a851c214-3044-4d7f-adb3-2ba31af69cae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4ec2b91-167a-413b-9697-dcab79d6d811",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 33,
    "yorig": 145
}