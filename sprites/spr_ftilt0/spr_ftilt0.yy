{
    "id": "9b2b4052-0fd0-45e1-b3e0-57115511d092",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ftilt0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 43,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e79cc310-c2ed-40d7-b0a4-75b262713db3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b2b4052-0fd0-45e1-b3e0-57115511d092",
            "compositeImage": {
                "id": "e0eb9d74-ae82-49af-ade3-ff9ad97a7c8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e79cc310-c2ed-40d7-b0a4-75b262713db3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fe96e10-c1c4-4b5d-979c-7ca0f29107bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e79cc310-c2ed-40d7-b0a4-75b262713db3",
                    "LayerId": "2689c9a6-fcc7-4a5b-b57a-32111c3594b1"
                }
            ]
        },
        {
            "id": "a9a60440-18db-441d-b5b6-ae276a641fb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b2b4052-0fd0-45e1-b3e0-57115511d092",
            "compositeImage": {
                "id": "b5248615-5640-4515-8633-62798a8e04f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9a60440-18db-441d-b5b6-ae276a641fb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64722d73-3d15-4d0f-9246-bad1b4a62c41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9a60440-18db-441d-b5b6-ae276a641fb9",
                    "LayerId": "2689c9a6-fcc7-4a5b-b57a-32111c3594b1"
                }
            ]
        },
        {
            "id": "bc8be4f9-49a1-4be6-9436-dcd30e88efcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b2b4052-0fd0-45e1-b3e0-57115511d092",
            "compositeImage": {
                "id": "924e548f-16d5-44a5-acf3-1e371cebe100",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc8be4f9-49a1-4be6-9436-dcd30e88efcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7167a7fe-f02b-44ff-a570-2632f9b83d7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc8be4f9-49a1-4be6-9436-dcd30e88efcd",
                    "LayerId": "2689c9a6-fcc7-4a5b-b57a-32111c3594b1"
                }
            ]
        },
        {
            "id": "0803e6cc-8205-480f-99cf-99f94de537d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b2b4052-0fd0-45e1-b3e0-57115511d092",
            "compositeImage": {
                "id": "1b9d6406-5d21-458a-8c29-ef142cef495f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0803e6cc-8205-480f-99cf-99f94de537d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2028e5b6-afeb-4427-93c0-7bd15afd3e17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0803e6cc-8205-480f-99cf-99f94de537d7",
                    "LayerId": "2689c9a6-fcc7-4a5b-b57a-32111c3594b1"
                }
            ]
        },
        {
            "id": "98b1d2c7-c064-45a1-a57c-ea0f653bcf18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b2b4052-0fd0-45e1-b3e0-57115511d092",
            "compositeImage": {
                "id": "c15d96ca-6797-4530-a9bc-97ad3d03e4b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98b1d2c7-c064-45a1-a57c-ea0f653bcf18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73dff8b6-c598-4c63-b8de-c82f53eac600",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98b1d2c7-c064-45a1-a57c-ea0f653bcf18",
                    "LayerId": "2689c9a6-fcc7-4a5b-b57a-32111c3594b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "2689c9a6-fcc7-4a5b-b57a-32111c3594b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b2b4052-0fd0-45e1-b3e0-57115511d092",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 8,
    "yorig": 12
}