{
    "id": "fa667c5e-85ec-4bec-81c3-8e84b937ce19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_window",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ef8536d-b933-431e-be13-25d7c7fdf67c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa667c5e-85ec-4bec-81c3-8e84b937ce19",
            "compositeImage": {
                "id": "e8d751b1-ba79-4ec5-9567-32909cce1eb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ef8536d-b933-431e-be13-25d7c7fdf67c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d21bbaf4-20fa-4c04-bf5a-6a106a53e635",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ef8536d-b933-431e-be13-25d7c7fdf67c",
                    "LayerId": "4ce4066b-dbef-483d-8917-ee9db7651f20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "4ce4066b-dbef-483d-8917-ee9db7651f20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa667c5e-85ec-4bec-81c3-8e84b937ce19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}