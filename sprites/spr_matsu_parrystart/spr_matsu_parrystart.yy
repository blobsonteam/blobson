{
    "id": "2033be4a-6d7c-442a-a3d2-481c7bcc3060",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_parrystart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 68,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27945127-3034-4b58-b34f-048351ec47ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2033be4a-6d7c-442a-a3d2-481c7bcc3060",
            "compositeImage": {
                "id": "f90a6164-9980-47c9-840c-6969275d9164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27945127-3034-4b58-b34f-048351ec47ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "946cb70a-b8f0-4d83-9cd6-eb514a3de895",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27945127-3034-4b58-b34f-048351ec47ce",
                    "LayerId": "d7901f37-cbec-473e-825d-d9393d722445"
                }
            ]
        },
        {
            "id": "5b91dbed-461f-4f69-a797-a4a88136d09f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2033be4a-6d7c-442a-a3d2-481c7bcc3060",
            "compositeImage": {
                "id": "681d6c34-57e2-4fe0-ab0f-e98b932dc08e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b91dbed-461f-4f69-a797-a4a88136d09f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75202225-6cb6-4c20-ad1a-e2aabd5f9f05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b91dbed-461f-4f69-a797-a4a88136d09f",
                    "LayerId": "d7901f37-cbec-473e-825d-d9393d722445"
                }
            ]
        },
        {
            "id": "327ced58-398c-4aca-9e66-38367b4c1c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2033be4a-6d7c-442a-a3d2-481c7bcc3060",
            "compositeImage": {
                "id": "8ca7e583-4e93-4f00-911e-a8280d3657ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "327ced58-398c-4aca-9e66-38367b4c1c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61290ae0-bff1-423c-8544-2ac760604b3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "327ced58-398c-4aca-9e66-38367b4c1c56",
                    "LayerId": "d7901f37-cbec-473e-825d-d9393d722445"
                }
            ]
        },
        {
            "id": "68e5423e-1e79-4ade-b624-1e9b97fc688a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2033be4a-6d7c-442a-a3d2-481c7bcc3060",
            "compositeImage": {
                "id": "13a8fe5b-7b25-4612-9b98-bcfed69e3bc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68e5423e-1e79-4ade-b624-1e9b97fc688a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a718c04c-76cc-425d-afaf-20190f7bd2d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68e5423e-1e79-4ade-b624-1e9b97fc688a",
                    "LayerId": "d7901f37-cbec-473e-825d-d9393d722445"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "d7901f37-cbec-473e-825d-d9393d722445",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2033be4a-6d7c-442a-a3d2-481c7bcc3060",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 69,
    "xorig": 26,
    "yorig": 95
}