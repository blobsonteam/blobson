{
    "id": "ade59726-ea8c-403e-a9c4-9a1f3921af2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fair0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 4,
    "bbox_right": 47,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6fcb35f2-e1cc-43ed-923e-b7f01d1db2c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ade59726-ea8c-403e-a9c4-9a1f3921af2c",
            "compositeImage": {
                "id": "20334cc4-b139-4d09-8550-24d564797d1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fcb35f2-e1cc-43ed-923e-b7f01d1db2c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c89ef5b1-c2d4-43d4-b63f-e02c89d59750",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fcb35f2-e1cc-43ed-923e-b7f01d1db2c3",
                    "LayerId": "59554ffd-841e-488e-940f-9a9ff35926f1"
                }
            ]
        },
        {
            "id": "c1403013-c060-42fc-99cc-54c740071f44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ade59726-ea8c-403e-a9c4-9a1f3921af2c",
            "compositeImage": {
                "id": "e3d2114a-acf2-4dcb-9a50-381cc4c1482b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1403013-c060-42fc-99cc-54c740071f44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e9a263d-6fe3-4e0d-872d-7767c299f728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1403013-c060-42fc-99cc-54c740071f44",
                    "LayerId": "59554ffd-841e-488e-940f-9a9ff35926f1"
                }
            ]
        },
        {
            "id": "f21b7854-2190-48d6-97b1-a875bb2e07e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ade59726-ea8c-403e-a9c4-9a1f3921af2c",
            "compositeImage": {
                "id": "9a267810-f80c-410b-aa07-340f813ed603",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f21b7854-2190-48d6-97b1-a875bb2e07e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "566b9057-3044-4d0e-9d0c-206b25b8d979",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f21b7854-2190-48d6-97b1-a875bb2e07e4",
                    "LayerId": "59554ffd-841e-488e-940f-9a9ff35926f1"
                }
            ]
        },
        {
            "id": "f5d22b6f-0d84-4366-af4c-e06886c8cf1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ade59726-ea8c-403e-a9c4-9a1f3921af2c",
            "compositeImage": {
                "id": "b9f6a344-bd20-4f16-881b-251398e454e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5d22b6f-0d84-4366-af4c-e06886c8cf1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bdad273-d1b1-4425-917c-9d84c304bc76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5d22b6f-0d84-4366-af4c-e06886c8cf1b",
                    "LayerId": "59554ffd-841e-488e-940f-9a9ff35926f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "59554ffd-841e-488e-940f-9a9ff35926f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ade59726-ea8c-403e-a9c4-9a1f3921af2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 12,
    "yorig": 23
}