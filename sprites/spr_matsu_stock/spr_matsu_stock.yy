{
    "id": "c75cd8bc-a591-40c0-95d5-7e87871968b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_stock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 3,
    "bbox_right": 27,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6901bf3d-9d26-43da-afa8-e72b1933e01d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c75cd8bc-a591-40c0-95d5-7e87871968b2",
            "compositeImage": {
                "id": "c40f2087-67f7-4941-a218-5249c352736a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6901bf3d-9d26-43da-afa8-e72b1933e01d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6493c209-2efb-4734-9e26-3eee751a2428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6901bf3d-9d26-43da-afa8-e72b1933e01d",
                    "LayerId": "632e4ac6-c41d-4dd6-bb20-2952c719518d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "632e4ac6-c41d-4dd6-bb20-2952c719518d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c75cd8bc-a591-40c0-95d5-7e87871968b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}