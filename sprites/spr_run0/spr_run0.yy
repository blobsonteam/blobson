{
    "id": "55629c72-ddc0-4e32-8911-e569d42deb69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_run0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c4c9f73-3a53-40dc-817f-c702af305c8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55629c72-ddc0-4e32-8911-e569d42deb69",
            "compositeImage": {
                "id": "3be8b16a-bd0e-4916-95be-8b545a9d7aa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c4c9f73-3a53-40dc-817f-c702af305c8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2ebd447-ddf2-4d41-aef5-a3c8ce8e8f0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c4c9f73-3a53-40dc-817f-c702af305c8a",
                    "LayerId": "531df3d6-0978-4676-a0c3-e45468a43d89"
                }
            ]
        },
        {
            "id": "9e598d7f-53a8-42c2-b2d7-7ed65dfc922e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55629c72-ddc0-4e32-8911-e569d42deb69",
            "compositeImage": {
                "id": "0cae8d89-4566-4a74-a6ef-2364558e9b71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e598d7f-53a8-42c2-b2d7-7ed65dfc922e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e32a9a5-e295-4d3c-98f0-5788c91787ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e598d7f-53a8-42c2-b2d7-7ed65dfc922e",
                    "LayerId": "531df3d6-0978-4676-a0c3-e45468a43d89"
                }
            ]
        },
        {
            "id": "27ec3715-0d66-40d7-aaa5-4213fa89b7be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55629c72-ddc0-4e32-8911-e569d42deb69",
            "compositeImage": {
                "id": "feb4046e-595c-4b18-905f-81483f331a39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27ec3715-0d66-40d7-aaa5-4213fa89b7be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15158be7-7230-4deb-9cc3-081043a95350",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27ec3715-0d66-40d7-aaa5-4213fa89b7be",
                    "LayerId": "531df3d6-0978-4676-a0c3-e45468a43d89"
                }
            ]
        },
        {
            "id": "f3298697-84b0-4e27-97aa-34d0f970eb3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55629c72-ddc0-4e32-8911-e569d42deb69",
            "compositeImage": {
                "id": "d68e4de3-fde1-4ace-823b-67d026162681",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3298697-84b0-4e27-97aa-34d0f970eb3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7de9770-fd75-4acc-af45-b207cacd9f37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3298697-84b0-4e27-97aa-34d0f970eb3b",
                    "LayerId": "531df3d6-0978-4676-a0c3-e45468a43d89"
                }
            ]
        },
        {
            "id": "88fa0083-9f33-4913-a32c-daa743973282",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55629c72-ddc0-4e32-8911-e569d42deb69",
            "compositeImage": {
                "id": "c62b92b9-baf5-4b9d-b25d-e4161a5599a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88fa0083-9f33-4913-a32c-daa743973282",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e093097a-ef64-41d1-8897-8f43f64aa4d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88fa0083-9f33-4913-a32c-daa743973282",
                    "LayerId": "531df3d6-0978-4676-a0c3-e45468a43d89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "531df3d6-0978-4676-a0c3-e45468a43d89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55629c72-ddc0-4e32-8911-e569d42deb69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}