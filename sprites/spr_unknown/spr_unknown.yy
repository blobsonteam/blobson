{
    "id": "20516a31-a556-463f-9553-f05e3bddccab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_unknown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed6d90e4-cfc4-4164-ada6-cc8d6e68491b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20516a31-a556-463f-9553-f05e3bddccab",
            "compositeImage": {
                "id": "254d1881-2148-40e7-b277-62c993a7317c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed6d90e4-cfc4-4164-ada6-cc8d6e68491b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "859961a3-9646-4b5e-96d1-a3fa770f83a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed6d90e4-cfc4-4164-ada6-cc8d6e68491b",
                    "LayerId": "ccfc9119-4eb8-4656-9517-64eed957c712"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ccfc9119-4eb8-4656-9517-64eed957c712",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20516a31-a556-463f-9553-f05e3bddccab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}