{
    "id": "8a3e9f91-61cb-4ae0-bc7f-d88f574901c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stage_icon_window",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e717b201-4460-4cd1-901a-634627a79e1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a3e9f91-61cb-4ae0-bc7f-d88f574901c4",
            "compositeImage": {
                "id": "bbe13ce6-cb2c-45e2-85fe-2b658abc333f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e717b201-4460-4cd1-901a-634627a79e1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3294dc50-162e-49f0-ae2a-a50ba22bb7ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e717b201-4460-4cd1-901a-634627a79e1a",
                    "LayerId": "ba7265bd-292a-45da-9b08-c08a471cb373"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "ba7265bd-292a-45da-9b08-c08a471cb373",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a3e9f91-61cb-4ae0-bc7f-d88f574901c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}