{
    "id": "1f119f44-b73a-4915-9330-9e214586c2bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_airborne",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 145,
    "bbox_left": 0,
    "bbox_right": 77,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40b19858-2634-42de-bc23-44edb8608d77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f119f44-b73a-4915-9330-9e214586c2bb",
            "compositeImage": {
                "id": "f7e5e1f2-a02b-4e14-9086-dba54596c7fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40b19858-2634-42de-bc23-44edb8608d77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2739fa83-ff53-48e2-8ec7-aedd90ac57e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40b19858-2634-42de-bc23-44edb8608d77",
                    "LayerId": "13379281-3747-424d-b3fd-91db14f07e66"
                }
            ]
        },
        {
            "id": "b4a0e4b8-5f75-46e9-baf1-01a352eeee60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f119f44-b73a-4915-9330-9e214586c2bb",
            "compositeImage": {
                "id": "fd85fe2b-fac7-4bc9-b3a4-3f28b46adec0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4a0e4b8-5f75-46e9-baf1-01a352eeee60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d1e2670-1cf1-4849-94d3-361cf012bba8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4a0e4b8-5f75-46e9-baf1-01a352eeee60",
                    "LayerId": "13379281-3747-424d-b3fd-91db14f07e66"
                }
            ]
        },
        {
            "id": "5f65b270-0706-426f-8175-c15ee4f9ef16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f119f44-b73a-4915-9330-9e214586c2bb",
            "compositeImage": {
                "id": "a9fd9877-3e75-471f-a9f6-0f6ac4124349",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f65b270-0706-426f-8175-c15ee4f9ef16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38d7ecf2-9340-42b6-8adc-fa7668be5666",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f65b270-0706-426f-8175-c15ee4f9ef16",
                    "LayerId": "13379281-3747-424d-b3fd-91db14f07e66"
                }
            ]
        },
        {
            "id": "d305543c-0874-4efc-9a62-4bec81c4173f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f119f44-b73a-4915-9330-9e214586c2bb",
            "compositeImage": {
                "id": "858a4aad-a4ca-43db-8842-90d809afab3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d305543c-0874-4efc-9a62-4bec81c4173f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e2002f2-32e7-435c-9fec-3e03e89b4bdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d305543c-0874-4efc-9a62-4bec81c4173f",
                    "LayerId": "13379281-3747-424d-b3fd-91db14f07e66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 146,
    "layers": [
        {
            "id": "13379281-3747-424d-b3fd-91db14f07e66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f119f44-b73a-4915-9330-9e214586c2bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0.15,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 33,
    "yorig": 145
}