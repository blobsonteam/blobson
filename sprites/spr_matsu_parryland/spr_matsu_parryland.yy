{
    "id": "5df52db4-be42-44e0-a04a-08b18a215bc3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_parryland",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b95e0439-e348-419b-9076-0073d5c9342c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5df52db4-be42-44e0-a04a-08b18a215bc3",
            "compositeImage": {
                "id": "06d6be5a-4e38-444b-97de-3fd159fa5b99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b95e0439-e348-419b-9076-0073d5c9342c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84cd3ee6-aec5-4703-815f-df7aa288d0d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b95e0439-e348-419b-9076-0073d5c9342c",
                    "LayerId": "4ce4917b-d4ea-4398-809e-63250e5d494c"
                }
            ]
        },
        {
            "id": "6739ca5a-fb63-4a0e-9c1d-229d1af969c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5df52db4-be42-44e0-a04a-08b18a215bc3",
            "compositeImage": {
                "id": "faab5007-e29b-46d3-8391-a65dc3361b71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6739ca5a-fb63-4a0e-9c1d-229d1af969c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f8aad70-c6f9-4c81-87c3-f6331b627cfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6739ca5a-fb63-4a0e-9c1d-229d1af969c7",
                    "LayerId": "4ce4917b-d4ea-4398-809e-63250e5d494c"
                }
            ]
        },
        {
            "id": "e2d2091a-261d-4d2e-8749-ed517a68c0cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5df52db4-be42-44e0-a04a-08b18a215bc3",
            "compositeImage": {
                "id": "9cb8b406-ad19-42ad-a54d-28d634642db4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2d2091a-261d-4d2e-8749-ed517a68c0cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45dd1055-bc4c-41de-b135-81496f05eba2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2d2091a-261d-4d2e-8749-ed517a68c0cd",
                    "LayerId": "4ce4917b-d4ea-4398-809e-63250e5d494c"
                }
            ]
        },
        {
            "id": "f5e2f10e-8536-4cb2-8afa-016f0bd32c2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5df52db4-be42-44e0-a04a-08b18a215bc3",
            "compositeImage": {
                "id": "5c34094d-62ac-4afb-9911-ba39197dbe10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5e2f10e-8536-4cb2-8afa-016f0bd32c2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94b7d215-542e-4ddb-81cd-ee51eee90367",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5e2f10e-8536-4cb2-8afa-016f0bd32c2e",
                    "LayerId": "4ce4917b-d4ea-4398-809e-63250e5d494c"
                }
            ]
        },
        {
            "id": "b11ace3c-188c-4da6-ba53-f6939f98c967",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5df52db4-be42-44e0-a04a-08b18a215bc3",
            "compositeImage": {
                "id": "3ad5126f-bc34-452e-8570-d67ca23626ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b11ace3c-188c-4da6-ba53-f6939f98c967",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc09b984-b653-4c2d-ab6f-3385763a2ab5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b11ace3c-188c-4da6-ba53-f6939f98c967",
                    "LayerId": "4ce4917b-d4ea-4398-809e-63250e5d494c"
                }
            ]
        },
        {
            "id": "5204b8d3-de41-4e5a-9d89-26b27d024745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5df52db4-be42-44e0-a04a-08b18a215bc3",
            "compositeImage": {
                "id": "303b1086-5ecd-4d29-9df3-1d25a931c310",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5204b8d3-de41-4e5a-9d89-26b27d024745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7174d72e-d90b-4a51-b2f3-a82806a50406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5204b8d3-de41-4e5a-9d89-26b27d024745",
                    "LayerId": "4ce4917b-d4ea-4398-809e-63250e5d494c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 97,
    "layers": [
        {
            "id": "4ce4917b-d4ea-4398-809e-63250e5d494c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5df52db4-be42-44e0-a04a-08b18a215bc3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 31,
    "yorig": 96
}