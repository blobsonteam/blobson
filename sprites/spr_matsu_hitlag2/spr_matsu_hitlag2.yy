{
    "id": "a5511d4e-9a57-4b0b-bdf8-239f4f7041ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_hitlag2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ec6c67e-84fa-437b-b648-a85625bfba87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5511d4e-9a57-4b0b-bdf8-239f4f7041ce",
            "compositeImage": {
                "id": "1f10811d-e72a-47de-8ac9-16513f821800",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ec6c67e-84fa-437b-b648-a85625bfba87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06dc3dd6-a656-4a6e-8673-d438127f7d42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ec6c67e-84fa-437b-b648-a85625bfba87",
                    "LayerId": "a5de6184-4a34-45a5-8d39-d067d7532c04"
                }
            ]
        },
        {
            "id": "5e56b243-04d7-4af2-a239-e503d3e1b4f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5511d4e-9a57-4b0b-bdf8-239f4f7041ce",
            "compositeImage": {
                "id": "a7c11f1e-5db3-4116-aca1-693fc8209add",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e56b243-04d7-4af2-a239-e503d3e1b4f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53532210-c81f-43ab-9531-179d0edd0989",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e56b243-04d7-4af2-a239-e503d3e1b4f1",
                    "LayerId": "a5de6184-4a34-45a5-8d39-d067d7532c04"
                }
            ]
        },
        {
            "id": "2b657f96-2479-4a96-9d15-1ae15a88fb46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5511d4e-9a57-4b0b-bdf8-239f4f7041ce",
            "compositeImage": {
                "id": "93cf2ce7-7fcb-40d8-a7a9-5c17037ef92d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b657f96-2479-4a96-9d15-1ae15a88fb46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1de5d41-296c-4f99-8f3d-c6524d65a86c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b657f96-2479-4a96-9d15-1ae15a88fb46",
                    "LayerId": "a5de6184-4a34-45a5-8d39-d067d7532c04"
                }
            ]
        },
        {
            "id": "54b2905c-b321-4ce3-ae68-9bf18259f7b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5511d4e-9a57-4b0b-bdf8-239f4f7041ce",
            "compositeImage": {
                "id": "5a8e39d3-7f67-4f38-b6c8-0733825f58b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54b2905c-b321-4ce3-ae68-9bf18259f7b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ab09f92-88b1-42e2-b36c-22a8e11c2a20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54b2905c-b321-4ce3-ae68-9bf18259f7b3",
                    "LayerId": "a5de6184-4a34-45a5-8d39-d067d7532c04"
                }
            ]
        },
        {
            "id": "f9dde0a5-7bd2-408b-b135-01c6f2e4d5e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5511d4e-9a57-4b0b-bdf8-239f4f7041ce",
            "compositeImage": {
                "id": "c1182c7c-f3d8-4a52-b52c-21805d735e47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9dde0a5-7bd2-408b-b135-01c6f2e4d5e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edcc566f-68e3-417b-ab68-21296181dc7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9dde0a5-7bd2-408b-b135-01c6f2e4d5e8",
                    "LayerId": "a5de6184-4a34-45a5-8d39-d067d7532c04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "a5de6184-4a34-45a5-8d39-d067d7532c04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5511d4e-9a57-4b0b-bdf8-239f4f7041ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 0.2,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 86
}