{
    "id": "206faf30-7bfa-488b-8b92-c33be9fa25eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_otus_portrait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2032,
    "bbox_left": 39,
    "bbox_right": 1401,
    "bbox_top": 63,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f2d571c-505c-491d-bf49-e0bbe2a57622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "206faf30-7bfa-488b-8b92-c33be9fa25eb",
            "compositeImage": {
                "id": "bd71a60d-d69f-4334-be23-a6c26accae19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f2d571c-505c-491d-bf49-e0bbe2a57622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d321d22-dd3f-4c48-b320-5586d298e6b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f2d571c-505c-491d-bf49-e0bbe2a57622",
                    "LayerId": "9724a164-d405-4a53-8013-6a53e85b04c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2095,
    "layers": [
        {
            "id": "9724a164-d405-4a53-8013-6a53e85b04c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "206faf30-7bfa-488b-8b92-c33be9fa25eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1568,
    "xorig": 0,
    "yorig": 0
}