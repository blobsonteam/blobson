{
    "id": "06e2e79f-2a1c-46e9-946e-9039a3af3f18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_otus_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 12,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12f9239a-c08b-43a4-b445-075fac159d31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06e2e79f-2a1c-46e9-946e-9039a3af3f18",
            "compositeImage": {
                "id": "16e9df2e-d545-47fd-9d4c-127f8acbbb1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12f9239a-c08b-43a4-b445-075fac159d31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6545a167-e762-4b18-98b4-26fad4a47938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f9239a-c08b-43a4-b445-075fac159d31",
                    "LayerId": "0c373df9-ab9e-40e2-8174-a72278aef1c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "0c373df9-ab9e-40e2-8174-a72278aef1c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06e2e79f-2a1c-46e9-946e-9039a3af3f18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}