{
    "id": "040b23e0-d758-4dfd-ad20-036bb9de2d30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite202",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 0,
    "bbox_right": 84,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbd2b9fb-8ca6-4820-bab2-b823c81af4a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "040b23e0-d758-4dfd-ad20-036bb9de2d30",
            "compositeImage": {
                "id": "4f5695bc-0bdf-47cd-abb4-d1c29926d15a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbd2b9fb-8ca6-4820-bab2-b823c81af4a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c27935cb-2a62-44fb-b005-cd7a7bf45788",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbd2b9fb-8ca6-4820-bab2-b823c81af4a6",
                    "LayerId": "0e7a5b6a-b630-4200-8825-bf986697f79a"
                }
            ]
        },
        {
            "id": "6d460122-76e2-49d6-8b96-871c82267d06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "040b23e0-d758-4dfd-ad20-036bb9de2d30",
            "compositeImage": {
                "id": "ecc79be8-9fed-4fbd-80fa-905c040fb81d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d460122-76e2-49d6-8b96-871c82267d06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5af75a60-60bb-4ec8-921b-ead3e65fad2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d460122-76e2-49d6-8b96-871c82267d06",
                    "LayerId": "0e7a5b6a-b630-4200-8825-bf986697f79a"
                }
            ]
        },
        {
            "id": "3aceb135-79a0-4d51-9272-01563eefbe15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "040b23e0-d758-4dfd-ad20-036bb9de2d30",
            "compositeImage": {
                "id": "0fd74905-7c59-4f74-986d-1ac6a314c519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3aceb135-79a0-4d51-9272-01563eefbe15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "145439e1-4be5-4a2c-b2d7-ce6fd786ae89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3aceb135-79a0-4d51-9272-01563eefbe15",
                    "LayerId": "0e7a5b6a-b630-4200-8825-bf986697f79a"
                }
            ]
        },
        {
            "id": "330fe402-a350-4f76-962b-4904c56eb8d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "040b23e0-d758-4dfd-ad20-036bb9de2d30",
            "compositeImage": {
                "id": "dbb625f0-70db-4539-99c1-fa33c4d9a128",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "330fe402-a350-4f76-962b-4904c56eb8d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4345ae5-3b7d-4c2b-a1c4-52588aa2be11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "330fe402-a350-4f76-962b-4904c56eb8d1",
                    "LayerId": "0e7a5b6a-b630-4200-8825-bf986697f79a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 123,
    "layers": [
        {
            "id": "0e7a5b6a-b630-4200-8825-bf986697f79a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "040b23e0-d758-4dfd-ad20-036bb9de2d30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 85,
    "xorig": -39,
    "yorig": 41
}