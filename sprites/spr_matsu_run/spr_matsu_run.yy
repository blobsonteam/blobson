{
    "id": "7234c078-56bf-4e24-9af8-10304bd4e5ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 5,
    "bbox_right": 88,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37ade282-0ea3-43e9-8cce-b92ca2857c86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7234c078-56bf-4e24-9af8-10304bd4e5ad",
            "compositeImage": {
                "id": "632e9889-4446-4aba-8700-a11a5278a239",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37ade282-0ea3-43e9-8cce-b92ca2857c86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85b7e172-b692-459d-b10a-ff96b2323147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37ade282-0ea3-43e9-8cce-b92ca2857c86",
                    "LayerId": "e4fe2730-8340-4145-ba65-29bc3704144c"
                }
            ]
        },
        {
            "id": "8a297f79-34ad-4201-bcb6-d391ebc5dd35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7234c078-56bf-4e24-9af8-10304bd4e5ad",
            "compositeImage": {
                "id": "b0fae779-72d9-4d9c-bf4e-1e4187bfeed1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a297f79-34ad-4201-bcb6-d391ebc5dd35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db4f1c36-0d9e-402d-8fb2-06b51748031c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a297f79-34ad-4201-bcb6-d391ebc5dd35",
                    "LayerId": "e4fe2730-8340-4145-ba65-29bc3704144c"
                }
            ]
        },
        {
            "id": "c30660b5-f8b0-42a7-8ced-d21151747fe2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7234c078-56bf-4e24-9af8-10304bd4e5ad",
            "compositeImage": {
                "id": "38ed404c-e73c-43fc-b5f6-03ba99912c77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c30660b5-f8b0-42a7-8ced-d21151747fe2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f1275fe-78da-437b-9aa3-9acd95178865",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c30660b5-f8b0-42a7-8ced-d21151747fe2",
                    "LayerId": "e4fe2730-8340-4145-ba65-29bc3704144c"
                }
            ]
        },
        {
            "id": "166c9bbe-9ed9-44da-bb49-65ab41781eee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7234c078-56bf-4e24-9af8-10304bd4e5ad",
            "compositeImage": {
                "id": "db01ea55-2d55-445b-a8b5-03d1633d726d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "166c9bbe-9ed9-44da-bb49-65ab41781eee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beddf129-fc2e-42da-b577-640dd7966928",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "166c9bbe-9ed9-44da-bb49-65ab41781eee",
                    "LayerId": "e4fe2730-8340-4145-ba65-29bc3704144c"
                }
            ]
        },
        {
            "id": "3991f129-eced-4fcf-9be1-8cb7b1917e26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7234c078-56bf-4e24-9af8-10304bd4e5ad",
            "compositeImage": {
                "id": "2e52e32c-73f8-4aa2-9ea0-8cb103134249",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3991f129-eced-4fcf-9be1-8cb7b1917e26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1682ef4-a0ec-4bad-81b3-cf87e594388f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3991f129-eced-4fcf-9be1-8cb7b1917e26",
                    "LayerId": "e4fe2730-8340-4145-ba65-29bc3704144c"
                }
            ]
        },
        {
            "id": "1bfeef1a-df57-4552-957c-ae7f0f53a4fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7234c078-56bf-4e24-9af8-10304bd4e5ad",
            "compositeImage": {
                "id": "bfe035e2-3293-496d-adb6-b16bda6de257",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bfeef1a-df57-4552-957c-ae7f0f53a4fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa5749a5-7d07-41c6-92be-545b937304a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bfeef1a-df57-4552-957c-ae7f0f53a4fd",
                    "LayerId": "e4fe2730-8340-4145-ba65-29bc3704144c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 92,
    "layers": [
        {
            "id": "e4fe2730-8340-4145-ba65-29bc3704144c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7234c078-56bf-4e24-9af8-10304bd4e5ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 95,
    "xorig": 57,
    "yorig": 91
}