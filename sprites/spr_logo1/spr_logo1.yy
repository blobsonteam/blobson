{
    "id": "749d7b05-bb2b-4e92-83d3-a65b5764dfa7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_logo1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 221,
    "bbox_left": 0,
    "bbox_right": 178,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ecddbd33-5c10-4872-b8a3-56754a936f8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749d7b05-bb2b-4e92-83d3-a65b5764dfa7",
            "compositeImage": {
                "id": "d59b6b7f-0cfe-4df8-9540-d26349481449",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecddbd33-5c10-4872-b8a3-56754a936f8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5703a7d-02c7-4685-b812-45f32df233e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecddbd33-5c10-4872-b8a3-56754a936f8a",
                    "LayerId": "9f61e862-fcf0-4345-b468-57b93ddf74f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 222,
    "layers": [
        {
            "id": "9f61e862-fcf0-4345-b468-57b93ddf74f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "749d7b05-bb2b-4e92-83d3-a65b5764dfa7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 179,
    "xorig": 89,
    "yorig": 111
}