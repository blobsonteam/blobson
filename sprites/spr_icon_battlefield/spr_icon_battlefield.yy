{
    "id": "8613eaab-13c0-4144-8f6e-3034579a1df5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_battlefield",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41624109-4773-4b41-b92e-9db5019b6af6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8613eaab-13c0-4144-8f6e-3034579a1df5",
            "compositeImage": {
                "id": "5759265a-7ef9-4746-9723-0c4d91c70842",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41624109-4773-4b41-b92e-9db5019b6af6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "954df090-db8c-47ce-87aa-9fe212799e36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41624109-4773-4b41-b92e-9db5019b6af6",
                    "LayerId": "72e7ad59-d796-4a04-8f15-562acb23582b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "72e7ad59-d796-4a04-8f15-562acb23582b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8613eaab-13c0-4144-8f6e-3034579a1df5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}