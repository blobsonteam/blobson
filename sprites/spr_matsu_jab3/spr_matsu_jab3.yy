{
    "id": "f5e29fbf-99d2-4cdc-9f2d-091a5c3a98ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_jab3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 101,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "786ee35d-894f-4fd6-b960-006063d70b47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e29fbf-99d2-4cdc-9f2d-091a5c3a98ba",
            "compositeImage": {
                "id": "09c5223b-f717-4535-916f-23ec7278fac8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "786ee35d-894f-4fd6-b960-006063d70b47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eba03000-86ed-4710-a2b2-8ba61d8199cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "786ee35d-894f-4fd6-b960-006063d70b47",
                    "LayerId": "034f4775-f50c-414b-b076-4e1d30e37144"
                }
            ]
        },
        {
            "id": "ce69a142-410a-44f0-8ef5-dbd7a15db317",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e29fbf-99d2-4cdc-9f2d-091a5c3a98ba",
            "compositeImage": {
                "id": "cd781f22-c9a2-4bbf-b26b-aac38a86aab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce69a142-410a-44f0-8ef5-dbd7a15db317",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fcd2c9d-95af-4932-a87b-1d147c6c0fe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce69a142-410a-44f0-8ef5-dbd7a15db317",
                    "LayerId": "034f4775-f50c-414b-b076-4e1d30e37144"
                }
            ]
        },
        {
            "id": "9bce6822-0764-403e-b644-b173b7c0ac52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e29fbf-99d2-4cdc-9f2d-091a5c3a98ba",
            "compositeImage": {
                "id": "08b6c0bb-eca2-4e45-844e-c45cb4f2987e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bce6822-0764-403e-b644-b173b7c0ac52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0779dadb-7c0b-429b-a938-4391793d4c8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bce6822-0764-403e-b644-b173b7c0ac52",
                    "LayerId": "034f4775-f50c-414b-b076-4e1d30e37144"
                }
            ]
        },
        {
            "id": "2693c943-f133-4c77-98d6-55ad5a3a51e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e29fbf-99d2-4cdc-9f2d-091a5c3a98ba",
            "compositeImage": {
                "id": "13f47dbd-9d14-4b08-84b7-140068b8ce2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2693c943-f133-4c77-98d6-55ad5a3a51e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5fc5ae4-4575-4337-b928-19e7085ad623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2693c943-f133-4c77-98d6-55ad5a3a51e7",
                    "LayerId": "034f4775-f50c-414b-b076-4e1d30e37144"
                }
            ]
        },
        {
            "id": "1d530d4a-2714-4751-8b7a-f64a8b5571fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e29fbf-99d2-4cdc-9f2d-091a5c3a98ba",
            "compositeImage": {
                "id": "856c9ede-2e29-4d1e-8db4-5d99bbd78f47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d530d4a-2714-4751-8b7a-f64a8b5571fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "729eb360-2332-4183-bd14-7c12f47715ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d530d4a-2714-4751-8b7a-f64a8b5571fa",
                    "LayerId": "034f4775-f50c-414b-b076-4e1d30e37144"
                }
            ]
        },
        {
            "id": "6552596d-0680-42f4-97e1-a8378fb64cbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e29fbf-99d2-4cdc-9f2d-091a5c3a98ba",
            "compositeImage": {
                "id": "0e71fc05-7e46-4515-8090-27f39fa9b8b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6552596d-0680-42f4-97e1-a8378fb64cbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d690c5f0-1d5b-4058-a53e-45d0d20b0a37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6552596d-0680-42f4-97e1-a8378fb64cbd",
                    "LayerId": "034f4775-f50c-414b-b076-4e1d30e37144"
                }
            ]
        },
        {
            "id": "bf6a8e72-3906-4ce2-8236-39f48e5fbaab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e29fbf-99d2-4cdc-9f2d-091a5c3a98ba",
            "compositeImage": {
                "id": "1bc0bd3c-2012-425c-b9f0-48ae1b31b600",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf6a8e72-3906-4ce2-8236-39f48e5fbaab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57f7c5a4-6c6d-41f4-831e-62adebb3b3c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf6a8e72-3906-4ce2-8236-39f48e5fbaab",
                    "LayerId": "034f4775-f50c-414b-b076-4e1d30e37144"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "034f4775-f50c-414b-b076-4e1d30e37144",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5e29fbf-99d2-4cdc-9f2d-091a5c3a98ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 102,
    "xorig": 26,
    "yorig": 95
}