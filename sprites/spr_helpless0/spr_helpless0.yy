{
    "id": "abbbf1d0-b432-45cc-83ac-f1a2ec2996b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_helpless0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6af8b261-704b-4778-97af-c41a5ceba11f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abbbf1d0-b432-45cc-83ac-f1a2ec2996b8",
            "compositeImage": {
                "id": "f4de2c06-8b9d-489f-a538-5b2208c6781d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6af8b261-704b-4778-97af-c41a5ceba11f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc9d58a8-8834-49ea-8e14-9a64ba3d3e78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6af8b261-704b-4778-97af-c41a5ceba11f",
                    "LayerId": "ec356e3d-2dd7-4235-8293-89f5be9b52a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "ec356e3d-2dd7-4235-8293-89f5be9b52a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abbbf1d0-b432-45cc-83ac-f1a2ec2996b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 18
}