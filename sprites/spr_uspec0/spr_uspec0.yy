{
    "id": "740a9224-a87e-4c77-8f65-cdd234933e44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_uspec0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bdbc9ec5-d67d-4f4a-858a-1b3033e8c2b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740a9224-a87e-4c77-8f65-cdd234933e44",
            "compositeImage": {
                "id": "65a24005-2b9a-4e94-8729-07b447e2b7cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdbc9ec5-d67d-4f4a-858a-1b3033e8c2b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "784ff611-fc53-4b6a-a78f-006f3b04f3c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdbc9ec5-d67d-4f4a-858a-1b3033e8c2b8",
                    "LayerId": "83b3e6fa-38c4-4a4e-a7e7-ab8c2398216e"
                },
                {
                    "id": "981913ea-ccdb-44a4-9b3d-24a203fc2546",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdbc9ec5-d67d-4f4a-858a-1b3033e8c2b8",
                    "LayerId": "bf721ded-df8f-4f18-8670-654f0cfff1d8"
                }
            ]
        },
        {
            "id": "abb6309d-faf5-4379-83e4-efa239212805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740a9224-a87e-4c77-8f65-cdd234933e44",
            "compositeImage": {
                "id": "60db28cd-497b-40e3-b882-6bc743128dbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abb6309d-faf5-4379-83e4-efa239212805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee1935a0-f66a-473d-85a7-aff76e86a016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abb6309d-faf5-4379-83e4-efa239212805",
                    "LayerId": "83b3e6fa-38c4-4a4e-a7e7-ab8c2398216e"
                },
                {
                    "id": "87f0b364-6e20-4b1d-adb3-3cde90375fa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abb6309d-faf5-4379-83e4-efa239212805",
                    "LayerId": "bf721ded-df8f-4f18-8670-654f0cfff1d8"
                }
            ]
        },
        {
            "id": "b6a71e42-412c-48c7-98ef-854f8cc81578",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740a9224-a87e-4c77-8f65-cdd234933e44",
            "compositeImage": {
                "id": "c9224609-95ef-44b5-a923-4bd91372ac6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6a71e42-412c-48c7-98ef-854f8cc81578",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9598bf8-1761-46fb-8200-e295d31d31d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6a71e42-412c-48c7-98ef-854f8cc81578",
                    "LayerId": "83b3e6fa-38c4-4a4e-a7e7-ab8c2398216e"
                },
                {
                    "id": "defa0554-fa74-4568-9f60-6a2a03cac30d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6a71e42-412c-48c7-98ef-854f8cc81578",
                    "LayerId": "bf721ded-df8f-4f18-8670-654f0cfff1d8"
                }
            ]
        },
        {
            "id": "4a562130-c9cd-4092-a838-79a1c2702a09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740a9224-a87e-4c77-8f65-cdd234933e44",
            "compositeImage": {
                "id": "65f27a82-cdc0-4379-b69a-a78e6448cff2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a562130-c9cd-4092-a838-79a1c2702a09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "751c8dd8-59b6-4169-8f40-5cf3384c3eda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a562130-c9cd-4092-a838-79a1c2702a09",
                    "LayerId": "bf721ded-df8f-4f18-8670-654f0cfff1d8"
                },
                {
                    "id": "b6c1dae2-2bfd-4b4d-807a-00ba82df4b5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a562130-c9cd-4092-a838-79a1c2702a09",
                    "LayerId": "83b3e6fa-38c4-4a4e-a7e7-ab8c2398216e"
                }
            ]
        },
        {
            "id": "5b4c7c0b-e39d-4e12-8df8-2f644c2bba50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740a9224-a87e-4c77-8f65-cdd234933e44",
            "compositeImage": {
                "id": "ceb4e4b7-f9bd-42c7-aad2-fc69444790b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b4c7c0b-e39d-4e12-8df8-2f644c2bba50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dc59df3-1311-440f-9017-18dd9b2aff2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b4c7c0b-e39d-4e12-8df8-2f644c2bba50",
                    "LayerId": "bf721ded-df8f-4f18-8670-654f0cfff1d8"
                },
                {
                    "id": "df7d0e04-3074-4668-9756-1fce6c8ceb54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b4c7c0b-e39d-4e12-8df8-2f644c2bba50",
                    "LayerId": "83b3e6fa-38c4-4a4e-a7e7-ab8c2398216e"
                }
            ]
        },
        {
            "id": "52e7896a-4319-416f-b82e-9d6f70dba6a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740a9224-a87e-4c77-8f65-cdd234933e44",
            "compositeImage": {
                "id": "0d0e85dc-403d-4415-bf21-35305f5cf3ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e7896a-4319-416f-b82e-9d6f70dba6a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "984e41c2-698e-47e2-9091-bd9da89d0bc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e7896a-4319-416f-b82e-9d6f70dba6a6",
                    "LayerId": "83b3e6fa-38c4-4a4e-a7e7-ab8c2398216e"
                },
                {
                    "id": "eec0e342-b423-4c48-b3fc-5a2dada33068",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e7896a-4319-416f-b82e-9d6f70dba6a6",
                    "LayerId": "bf721ded-df8f-4f18-8670-654f0cfff1d8"
                }
            ]
        },
        {
            "id": "d9f208bc-1d83-4d0d-9482-0660119ad349",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "740a9224-a87e-4c77-8f65-cdd234933e44",
            "compositeImage": {
                "id": "1c0f9631-aa17-4db5-bb81-c96bbd7df8fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9f208bc-1d83-4d0d-9482-0660119ad349",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c055567-9fed-4577-a17c-d8be5134e5ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9f208bc-1d83-4d0d-9482-0660119ad349",
                    "LayerId": "83b3e6fa-38c4-4a4e-a7e7-ab8c2398216e"
                },
                {
                    "id": "b9c35860-cf1d-4ad0-b9b9-b7274d912cf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9f208bc-1d83-4d0d-9482-0660119ad349",
                    "LayerId": "bf721ded-df8f-4f18-8670-654f0cfff1d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bf721ded-df8f-4f18-8670-654f0cfff1d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "740a9224-a87e-4c77-8f65-cdd234933e44",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "83b3e6fa-38c4-4a4e-a7e7-ab8c2398216e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "740a9224-a87e-4c77-8f65-cdd234933e44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 17
}