{
    "id": "067a0551-d0a4-4686-9364-8f90d6827660",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spot_dodge0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "844ff89d-9a32-4a81-853a-e8e676e0dfdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "067a0551-d0a4-4686-9364-8f90d6827660",
            "compositeImage": {
                "id": "1ab099af-9ea9-473c-ab6b-d564e9206852",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "844ff89d-9a32-4a81-853a-e8e676e0dfdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31ae0936-5f77-4ea3-bd83-f5d4ebd26165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "844ff89d-9a32-4a81-853a-e8e676e0dfdf",
                    "LayerId": "7dd78bf9-2a64-452a-9458-ae73c9b185b2"
                }
            ]
        },
        {
            "id": "d3184b12-7cb6-470a-948c-7602c1c58515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "067a0551-d0a4-4686-9364-8f90d6827660",
            "compositeImage": {
                "id": "fd20cf11-ba11-4b82-abdc-bf6bfa89808a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3184b12-7cb6-470a-948c-7602c1c58515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ce5fab5-daba-4a69-8db6-6cc835d433ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3184b12-7cb6-470a-948c-7602c1c58515",
                    "LayerId": "7dd78bf9-2a64-452a-9458-ae73c9b185b2"
                }
            ]
        },
        {
            "id": "5dd04a97-40f1-46e6-8507-37b1093bf01d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "067a0551-d0a4-4686-9364-8f90d6827660",
            "compositeImage": {
                "id": "36277bf9-99c3-4af6-96b3-024d39eb2378",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dd04a97-40f1-46e6-8507-37b1093bf01d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acd882c6-ef5e-4e14-8de0-9673c336e01b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd04a97-40f1-46e6-8507-37b1093bf01d",
                    "LayerId": "7dd78bf9-2a64-452a-9458-ae73c9b185b2"
                }
            ]
        },
        {
            "id": "49fe5c28-6cb8-4da0-9235-092690fee703",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "067a0551-d0a4-4686-9364-8f90d6827660",
            "compositeImage": {
                "id": "d822b2d4-9e55-42a7-9870-1c53ccec50b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49fe5c28-6cb8-4da0-9235-092690fee703",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efc34377-baaa-4b03-91e8-7922a51e7e59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49fe5c28-6cb8-4da0-9235-092690fee703",
                    "LayerId": "7dd78bf9-2a64-452a-9458-ae73c9b185b2"
                }
            ]
        },
        {
            "id": "dcf0bc89-fb63-4528-9216-7cfed627b6b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "067a0551-d0a4-4686-9364-8f90d6827660",
            "compositeImage": {
                "id": "e6337096-daee-4e2a-bae8-8bb200783cc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcf0bc89-fb63-4528-9216-7cfed627b6b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c157f060-6d53-4319-bad7-dfea87a217ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcf0bc89-fb63-4528-9216-7cfed627b6b1",
                    "LayerId": "7dd78bf9-2a64-452a-9458-ae73c9b185b2"
                }
            ]
        },
        {
            "id": "882f4604-3c79-4fb5-b4ac-0443f33aa22f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "067a0551-d0a4-4686-9364-8f90d6827660",
            "compositeImage": {
                "id": "85d9d7c4-5d4f-49c1-ae76-4395eba4eb7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "882f4604-3c79-4fb5-b4ac-0443f33aa22f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a52e0ebb-edc7-42fc-b4ae-a106158cf272",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "882f4604-3c79-4fb5-b4ac-0443f33aa22f",
                    "LayerId": "7dd78bf9-2a64-452a-9458-ae73c9b185b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "7dd78bf9-2a64-452a-9458-ae73c9b185b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "067a0551-d0a4-4686-9364-8f90d6827660",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}