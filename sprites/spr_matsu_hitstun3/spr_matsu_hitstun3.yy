{
    "id": "c98da9f5-eeaa-491f-a211-bb71cfe518e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_hitstun3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 109,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bfe8561-780f-417e-b97b-229a09570ad1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c98da9f5-eeaa-491f-a211-bb71cfe518e6",
            "compositeImage": {
                "id": "322b9646-4d6c-4cef-873e-e2b688c070c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bfe8561-780f-417e-b97b-229a09570ad1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efc609df-d828-4a1e-aae3-084684cf41da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bfe8561-780f-417e-b97b-229a09570ad1",
                    "LayerId": "bb99864e-1bd4-4271-833d-a7fba9885554"
                }
            ]
        },
        {
            "id": "d9dd0802-f5b9-41fd-9e2c-c88ab7cd97ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c98da9f5-eeaa-491f-a211-bb71cfe518e6",
            "compositeImage": {
                "id": "2d43d2e6-5660-4081-895c-9adbead91079",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9dd0802-f5b9-41fd-9e2c-c88ab7cd97ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a22c842e-0340-4ecf-9bcc-6638cc0af55d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9dd0802-f5b9-41fd-9e2c-c88ab7cd97ec",
                    "LayerId": "bb99864e-1bd4-4271-833d-a7fba9885554"
                }
            ]
        },
        {
            "id": "4f1be235-5acf-4873-944b-e48a61d97d08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c98da9f5-eeaa-491f-a211-bb71cfe518e6",
            "compositeImage": {
                "id": "48ead53b-d72f-459d-88f0-49aaaf40c878",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f1be235-5acf-4873-944b-e48a61d97d08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "532935b2-ca74-401c-a867-7c8323346286",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f1be235-5acf-4873-944b-e48a61d97d08",
                    "LayerId": "bb99864e-1bd4-4271-833d-a7fba9885554"
                }
            ]
        },
        {
            "id": "8f790ea6-74ee-47f9-9296-5321b539b50f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c98da9f5-eeaa-491f-a211-bb71cfe518e6",
            "compositeImage": {
                "id": "114e98b0-07d8-4147-bab7-49a03a15c3fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f790ea6-74ee-47f9-9296-5321b539b50f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a990a2f9-ac04-4d89-90ab-381502e9b54a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f790ea6-74ee-47f9-9296-5321b539b50f",
                    "LayerId": "bb99864e-1bd4-4271-833d-a7fba9885554"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "bb99864e-1bd4-4271-833d-a7fba9885554",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c98da9f5-eeaa-491f-a211-bb71cfe518e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 110,
    "xorig": 36,
    "yorig": 89
}