{
    "id": "b4019245-be20-44af-82bc-5bfa22267b70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_midair0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af54b76b-9bf3-4860-af72-758c6088d86d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4019245-be20-44af-82bc-5bfa22267b70",
            "compositeImage": {
                "id": "bb9213b3-8e69-4555-b96f-7a0824f7fb6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af54b76b-9bf3-4860-af72-758c6088d86d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34969f38-9170-4473-9378-0b44894d2c7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af54b76b-9bf3-4860-af72-758c6088d86d",
                    "LayerId": "db336629-b463-4c3a-a5b5-b63e84860c2b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "db336629-b463-4c3a-a5b5-b63e84860c2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4019245-be20-44af-82bc-5bfa22267b70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}