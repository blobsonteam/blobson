{
    "id": "0f04389d-bdf1-45b2-bfc4-4a342cec3534",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sarina_cs_portrait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 439,
    "bbox_left": 1,
    "bbox_right": 476,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65d7c476-16e0-431b-a281-2ffad035f529",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f04389d-bdf1-45b2-bfc4-4a342cec3534",
            "compositeImage": {
                "id": "40730bfd-0a8d-4667-b20c-d08da522d136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65d7c476-16e0-431b-a281-2ffad035f529",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "323df336-509c-40f0-9128-c7e8ede6a178",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65d7c476-16e0-431b-a281-2ffad035f529",
                    "LayerId": "758c3208-0779-4f41-8761-b12d83215065"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 469,
    "layers": [
        {
            "id": "758c3208-0779-4f41-8761-b12d83215065",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f04389d-bdf1-45b2-bfc4-4a342cec3534",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 484,
    "xorig": 0,
    "yorig": 0
}