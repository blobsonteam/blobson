{
    "id": "7fe47009-5cbe-4566-a10f-1478dedbc651",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ledge_attack0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 23,
    "bbox_right": 55,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cdc35578-8fb3-4005-91a5-4ecda3a7599d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fe47009-5cbe-4566-a10f-1478dedbc651",
            "compositeImage": {
                "id": "3c35838f-b7e2-4887-8011-0f1191191c43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdc35578-8fb3-4005-91a5-4ecda3a7599d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c6eb590-a0ba-4b73-8ab4-e955f568fc7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdc35578-8fb3-4005-91a5-4ecda3a7599d",
                    "LayerId": "ee25b738-2237-4307-a7dd-66667f015fdc"
                }
            ]
        },
        {
            "id": "f60677ee-1fcf-4290-b8a0-86ccb0d64d66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fe47009-5cbe-4566-a10f-1478dedbc651",
            "compositeImage": {
                "id": "af6b8914-f98b-4500-880b-1d99051b1109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f60677ee-1fcf-4290-b8a0-86ccb0d64d66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "924e3fbc-ea82-4428-a3c2-a96e345a9065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f60677ee-1fcf-4290-b8a0-86ccb0d64d66",
                    "LayerId": "ee25b738-2237-4307-a7dd-66667f015fdc"
                }
            ]
        },
        {
            "id": "1fded1e8-25f7-4e43-bfd8-0cd718b71d69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fe47009-5cbe-4566-a10f-1478dedbc651",
            "compositeImage": {
                "id": "24b2172e-a5ac-4d06-84de-26358dad400c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fded1e8-25f7-4e43-bfd8-0cd718b71d69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5229bbb-d80a-4f12-86d1-0188c680d641",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fded1e8-25f7-4e43-bfd8-0cd718b71d69",
                    "LayerId": "ee25b738-2237-4307-a7dd-66667f015fdc"
                }
            ]
        },
        {
            "id": "5a72707d-95da-46f3-aa1b-0441d70ebf34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fe47009-5cbe-4566-a10f-1478dedbc651",
            "compositeImage": {
                "id": "8bcb049e-e101-407c-829e-ccba685dee52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a72707d-95da-46f3-aa1b-0441d70ebf34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b4564a8-925f-4a06-ad1c-ee77bb5694d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a72707d-95da-46f3-aa1b-0441d70ebf34",
                    "LayerId": "ee25b738-2237-4307-a7dd-66667f015fdc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ee25b738-2237-4307-a7dd-66667f015fdc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7fe47009-5cbe-4566-a10f-1478dedbc651",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 25
}