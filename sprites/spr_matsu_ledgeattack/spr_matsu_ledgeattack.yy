{
    "id": "333e751b-b781-42fe-8bdf-3c080ba3152d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_ledgeattack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 0,
    "bbox_right": 107,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31a9772c-a830-4721-95de-70b15ff910d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "333e751b-b781-42fe-8bdf-3c080ba3152d",
            "compositeImage": {
                "id": "112e4acc-aa48-4b10-a29c-a67a48b11ae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31a9772c-a830-4721-95de-70b15ff910d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9010935-a79b-490a-a1e0-fcbfb5329e4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31a9772c-a830-4721-95de-70b15ff910d9",
                    "LayerId": "b4634f4b-5a3c-49a1-88b4-1d0c28d80398"
                }
            ]
        },
        {
            "id": "d596dbb0-2114-483e-adbe-fffd2f3dfe27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "333e751b-b781-42fe-8bdf-3c080ba3152d",
            "compositeImage": {
                "id": "3cd3debd-72c3-4e74-b7e6-dce64fd39615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d596dbb0-2114-483e-adbe-fffd2f3dfe27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a44de4ef-484f-419c-9f35-273fc003617d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d596dbb0-2114-483e-adbe-fffd2f3dfe27",
                    "LayerId": "b4634f4b-5a3c-49a1-88b4-1d0c28d80398"
                }
            ]
        },
        {
            "id": "4aa9acc8-5f4f-45fa-a530-415f8016d4d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "333e751b-b781-42fe-8bdf-3c080ba3152d",
            "compositeImage": {
                "id": "e796b865-2576-45c5-9bb9-8fe94eac397c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4aa9acc8-5f4f-45fa-a530-415f8016d4d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "768c4b5d-5302-4536-b514-1a7c1d5b8584",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4aa9acc8-5f4f-45fa-a530-415f8016d4d0",
                    "LayerId": "b4634f4b-5a3c-49a1-88b4-1d0c28d80398"
                }
            ]
        },
        {
            "id": "1a2f2c82-9f7d-48eb-be13-890a946624de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "333e751b-b781-42fe-8bdf-3c080ba3152d",
            "compositeImage": {
                "id": "f2b01f20-568f-4219-98ce-5a96d72b3733",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a2f2c82-9f7d-48eb-be13-890a946624de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed7fc1d7-65be-41f3-bd5a-d0098a205fee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a2f2c82-9f7d-48eb-be13-890a946624de",
                    "LayerId": "b4634f4b-5a3c-49a1-88b4-1d0c28d80398"
                }
            ]
        },
        {
            "id": "6c688347-256d-4fec-afb5-4c65ff65efa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "333e751b-b781-42fe-8bdf-3c080ba3152d",
            "compositeImage": {
                "id": "398b64bd-aca2-4939-953d-7013adb5b7f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c688347-256d-4fec-afb5-4c65ff65efa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bcb1059-59b6-48bc-ab55-2d6778540472",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c688347-256d-4fec-afb5-4c65ff65efa9",
                    "LayerId": "b4634f4b-5a3c-49a1-88b4-1d0c28d80398"
                }
            ]
        },
        {
            "id": "fab0bfa1-8791-4923-ab75-14c6d7fedad7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "333e751b-b781-42fe-8bdf-3c080ba3152d",
            "compositeImage": {
                "id": "815ba28b-c498-4f94-b0d4-c55042c75e48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fab0bfa1-8791-4923-ab75-14c6d7fedad7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e192b78a-70c1-4767-8531-83410f36b94d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fab0bfa1-8791-4923-ab75-14c6d7fedad7",
                    "LayerId": "b4634f4b-5a3c-49a1-88b4-1d0c28d80398"
                }
            ]
        },
        {
            "id": "4c3b15a5-5e9c-426f-8744-68d69c0f5670",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "333e751b-b781-42fe-8bdf-3c080ba3152d",
            "compositeImage": {
                "id": "45931a5c-fc64-4e93-bbdf-4cdfba781004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c3b15a5-5e9c-426f-8744-68d69c0f5670",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac37eca2-9629-4450-868b-0103da4625ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c3b15a5-5e9c-426f-8744-68d69c0f5670",
                    "LayerId": "b4634f4b-5a3c-49a1-88b4-1d0c28d80398"
                }
            ]
        },
        {
            "id": "6c2e31bc-6c71-4a58-9f52-2528a23ce24d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "333e751b-b781-42fe-8bdf-3c080ba3152d",
            "compositeImage": {
                "id": "4a5b4f3c-3c66-453e-9884-2e42f2b72074",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c2e31bc-6c71-4a58-9f52-2528a23ce24d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d3bd0ab-192e-4cb9-ac73-b2a482d462fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c2e31bc-6c71-4a58-9f52-2528a23ce24d",
                    "LayerId": "b4634f4b-5a3c-49a1-88b4-1d0c28d80398"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 91,
    "layers": [
        {
            "id": "b4634f4b-5a3c-49a1-88b4-1d0c28d80398",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "333e751b-b781-42fe-8bdf-3c080ba3152d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 45,
    "yorig": 90
}