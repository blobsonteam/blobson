{
    "id": "3cc08042-198c-4673-8204-ca261fad9f49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb8b2ed2-9e82-4fc1-be19-a5f657377d7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cc08042-198c-4673-8204-ca261fad9f49",
            "compositeImage": {
                "id": "05a1fb1f-a1ef-418e-8886-a7c23c99e2eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb8b2ed2-9e82-4fc1-be19-a5f657377d7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a7016c7-1e2d-4e6a-bd80-56fa88367bac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb8b2ed2-9e82-4fc1-be19-a5f657377d7f",
                    "LayerId": "0ba5fb0b-6986-4374-97b8-9af3e845639b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "0ba5fb0b-6986-4374-97b8-9af3e845639b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3cc08042-198c-4673-8204-ca261fad9f49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}