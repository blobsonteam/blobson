{
    "id": "73da8f59-4ab5-440d-8824-1d57b4fd0492",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_dheavy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 0,
    "bbox_right": 130,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a677f5f-60c6-42d8-82e8-297b9a885aa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73da8f59-4ab5-440d-8824-1d57b4fd0492",
            "compositeImage": {
                "id": "1c912fd8-4cdb-4023-a183-00b6632b9c58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a677f5f-60c6-42d8-82e8-297b9a885aa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee77a980-baf3-42be-aac0-fd92908f7f66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a677f5f-60c6-42d8-82e8-297b9a885aa3",
                    "LayerId": "26fdbf66-b8e6-4087-9ba9-4ab9b0e186a9"
                }
            ]
        },
        {
            "id": "9c9dad6d-a58d-483b-9d74-e2e7afe246ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73da8f59-4ab5-440d-8824-1d57b4fd0492",
            "compositeImage": {
                "id": "81628a67-a8ee-43bd-a0e3-1497d327ad57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c9dad6d-a58d-483b-9d74-e2e7afe246ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a526f698-dcf4-4f37-898e-e83d517f96d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c9dad6d-a58d-483b-9d74-e2e7afe246ed",
                    "LayerId": "26fdbf66-b8e6-4087-9ba9-4ab9b0e186a9"
                }
            ]
        },
        {
            "id": "13d22103-490f-4105-9491-5c16f9174ff9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73da8f59-4ab5-440d-8824-1d57b4fd0492",
            "compositeImage": {
                "id": "f445b47d-98f0-41c9-944f-376056cc7dbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13d22103-490f-4105-9491-5c16f9174ff9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b10a2bf-ce32-4191-8488-1603adf4d7d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13d22103-490f-4105-9491-5c16f9174ff9",
                    "LayerId": "26fdbf66-b8e6-4087-9ba9-4ab9b0e186a9"
                }
            ]
        },
        {
            "id": "f0c74276-f0ac-457e-89f2-6ed486fd3f85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73da8f59-4ab5-440d-8824-1d57b4fd0492",
            "compositeImage": {
                "id": "0d869df7-9331-4ca3-af5a-fbdc09123a27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0c74276-f0ac-457e-89f2-6ed486fd3f85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f595e03-7b77-407e-a714-9a1b3f95d439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0c74276-f0ac-457e-89f2-6ed486fd3f85",
                    "LayerId": "26fdbf66-b8e6-4087-9ba9-4ab9b0e186a9"
                }
            ]
        },
        {
            "id": "7246c84d-69f8-4022-9823-54cceb07e71d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73da8f59-4ab5-440d-8824-1d57b4fd0492",
            "compositeImage": {
                "id": "89a9e897-4d66-45b8-a476-87c11591999a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7246c84d-69f8-4022-9823-54cceb07e71d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bccdd94-6508-4253-b06c-19bff29a4166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7246c84d-69f8-4022-9823-54cceb07e71d",
                    "LayerId": "26fdbf66-b8e6-4087-9ba9-4ab9b0e186a9"
                }
            ]
        },
        {
            "id": "e8309ce5-b562-4229-9018-43b3d21d58f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73da8f59-4ab5-440d-8824-1d57b4fd0492",
            "compositeImage": {
                "id": "5ef403f5-d754-4341-beab-d133964ca805",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8309ce5-b562-4229-9018-43b3d21d58f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb5a3c7c-5c67-452a-a442-1e8e1046d1c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8309ce5-b562-4229-9018-43b3d21d58f3",
                    "LayerId": "26fdbf66-b8e6-4087-9ba9-4ab9b0e186a9"
                }
            ]
        },
        {
            "id": "322adf27-6178-4a4c-9e14-15cff3025fa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73da8f59-4ab5-440d-8824-1d57b4fd0492",
            "compositeImage": {
                "id": "d11bd612-25ec-46ff-b3be-d513831412ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "322adf27-6178-4a4c-9e14-15cff3025fa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daf70ff7-7b7d-4d49-9174-7c6ea6be46bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "322adf27-6178-4a4c-9e14-15cff3025fa1",
                    "LayerId": "26fdbf66-b8e6-4087-9ba9-4ab9b0e186a9"
                }
            ]
        },
        {
            "id": "de1ddcc5-1033-4a85-b710-5a376f33faa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73da8f59-4ab5-440d-8824-1d57b4fd0492",
            "compositeImage": {
                "id": "675635fb-b72a-4e73-8f0d-2214f2b6f723",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de1ddcc5-1033-4a85-b710-5a376f33faa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d60e0037-a19a-4cea-bb8e-de7d43ce1f07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de1ddcc5-1033-4a85-b710-5a376f33faa5",
                    "LayerId": "26fdbf66-b8e6-4087-9ba9-4ab9b0e186a9"
                }
            ]
        },
        {
            "id": "3cd9a6e3-6ae7-416b-8910-9ea45ddd8eed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73da8f59-4ab5-440d-8824-1d57b4fd0492",
            "compositeImage": {
                "id": "46225a2d-073a-463f-90bc-4268f9224b65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cd9a6e3-6ae7-416b-8910-9ea45ddd8eed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c259e81-a86e-4d54-b038-2e48242636e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cd9a6e3-6ae7-416b-8910-9ea45ddd8eed",
                    "LayerId": "26fdbf66-b8e6-4087-9ba9-4ab9b0e186a9"
                }
            ]
        },
        {
            "id": "cf6a7b36-3adf-4a5a-bf2b-b1820c944152",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73da8f59-4ab5-440d-8824-1d57b4fd0492",
            "compositeImage": {
                "id": "118dbc44-bf86-498f-9162-5c4a2c24c3ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf6a7b36-3adf-4a5a-bf2b-b1820c944152",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a162faa7-9ca7-4a1b-9a4f-5c7180141a05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf6a7b36-3adf-4a5a-bf2b-b1820c944152",
                    "LayerId": "26fdbf66-b8e6-4087-9ba9-4ab9b0e186a9"
                }
            ]
        },
        {
            "id": "c674ae82-1cf6-4cc1-ad21-cea645cb5a24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73da8f59-4ab5-440d-8824-1d57b4fd0492",
            "compositeImage": {
                "id": "c97432b5-4ad7-4f3d-8e90-b217a9904bf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c674ae82-1cf6-4cc1-ad21-cea645cb5a24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72f6b2aa-900d-4710-b646-7a5334f8ad21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c674ae82-1cf6-4cc1-ad21-cea645cb5a24",
                    "LayerId": "26fdbf66-b8e6-4087-9ba9-4ab9b0e186a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 108,
    "layers": [
        {
            "id": "26fdbf66-b8e6-4087-9ba9-4ab9b0e186a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73da8f59-4ab5-440d-8824-1d57b4fd0492",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 131,
    "xorig": 66,
    "yorig": 107
}