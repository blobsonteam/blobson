{
    "id": "307534f9-4195-43ad-8164-3f13eca0f07b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_uair_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 27,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7cf814d3-6068-4720-ab94-fbc3d73e1530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "307534f9-4195-43ad-8164-3f13eca0f07b",
            "compositeImage": {
                "id": "299ce999-010a-49aa-9631-83b7d37ab574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cf814d3-6068-4720-ab94-fbc3d73e1530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca0c4eb7-5aff-40f8-ad5f-54c197c234c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cf814d3-6068-4720-ab94-fbc3d73e1530",
                    "LayerId": "abc46a28-9d26-41af-806c-39b9c39650df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 132,
    "layers": [
        {
            "id": "abc46a28-9d26-41af-806c-39b9c39650df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "307534f9-4195-43ad-8164-3f13eca0f07b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 29,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 39,
    "yorig": 129
}