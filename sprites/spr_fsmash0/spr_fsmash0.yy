{
    "id": "fb7e2e93-d648-41e2-b633-a357af9f8938",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fsmash0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 42,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a7dfd61-927e-4e64-8ee2-6d65f3cbfa3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb7e2e93-d648-41e2-b633-a357af9f8938",
            "compositeImage": {
                "id": "3d36529e-6207-466e-948b-ddcf4b3e4933",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a7dfd61-927e-4e64-8ee2-6d65f3cbfa3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c2a1e1a-76f3-480b-a31a-bcee21e05e63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a7dfd61-927e-4e64-8ee2-6d65f3cbfa3d",
                    "LayerId": "7d306019-a3c7-4f0c-b764-ae4a28ebc3af"
                }
            ]
        },
        {
            "id": "7e177796-3297-4a64-b7cb-b1b6aea6ed64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb7e2e93-d648-41e2-b633-a357af9f8938",
            "compositeImage": {
                "id": "01b6bac8-9739-4b34-adc1-e17fcbb2ef91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e177796-3297-4a64-b7cb-b1b6aea6ed64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aec64502-aacc-4c7a-83da-89e2a2a6ad48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e177796-3297-4a64-b7cb-b1b6aea6ed64",
                    "LayerId": "7d306019-a3c7-4f0c-b764-ae4a28ebc3af"
                }
            ]
        },
        {
            "id": "3ae0258e-2416-46f1-8baf-71e89cfcfa95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb7e2e93-d648-41e2-b633-a357af9f8938",
            "compositeImage": {
                "id": "0413661f-21d8-4e7e-a385-04d9f392e6f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ae0258e-2416-46f1-8baf-71e89cfcfa95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "365cf2ac-c676-4298-b169-3ec1e8101cbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ae0258e-2416-46f1-8baf-71e89cfcfa95",
                    "LayerId": "7d306019-a3c7-4f0c-b764-ae4a28ebc3af"
                }
            ]
        },
        {
            "id": "fc0a81a3-61c0-47d2-8939-9135dfe1a284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb7e2e93-d648-41e2-b633-a357af9f8938",
            "compositeImage": {
                "id": "9f1db7fa-dd4d-4b10-ba92-e5410c470ba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc0a81a3-61c0-47d2-8939-9135dfe1a284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e745bc4-299e-4a33-a4d5-b4b2c64377f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc0a81a3-61c0-47d2-8939-9135dfe1a284",
                    "LayerId": "7d306019-a3c7-4f0c-b764-ae4a28ebc3af"
                }
            ]
        },
        {
            "id": "fc71f10d-1891-409d-a42e-b988ac7eec80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb7e2e93-d648-41e2-b633-a357af9f8938",
            "compositeImage": {
                "id": "b0253274-fa60-46bf-a72d-3e644f4ea793",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc71f10d-1891-409d-a42e-b988ac7eec80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5ee6d29-3d4c-47fe-86fa-3839b6184105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc71f10d-1891-409d-a42e-b988ac7eec80",
                    "LayerId": "7d306019-a3c7-4f0c-b764-ae4a28ebc3af"
                }
            ]
        },
        {
            "id": "07da9a3a-2ff5-4f52-a3f5-589e34a51015",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb7e2e93-d648-41e2-b633-a357af9f8938",
            "compositeImage": {
                "id": "65d32307-36d2-4d0f-8fe4-e3987fc36430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07da9a3a-2ff5-4f52-a3f5-589e34a51015",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7acf8aab-3a3e-45cc-a1b7-0bef03d358f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07da9a3a-2ff5-4f52-a3f5-589e34a51015",
                    "LayerId": "7d306019-a3c7-4f0c-b764-ae4a28ebc3af"
                }
            ]
        },
        {
            "id": "2f003c24-a6ad-4c11-b787-cc7bd2ef4202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb7e2e93-d648-41e2-b633-a357af9f8938",
            "compositeImage": {
                "id": "a850a988-9d22-4731-a3bf-80cfc9524f7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f003c24-a6ad-4c11-b787-cc7bd2ef4202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3d3cb25-5745-4dc6-9c59-e6923a8cf59c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f003c24-a6ad-4c11-b787-cc7bd2ef4202",
                    "LayerId": "7d306019-a3c7-4f0c-b764-ae4a28ebc3af"
                }
            ]
        },
        {
            "id": "aa1f2fd8-ba71-4d35-8e2f-eccf26e00c13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb7e2e93-d648-41e2-b633-a357af9f8938",
            "compositeImage": {
                "id": "284dc6f1-e6f5-4df3-bea0-2f83cbd8e15e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa1f2fd8-ba71-4d35-8e2f-eccf26e00c13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36c8b613-ee42-4be9-8005-9c78c642869e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa1f2fd8-ba71-4d35-8e2f-eccf26e00c13",
                    "LayerId": "7d306019-a3c7-4f0c-b764-ae4a28ebc3af"
                }
            ]
        },
        {
            "id": "164e5343-52be-4e1c-a8f1-b75a80700df8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb7e2e93-d648-41e2-b633-a357af9f8938",
            "compositeImage": {
                "id": "6647b9c9-4e06-48ac-9027-739d7062efdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "164e5343-52be-4e1c-a8f1-b75a80700df8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63e5965b-91b9-4131-9e8e-826fdd253576",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "164e5343-52be-4e1c-a8f1-b75a80700df8",
                    "LayerId": "7d306019-a3c7-4f0c-b764-ae4a28ebc3af"
                }
            ]
        },
        {
            "id": "b3856529-08bf-48d8-8f9b-67761d199455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb7e2e93-d648-41e2-b633-a357af9f8938",
            "compositeImage": {
                "id": "46296ce1-3346-4662-b7e6-95147a43c102",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3856529-08bf-48d8-8f9b-67761d199455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fefd6b51-3f13-4470-bfdf-9001784e14cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3856529-08bf-48d8-8f9b-67761d199455",
                    "LayerId": "7d306019-a3c7-4f0c-b764-ae4a28ebc3af"
                }
            ]
        },
        {
            "id": "a39dd381-f724-4b39-a6e5-014c7644dda6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb7e2e93-d648-41e2-b633-a357af9f8938",
            "compositeImage": {
                "id": "7bdc5ae4-b4a1-4302-a3d4-08f249052e94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a39dd381-f724-4b39-a6e5-014c7644dda6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "164bc635-07b1-4207-a0e5-d3965711fa68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a39dd381-f724-4b39-a6e5-014c7644dda6",
                    "LayerId": "7d306019-a3c7-4f0c-b764-ae4a28ebc3af"
                }
            ]
        },
        {
            "id": "d66a2834-02ef-4a1c-bd0a-0ba8be651917",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb7e2e93-d648-41e2-b633-a357af9f8938",
            "compositeImage": {
                "id": "3588f59b-36eb-4e33-9659-519ef0c52383",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d66a2834-02ef-4a1c-bd0a-0ba8be651917",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "522d5b59-82aa-446f-bcdd-7ec831579c15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d66a2834-02ef-4a1c-bd0a-0ba8be651917",
                    "LayerId": "7d306019-a3c7-4f0c-b764-ae4a28ebc3af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "7d306019-a3c7-4f0c-b764-ae4a28ebc3af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb7e2e93-d648-41e2-b633-a357af9f8938",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 8,
    "yorig": 20
}