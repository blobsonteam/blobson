{
    "id": "e1901205-8175-4104-be1e-5ce32d2357c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_waveland0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e01672e1-a112-4427-8c3f-97ff9157f839",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1901205-8175-4104-be1e-5ce32d2357c4",
            "compositeImage": {
                "id": "df5b0577-e7d8-4f59-bfba-bf33029c5e99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e01672e1-a112-4427-8c3f-97ff9157f839",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a460d85-5f96-4256-9ef6-42e60c05ccee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e01672e1-a112-4427-8c3f-97ff9157f839",
                    "LayerId": "4255bb24-a616-454c-9a88-79ec4d093ca7"
                }
            ]
        },
        {
            "id": "c8862153-0bcb-4890-9ee8-9f055243b58f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1901205-8175-4104-be1e-5ce32d2357c4",
            "compositeImage": {
                "id": "d2cf730e-2b37-4b0f-a1e9-f49b71574042",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8862153-0bcb-4890-9ee8-9f055243b58f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcb60933-a668-4450-aa94-069092fea76c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8862153-0bcb-4890-9ee8-9f055243b58f",
                    "LayerId": "4255bb24-a616-454c-9a88-79ec4d093ca7"
                }
            ]
        },
        {
            "id": "74af9f69-943d-408e-befb-eb948f4d0436",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1901205-8175-4104-be1e-5ce32d2357c4",
            "compositeImage": {
                "id": "d4bdba3d-154f-44a3-803c-e65903001a83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74af9f69-943d-408e-befb-eb948f4d0436",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1537b609-3d79-4382-82cb-e5cf4dfece1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74af9f69-943d-408e-befb-eb948f4d0436",
                    "LayerId": "4255bb24-a616-454c-9a88-79ec4d093ca7"
                }
            ]
        },
        {
            "id": "f47a67eb-56b4-4d94-88ab-5d4771436467",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1901205-8175-4104-be1e-5ce32d2357c4",
            "compositeImage": {
                "id": "7b358daf-c135-480f-961e-9544a14f5ee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f47a67eb-56b4-4d94-88ab-5d4771436467",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01250619-f157-4041-b720-c4334230b3c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f47a67eb-56b4-4d94-88ab-5d4771436467",
                    "LayerId": "4255bb24-a616-454c-9a88-79ec4d093ca7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "4255bb24-a616-454c-9a88-79ec4d093ca7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1901205-8175-4104-be1e-5ce32d2357c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}