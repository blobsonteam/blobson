{
    "id": "831bab4c-0c5b-457b-bea2-7b665ea1417c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_bthrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 0,
    "bbox_right": 84,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5f7f512-1316-43bd-9183-11fa3d5c3534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "831bab4c-0c5b-457b-bea2-7b665ea1417c",
            "compositeImage": {
                "id": "9c31cc19-c068-474d-9cd9-b771975ff02a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5f7f512-1316-43bd-9183-11fa3d5c3534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa03ca1f-da6e-48d5-907d-176eb361797f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5f7f512-1316-43bd-9183-11fa3d5c3534",
                    "LayerId": "dc176dba-20a1-4008-accd-b991260f038d"
                }
            ]
        },
        {
            "id": "f3d2b1d8-f5c9-488e-9035-3c4369d19fad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "831bab4c-0c5b-457b-bea2-7b665ea1417c",
            "compositeImage": {
                "id": "0a97fa99-b31a-4072-a8d2-c9442b125ef9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3d2b1d8-f5c9-488e-9035-3c4369d19fad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41508768-98b6-4021-99a6-ba6a63564b3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3d2b1d8-f5c9-488e-9035-3c4369d19fad",
                    "LayerId": "dc176dba-20a1-4008-accd-b991260f038d"
                }
            ]
        },
        {
            "id": "92cd9e61-3d25-4dd5-8563-2d16779a66d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "831bab4c-0c5b-457b-bea2-7b665ea1417c",
            "compositeImage": {
                "id": "ee859810-081d-403e-bc56-6ecad2c03eac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92cd9e61-3d25-4dd5-8563-2d16779a66d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6448c23-e90b-406e-8d65-197011abffdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92cd9e61-3d25-4dd5-8563-2d16779a66d4",
                    "LayerId": "dc176dba-20a1-4008-accd-b991260f038d"
                }
            ]
        },
        {
            "id": "572c935d-bcfc-4ff7-9e7b-7428c201b840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "831bab4c-0c5b-457b-bea2-7b665ea1417c",
            "compositeImage": {
                "id": "f6998872-1fc1-4d06-82db-fbaa0783600a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "572c935d-bcfc-4ff7-9e7b-7428c201b840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da1dec38-f23e-47f6-8eb2-e3526bdb2b8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "572c935d-bcfc-4ff7-9e7b-7428c201b840",
                    "LayerId": "dc176dba-20a1-4008-accd-b991260f038d"
                }
            ]
        },
        {
            "id": "b9995e2e-961f-43b5-91e6-6e335c1a9dc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "831bab4c-0c5b-457b-bea2-7b665ea1417c",
            "compositeImage": {
                "id": "f335713d-6c45-4fa2-87d4-2b46b5a7d7f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9995e2e-961f-43b5-91e6-6e335c1a9dc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa19bae7-546f-4a1a-8fe3-703065ef57df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9995e2e-961f-43b5-91e6-6e335c1a9dc9",
                    "LayerId": "dc176dba-20a1-4008-accd-b991260f038d"
                }
            ]
        },
        {
            "id": "25207968-9651-47b4-8a4b-62f9bbb6adc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "831bab4c-0c5b-457b-bea2-7b665ea1417c",
            "compositeImage": {
                "id": "0c1ea157-773f-400e-9b19-f3b734ae25a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25207968-9651-47b4-8a4b-62f9bbb6adc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bbe1435-195f-4256-9822-b419ebf57d1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25207968-9651-47b4-8a4b-62f9bbb6adc5",
                    "LayerId": "dc176dba-20a1-4008-accd-b991260f038d"
                }
            ]
        },
        {
            "id": "19a10b54-abc1-4de2-ae32-73b187ed3666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "831bab4c-0c5b-457b-bea2-7b665ea1417c",
            "compositeImage": {
                "id": "b3529ce7-d1fc-4157-b015-db6782b99bf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19a10b54-abc1-4de2-ae32-73b187ed3666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17c71adb-4f68-4163-847e-afa9b655d43f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19a10b54-abc1-4de2-ae32-73b187ed3666",
                    "LayerId": "dc176dba-20a1-4008-accd-b991260f038d"
                }
            ]
        },
        {
            "id": "86bf4450-ad71-4942-a094-98dcc67b9306",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "831bab4c-0c5b-457b-bea2-7b665ea1417c",
            "compositeImage": {
                "id": "603672a0-e784-4c46-abec-02d1d95c8e3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86bf4450-ad71-4942-a094-98dcc67b9306",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e937377e-d6f1-4cbb-81b1-52d7fbbee789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86bf4450-ad71-4942-a094-98dcc67b9306",
                    "LayerId": "dc176dba-20a1-4008-accd-b991260f038d"
                }
            ]
        },
        {
            "id": "9ef23991-0835-40a9-a60c-8ab099095a8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "831bab4c-0c5b-457b-bea2-7b665ea1417c",
            "compositeImage": {
                "id": "c3fa579f-d9c4-4c1b-8bd0-819bdf7c5773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ef23991-0835-40a9-a60c-8ab099095a8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7c61045-6d28-44ab-a79e-16dec8b2ad81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ef23991-0835-40a9-a60c-8ab099095a8d",
                    "LayerId": "dc176dba-20a1-4008-accd-b991260f038d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 111,
    "layers": [
        {
            "id": "dc176dba-20a1-4008-accd-b991260f038d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "831bab4c-0c5b-457b-bea2-7b665ea1417c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 85,
    "xorig": 45,
    "yorig": 110
}