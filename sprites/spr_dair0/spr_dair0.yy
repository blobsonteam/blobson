{
    "id": "fbe9faaf-f937-4655-b08f-0511eb442f57",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dair0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 7,
    "bbox_right": 26,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4625be36-6567-4c67-9f56-062acaca757a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbe9faaf-f937-4655-b08f-0511eb442f57",
            "compositeImage": {
                "id": "d1c42fa3-a77d-4286-a36e-8800ad524ad5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4625be36-6567-4c67-9f56-062acaca757a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba60b010-6c7e-43dc-8cf5-5c5a89e538e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4625be36-6567-4c67-9f56-062acaca757a",
                    "LayerId": "70613704-8970-4ece-8507-010630b4f6f5"
                }
            ]
        },
        {
            "id": "c5813c43-ff7f-41f9-8f4b-62ae0d01ec98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbe9faaf-f937-4655-b08f-0511eb442f57",
            "compositeImage": {
                "id": "e596e601-9a30-4d29-8d42-eaf7a16c021d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5813c43-ff7f-41f9-8f4b-62ae0d01ec98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f96016f2-7acb-4c65-bbf0-e7ffb780761e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5813c43-ff7f-41f9-8f4b-62ae0d01ec98",
                    "LayerId": "70613704-8970-4ece-8507-010630b4f6f5"
                }
            ]
        },
        {
            "id": "ffca6717-21e7-461e-a377-b7ca0124e6e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbe9faaf-f937-4655-b08f-0511eb442f57",
            "compositeImage": {
                "id": "cb37bccc-6e9d-4041-8c16-9d8acd40513c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffca6717-21e7-461e-a377-b7ca0124e6e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e2c7895-eead-49ae-bb65-df36b2bcd64f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffca6717-21e7-461e-a377-b7ca0124e6e5",
                    "LayerId": "70613704-8970-4ece-8507-010630b4f6f5"
                }
            ]
        },
        {
            "id": "25568c93-049b-4b34-93a7-830aef89c8f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbe9faaf-f937-4655-b08f-0511eb442f57",
            "compositeImage": {
                "id": "32f9d095-1081-4ab5-a7e4-f6912359c248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25568c93-049b-4b34-93a7-830aef89c8f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19e5beab-7912-49a1-a9b6-633642f7780e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25568c93-049b-4b34-93a7-830aef89c8f2",
                    "LayerId": "70613704-8970-4ece-8507-010630b4f6f5"
                }
            ]
        },
        {
            "id": "a1d93919-a19c-4c35-8f73-17dbaa2f5e31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbe9faaf-f937-4655-b08f-0511eb442f57",
            "compositeImage": {
                "id": "94055a40-9216-45de-a04e-232aab221718",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1d93919-a19c-4c35-8f73-17dbaa2f5e31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b49a80f-cdbf-4743-bc79-42fe910e6613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1d93919-a19c-4c35-8f73-17dbaa2f5e31",
                    "LayerId": "70613704-8970-4ece-8507-010630b4f6f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "70613704-8970-4ece-8507-010630b4f6f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbe9faaf-f937-4655-b08f-0511eb442f57",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 20
}