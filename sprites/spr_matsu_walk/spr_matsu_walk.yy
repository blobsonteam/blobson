{
    "id": "b0582dc7-452b-4e0d-888d-d38857518a04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 4,
    "bbox_right": 61,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "708c1715-f82b-4122-a522-123f1b4baf83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0582dc7-452b-4e0d-888d-d38857518a04",
            "compositeImage": {
                "id": "15fd779c-e22a-4b9d-973a-de24df73ed07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "708c1715-f82b-4122-a522-123f1b4baf83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41584e8b-5bda-47a7-a775-5759ebf62e47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "708c1715-f82b-4122-a522-123f1b4baf83",
                    "LayerId": "cf33425e-f6cd-41fc-b217-466e4f41f746"
                }
            ]
        },
        {
            "id": "dbc7772d-1cf0-479f-880e-c5fb651fe8ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0582dc7-452b-4e0d-888d-d38857518a04",
            "compositeImage": {
                "id": "c5b56626-d682-4079-a3aa-550bf37a4c69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbc7772d-1cf0-479f-880e-c5fb651fe8ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e3e91bd-793d-4cdc-bcb5-1b6eee3ad0ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbc7772d-1cf0-479f-880e-c5fb651fe8ac",
                    "LayerId": "cf33425e-f6cd-41fc-b217-466e4f41f746"
                }
            ]
        },
        {
            "id": "957e997b-c354-4bb7-9c1a-52770e98eafa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0582dc7-452b-4e0d-888d-d38857518a04",
            "compositeImage": {
                "id": "e1b889e5-799a-4461-b033-8dbc9f8fdf5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "957e997b-c354-4bb7-9c1a-52770e98eafa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f91e1b0-1543-4b06-907a-9e559f1bd5f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "957e997b-c354-4bb7-9c1a-52770e98eafa",
                    "LayerId": "cf33425e-f6cd-41fc-b217-466e4f41f746"
                }
            ]
        },
        {
            "id": "44a4fb2c-6091-4152-a886-b908d9687abb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0582dc7-452b-4e0d-888d-d38857518a04",
            "compositeImage": {
                "id": "53d5ea1a-2be9-49c9-bec1-d45a9efde470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44a4fb2c-6091-4152-a886-b908d9687abb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e8e61bf-2dc3-4f08-9062-b9cb02089cfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44a4fb2c-6091-4152-a886-b908d9687abb",
                    "LayerId": "cf33425e-f6cd-41fc-b217-466e4f41f746"
                }
            ]
        },
        {
            "id": "5e1017ad-9f86-4536-aeed-44b96afd69ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0582dc7-452b-4e0d-888d-d38857518a04",
            "compositeImage": {
                "id": "aaa871bf-6004-485f-bfcc-1e96a13c7444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e1017ad-9f86-4536-aeed-44b96afd69ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8212cb00-d023-4bae-9ce8-cc663ab407da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e1017ad-9f86-4536-aeed-44b96afd69ae",
                    "LayerId": "cf33425e-f6cd-41fc-b217-466e4f41f746"
                }
            ]
        },
        {
            "id": "15f9ea3e-d31f-4576-8b75-a593a4206bb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0582dc7-452b-4e0d-888d-d38857518a04",
            "compositeImage": {
                "id": "cd4abe95-53c0-4694-bc45-c4ff3113304e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15f9ea3e-d31f-4576-8b75-a593a4206bb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81ebbfef-dcf6-46aa-86f0-1514b323c279",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15f9ea3e-d31f-4576-8b75-a593a4206bb6",
                    "LayerId": "cf33425e-f6cd-41fc-b217-466e4f41f746"
                }
            ]
        },
        {
            "id": "e56b2f6d-c5a1-4553-95be-a15c15e4c36c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0582dc7-452b-4e0d-888d-d38857518a04",
            "compositeImage": {
                "id": "60c22f5a-a46a-4d10-94c8-7ab1133718e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e56b2f6d-c5a1-4553-95be-a15c15e4c36c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "381b1c6b-3791-4a38-82e6-7ff67ae7e0a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e56b2f6d-c5a1-4553-95be-a15c15e4c36c",
                    "LayerId": "cf33425e-f6cd-41fc-b217-466e4f41f746"
                }
            ]
        },
        {
            "id": "29016e34-f73f-417d-9fa1-21052228a5f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0582dc7-452b-4e0d-888d-d38857518a04",
            "compositeImage": {
                "id": "22bf25c7-4e9e-4e36-90b7-71f10a9d59ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29016e34-f73f-417d-9fa1-21052228a5f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9334c54d-2a14-44a3-bc30-acad2304afb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29016e34-f73f-417d-9fa1-21052228a5f9",
                    "LayerId": "cf33425e-f6cd-41fc-b217-466e4f41f746"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "cf33425e-f6cd-41fc-b217-466e4f41f746",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0582dc7-452b-4e0d-888d-d38857518a04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0.2,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 30,
    "yorig": 99
}