{
    "id": "b3b166a4-d212-4585-9bd8-ff26dc0d0cd7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_crouch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 15,
    "bbox_right": 79,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f630a5ef-6aba-489d-b5c4-edfad64ef505",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b166a4-d212-4585-9bd8-ff26dc0d0cd7",
            "compositeImage": {
                "id": "00c1164c-971d-45b8-ad27-cf94a83a1c5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f630a5ef-6aba-489d-b5c4-edfad64ef505",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaefb1d1-40f7-4a9a-bde3-cb241c3b3765",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f630a5ef-6aba-489d-b5c4-edfad64ef505",
                    "LayerId": "a05172ef-c4c6-4f0c-a031-02978cadf157"
                }
            ]
        },
        {
            "id": "bb3fdb5e-43c2-4b20-825c-0c3c195e22b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b166a4-d212-4585-9bd8-ff26dc0d0cd7",
            "compositeImage": {
                "id": "78609fff-c503-4603-86b1-12a8dafc1072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb3fdb5e-43c2-4b20-825c-0c3c195e22b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a452868-c2af-4d0c-9b57-21e06ebdee40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb3fdb5e-43c2-4b20-825c-0c3c195e22b3",
                    "LayerId": "a05172ef-c4c6-4f0c-a031-02978cadf157"
                }
            ]
        },
        {
            "id": "69caccf0-343c-4e24-b1f6-33b07ba2156b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b166a4-d212-4585-9bd8-ff26dc0d0cd7",
            "compositeImage": {
                "id": "5737cbad-7fd7-4733-aa79-863349bea4a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69caccf0-343c-4e24-b1f6-33b07ba2156b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c3bc860-e4be-49a0-9006-e953c8765084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69caccf0-343c-4e24-b1f6-33b07ba2156b",
                    "LayerId": "a05172ef-c4c6-4f0c-a031-02978cadf157"
                }
            ]
        },
        {
            "id": "9ff55af4-d13a-45f6-a372-cef427768ef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b166a4-d212-4585-9bd8-ff26dc0d0cd7",
            "compositeImage": {
                "id": "bca8be37-c5d4-4659-abb7-b1f5e16e1f62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ff55af4-d13a-45f6-a372-cef427768ef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60129e16-5556-4acc-92be-20a58024ceeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ff55af4-d13a-45f6-a372-cef427768ef0",
                    "LayerId": "a05172ef-c4c6-4f0c-a031-02978cadf157"
                }
            ]
        },
        {
            "id": "331e7b1a-7b37-445a-9014-178134f89f83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b166a4-d212-4585-9bd8-ff26dc0d0cd7",
            "compositeImage": {
                "id": "64d6e0e9-41ff-448f-a1bf-bb8364665bd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "331e7b1a-7b37-445a-9014-178134f89f83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e09e17de-55b0-45f0-8460-c77f16f28104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "331e7b1a-7b37-445a-9014-178134f89f83",
                    "LayerId": "a05172ef-c4c6-4f0c-a031-02978cadf157"
                }
            ]
        },
        {
            "id": "fbcfc031-81a7-4c01-ae56-458fae704994",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b166a4-d212-4585-9bd8-ff26dc0d0cd7",
            "compositeImage": {
                "id": "fbafe510-a998-41b6-b490-33e3afd3b9c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbcfc031-81a7-4c01-ae56-458fae704994",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90a631b4-ebc0-4638-af7c-f7b9fe8554f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbcfc031-81a7-4c01-ae56-458fae704994",
                    "LayerId": "a05172ef-c4c6-4f0c-a031-02978cadf157"
                }
            ]
        },
        {
            "id": "e3cb4080-54ec-47ac-a66f-fa47cff8b052",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b166a4-d212-4585-9bd8-ff26dc0d0cd7",
            "compositeImage": {
                "id": "a5e9cb56-969f-49e3-81b2-1413f117a019",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3cb4080-54ec-47ac-a66f-fa47cff8b052",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc77a7df-1253-4ccf-8c87-91d2576851f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3cb4080-54ec-47ac-a66f-fa47cff8b052",
                    "LayerId": "a05172ef-c4c6-4f0c-a031-02978cadf157"
                }
            ]
        },
        {
            "id": "e391c2a0-3904-4f5a-bfc7-dd0da8f9da78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b166a4-d212-4585-9bd8-ff26dc0d0cd7",
            "compositeImage": {
                "id": "2644ff62-7974-416d-ba57-479ea9231c22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e391c2a0-3904-4f5a-bfc7-dd0da8f9da78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cac4293-0596-4b7c-a499-2aa8ae87cd16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e391c2a0-3904-4f5a-bfc7-dd0da8f9da78",
                    "LayerId": "a05172ef-c4c6-4f0c-a031-02978cadf157"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "a05172ef-c4c6-4f0c-a031-02978cadf157",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3b166a4-d212-4585-9bd8-ff26dc0d0cd7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 46,
    "yorig": 119
}