{
    "id": "77ba4a6d-a353-45bf-bcde-6980ef7f5140",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rolling0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9327c50e-ce42-4f9a-b79f-a8e9a7f5d5ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77ba4a6d-a353-45bf-bcde-6980ef7f5140",
            "compositeImage": {
                "id": "c709cf64-4c8f-4abd-9822-feec5a627e4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9327c50e-ce42-4f9a-b79f-a8e9a7f5d5ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c75c365-d34b-43eb-a870-a7fc19b5b2e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9327c50e-ce42-4f9a-b79f-a8e9a7f5d5ef",
                    "LayerId": "e7080257-158a-44c9-a280-76f8ed61e649"
                }
            ]
        },
        {
            "id": "f9e817d3-b795-4792-8e26-cfb3d44ea0db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77ba4a6d-a353-45bf-bcde-6980ef7f5140",
            "compositeImage": {
                "id": "53bf7e4b-d6ae-404b-a6d7-e1ebb0e8ada7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9e817d3-b795-4792-8e26-cfb3d44ea0db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "295296f0-bc61-4d2f-8297-8d0b70c5d421",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9e817d3-b795-4792-8e26-cfb3d44ea0db",
                    "LayerId": "e7080257-158a-44c9-a280-76f8ed61e649"
                }
            ]
        },
        {
            "id": "e635e09c-8cbc-4a78-a39f-9e554de3a47d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77ba4a6d-a353-45bf-bcde-6980ef7f5140",
            "compositeImage": {
                "id": "6a109b40-908e-4d71-aaaa-f5d6be052099",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e635e09c-8cbc-4a78-a39f-9e554de3a47d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86666888-fc36-40a5-8647-92aea701beb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e635e09c-8cbc-4a78-a39f-9e554de3a47d",
                    "LayerId": "e7080257-158a-44c9-a280-76f8ed61e649"
                }
            ]
        },
        {
            "id": "38c13a88-71b4-4ed6-bb27-fba3f2cc0648",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77ba4a6d-a353-45bf-bcde-6980ef7f5140",
            "compositeImage": {
                "id": "89cc56e9-f4e1-42e9-a4a4-5b2f1d1c7236",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38c13a88-71b4-4ed6-bb27-fba3f2cc0648",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2301339c-349d-4329-97b4-f815f222399b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38c13a88-71b4-4ed6-bb27-fba3f2cc0648",
                    "LayerId": "e7080257-158a-44c9-a280-76f8ed61e649"
                }
            ]
        },
        {
            "id": "b126daaf-fffc-4bf5-813e-361e127ec62e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77ba4a6d-a353-45bf-bcde-6980ef7f5140",
            "compositeImage": {
                "id": "535ad7f6-ccb1-44b1-acb6-d9c6c314e69e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b126daaf-fffc-4bf5-813e-361e127ec62e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81cd59c6-b926-4e0c-ad8c-eaa0eec0b3fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b126daaf-fffc-4bf5-813e-361e127ec62e",
                    "LayerId": "e7080257-158a-44c9-a280-76f8ed61e649"
                }
            ]
        },
        {
            "id": "6d29d4df-50e7-4244-ba2a-a74f4df531ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77ba4a6d-a353-45bf-bcde-6980ef7f5140",
            "compositeImage": {
                "id": "7cafa671-ec92-4873-9a07-460fc64abb2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d29d4df-50e7-4244-ba2a-a74f4df531ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d1116dd-5158-44d0-a89f-6e8a08f6a841",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d29d4df-50e7-4244-ba2a-a74f4df531ea",
                    "LayerId": "e7080257-158a-44c9-a280-76f8ed61e649"
                }
            ]
        },
        {
            "id": "229d8293-6daf-4268-bcd5-7d97e6e64ecd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77ba4a6d-a353-45bf-bcde-6980ef7f5140",
            "compositeImage": {
                "id": "1bdb0169-db64-450f-adeb-d9c3b9034b24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "229d8293-6daf-4268-bcd5-7d97e6e64ecd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab56d19b-f54b-4bb4-b00a-2d60192d225b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "229d8293-6daf-4268-bcd5-7d97e6e64ecd",
                    "LayerId": "e7080257-158a-44c9-a280-76f8ed61e649"
                }
            ]
        },
        {
            "id": "aaad62d2-11ce-493f-bc2f-bbe009a0131b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77ba4a6d-a353-45bf-bcde-6980ef7f5140",
            "compositeImage": {
                "id": "de095dc1-79f4-4d37-8550-bc3bc29e3262",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaad62d2-11ce-493f-bc2f-bbe009a0131b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aa24c15-ae56-4ff1-b90a-759fa1436a2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaad62d2-11ce-493f-bc2f-bbe009a0131b",
                    "LayerId": "e7080257-158a-44c9-a280-76f8ed61e649"
                }
            ]
        },
        {
            "id": "b4eec68e-c078-4ec2-83ed-f40acfbb285b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77ba4a6d-a353-45bf-bcde-6980ef7f5140",
            "compositeImage": {
                "id": "cc7f40ce-5023-427f-aaf2-59dd73698e90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4eec68e-c078-4ec2-83ed-f40acfbb285b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "545505a8-d7aa-44b4-a374-f0e20abea0a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4eec68e-c078-4ec2-83ed-f40acfbb285b",
                    "LayerId": "e7080257-158a-44c9-a280-76f8ed61e649"
                }
            ]
        },
        {
            "id": "2875534b-13c9-4a66-9abe-0ae650ae20be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77ba4a6d-a353-45bf-bcde-6980ef7f5140",
            "compositeImage": {
                "id": "284ab04d-340b-427e-af85-65bf0d75c471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2875534b-13c9-4a66-9abe-0ae650ae20be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cad28115-f986-4ab9-8f45-67db8f701b5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2875534b-13c9-4a66-9abe-0ae650ae20be",
                    "LayerId": "e7080257-158a-44c9-a280-76f8ed61e649"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "e7080257-158a-44c9-a280-76f8ed61e649",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77ba4a6d-a353-45bf-bcde-6980ef7f5140",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}