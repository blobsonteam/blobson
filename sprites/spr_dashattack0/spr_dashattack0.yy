{
    "id": "77b4107e-733d-423f-a0d8-ce8542fbd1fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dashattack0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 3,
    "bbox_right": 35,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3021b5e-1381-4eeb-a225-dc0aa785fca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b4107e-733d-423f-a0d8-ce8542fbd1fb",
            "compositeImage": {
                "id": "a5bd6230-0f93-49cb-b630-aaf737a19951",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3021b5e-1381-4eeb-a225-dc0aa785fca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37ce97b0-c48e-4ef9-8d3d-04cb40018432",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3021b5e-1381-4eeb-a225-dc0aa785fca3",
                    "LayerId": "461d0072-17b7-414f-8c54-f64ea403f16b"
                },
                {
                    "id": "5c9e6d3e-da30-460c-bb54-2fdbe5d67357",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3021b5e-1381-4eeb-a225-dc0aa785fca3",
                    "LayerId": "1d752112-5333-4bbb-9722-27280263f0f8"
                }
            ]
        },
        {
            "id": "2103b51d-2936-44f4-89e7-bffd8705dbe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b4107e-733d-423f-a0d8-ce8542fbd1fb",
            "compositeImage": {
                "id": "478fde07-c395-4560-b157-273c88ff9617",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2103b51d-2936-44f4-89e7-bffd8705dbe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e095c0a3-a885-476f-a6f5-d0ec0ab245a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2103b51d-2936-44f4-89e7-bffd8705dbe5",
                    "LayerId": "461d0072-17b7-414f-8c54-f64ea403f16b"
                },
                {
                    "id": "adaca04d-9129-40a0-b54c-4e3f06e709be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2103b51d-2936-44f4-89e7-bffd8705dbe5",
                    "LayerId": "1d752112-5333-4bbb-9722-27280263f0f8"
                }
            ]
        },
        {
            "id": "5a3ebeef-b505-43ec-8d36-d7ef8e3d8975",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b4107e-733d-423f-a0d8-ce8542fbd1fb",
            "compositeImage": {
                "id": "b40b7c98-ebcc-4c7c-badb-869b9b4d49da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a3ebeef-b505-43ec-8d36-d7ef8e3d8975",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7caf521-7d79-4336-a56c-af6eb68805a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a3ebeef-b505-43ec-8d36-d7ef8e3d8975",
                    "LayerId": "461d0072-17b7-414f-8c54-f64ea403f16b"
                },
                {
                    "id": "c6948867-cc43-42ee-9ae0-18eb6d3de8d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a3ebeef-b505-43ec-8d36-d7ef8e3d8975",
                    "LayerId": "1d752112-5333-4bbb-9722-27280263f0f8"
                }
            ]
        },
        {
            "id": "b513befd-e700-47e6-9737-e457822fddba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b4107e-733d-423f-a0d8-ce8542fbd1fb",
            "compositeImage": {
                "id": "dea3c5d5-eb2a-4f89-bcaf-ffbb56e04432",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b513befd-e700-47e6-9737-e457822fddba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2992f6da-e5f1-407a-b672-f55456f62206",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b513befd-e700-47e6-9737-e457822fddba",
                    "LayerId": "461d0072-17b7-414f-8c54-f64ea403f16b"
                },
                {
                    "id": "1034fbae-4c32-4076-a163-7a5d69291dc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b513befd-e700-47e6-9737-e457822fddba",
                    "LayerId": "1d752112-5333-4bbb-9722-27280263f0f8"
                }
            ]
        },
        {
            "id": "3da85ee4-be14-4546-8f37-9f79978f3473",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b4107e-733d-423f-a0d8-ce8542fbd1fb",
            "compositeImage": {
                "id": "511f0f47-d259-4382-af35-29d040af1a5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3da85ee4-be14-4546-8f37-9f79978f3473",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10b80eac-f61d-4dc1-9838-7f1cb6d3623b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3da85ee4-be14-4546-8f37-9f79978f3473",
                    "LayerId": "461d0072-17b7-414f-8c54-f64ea403f16b"
                },
                {
                    "id": "70194144-be81-4dab-b767-c6bd5495b905",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3da85ee4-be14-4546-8f37-9f79978f3473",
                    "LayerId": "1d752112-5333-4bbb-9722-27280263f0f8"
                }
            ]
        },
        {
            "id": "953ca521-8d97-4747-8753-e42b3bfeec37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b4107e-733d-423f-a0d8-ce8542fbd1fb",
            "compositeImage": {
                "id": "cec4146e-78bb-4320-b2e0-ab1921b7a455",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "953ca521-8d97-4747-8753-e42b3bfeec37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a317b7e-dc82-458b-8d19-f85476f46792",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "953ca521-8d97-4747-8753-e42b3bfeec37",
                    "LayerId": "461d0072-17b7-414f-8c54-f64ea403f16b"
                },
                {
                    "id": "b6422eed-373f-4c2b-b5cb-cfb0d3ced948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "953ca521-8d97-4747-8753-e42b3bfeec37",
                    "LayerId": "1d752112-5333-4bbb-9722-27280263f0f8"
                }
            ]
        },
        {
            "id": "a8dd57f1-be0b-49b4-a478-95e0d33cbefd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b4107e-733d-423f-a0d8-ce8542fbd1fb",
            "compositeImage": {
                "id": "f7020f86-6d63-4511-9f14-e50140cb32fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8dd57f1-be0b-49b4-a478-95e0d33cbefd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2613f341-1c31-4539-b760-39055de292e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8dd57f1-be0b-49b4-a478-95e0d33cbefd",
                    "LayerId": "461d0072-17b7-414f-8c54-f64ea403f16b"
                },
                {
                    "id": "19f08181-74fc-4da1-819f-0e455f5039bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8dd57f1-be0b-49b4-a478-95e0d33cbefd",
                    "LayerId": "1d752112-5333-4bbb-9722-27280263f0f8"
                }
            ]
        },
        {
            "id": "74f98e38-50ea-41c8-b4c4-0bddc57b341c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b4107e-733d-423f-a0d8-ce8542fbd1fb",
            "compositeImage": {
                "id": "542d53b0-c875-4b3a-95be-3196b7ef7326",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74f98e38-50ea-41c8-b4c4-0bddc57b341c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5e831d7-4494-402f-86e0-a595e1641a45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74f98e38-50ea-41c8-b4c4-0bddc57b341c",
                    "LayerId": "461d0072-17b7-414f-8c54-f64ea403f16b"
                },
                {
                    "id": "3a7d65c5-8504-437f-84a5-dd25b0030059",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74f98e38-50ea-41c8-b4c4-0bddc57b341c",
                    "LayerId": "1d752112-5333-4bbb-9722-27280263f0f8"
                }
            ]
        },
        {
            "id": "5902443e-50ee-434d-aa37-85937eedf9d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b4107e-733d-423f-a0d8-ce8542fbd1fb",
            "compositeImage": {
                "id": "f34f28b7-b52d-43e8-a13d-405f81033bc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5902443e-50ee-434d-aa37-85937eedf9d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce1e792-ef36-45c7-8963-f8b42867e126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5902443e-50ee-434d-aa37-85937eedf9d2",
                    "LayerId": "461d0072-17b7-414f-8c54-f64ea403f16b"
                },
                {
                    "id": "982a0c4a-0737-4f21-97b4-04893385834e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5902443e-50ee-434d-aa37-85937eedf9d2",
                    "LayerId": "1d752112-5333-4bbb-9722-27280263f0f8"
                }
            ]
        },
        {
            "id": "221517bb-c7c8-4ec9-bf5f-6447f7bfd9cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b4107e-733d-423f-a0d8-ce8542fbd1fb",
            "compositeImage": {
                "id": "0e0ae6fc-c20d-4952-993c-b8bfe694dfa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "221517bb-c7c8-4ec9-bf5f-6447f7bfd9cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c72332b3-4e3b-47e5-913e-8f0a8f266a11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "221517bb-c7c8-4ec9-bf5f-6447f7bfd9cb",
                    "LayerId": "1d752112-5333-4bbb-9722-27280263f0f8"
                },
                {
                    "id": "08a1c80d-f143-4746-855c-7c8b70256187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "221517bb-c7c8-4ec9-bf5f-6447f7bfd9cb",
                    "LayerId": "461d0072-17b7-414f-8c54-f64ea403f16b"
                }
            ]
        },
        {
            "id": "83a1df78-ec78-4051-a00c-2a5b0067771c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b4107e-733d-423f-a0d8-ce8542fbd1fb",
            "compositeImage": {
                "id": "e1ca2d42-8136-4a07-bc15-073f84d2db2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83a1df78-ec78-4051-a00c-2a5b0067771c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30eb50dc-fb68-4ada-9811-14f57fb1fd07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83a1df78-ec78-4051-a00c-2a5b0067771c",
                    "LayerId": "461d0072-17b7-414f-8c54-f64ea403f16b"
                },
                {
                    "id": "431b3148-6562-4e65-be34-8fe4e612cb77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83a1df78-ec78-4051-a00c-2a5b0067771c",
                    "LayerId": "1d752112-5333-4bbb-9722-27280263f0f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "1d752112-5333-4bbb-9722-27280263f0f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77b4107e-733d-423f-a0d8-ce8542fbd1fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "461d0072-17b7-414f-8c54-f64ea403f16b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77b4107e-733d-423f-a0d8-ce8542fbd1fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 18
}