{
    "id": "81fd3ea5-3c28-4d8d-99fc-70915fc21243",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yuki_runstop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 0,
    "bbox_right": 136,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc365aea-298f-4b39-9d49-4df17cdb695e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81fd3ea5-3c28-4d8d-99fc-70915fc21243",
            "compositeImage": {
                "id": "d7acf0cd-a6e7-4535-b844-f04443614151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc365aea-298f-4b39-9d49-4df17cdb695e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "293749ca-dd85-4ef8-928e-242ffb56c118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc365aea-298f-4b39-9d49-4df17cdb695e",
                    "LayerId": "70589a06-dab3-4c65-a110-c941ab2e0759"
                }
            ]
        },
        {
            "id": "838c31e9-8ab9-4b90-b2e0-3c9f84dd0d46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81fd3ea5-3c28-4d8d-99fc-70915fc21243",
            "compositeImage": {
                "id": "50a32fd5-8db4-42a0-a9c1-4d0eb12861cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "838c31e9-8ab9-4b90-b2e0-3c9f84dd0d46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "076c7ad0-578d-4840-9f7d-bb4125230025",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "838c31e9-8ab9-4b90-b2e0-3c9f84dd0d46",
                    "LayerId": "70589a06-dab3-4c65-a110-c941ab2e0759"
                }
            ]
        },
        {
            "id": "103da9dc-2325-422d-9c7f-85477adccdf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81fd3ea5-3c28-4d8d-99fc-70915fc21243",
            "compositeImage": {
                "id": "af6b34a0-395e-411a-aebb-57e660a3b196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "103da9dc-2325-422d-9c7f-85477adccdf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dd47357-5745-4710-b5cf-6973b8187e60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "103da9dc-2325-422d-9c7f-85477adccdf2",
                    "LayerId": "70589a06-dab3-4c65-a110-c941ab2e0759"
                }
            ]
        },
        {
            "id": "e1159a97-37f6-44cf-823a-71e824975a67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81fd3ea5-3c28-4d8d-99fc-70915fc21243",
            "compositeImage": {
                "id": "a9d1876f-3af5-4d3f-a364-631650021822",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1159a97-37f6-44cf-823a-71e824975a67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "199e2fbd-6184-4f2f-8ad3-3a8f7c107cd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1159a97-37f6-44cf-823a-71e824975a67",
                    "LayerId": "70589a06-dab3-4c65-a110-c941ab2e0759"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "70589a06-dab3-4c65-a110-c941ab2e0759",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81fd3ea5-3c28-4d8d-99fc-70915fc21243",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 137,
    "xorig": 0,
    "yorig": 0
}