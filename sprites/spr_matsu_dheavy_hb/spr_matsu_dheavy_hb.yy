{
    "id": "b3d288d8-1e53-4a4d-9260-cd6abb17b9cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_dheavy_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 62,
    "bbox_right": 130,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "411c6d07-d9fa-4911-ba58-9029ecac4f19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3d288d8-1e53-4a4d-9260-cd6abb17b9cc",
            "compositeImage": {
                "id": "ca7ffef5-21eb-402b-b29b-bc344b48ccdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "411c6d07-d9fa-4911-ba58-9029ecac4f19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99d1cb3b-8c89-4c39-b064-3806893a4a73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "411c6d07-d9fa-4911-ba58-9029ecac4f19",
                    "LayerId": "66b7ea45-a11e-4b42-9dba-9ec25c781f73"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 108,
    "layers": [
        {
            "id": "66b7ea45-a11e-4b42-9dba-9ec25c781f73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3d288d8-1e53-4a4d-9260-cd6abb17b9cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 131,
    "xorig": 66,
    "yorig": 107
}