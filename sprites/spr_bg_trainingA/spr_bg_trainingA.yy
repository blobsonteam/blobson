{
    "id": "7e268835-ace1-43a0-8f76-3410682fb119",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_trainingA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1099,
    "bbox_left": 0,
    "bbox_right": 2047,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd12cf57-c34f-4138-a38a-72c25cc4ad7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e268835-ace1-43a0-8f76-3410682fb119",
            "compositeImage": {
                "id": "2c855ca6-0beb-4b8d-9712-e1db71781a12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd12cf57-c34f-4138-a38a-72c25cc4ad7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac561873-f35d-4f99-86d6-969501822ecb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd12cf57-c34f-4138-a38a-72c25cc4ad7f",
                    "LayerId": "aa212959-cd07-4cb2-b59a-a01beec40bb8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1100,
    "layers": [
        {
            "id": "aa212959-cd07-4cb2-b59a-a01beec40bb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e268835-ace1-43a0-8f76-3410682fb119",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2048,
    "xorig": 1024,
    "yorig": 550
}