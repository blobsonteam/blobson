{
    "id": "bff3a4de-d464-4592-bbd7-5673c690fc76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pixelated_pope",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 0,
    "bbox_right": 145,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b5874b1-e2ee-4bff-8452-c2f7ee057e4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "f8b929e3-2b11-492a-8757-91e86b4cc355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b5874b1-e2ee-4bff-8452-c2f7ee057e4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08851934-b339-4b05-8fb4-9edf8ef1c583",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b5874b1-e2ee-4bff-8452-c2f7ee057e4a",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "ba5a66f9-180e-417c-91af-2d52be1617ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "200c79cb-208c-431a-a74a-b78ba45c3301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba5a66f9-180e-417c-91af-2d52be1617ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edc08b31-5460-4a06-a55c-ec5f1cedb449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba5a66f9-180e-417c-91af-2d52be1617ac",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "322469f7-d01c-42af-982b-de2f6d671718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "2401e5a2-b4b0-481a-82b6-dc1310f295c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "322469f7-d01c-42af-982b-de2f6d671718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3f90f10-7f3b-4174-b677-fd402f3a4c09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "322469f7-d01c-42af-982b-de2f6d671718",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "11e3ceb0-aa80-4231-b44c-229d3052c4cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "47d62a1d-af37-4e55-b1ef-7cee854a7d8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11e3ceb0-aa80-4231-b44c-229d3052c4cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fe6967b-72ce-485b-9e14-75166bda7fb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11e3ceb0-aa80-4231-b44c-229d3052c4cf",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "ad16601d-3ba0-49a3-9aa2-aa4168dd9def",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "dfc07202-bbc7-4803-b08f-f0746f882965",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad16601d-3ba0-49a3-9aa2-aa4168dd9def",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73333fbd-faae-45bf-b3ed-936245169aff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad16601d-3ba0-49a3-9aa2-aa4168dd9def",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "7f7b98c3-bfc6-4aaf-91da-5dc9de49233e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "abfb6240-813b-4cac-b59b-810a5dc2a44b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f7b98c3-bfc6-4aaf-91da-5dc9de49233e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e93f6cf0-b78c-4c6c-8122-43f0c778ac83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f7b98c3-bfc6-4aaf-91da-5dc9de49233e",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "ea580dbe-934f-468d-88c8-5f479ce4627b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "2cbb4f85-6425-46ca-8764-a713b6f777c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea580dbe-934f-468d-88c8-5f479ce4627b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37869f85-b752-4ee0-b116-e91f604b0961",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea580dbe-934f-468d-88c8-5f479ce4627b",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "be5013e8-9ffb-426d-8d8d-b3dcba20955d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "0b871363-b591-4e61-bf7c-3d16e62f5393",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be5013e8-9ffb-426d-8d8d-b3dcba20955d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82719a66-192f-4c02-8309-2f185c2497bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be5013e8-9ffb-426d-8d8d-b3dcba20955d",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "b4bc6105-0221-4c92-96d8-f02112b7f214",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "0ab98918-ec7c-445e-b0f9-cb0bd0a97285",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4bc6105-0221-4c92-96d8-f02112b7f214",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01761934-bb0a-4f82-b533-48baba379384",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4bc6105-0221-4c92-96d8-f02112b7f214",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "df694b81-eacb-4bd8-b0e8-a6e58aaf5d3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "3dd830be-b16f-435f-aa99-f58098104758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df694b81-eacb-4bd8-b0e8-a6e58aaf5d3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63f3933a-6347-4b7d-ad8f-d2571e60a1d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df694b81-eacb-4bd8-b0e8-a6e58aaf5d3c",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "64caef97-e761-4b52-81b2-7692ff883e69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "8d13f3f9-f0bb-41f7-95d4-06791a7af9f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64caef97-e761-4b52-81b2-7692ff883e69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dde8a01-92a6-4378-bd85-ef4b05bff31b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64caef97-e761-4b52-81b2-7692ff883e69",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "5393e2e9-9b0d-4bae-a3fb-55de81711ca0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "46ed6661-0349-4f8a-bbd1-f6410388b0f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5393e2e9-9b0d-4bae-a3fb-55de81711ca0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4146bb62-662e-44ef-93c8-107e3977ec22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5393e2e9-9b0d-4bae-a3fb-55de81711ca0",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "1eda73fd-e68d-440c-9d0d-4af7d39525ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "394ea96a-d88e-4579-b499-672a0818a012",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1eda73fd-e68d-440c-9d0d-4af7d39525ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18975ad1-502d-4cb6-8bb8-100b6abb4975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1eda73fd-e68d-440c-9d0d-4af7d39525ad",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "9cd42d79-df52-44ee-99f0-5a4caedf9b5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "90d9290c-7634-48c2-95c3-7577ead38568",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cd42d79-df52-44ee-99f0-5a4caedf9b5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51ff69b4-b344-4d40-99e3-cf6c6b950f85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cd42d79-df52-44ee-99f0-5a4caedf9b5a",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "1e26fcc0-a834-4bbb-946c-ddd15b1e8559",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "0991ecb6-3829-4962-b251-e16d251e3f79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e26fcc0-a834-4bbb-946c-ddd15b1e8559",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ccc4f9e-5aa7-4445-9d1b-949169ac1adb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e26fcc0-a834-4bbb-946c-ddd15b1e8559",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "86ddf8b7-b20c-4b8a-827d-9dc52a5a6779",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "edb5818f-5b5e-4d0e-bd03-daa2bb45cd31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86ddf8b7-b20c-4b8a-827d-9dc52a5a6779",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6890f6f2-5faf-4fd1-a25d-7e2745125907",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86ddf8b7-b20c-4b8a-827d-9dc52a5a6779",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "95020c2d-a712-4225-b0bb-3ee860f58dea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "38b18a41-6f51-4a27-b9d0-c1a2dfd3a211",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95020c2d-a712-4225-b0bb-3ee860f58dea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bc14543-d852-45b2-9db0-85ab188bbac0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95020c2d-a712-4225-b0bb-3ee860f58dea",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "a8d80b6e-dea5-458d-8405-c1897ea2afbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "fd2223ae-db03-40d5-afbc-d7a0e06785ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8d80b6e-dea5-458d-8405-c1897ea2afbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60c98a73-fd56-4a97-8324-455a730dddc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8d80b6e-dea5-458d-8405-c1897ea2afbd",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        },
        {
            "id": "0972896c-7772-4107-bd1d-1790b7811f6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "compositeImage": {
                "id": "f5176f62-7569-4497-9397-d45b684b8473",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0972896c-7772-4107-bd1d-1790b7811f6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a52a740b-f873-4fc2-ae47-3bcd01b80121",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0972896c-7772-4107-bd1d-1790b7811f6b",
                    "LayerId": "99f3ea43-6769-45ba-8ac5-9321bce4e754"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 254,
    "layers": [
        {
            "id": "99f3ea43-6769-45ba-8ac5-9321bce4e754",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bff3a4de-d464-4592-bbd7-5673c690fc76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 146,
    "xorig": 73,
    "yorig": 127
}