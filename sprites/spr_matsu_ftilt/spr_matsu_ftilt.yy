{
    "id": "3f3dc8a9-da5d-488d-b663-29af9ee78598",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_ftilt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 0,
    "bbox_right": 140,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e80009d-3c00-4aee-8cfd-cc99f2c44a7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f3dc8a9-da5d-488d-b663-29af9ee78598",
            "compositeImage": {
                "id": "390e625e-ffbd-487d-91a9-4b5ea89e8816",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e80009d-3c00-4aee-8cfd-cc99f2c44a7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbdfb649-e3e6-4b9e-bc15-71a718fde69e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e80009d-3c00-4aee-8cfd-cc99f2c44a7e",
                    "LayerId": "af9c50e9-3688-4904-925b-cd4ab9884508"
                }
            ]
        },
        {
            "id": "61b6052a-3597-452c-bfbf-6da536f32acd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f3dc8a9-da5d-488d-b663-29af9ee78598",
            "compositeImage": {
                "id": "c94ae130-08e2-413d-8643-9309d9191b0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61b6052a-3597-452c-bfbf-6da536f32acd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd2abdac-9df6-4435-9d1d-79b5a8873855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61b6052a-3597-452c-bfbf-6da536f32acd",
                    "LayerId": "af9c50e9-3688-4904-925b-cd4ab9884508"
                }
            ]
        },
        {
            "id": "a2188787-a19d-41a5-854d-6790700a6310",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f3dc8a9-da5d-488d-b663-29af9ee78598",
            "compositeImage": {
                "id": "0320ab48-ce43-4d70-a7e1-a683660c2cd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2188787-a19d-41a5-854d-6790700a6310",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8889146e-d8e3-42e4-b700-b0b9ac36ae20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2188787-a19d-41a5-854d-6790700a6310",
                    "LayerId": "af9c50e9-3688-4904-925b-cd4ab9884508"
                }
            ]
        },
        {
            "id": "42848e62-4d8c-4749-a941-3fcd6f244308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f3dc8a9-da5d-488d-b663-29af9ee78598",
            "compositeImage": {
                "id": "f97dd959-a481-45c3-8a9a-b70512178e07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42848e62-4d8c-4749-a941-3fcd6f244308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19c4f2ba-e69d-4b79-a4dc-b5c811d86bf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42848e62-4d8c-4749-a941-3fcd6f244308",
                    "LayerId": "af9c50e9-3688-4904-925b-cd4ab9884508"
                }
            ]
        },
        {
            "id": "f3278612-471b-4f6c-bb9e-00c27cdadc8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f3dc8a9-da5d-488d-b663-29af9ee78598",
            "compositeImage": {
                "id": "e1021267-aa37-4ddb-a4f9-ffa29127fea9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3278612-471b-4f6c-bb9e-00c27cdadc8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81768eb8-7d9a-4117-8991-67214209ae70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3278612-471b-4f6c-bb9e-00c27cdadc8d",
                    "LayerId": "af9c50e9-3688-4904-925b-cd4ab9884508"
                }
            ]
        },
        {
            "id": "f38fd166-b55c-4fa2-9643-850d1cdd3440",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f3dc8a9-da5d-488d-b663-29af9ee78598",
            "compositeImage": {
                "id": "becac1fa-a4ea-4d2a-b043-3770813fdf0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f38fd166-b55c-4fa2-9643-850d1cdd3440",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde75843-61b2-4f21-9123-bad6c1651e52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f38fd166-b55c-4fa2-9643-850d1cdd3440",
                    "LayerId": "af9c50e9-3688-4904-925b-cd4ab9884508"
                }
            ]
        },
        {
            "id": "32509576-0069-484a-997c-653f82102b57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f3dc8a9-da5d-488d-b663-29af9ee78598",
            "compositeImage": {
                "id": "918a6bc7-eb65-4d37-bfb2-1617c2632503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32509576-0069-484a-997c-653f82102b57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bad0792-6e69-46f7-9e3b-49fc7aa2cf68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32509576-0069-484a-997c-653f82102b57",
                    "LayerId": "af9c50e9-3688-4904-925b-cd4ab9884508"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 98,
    "layers": [
        {
            "id": "af9c50e9-3688-4904-925b-cd4ab9884508",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f3dc8a9-da5d-488d-b663-29af9ee78598",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 141,
    "xorig": 41,
    "yorig": 97
}