{
    "id": "3bb20f2d-ed57-47e2-bde0-6ef7e9efb1bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_uheavy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 12,
    "bbox_right": 84,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f8bef8b5-5c7f-474c-a123-71bb56647334",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb20f2d-ed57-47e2-bde0-6ef7e9efb1bd",
            "compositeImage": {
                "id": "ffa2fd27-0fc8-4b40-90bf-7d0cd5aaefef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8bef8b5-5c7f-474c-a123-71bb56647334",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a745264-dacd-4a97-855f-c16016073be8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8bef8b5-5c7f-474c-a123-71bb56647334",
                    "LayerId": "967291c2-3a3b-4bc1-b925-7c7443206e46"
                }
            ]
        },
        {
            "id": "90a14630-126f-4130-ab54-d3bf33f39343",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb20f2d-ed57-47e2-bde0-6ef7e9efb1bd",
            "compositeImage": {
                "id": "4600d70e-d230-497a-824f-4b89d6d04841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90a14630-126f-4130-ab54-d3bf33f39343",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c12f697-d77e-4f9a-8d72-f5d3b65c8588",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90a14630-126f-4130-ab54-d3bf33f39343",
                    "LayerId": "967291c2-3a3b-4bc1-b925-7c7443206e46"
                }
            ]
        },
        {
            "id": "5a727db2-147d-492d-87e2-85de1f1969f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb20f2d-ed57-47e2-bde0-6ef7e9efb1bd",
            "compositeImage": {
                "id": "a98fb4c2-ad4d-432c-b1d6-5dc0da6b273c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a727db2-147d-492d-87e2-85de1f1969f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d8e4e2c-42ae-4f33-af1d-11c5daaf8e16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a727db2-147d-492d-87e2-85de1f1969f4",
                    "LayerId": "967291c2-3a3b-4bc1-b925-7c7443206e46"
                }
            ]
        },
        {
            "id": "d0db4ab3-19cc-4759-a236-d7b73fff9f68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb20f2d-ed57-47e2-bde0-6ef7e9efb1bd",
            "compositeImage": {
                "id": "5ba5da8a-0727-47cc-aca7-9e73b81247d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0db4ab3-19cc-4759-a236-d7b73fff9f68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32fdc254-b9fe-48a7-a94c-b5064c01130d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0db4ab3-19cc-4759-a236-d7b73fff9f68",
                    "LayerId": "967291c2-3a3b-4bc1-b925-7c7443206e46"
                }
            ]
        },
        {
            "id": "c147c5ad-174c-4c29-8119-dd50fd578af5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb20f2d-ed57-47e2-bde0-6ef7e9efb1bd",
            "compositeImage": {
                "id": "7dee6812-2a9e-4113-a966-c15a171a5fd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c147c5ad-174c-4c29-8119-dd50fd578af5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b895bde-44d8-42e0-949c-84c41ff2c7ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c147c5ad-174c-4c29-8119-dd50fd578af5",
                    "LayerId": "967291c2-3a3b-4bc1-b925-7c7443206e46"
                }
            ]
        },
        {
            "id": "dcf84a12-fdee-43d9-81dc-8ee8d98fd02a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb20f2d-ed57-47e2-bde0-6ef7e9efb1bd",
            "compositeImage": {
                "id": "5840f6a2-7392-4490-9f35-cbd721d4a96e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcf84a12-fdee-43d9-81dc-8ee8d98fd02a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de5e3eb4-79cf-4a25-99e0-97b9bdcaf73c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcf84a12-fdee-43d9-81dc-8ee8d98fd02a",
                    "LayerId": "967291c2-3a3b-4bc1-b925-7c7443206e46"
                }
            ]
        },
        {
            "id": "c9d97da1-46b8-4d7e-9f75-193e3f835a43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb20f2d-ed57-47e2-bde0-6ef7e9efb1bd",
            "compositeImage": {
                "id": "92277ea4-333c-464b-ba4f-5a7f03a83646",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9d97da1-46b8-4d7e-9f75-193e3f835a43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0290e917-e22a-41cd-ba93-9d64f7e5c000",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9d97da1-46b8-4d7e-9f75-193e3f835a43",
                    "LayerId": "967291c2-3a3b-4bc1-b925-7c7443206e46"
                }
            ]
        },
        {
            "id": "1ea0b70c-7131-4e9b-9f71-d41fca068e62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb20f2d-ed57-47e2-bde0-6ef7e9efb1bd",
            "compositeImage": {
                "id": "ab51e51f-a5b6-4bd0-aa1c-de8075fd869e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ea0b70c-7131-4e9b-9f71-d41fca068e62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26619586-143a-4e23-870f-472a1615d228",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ea0b70c-7131-4e9b-9f71-d41fca068e62",
                    "LayerId": "967291c2-3a3b-4bc1-b925-7c7443206e46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 118,
    "layers": [
        {
            "id": "967291c2-3a3b-4bc1-b925-7c7443206e46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bb20f2d-ed57-47e2-bde0-6ef7e9efb1bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 98,
    "xorig": 45,
    "yorig": 117
}