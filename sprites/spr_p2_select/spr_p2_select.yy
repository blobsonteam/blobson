{
    "id": "c289b781-580b-48e2-9ef9-8fee40a1355b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p2_select",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ad5200a-eccf-4791-a251-e7565d5c471f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c289b781-580b-48e2-9ef9-8fee40a1355b",
            "compositeImage": {
                "id": "ecbbe995-9943-484f-a1c7-c612d6f84d3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ad5200a-eccf-4791-a251-e7565d5c471f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba718945-f1ea-42f3-b04d-bad1aa7f5c7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ad5200a-eccf-4791-a251-e7565d5c471f",
                    "LayerId": "2e8b0533-9cd1-4b00-b01f-cef8ee45c573"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "2e8b0533-9cd1-4b00-b01f-cef8ee45c573",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c289b781-580b-48e2-9ef9-8fee40a1355b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}