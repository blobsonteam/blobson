{
    "id": "74b96299-a1cf-4add-9056-9114189cf9e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fx_energy1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 169,
    "bbox_left": 0,
    "bbox_right": 245,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef131156-6a0d-4b07-ba7a-d212ea5d7c81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "57e9d269-0505-481a-beff-56707474c692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef131156-6a0d-4b07-ba7a-d212ea5d7c81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "900470ad-eff9-4f86-b759-424373555f1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef131156-6a0d-4b07-ba7a-d212ea5d7c81",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "00d7feaa-85b7-4537-a4b8-a6cf05b9bc06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "7493f20a-741e-4885-87e6-ce6a032ba7fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00d7feaa-85b7-4537-a4b8-a6cf05b9bc06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56267cbe-bc5d-4e36-92da-5b80f99ce0f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00d7feaa-85b7-4537-a4b8-a6cf05b9bc06",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "1f19fefd-019d-46c3-8043-522e82bcd62f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "99e0a0b6-bfb3-45bc-9d15-d5ec02739714",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f19fefd-019d-46c3-8043-522e82bcd62f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d856508c-6545-481a-ad93-3d21fc361d5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f19fefd-019d-46c3-8043-522e82bcd62f",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "7f2ec3a3-b0e2-4b10-b03a-dbe7aff19a86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "e87419bd-7526-4db0-be34-f5d52db0c537",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f2ec3a3-b0e2-4b10-b03a-dbe7aff19a86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39d7f682-2aa4-4345-8e6b-881c808d8122",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f2ec3a3-b0e2-4b10-b03a-dbe7aff19a86",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "cd073d86-4669-4d0b-84c2-e9baf9ec6aea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "f47b8ede-e383-4378-85b9-83e0906d5e6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd073d86-4669-4d0b-84c2-e9baf9ec6aea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01ec76c2-5211-4428-bb8f-c653adb1101c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd073d86-4669-4d0b-84c2-e9baf9ec6aea",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "e22dd3a7-d154-4f55-b613-1f999484b775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "1d4690d9-f29c-46fb-9f2a-1a1171b9aa67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e22dd3a7-d154-4f55-b613-1f999484b775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a043ce00-dcf1-4242-9931-f45d7cdef4a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e22dd3a7-d154-4f55-b613-1f999484b775",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "f8032ca0-eb9d-4a13-bb81-2e7d65853db5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "8ea2cbc7-bbb9-4656-852d-5c6a416c94a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8032ca0-eb9d-4a13-bb81-2e7d65853db5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5512ae49-487c-4281-ad71-302e1bf13bcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8032ca0-eb9d-4a13-bb81-2e7d65853db5",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "d62942d3-f571-45b7-ae39-999d481de751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "f11f5d8f-818c-4aa9-b0c3-5da7bf826c7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d62942d3-f571-45b7-ae39-999d481de751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c73de8e0-67c7-4e74-a587-35a2da370b76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d62942d3-f571-45b7-ae39-999d481de751",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "82e95ed4-2b0e-4f34-a7f1-583008bb5660",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "61786343-53e8-4c4d-9b19-37c5bf5021e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82e95ed4-2b0e-4f34-a7f1-583008bb5660",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "452fb62c-8de6-432e-a5da-e8cc29a6682e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82e95ed4-2b0e-4f34-a7f1-583008bb5660",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "eed68ed0-9d2e-432c-be4e-9c309b7e955c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "9019fc0d-98f1-4b1f-96d1-2d1c99266ab0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eed68ed0-9d2e-432c-be4e-9c309b7e955c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a27f4a27-481a-4932-8893-4f1317c3725d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eed68ed0-9d2e-432c-be4e-9c309b7e955c",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "a0eee36e-65d8-4673-9234-b1a70fe7660e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "f9818b89-785b-4b88-bdcb-bd4847876184",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0eee36e-65d8-4673-9234-b1a70fe7660e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bcd1a76-1334-4f3d-ab66-08f64e79138b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0eee36e-65d8-4673-9234-b1a70fe7660e",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "242d302a-7328-4734-a562-3f47d292cef6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "04375f83-1856-43df-b30f-4fc957d1a7a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "242d302a-7328-4734-a562-3f47d292cef6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7df138d-283d-43a3-8fcf-903b6f812abb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "242d302a-7328-4734-a562-3f47d292cef6",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "9025e951-46e1-40f2-ae2b-853c2f7d4be8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "8fb2fabc-ad44-498f-9699-5ccbbd7ed820",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9025e951-46e1-40f2-ae2b-853c2f7d4be8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12cd3da3-5768-4fd2-8e7f-fd30dadd7566",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9025e951-46e1-40f2-ae2b-853c2f7d4be8",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "d2634bc6-630a-41de-8b26-829d9507b494",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "ffe6b861-e028-41d5-91db-a181eb17d3c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2634bc6-630a-41de-8b26-829d9507b494",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "579bc76e-74b7-4baf-ab7d-5d0989152024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2634bc6-630a-41de-8b26-829d9507b494",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "7c4e3690-c8da-4599-bf75-b9d9fb51fb25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "0e647da2-288d-49bb-8608-7e924e2bc39f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c4e3690-c8da-4599-bf75-b9d9fb51fb25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0cfb8c0-aa7f-4319-a3ca-1f63a2ae55c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c4e3690-c8da-4599-bf75-b9d9fb51fb25",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "40df4e30-73e8-42e4-a0bb-69872e47357b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "15e1e902-1129-4dd2-bfc0-8b07f51dca8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40df4e30-73e8-42e4-a0bb-69872e47357b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "046cd86d-25e0-4835-908a-cce9bb26538c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40df4e30-73e8-42e4-a0bb-69872e47357b",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "cfc87e15-c950-4580-9eef-272c92fb7f8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "7d54e239-50dd-4e61-86a4-c8502b66307a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfc87e15-c950-4580-9eef-272c92fb7f8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "949a1c2f-044e-486f-8e53-9fabcd91c6c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfc87e15-c950-4580-9eef-272c92fb7f8c",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "2d372c6d-03e6-4820-ac1a-f68574694d78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "a6ff8a23-5ced-4cf7-972f-a2bf59f3a471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d372c6d-03e6-4820-ac1a-f68574694d78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfc3f9e9-6f29-4fe4-aa53-7bc9e5adf4e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d372c6d-03e6-4820-ac1a-f68574694d78",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "f70e42de-06a4-4aed-bd78-e13d74db2c33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "ac414a6c-367b-4b0e-8ce2-955b9fc00ad3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f70e42de-06a4-4aed-bd78-e13d74db2c33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ccd4da0-054c-4c33-82b4-b0f4e7e489f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f70e42de-06a4-4aed-bd78-e13d74db2c33",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "25d82e90-b076-4aa1-bb6c-07146c8e7df4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "356996bf-cac6-44d8-8cfe-7db9a9452351",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25d82e90-b076-4aa1-bb6c-07146c8e7df4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d02285dc-d618-4ff4-9a94-d32f68b7492a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25d82e90-b076-4aa1-bb6c-07146c8e7df4",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "42386786-5acd-4489-a1f3-b5bbd9f2119e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "c89dd085-cb93-4c82-9051-69ae17d91786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42386786-5acd-4489-a1f3-b5bbd9f2119e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c3eaf9f-8f29-4363-ae6a-1de1d791ec3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42386786-5acd-4489-a1f3-b5bbd9f2119e",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "5d67e92b-dc58-423c-b5e2-b16f26c84c8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "f183af42-4679-40b7-8fc3-1e815ecb886f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d67e92b-dc58-423c-b5e2-b16f26c84c8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "341dca47-93e9-4c17-993e-06960aa22d89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d67e92b-dc58-423c-b5e2-b16f26c84c8e",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "bdfe2149-2ca7-4293-b393-881557686b3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "4d268568-016b-447d-b129-37f9985c4f3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdfe2149-2ca7-4293-b393-881557686b3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dec8b3e3-2caf-4357-86bd-af4b51c87ea4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdfe2149-2ca7-4293-b393-881557686b3b",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "8bd6f8c2-9e3d-4a9b-a03f-fd1309a08c31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "6ea774e6-8442-45c7-99b6-ff59b3b42e74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bd6f8c2-9e3d-4a9b-a03f-fd1309a08c31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e527cee-985f-44c2-9b16-743c31ae7a30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bd6f8c2-9e3d-4a9b-a03f-fd1309a08c31",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "a5554879-9720-44d3-907b-55758aab0d39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "507c9e2a-b2a9-4090-99a2-fab72290a172",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5554879-9720-44d3-907b-55758aab0d39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "290c00d0-29fe-4a5d-917b-07fed5f6dd64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5554879-9720-44d3-907b-55758aab0d39",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "cd9d4422-3174-4f8d-bda6-3d28c925bc4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "bf3ee85f-5d47-48b9-8d10-01ab6a213cb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd9d4422-3174-4f8d-bda6-3d28c925bc4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ece9c866-8348-4b38-b12c-9d5a031228df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd9d4422-3174-4f8d-bda6-3d28c925bc4a",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "c1029254-129f-4415-8080-dd2a0b8e350a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "9f8279cc-2217-41fb-a922-99469ca2980a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1029254-129f-4415-8080-dd2a0b8e350a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3100649-b726-457a-9d74-234206deaf23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1029254-129f-4415-8080-dd2a0b8e350a",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "eee14581-f149-4f9d-aae2-3b00671dba28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "0c3f7d5a-4904-4a14-8e35-b6dad5f5b882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eee14581-f149-4f9d-aae2-3b00671dba28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d29ccf0-2a59-4312-9455-d888c0117d62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eee14581-f149-4f9d-aae2-3b00671dba28",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "e25cfa4f-755a-4e9c-9a4f-46e9f1a0f675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "7d67c523-5b40-4194-b19e-b180ae47e70d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e25cfa4f-755a-4e9c-9a4f-46e9f1a0f675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d03cd3f3-4dba-4c06-8f90-5b4059dded37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e25cfa4f-755a-4e9c-9a4f-46e9f1a0f675",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        },
        {
            "id": "61cc8cca-f06a-4564-9730-919aceafc1b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "compositeImage": {
                "id": "61ea1aa2-34c7-4e74-b488-7b363528006d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61cc8cca-f06a-4564-9730-919aceafc1b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b13f5713-d60f-4643-a8fd-ae8754667768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61cc8cca-f06a-4564-9730-919aceafc1b0",
                    "LayerId": "d0045443-9892-4e75-993d-607628496e41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 170,
    "layers": [
        {
            "id": "d0045443-9892-4e75-993d-607628496e41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74b96299-a1cf-4add-9056-9114189cf9e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 246,
    "xorig": 123,
    "yorig": 105
}