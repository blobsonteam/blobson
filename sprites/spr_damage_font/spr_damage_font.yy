{
    "id": "4a8a79e4-0572-442a-ab23-174af388f43d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_damage_font",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 27,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfa2a3ad-209f-423a-9c15-b39c772e06d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a8a79e4-0572-442a-ab23-174af388f43d",
            "compositeImage": {
                "id": "0a700cde-ec5c-4215-9cbe-71eb1188c90d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfa2a3ad-209f-423a-9c15-b39c772e06d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98a83645-8f7f-4a9b-855b-2289f441f185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfa2a3ad-209f-423a-9c15-b39c772e06d9",
                    "LayerId": "cdb10c57-ec6f-4443-8d93-975edb2697f8"
                }
            ]
        },
        {
            "id": "58aad2df-d70c-44d3-91d6-c1c6ac2428b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a8a79e4-0572-442a-ab23-174af388f43d",
            "compositeImage": {
                "id": "99ea3f1d-a5d4-493c-9c2b-36093e73c6b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58aad2df-d70c-44d3-91d6-c1c6ac2428b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "708b9b29-c8ca-4092-8644-4361a683095a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58aad2df-d70c-44d3-91d6-c1c6ac2428b4",
                    "LayerId": "cdb10c57-ec6f-4443-8d93-975edb2697f8"
                }
            ]
        },
        {
            "id": "598388dd-3da9-46ca-bb7b-3ff8375174fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a8a79e4-0572-442a-ab23-174af388f43d",
            "compositeImage": {
                "id": "ec4d9890-9402-4596-bd5d-4870cd3026d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "598388dd-3da9-46ca-bb7b-3ff8375174fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28ad7a6d-1340-4c8a-af48-7ee9d9d84dbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "598388dd-3da9-46ca-bb7b-3ff8375174fb",
                    "LayerId": "cdb10c57-ec6f-4443-8d93-975edb2697f8"
                }
            ]
        },
        {
            "id": "efed5a21-4aa8-4b9d-bfb0-7064383ea077",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a8a79e4-0572-442a-ab23-174af388f43d",
            "compositeImage": {
                "id": "06955f96-5713-454e-b3b9-a1aebf67794a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efed5a21-4aa8-4b9d-bfb0-7064383ea077",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba39d68d-192d-4a7f-888b-b773f085f0dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efed5a21-4aa8-4b9d-bfb0-7064383ea077",
                    "LayerId": "cdb10c57-ec6f-4443-8d93-975edb2697f8"
                }
            ]
        },
        {
            "id": "435496b6-ccc8-46cd-9ae1-315e9ad19971",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a8a79e4-0572-442a-ab23-174af388f43d",
            "compositeImage": {
                "id": "9eefc714-9a71-4bd1-8e51-2c053f1e345d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "435496b6-ccc8-46cd-9ae1-315e9ad19971",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f479e457-5566-454d-a8a6-93048468a684",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "435496b6-ccc8-46cd-9ae1-315e9ad19971",
                    "LayerId": "cdb10c57-ec6f-4443-8d93-975edb2697f8"
                }
            ]
        },
        {
            "id": "c97acf6c-bdae-43f8-8798-e727810ad0a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a8a79e4-0572-442a-ab23-174af388f43d",
            "compositeImage": {
                "id": "846b46fb-3bd3-405e-bd97-517737838fa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c97acf6c-bdae-43f8-8798-e727810ad0a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "255c70b2-cadf-4b38-b31c-e2893a185901",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c97acf6c-bdae-43f8-8798-e727810ad0a9",
                    "LayerId": "cdb10c57-ec6f-4443-8d93-975edb2697f8"
                }
            ]
        },
        {
            "id": "300eece8-04da-4492-84fb-f41d57b518fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a8a79e4-0572-442a-ab23-174af388f43d",
            "compositeImage": {
                "id": "ceb40a4c-6b87-453c-8ff6-1a45aa1723a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "300eece8-04da-4492-84fb-f41d57b518fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "692d0f7c-f815-47aa-8c55-755ed925b2ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "300eece8-04da-4492-84fb-f41d57b518fb",
                    "LayerId": "cdb10c57-ec6f-4443-8d93-975edb2697f8"
                }
            ]
        },
        {
            "id": "5c5363cc-12b0-4a4b-92fc-4e26e3738746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a8a79e4-0572-442a-ab23-174af388f43d",
            "compositeImage": {
                "id": "d4c233b0-51aa-4309-ad9a-dbb429ec5809",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c5363cc-12b0-4a4b-92fc-4e26e3738746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98ed2f53-3676-46b9-b76b-3b41603d7be7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c5363cc-12b0-4a4b-92fc-4e26e3738746",
                    "LayerId": "cdb10c57-ec6f-4443-8d93-975edb2697f8"
                }
            ]
        },
        {
            "id": "67f238f9-cdbd-40d5-ba20-3b2fa5360f32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a8a79e4-0572-442a-ab23-174af388f43d",
            "compositeImage": {
                "id": "8435ac6c-405d-4703-a35a-549676bc37e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67f238f9-cdbd-40d5-ba20-3b2fa5360f32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a2b9785-77ed-4318-b9d1-8b65548b9448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67f238f9-cdbd-40d5-ba20-3b2fa5360f32",
                    "LayerId": "cdb10c57-ec6f-4443-8d93-975edb2697f8"
                }
            ]
        },
        {
            "id": "636a9faa-902f-4980-b30d-f8c3158590a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a8a79e4-0572-442a-ab23-174af388f43d",
            "compositeImage": {
                "id": "a5182c74-eab3-4d17-b395-eb78c6d2c99e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "636a9faa-902f-4980-b30d-f8c3158590a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "820f606a-1e2a-4fac-9873-5241fc126a99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "636a9faa-902f-4980-b30d-f8c3158590a1",
                    "LayerId": "cdb10c57-ec6f-4443-8d93-975edb2697f8"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 32,
    "layers": [
        {
            "id": "cdb10c57-ec6f-4443-8d93-975edb2697f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a8a79e4-0572-442a-ab23-174af388f43d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}