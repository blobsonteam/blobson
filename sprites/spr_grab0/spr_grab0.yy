{
    "id": "97abfcae-2304-4c6a-a2e1-753736572f91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grab0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 60,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec3b2687-17f3-4ff1-97b3-9d79d228bcde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97abfcae-2304-4c6a-a2e1-753736572f91",
            "compositeImage": {
                "id": "a4d07e63-dc2a-4ddf-97d1-5490c4db2e72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec3b2687-17f3-4ff1-97b3-9d79d228bcde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d3e1741-5544-4a65-9464-e0ca991ebf4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec3b2687-17f3-4ff1-97b3-9d79d228bcde",
                    "LayerId": "a42e2aa1-d991-4b49-95ee-54b4c476e639"
                }
            ]
        },
        {
            "id": "03d8a982-0abd-43c2-b108-6df11ad715a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97abfcae-2304-4c6a-a2e1-753736572f91",
            "compositeImage": {
                "id": "5bc9a44c-fafd-4ee6-80a3-96c1dee0fe92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03d8a982-0abd-43c2-b108-6df11ad715a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0bf9a74-cc42-425a-8857-0466da44db18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03d8a982-0abd-43c2-b108-6df11ad715a0",
                    "LayerId": "a42e2aa1-d991-4b49-95ee-54b4c476e639"
                }
            ]
        },
        {
            "id": "6e8f7bca-bcb3-400a-9ec0-5b9f35057783",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97abfcae-2304-4c6a-a2e1-753736572f91",
            "compositeImage": {
                "id": "5d110568-a0a9-48fa-a005-7736bce8ec78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e8f7bca-bcb3-400a-9ec0-5b9f35057783",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fdb36f7-851a-490f-a795-f75ac60208bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e8f7bca-bcb3-400a-9ec0-5b9f35057783",
                    "LayerId": "a42e2aa1-d991-4b49-95ee-54b4c476e639"
                }
            ]
        },
        {
            "id": "2d693d17-103f-42ee-99da-b3a13c23691a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97abfcae-2304-4c6a-a2e1-753736572f91",
            "compositeImage": {
                "id": "45eae709-a0ea-4977-8bc5-f349dcaa61bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d693d17-103f-42ee-99da-b3a13c23691a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd84f766-04d6-492a-ad8c-f88afd176236",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d693d17-103f-42ee-99da-b3a13c23691a",
                    "LayerId": "a42e2aa1-d991-4b49-95ee-54b4c476e639"
                }
            ]
        },
        {
            "id": "1c8fe19e-c964-4ad0-9bbd-a8720cc5ff3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97abfcae-2304-4c6a-a2e1-753736572f91",
            "compositeImage": {
                "id": "17291129-1722-485d-ba35-b71b21defa67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c8fe19e-c964-4ad0-9bbd-a8720cc5ff3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f523088-69ea-440b-b930-152d3c12bfa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c8fe19e-c964-4ad0-9bbd-a8720cc5ff3f",
                    "LayerId": "a42e2aa1-d991-4b49-95ee-54b4c476e639"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "a42e2aa1-d991-4b49-95ee-54b4c476e639",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97abfcae-2304-4c6a-a2e1-753736572f91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 17,
    "yorig": 12
}