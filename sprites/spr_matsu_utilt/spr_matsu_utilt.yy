{
    "id": "cec7ab92-b016-42b7-bd55-c65e5bde6e22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_utilt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 109,
    "bbox_left": 0,
    "bbox_right": 72,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9dfc3805-22bd-4993-be3b-20fa157db98d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec7ab92-b016-42b7-bd55-c65e5bde6e22",
            "compositeImage": {
                "id": "44fb1b4c-8738-4647-9a38-7551bc9e2d91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dfc3805-22bd-4993-be3b-20fa157db98d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5519eccb-fd3b-4513-90dc-fc80d33f0950",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dfc3805-22bd-4993-be3b-20fa157db98d",
                    "LayerId": "c3313bd1-42a5-4cde-9df5-90bb4bec628a"
                }
            ]
        },
        {
            "id": "5855be33-738a-4c13-a17e-41962fa60f58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec7ab92-b016-42b7-bd55-c65e5bde6e22",
            "compositeImage": {
                "id": "e6ed3da7-b26c-41d7-ad4a-dc3fdbf0f0f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5855be33-738a-4c13-a17e-41962fa60f58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4aac215-e044-4d5e-9e14-d76333b4d1be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5855be33-738a-4c13-a17e-41962fa60f58",
                    "LayerId": "c3313bd1-42a5-4cde-9df5-90bb4bec628a"
                }
            ]
        },
        {
            "id": "39fc883a-31e0-4294-a5d9-17cc069ac024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec7ab92-b016-42b7-bd55-c65e5bde6e22",
            "compositeImage": {
                "id": "1df9ff6d-01a5-43f4-9d97-727e5d16c1d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39fc883a-31e0-4294-a5d9-17cc069ac024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "718773c6-8bdd-4011-b864-e39d43324fe4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39fc883a-31e0-4294-a5d9-17cc069ac024",
                    "LayerId": "c3313bd1-42a5-4cde-9df5-90bb4bec628a"
                }
            ]
        },
        {
            "id": "4b8e84e9-9cc7-44cc-a67e-b2476c64ddf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec7ab92-b016-42b7-bd55-c65e5bde6e22",
            "compositeImage": {
                "id": "3db0768e-3ec1-4c1a-a4ef-9c78f2efa055",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b8e84e9-9cc7-44cc-a67e-b2476c64ddf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38cf9a1f-cfea-4d8b-815f-eb2b1ebf95c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b8e84e9-9cc7-44cc-a67e-b2476c64ddf6",
                    "LayerId": "c3313bd1-42a5-4cde-9df5-90bb4bec628a"
                }
            ]
        },
        {
            "id": "9fd20ee3-f7f2-42f7-b155-b4a546b1bbf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec7ab92-b016-42b7-bd55-c65e5bde6e22",
            "compositeImage": {
                "id": "5549041d-f065-4ea4-a3bf-eecba76a1061",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fd20ee3-f7f2-42f7-b155-b4a546b1bbf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "846d648f-8773-4f7a-80e2-d227a7ad67aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fd20ee3-f7f2-42f7-b155-b4a546b1bbf5",
                    "LayerId": "c3313bd1-42a5-4cde-9df5-90bb4bec628a"
                }
            ]
        },
        {
            "id": "7fb86cdb-758a-4105-9fab-08f08b34ea25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec7ab92-b016-42b7-bd55-c65e5bde6e22",
            "compositeImage": {
                "id": "9a614f15-9987-46e8-b956-a2d986131e7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fb86cdb-758a-4105-9fab-08f08b34ea25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15401779-b64d-40a5-bf03-9399a73b308a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fb86cdb-758a-4105-9fab-08f08b34ea25",
                    "LayerId": "c3313bd1-42a5-4cde-9df5-90bb4bec628a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "c3313bd1-42a5-4cde-9df5-90bb4bec628a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cec7ab92-b016-42b7-bd55-c65e5bde6e22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 73,
    "xorig": 22,
    "yorig": 109
}