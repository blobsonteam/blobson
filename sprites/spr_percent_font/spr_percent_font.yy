{
    "id": "bc70ce6b-18d6-4bd4-bafd-fc93e798657d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_percent_font",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 6,
    "bbox_right": 26,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f7e4708-93e4-4f09-af57-ce2c308ec980",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc70ce6b-18d6-4bd4-bafd-fc93e798657d",
            "compositeImage": {
                "id": "7256b799-fed1-4ebc-aeb4-5d8c5ebd98ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f7e4708-93e4-4f09-af57-ce2c308ec980",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84b35f5d-c11c-4ca5-9599-e7c56352d461",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f7e4708-93e4-4f09-af57-ce2c308ec980",
                    "LayerId": "134984ae-521a-4037-93b8-52c864fd63ea"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 32,
    "layers": [
        {
            "id": "134984ae-521a-4037-93b8-52c864fd63ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc70ce6b-18d6-4bd4-bafd-fc93e798657d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}