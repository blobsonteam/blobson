{
    "id": "c0523e0b-17ff-4ce7-9d84-ecc89325bd34",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tumble0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c904803c-ef06-4679-a60e-33912820daf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0523e0b-17ff-4ce7-9d84-ecc89325bd34",
            "compositeImage": {
                "id": "9a0ad83f-9992-41c2-b39b-7b3dea95ecb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c904803c-ef06-4679-a60e-33912820daf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "153c57d0-7895-4e09-93d8-4319a3912151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c904803c-ef06-4679-a60e-33912820daf2",
                    "LayerId": "b3c3c667-7b66-47b8-935c-2a4182653f9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "b3c3c667-7b66-47b8-935c-2a4182653f9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0523e0b-17ff-4ce7-9d84-ecc89325bd34",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 18
}