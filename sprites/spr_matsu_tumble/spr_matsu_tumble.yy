{
    "id": "a1866e15-ff71-473f-9652-1690e32d621b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_tumble",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 0,
    "bbox_right": 109,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d315b1c9-9367-4431-91ac-22ae17e41383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1866e15-ff71-473f-9652-1690e32d621b",
            "compositeImage": {
                "id": "c60a5344-046e-4eb4-993f-30a683ebeb62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d315b1c9-9367-4431-91ac-22ae17e41383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4a8afc0-615c-4d63-9cb6-ca35875ba6b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d315b1c9-9367-4431-91ac-22ae17e41383",
                    "LayerId": "cb0c58a4-65ff-4219-a110-6ac73e9c3f7e"
                }
            ]
        },
        {
            "id": "143ec3fc-1148-4372-b0c9-a9566a251a51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1866e15-ff71-473f-9652-1690e32d621b",
            "compositeImage": {
                "id": "88903874-487d-41ab-8de8-8a9c5c06fec8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "143ec3fc-1148-4372-b0c9-a9566a251a51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbba4373-9a6c-4962-bf2f-a8a045a3edba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "143ec3fc-1148-4372-b0c9-a9566a251a51",
                    "LayerId": "cb0c58a4-65ff-4219-a110-6ac73e9c3f7e"
                }
            ]
        },
        {
            "id": "2e487e3c-90e3-4b5d-874b-dd1a91d96b81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1866e15-ff71-473f-9652-1690e32d621b",
            "compositeImage": {
                "id": "b52da339-2dd3-44e8-9d86-ecd0a7829b18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e487e3c-90e3-4b5d-874b-dd1a91d96b81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8ca5c0e-9dcf-47a5-96cf-7c9691e543ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e487e3c-90e3-4b5d-874b-dd1a91d96b81",
                    "LayerId": "cb0c58a4-65ff-4219-a110-6ac73e9c3f7e"
                }
            ]
        },
        {
            "id": "e4e341ba-b2c3-4741-b0fa-2194002a4cf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1866e15-ff71-473f-9652-1690e32d621b",
            "compositeImage": {
                "id": "c346efd0-bf11-4ea2-bf1c-f1594ad920c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4e341ba-b2c3-4741-b0fa-2194002a4cf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4e61e7e-378d-4419-9e11-e8e0aac9bd78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4e341ba-b2c3-4741-b0fa-2194002a4cf9",
                    "LayerId": "cb0c58a4-65ff-4219-a110-6ac73e9c3f7e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "cb0c58a4-65ff-4219-a110-6ac73e9c3f7e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1866e15-ff71-473f-9652-1690e32d621b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 110,
    "xorig": 36,
    "yorig": 89
}