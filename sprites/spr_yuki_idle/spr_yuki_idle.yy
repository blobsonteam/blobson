{
    "id": "01534e49-6216-41ad-bb14-d3039c915535",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yuki_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 0,
    "bbox_right": 123,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4cef0d4-c893-441f-8f94-42e8c26b1577",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01534e49-6216-41ad-bb14-d3039c915535",
            "compositeImage": {
                "id": "6632ed7b-5ca7-4a08-ba2c-89c7faa4c520",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4cef0d4-c893-441f-8f94-42e8c26b1577",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22aeb24b-5515-48d3-a3bb-9a9ad0e33ec7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4cef0d4-c893-441f-8f94-42e8c26b1577",
                    "LayerId": "bbaca01b-7331-422c-8945-9e4cc6fb497a"
                }
            ]
        },
        {
            "id": "1e5da5c3-e570-4507-bd25-3b8a1f22f9f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01534e49-6216-41ad-bb14-d3039c915535",
            "compositeImage": {
                "id": "ac6b3d22-2ac9-4c31-805d-e6998a0b6a39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e5da5c3-e570-4507-bd25-3b8a1f22f9f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4835439-ae37-4e34-b32b-199dcc0fa8ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e5da5c3-e570-4507-bd25-3b8a1f22f9f5",
                    "LayerId": "bbaca01b-7331-422c-8945-9e4cc6fb497a"
                }
            ]
        },
        {
            "id": "d4b44ca0-f190-41f2-b00e-3d9b2270c2e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01534e49-6216-41ad-bb14-d3039c915535",
            "compositeImage": {
                "id": "8e7fe22a-b86f-4b91-ad4f-dad880282ce7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4b44ca0-f190-41f2-b00e-3d9b2270c2e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9949e20-ea5f-4f3f-b60a-b40bad0d7b93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4b44ca0-f190-41f2-b00e-3d9b2270c2e5",
                    "LayerId": "bbaca01b-7331-422c-8945-9e4cc6fb497a"
                }
            ]
        },
        {
            "id": "c98de54b-bc72-4aa8-b40a-f176c63f3749",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01534e49-6216-41ad-bb14-d3039c915535",
            "compositeImage": {
                "id": "d2a635e5-bfb1-4d29-9ae2-4ad3126e32ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c98de54b-bc72-4aa8-b40a-f176c63f3749",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5b02bab-bd6c-4f09-83fe-58dca69e7c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c98de54b-bc72-4aa8-b40a-f176c63f3749",
                    "LayerId": "bbaca01b-7331-422c-8945-9e4cc6fb497a"
                }
            ]
        },
        {
            "id": "58ec62b1-64b3-4c13-b6d8-25dc39357c14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01534e49-6216-41ad-bb14-d3039c915535",
            "compositeImage": {
                "id": "bdbd6973-5e9b-4fdc-a575-d1fcac1ed819",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58ec62b1-64b3-4c13-b6d8-25dc39357c14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de772c31-3408-4cd6-b558-3212c7abebf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58ec62b1-64b3-4c13-b6d8-25dc39357c14",
                    "LayerId": "bbaca01b-7331-422c-8945-9e4cc6fb497a"
                }
            ]
        },
        {
            "id": "19ecd2f9-5507-4f42-8fbd-8e43a8368a8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01534e49-6216-41ad-bb14-d3039c915535",
            "compositeImage": {
                "id": "2504e4be-08ae-49b8-b9b1-86f7c1b5c032",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19ecd2f9-5507-4f42-8fbd-8e43a8368a8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2956820-9753-4323-a785-c92f57328f80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ecd2f9-5507-4f42-8fbd-8e43a8368a8d",
                    "LayerId": "bbaca01b-7331-422c-8945-9e4cc6fb497a"
                }
            ]
        },
        {
            "id": "444603d9-cc07-4ebc-842e-62f452996701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01534e49-6216-41ad-bb14-d3039c915535",
            "compositeImage": {
                "id": "f84ddfdf-2e97-49cd-ab47-1ddf98f85237",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "444603d9-cc07-4ebc-842e-62f452996701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfea67da-9a68-41a0-8f63-5d74999e4caf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "444603d9-cc07-4ebc-842e-62f452996701",
                    "LayerId": "bbaca01b-7331-422c-8945-9e4cc6fb497a"
                }
            ]
        },
        {
            "id": "87ac9b2a-9cf5-4d94-bd09-43d0c092113e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01534e49-6216-41ad-bb14-d3039c915535",
            "compositeImage": {
                "id": "f45e5a1d-7dad-493a-8458-b56fd0808048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87ac9b2a-9cf5-4d94-bd09-43d0c092113e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4f6da6e-d193-41f2-a283-e0ded3c89b47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87ac9b2a-9cf5-4d94-bd09-43d0c092113e",
                    "LayerId": "bbaca01b-7331-422c-8945-9e4cc6fb497a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 102,
    "layers": [
        {
            "id": "bbaca01b-7331-422c-8945-9e4cc6fb497a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01534e49-6216-41ad-bb14-d3039c915535",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 71,
    "yorig": 101
}