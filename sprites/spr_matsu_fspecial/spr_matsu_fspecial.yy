{
    "id": "92d1d729-aa49-4652-aa65-2a8583411ec8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_fspecial",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 109,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fd32a9a-57ea-4c30-86b4-ea402b5a2431",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d1d729-aa49-4652-aa65-2a8583411ec8",
            "compositeImage": {
                "id": "aa524305-b67a-4a4e-9ec8-b1b6dfc1b74a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd32a9a-57ea-4c30-86b4-ea402b5a2431",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb306f0e-3262-4887-99cf-860515f89435",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd32a9a-57ea-4c30-86b4-ea402b5a2431",
                    "LayerId": "a5cec116-587a-419d-a0bf-6d4fc1b1ffa0"
                }
            ]
        },
        {
            "id": "4763772e-0416-47b8-85e7-cfb4355f8975",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d1d729-aa49-4652-aa65-2a8583411ec8",
            "compositeImage": {
                "id": "9d73b0c5-f669-453e-8f29-cf858245bc72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4763772e-0416-47b8-85e7-cfb4355f8975",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df4c966e-ffa0-421b-be0e-b5735547bd91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4763772e-0416-47b8-85e7-cfb4355f8975",
                    "LayerId": "a5cec116-587a-419d-a0bf-6d4fc1b1ffa0"
                }
            ]
        },
        {
            "id": "0b864f7b-68dc-4b4f-bcdf-b177d7f57acc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d1d729-aa49-4652-aa65-2a8583411ec8",
            "compositeImage": {
                "id": "1ce670d5-7932-4391-ae51-b084650be213",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b864f7b-68dc-4b4f-bcdf-b177d7f57acc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f957098e-d1e2-4c65-8f47-dd270f42bdc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b864f7b-68dc-4b4f-bcdf-b177d7f57acc",
                    "LayerId": "a5cec116-587a-419d-a0bf-6d4fc1b1ffa0"
                }
            ]
        },
        {
            "id": "4b2f5ad9-7400-4507-a697-4465f95d4753",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d1d729-aa49-4652-aa65-2a8583411ec8",
            "compositeImage": {
                "id": "f1b05967-b84b-4bc0-b12a-19a6544a746c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b2f5ad9-7400-4507-a697-4465f95d4753",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdfec8ba-e956-45ec-9368-d50e38aa68fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b2f5ad9-7400-4507-a697-4465f95d4753",
                    "LayerId": "a5cec116-587a-419d-a0bf-6d4fc1b1ffa0"
                }
            ]
        },
        {
            "id": "e2da46b7-7e77-436b-a08b-df64a803fe39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d1d729-aa49-4652-aa65-2a8583411ec8",
            "compositeImage": {
                "id": "6c7808bc-7118-435f-8ec5-5b2d64843fe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2da46b7-7e77-436b-a08b-df64a803fe39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2a8bb55-985a-4e24-9853-549e73cd3818",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2da46b7-7e77-436b-a08b-df64a803fe39",
                    "LayerId": "a5cec116-587a-419d-a0bf-6d4fc1b1ffa0"
                }
            ]
        },
        {
            "id": "b967c120-6772-4dad-b866-6b3c20caab2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d1d729-aa49-4652-aa65-2a8583411ec8",
            "compositeImage": {
                "id": "07e6b9ac-1e90-44b6-9e21-b72814c802df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b967c120-6772-4dad-b866-6b3c20caab2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f38fee0d-762d-47c8-91d6-982a00b84505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b967c120-6772-4dad-b866-6b3c20caab2d",
                    "LayerId": "a5cec116-587a-419d-a0bf-6d4fc1b1ffa0"
                }
            ]
        },
        {
            "id": "8bc10b3d-292b-4793-94ad-c4d391419537",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d1d729-aa49-4652-aa65-2a8583411ec8",
            "compositeImage": {
                "id": "69c10144-6565-4b75-9c7c-a2a2a2081bbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bc10b3d-292b-4793-94ad-c4d391419537",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "180b8d27-e729-443c-9e4e-f881f4ae8df0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bc10b3d-292b-4793-94ad-c4d391419537",
                    "LayerId": "a5cec116-587a-419d-a0bf-6d4fc1b1ffa0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "a5cec116-587a-419d-a0bf-6d4fc1b1ffa0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92d1d729-aa49-4652-aa65-2a8583411ec8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 44,
    "yorig": 91
}