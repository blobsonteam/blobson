{
    "id": "2bfa329e-8cf5-4510-891c-651f165fbe08",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_trainingD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1099,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d975f50e-92d9-4cb6-8399-6edf74dc3d27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bfa329e-8cf5-4510-891c-651f165fbe08",
            "compositeImage": {
                "id": "21dd8a5b-0713-48d0-b657-3264ba2a74cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d975f50e-92d9-4cb6-8399-6edf74dc3d27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e6c05a4-ca39-4166-9e88-5b53ec27f229",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d975f50e-92d9-4cb6-8399-6edf74dc3d27",
                    "LayerId": "e352a771-cadf-455f-b31c-95e83b535836"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1100,
    "layers": [
        {
            "id": "e352a771-cadf-455f-b31c-95e83b535836",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2bfa329e-8cf5-4510-891c-651f165fbe08",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1000,
    "xorig": 500,
    "yorig": 550
}