{
    "id": "114cf66b-8e15-4f3e-b7be-c7c2b3769221",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_trainingC",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 764,
    "bbox_left": 704,
    "bbox_right": 1343,
    "bbox_top": 123,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9f2434a-5183-4086-a769-19da22587f63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "114cf66b-8e15-4f3e-b7be-c7c2b3769221",
            "compositeImage": {
                "id": "32b2adbe-d213-4f2b-80bd-d0937b27cb4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9f2434a-5183-4086-a769-19da22587f63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f03bef78-4c0b-4772-aba8-d5f17006f257",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9f2434a-5183-4086-a769-19da22587f63",
                    "LayerId": "e3770b98-ea53-486e-8070-6b460775f4e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1100,
    "layers": [
        {
            "id": "e3770b98-ea53-486e-8070-6b460775f4e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "114cf66b-8e15-4f3e-b7be-c7c2b3769221",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2048,
    "xorig": 1024,
    "yorig": 550
}