{
    "id": "f2236aac-0cf6-4e37-a9f1-9c5987dc9580",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_waveland",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 54,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1e8e123-7628-4f5e-9cf8-f65f957ea6b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2236aac-0cf6-4e37-a9f1-9c5987dc9580",
            "compositeImage": {
                "id": "6428790b-fd93-4c1d-9dba-d8d9a50e1f85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1e8e123-7628-4f5e-9cf8-f65f957ea6b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a3fe6d7-a6af-4ad7-8781-75644c9f521c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1e8e123-7628-4f5e-9cf8-f65f957ea6b9",
                    "LayerId": "9a118dcf-a064-4e9e-838d-6220b2abb592"
                }
            ]
        },
        {
            "id": "e477e3fc-fce4-432f-a649-d61a2dcaf4b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2236aac-0cf6-4e37-a9f1-9c5987dc9580",
            "compositeImage": {
                "id": "3ea6db18-579f-455c-b6b5-f7a849d44a7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e477e3fc-fce4-432f-a649-d61a2dcaf4b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6bf8be1-b09d-49a1-ae4c-3bb9fa76c730",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e477e3fc-fce4-432f-a649-d61a2dcaf4b5",
                    "LayerId": "9a118dcf-a064-4e9e-838d-6220b2abb592"
                }
            ]
        },
        {
            "id": "aec0a473-572c-4734-96e1-f2949a07302b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2236aac-0cf6-4e37-a9f1-9c5987dc9580",
            "compositeImage": {
                "id": "1d047aac-d560-40f8-b1e9-09523984732e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aec0a473-572c-4734-96e1-f2949a07302b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41065236-b611-4827-accd-31e5ef00c13c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aec0a473-572c-4734-96e1-f2949a07302b",
                    "LayerId": "9a118dcf-a064-4e9e-838d-6220b2abb592"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "9a118dcf-a064-4e9e-838d-6220b2abb592",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2236aac-0cf6-4e37-a9f1-9c5987dc9580",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 25,
    "yorig": 76
}