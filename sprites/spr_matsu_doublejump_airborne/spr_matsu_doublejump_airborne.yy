{
    "id": "0c94908b-d957-4d98-a257-76c411afbb67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_doublejump_airborne",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0d5d796-e60a-44a3-a76d-f2dcbb18781a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c94908b-d957-4d98-a257-76c411afbb67",
            "compositeImage": {
                "id": "70901da4-9757-4d51-a086-0622ab9f3066",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0d5d796-e60a-44a3-a76d-f2dcbb18781a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "066aec55-2e14-4db7-861d-84204677f203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0d5d796-e60a-44a3-a76d-f2dcbb18781a",
                    "LayerId": "2b0a4add-a5f4-4f99-92e5-afe6c6ef79aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 146,
    "layers": [
        {
            "id": "2b0a4add-a5f4-4f99-92e5-afe6c6ef79aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c94908b-d957-4d98-a257-76c411afbb67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 86,
    "xorig": 43,
    "yorig": 145
}