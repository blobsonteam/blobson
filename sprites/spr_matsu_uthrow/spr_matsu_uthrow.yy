{
    "id": "b63dc0cd-cd9f-4cb0-b848-8d88707a844e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_uthrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 106,
    "bbox_left": 0,
    "bbox_right": 85,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef3b793d-3762-4c12-9835-9dbed3e1b64c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b63dc0cd-cd9f-4cb0-b848-8d88707a844e",
            "compositeImage": {
                "id": "c13c5927-cdb9-4e19-ae57-a7bdead4e5d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef3b793d-3762-4c12-9835-9dbed3e1b64c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8756c87-2c56-4407-b6f0-9f1a2d98d8be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef3b793d-3762-4c12-9835-9dbed3e1b64c",
                    "LayerId": "57ab341a-18db-4308-b6d5-ef6377c1af1c"
                }
            ]
        },
        {
            "id": "54ab1660-5d47-4c20-8f4a-5f4a4501d779",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b63dc0cd-cd9f-4cb0-b848-8d88707a844e",
            "compositeImage": {
                "id": "837a59a0-f4e9-4575-b4cc-d52ca430fe53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54ab1660-5d47-4c20-8f4a-5f4a4501d779",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce00e423-a808-4e0e-996a-5f8a964d6ce9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54ab1660-5d47-4c20-8f4a-5f4a4501d779",
                    "LayerId": "57ab341a-18db-4308-b6d5-ef6377c1af1c"
                }
            ]
        },
        {
            "id": "d062441a-653e-4ace-be0c-22c4660ef758",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b63dc0cd-cd9f-4cb0-b848-8d88707a844e",
            "compositeImage": {
                "id": "282d2f93-b88e-4b9f-8ca7-6ce6ffc4474f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d062441a-653e-4ace-be0c-22c4660ef758",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cb72c99-bff4-4d04-9c30-cc89f5a31a39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d062441a-653e-4ace-be0c-22c4660ef758",
                    "LayerId": "57ab341a-18db-4308-b6d5-ef6377c1af1c"
                }
            ]
        },
        {
            "id": "f4dbcd92-d86f-4fc6-9123-be43c6fd3c8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b63dc0cd-cd9f-4cb0-b848-8d88707a844e",
            "compositeImage": {
                "id": "c622b7f6-a969-4e1e-af87-4e3c849a2710",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4dbcd92-d86f-4fc6-9123-be43c6fd3c8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60eaf116-cb20-4d08-9c5b-6ec85ae18ecc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4dbcd92-d86f-4fc6-9123-be43c6fd3c8f",
                    "LayerId": "57ab341a-18db-4308-b6d5-ef6377c1af1c"
                }
            ]
        },
        {
            "id": "7e1d6edc-ec48-4783-8439-01efe887f2f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b63dc0cd-cd9f-4cb0-b848-8d88707a844e",
            "compositeImage": {
                "id": "cfb504f9-0683-43f2-8a3e-fa2c97a4a717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e1d6edc-ec48-4783-8439-01efe887f2f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b472034-e184-4004-a786-99ddc57f9a57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e1d6edc-ec48-4783-8439-01efe887f2f8",
                    "LayerId": "57ab341a-18db-4308-b6d5-ef6377c1af1c"
                }
            ]
        },
        {
            "id": "f2d04dbe-4581-4966-a0af-56e6e07d0433",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b63dc0cd-cd9f-4cb0-b848-8d88707a844e",
            "compositeImage": {
                "id": "54dbd5ca-2f5b-4ad1-a29f-e8ca79cad17b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2d04dbe-4581-4966-a0af-56e6e07d0433",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bbcf17b-aaa0-44a9-a4f1-e8da1a98cef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2d04dbe-4581-4966-a0af-56e6e07d0433",
                    "LayerId": "57ab341a-18db-4308-b6d5-ef6377c1af1c"
                }
            ]
        },
        {
            "id": "5ecdddf1-0550-4b41-9e08-f9865604ca29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b63dc0cd-cd9f-4cb0-b848-8d88707a844e",
            "compositeImage": {
                "id": "a5c99d4c-15fb-4a09-8594-7b1a2d5c4642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ecdddf1-0550-4b41-9e08-f9865604ca29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b441436-fbd8-44e3-b1ec-8b8abdc79c55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ecdddf1-0550-4b41-9e08-f9865604ca29",
                    "LayerId": "57ab341a-18db-4308-b6d5-ef6377c1af1c"
                }
            ]
        },
        {
            "id": "eaa20c69-295e-4df7-ab3d-596e06840384",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b63dc0cd-cd9f-4cb0-b848-8d88707a844e",
            "compositeImage": {
                "id": "fb26676e-bf1c-411e-9f2d-ade682c16eb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaa20c69-295e-4df7-ab3d-596e06840384",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "350e08a4-7797-4072-853a-94453a864d66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaa20c69-295e-4df7-ab3d-596e06840384",
                    "LayerId": "57ab341a-18db-4308-b6d5-ef6377c1af1c"
                }
            ]
        },
        {
            "id": "775f8ea9-3b5b-4cd2-91c3-62923c1b4a79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b63dc0cd-cd9f-4cb0-b848-8d88707a844e",
            "compositeImage": {
                "id": "7d2d6168-365f-4ad9-b2f0-0b8e049f746a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "775f8ea9-3b5b-4cd2-91c3-62923c1b4a79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44b05688-6f49-41a6-b337-1b0ada572026",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "775f8ea9-3b5b-4cd2-91c3-62923c1b4a79",
                    "LayerId": "57ab341a-18db-4308-b6d5-ef6377c1af1c"
                }
            ]
        },
        {
            "id": "9dc9b932-a260-4889-b6c9-ccc432d1ef4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b63dc0cd-cd9f-4cb0-b848-8d88707a844e",
            "compositeImage": {
                "id": "dc5eae16-4526-4346-87d5-1565d852e85e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dc9b932-a260-4889-b6c9-ccc432d1ef4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09c1683b-98fa-4fc2-a87f-d7182ed044dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dc9b932-a260-4889-b6c9-ccc432d1ef4a",
                    "LayerId": "57ab341a-18db-4308-b6d5-ef6377c1af1c"
                }
            ]
        },
        {
            "id": "9a6bc2fb-11d3-4419-b4bb-6847d3850e57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b63dc0cd-cd9f-4cb0-b848-8d88707a844e",
            "compositeImage": {
                "id": "5430cfc1-5a2d-4371-9b75-185c8d0ba8e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a6bc2fb-11d3-4419-b4bb-6847d3850e57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7413fab9-3a30-40fd-a77a-d148e84732eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a6bc2fb-11d3-4419-b4bb-6847d3850e57",
                    "LayerId": "57ab341a-18db-4308-b6d5-ef6377c1af1c"
                }
            ]
        },
        {
            "id": "c25c4c19-1484-4d2d-b70b-9e8db579ea98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b63dc0cd-cd9f-4cb0-b848-8d88707a844e",
            "compositeImage": {
                "id": "65148b65-10a6-457a-917d-fc55fa9dcbc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c25c4c19-1484-4d2d-b70b-9e8db579ea98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d879616-f81d-4434-8fb8-0c07f725d7b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c25c4c19-1484-4d2d-b70b-9e8db579ea98",
                    "LayerId": "57ab341a-18db-4308-b6d5-ef6377c1af1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 107,
    "layers": [
        {
            "id": "57ab341a-18db-4308-b6d5-ef6377c1af1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b63dc0cd-cd9f-4cb0-b848-8d88707a844e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 86,
    "xorig": 31,
    "yorig": 106
}