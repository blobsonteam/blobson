{
    "id": "2a590b96-e925-4173-90e8-1097422f08ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_uair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 131,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a9d6fbc-0922-4e60-8524-3b4ffb30cbe8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a590b96-e925-4173-90e8-1097422f08ea",
            "compositeImage": {
                "id": "e47cf4e2-3de7-4cfb-b21f-ca4b4a631711",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a9d6fbc-0922-4e60-8524-3b4ffb30cbe8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f13a4608-40ae-45e7-8efe-5be4e43c209d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a9d6fbc-0922-4e60-8524-3b4ffb30cbe8",
                    "LayerId": "4f504ebd-d50e-473f-8f03-59e868e83bca"
                }
            ]
        },
        {
            "id": "727615c9-aa92-4df3-b3ce-4eaee18cbd5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a590b96-e925-4173-90e8-1097422f08ea",
            "compositeImage": {
                "id": "43e4eedd-e3b3-490d-b844-0a7c1405428c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "727615c9-aa92-4df3-b3ce-4eaee18cbd5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dae82f9-0fcc-456a-9df8-6d1a46c0451f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "727615c9-aa92-4df3-b3ce-4eaee18cbd5f",
                    "LayerId": "4f504ebd-d50e-473f-8f03-59e868e83bca"
                }
            ]
        },
        {
            "id": "87c09ea0-2673-4e18-9b64-995bfd638128",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a590b96-e925-4173-90e8-1097422f08ea",
            "compositeImage": {
                "id": "c9918f59-aae9-41ab-b59a-8ff305985c40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87c09ea0-2673-4e18-9b64-995bfd638128",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95c6f4c3-9b65-4a0b-bd6f-c3e8b7d3b9db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87c09ea0-2673-4e18-9b64-995bfd638128",
                    "LayerId": "4f504ebd-d50e-473f-8f03-59e868e83bca"
                }
            ]
        },
        {
            "id": "889d64e8-450d-40b8-bf2e-155044a4a2f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a590b96-e925-4173-90e8-1097422f08ea",
            "compositeImage": {
                "id": "c9caebe4-921e-4892-8565-49873337d216",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "889d64e8-450d-40b8-bf2e-155044a4a2f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d2b03a3-1903-4be0-85d0-0cdadf3cad93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "889d64e8-450d-40b8-bf2e-155044a4a2f0",
                    "LayerId": "4f504ebd-d50e-473f-8f03-59e868e83bca"
                }
            ]
        },
        {
            "id": "3b91e475-024f-4655-881f-913387580035",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a590b96-e925-4173-90e8-1097422f08ea",
            "compositeImage": {
                "id": "3c7d7169-60e5-4eca-a4ab-17d14f199d82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b91e475-024f-4655-881f-913387580035",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10a2e3cc-ed36-4b7b-a270-41f81a8b9621",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b91e475-024f-4655-881f-913387580035",
                    "LayerId": "4f504ebd-d50e-473f-8f03-59e868e83bca"
                }
            ]
        },
        {
            "id": "0055382d-53c7-48d1-8b3f-89c7ffcd36e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a590b96-e925-4173-90e8-1097422f08ea",
            "compositeImage": {
                "id": "43c2b08d-457e-4b03-9563-17e697e2ca6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0055382d-53c7-48d1-8b3f-89c7ffcd36e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2716fd8-ef77-4de0-ba29-f766f1807a51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0055382d-53c7-48d1-8b3f-89c7ffcd36e8",
                    "LayerId": "4f504ebd-d50e-473f-8f03-59e868e83bca"
                }
            ]
        },
        {
            "id": "33eb42dd-a8bb-4984-abdb-0795f70e3be4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a590b96-e925-4173-90e8-1097422f08ea",
            "compositeImage": {
                "id": "2ee3ee0e-0a2e-4a98-93ae-eb3c409e59b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33eb42dd-a8bb-4984-abdb-0795f70e3be4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1c0e76c-a5ef-491a-9963-30e505106423",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33eb42dd-a8bb-4984-abdb-0795f70e3be4",
                    "LayerId": "4f504ebd-d50e-473f-8f03-59e868e83bca"
                }
            ]
        },
        {
            "id": "c3a9efc8-4d86-4793-929d-1f13c23d7f63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a590b96-e925-4173-90e8-1097422f08ea",
            "compositeImage": {
                "id": "60308420-aee6-4be3-b168-13bb6a8356d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3a9efc8-4d86-4793-929d-1f13c23d7f63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf5780d4-bb45-4869-9fee-a20d64ec243f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3a9efc8-4d86-4793-929d-1f13c23d7f63",
                    "LayerId": "4f504ebd-d50e-473f-8f03-59e868e83bca"
                }
            ]
        },
        {
            "id": "ae51764f-3091-4af5-b285-b2857c31b16c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a590b96-e925-4173-90e8-1097422f08ea",
            "compositeImage": {
                "id": "9f085810-7307-41ee-b3fd-7d35ee70a2f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae51764f-3091-4af5-b285-b2857c31b16c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73b2b3fd-ea6d-498c-9599-74df66fbbbf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae51764f-3091-4af5-b285-b2857c31b16c",
                    "LayerId": "4f504ebd-d50e-473f-8f03-59e868e83bca"
                }
            ]
        },
        {
            "id": "80e36ff5-760c-402d-9b9d-667eb8392520",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a590b96-e925-4173-90e8-1097422f08ea",
            "compositeImage": {
                "id": "faf9659a-1153-49f8-92c7-29ff0f49de59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80e36ff5-760c-402d-9b9d-667eb8392520",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5194906b-340d-43fb-b09b-aefd52e1a3e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80e36ff5-760c-402d-9b9d-667eb8392520",
                    "LayerId": "4f504ebd-d50e-473f-8f03-59e868e83bca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 132,
    "layers": [
        {
            "id": "4f504ebd-d50e-473f-8f03-59e868e83bca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a590b96-e925-4173-90e8-1097422f08ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 39,
    "yorig": 129
}