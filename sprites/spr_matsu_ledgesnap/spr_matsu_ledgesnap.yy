{
    "id": "0b26e853-4aca-4fbb-91b7-056780822173",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_ledgesnap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 0,
    "bbox_right": 107,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2dc44fd-141c-4f71-bbda-59560d637462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b26e853-4aca-4fbb-91b7-056780822173",
            "compositeImage": {
                "id": "4f9d80d4-e0a4-4dda-afb3-04fdd6a8d601",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2dc44fd-141c-4f71-bbda-59560d637462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7aba8463-b5a1-4059-9ba8-288f8048504c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2dc44fd-141c-4f71-bbda-59560d637462",
                    "LayerId": "e5db9265-a874-4f7a-9667-6b6420a46686"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 74,
    "layers": [
        {
            "id": "e5db9265-a874-4f7a-9667-6b6420a46686",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b26e853-4aca-4fbb-91b7-056780822173",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 55,
    "yorig": 73
}