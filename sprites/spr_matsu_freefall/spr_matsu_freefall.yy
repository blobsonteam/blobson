{
    "id": "f2f14cf1-6daa-40b2-8200-e6b8c5bd3d0d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_freefall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 0,
    "bbox_right": 101,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a5b71a7-fccc-499d-81a1-bb71ef6f51d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2f14cf1-6daa-40b2-8200-e6b8c5bd3d0d",
            "compositeImage": {
                "id": "1ed63277-dc30-41a9-8079-ce1635c494b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a5b71a7-fccc-499d-81a1-bb71ef6f51d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ebf79c1-594b-4d1a-98a4-6ddd081772f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a5b71a7-fccc-499d-81a1-bb71ef6f51d4",
                    "LayerId": "ee809cfe-9153-4728-aff6-21d12ec68420"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "ee809cfe-9153-4728-aff6-21d12ec68420",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2f14cf1-6daa-40b2-8200-e6b8c5bd3d0d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 102,
    "xorig": 51,
    "yorig": 67
}