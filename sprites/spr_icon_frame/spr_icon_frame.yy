{
    "id": "9a176426-af70-4bb6-aa26-b431fedf11ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_frame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 7,
    "bbox_right": 90,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81921ffc-7264-49e2-bbb5-16e0a2da7b1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a176426-af70-4bb6-aa26-b431fedf11ef",
            "compositeImage": {
                "id": "7e13a451-33b6-4505-b3a5-5502566da61a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81921ffc-7264-49e2-bbb5-16e0a2da7b1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b2e41e0-f18f-4c43-a92e-62b1fee8ccb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81921ffc-7264-49e2-bbb5-16e0a2da7b1f",
                    "LayerId": "f8343f3e-8450-4c33-ad53-783e0367ebf9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "f8343f3e-8450-4c33-ad53-783e0367ebf9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a176426-af70-4bb6-aa26-b431fedf11ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}