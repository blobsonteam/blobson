{
    "id": "4505ade2-1e7f-4031-b45c-ba903e0fe5d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_dtilt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 0,
    "bbox_right": 115,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c77ab45b-b2cb-46cc-8bd0-6371b76b8b8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4505ade2-1e7f-4031-b45c-ba903e0fe5d4",
            "compositeImage": {
                "id": "1a5ca258-973d-4160-924e-b74017b8289e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c77ab45b-b2cb-46cc-8bd0-6371b76b8b8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0a45291-ddc1-4f89-9c9c-1d37ab0ed2ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c77ab45b-b2cb-46cc-8bd0-6371b76b8b8d",
                    "LayerId": "e7a0fe87-7662-4c42-86ee-549a17935a4a"
                }
            ]
        },
        {
            "id": "398a4792-d831-4a86-ac08-da383aeb057d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4505ade2-1e7f-4031-b45c-ba903e0fe5d4",
            "compositeImage": {
                "id": "028769f8-ea75-49c0-beac-c8efd7243ddb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "398a4792-d831-4a86-ac08-da383aeb057d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b1aa6e7-8732-4de5-b828-3a10c7ab6726",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "398a4792-d831-4a86-ac08-da383aeb057d",
                    "LayerId": "e7a0fe87-7662-4c42-86ee-549a17935a4a"
                }
            ]
        },
        {
            "id": "e80b9a86-e2be-4de1-8821-15cd4906e8c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4505ade2-1e7f-4031-b45c-ba903e0fe5d4",
            "compositeImage": {
                "id": "03d11378-faf0-4aaf-a1ae-5da5a9689c1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e80b9a86-e2be-4de1-8821-15cd4906e8c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a373e2e-0af7-4265-9483-474206d3cece",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e80b9a86-e2be-4de1-8821-15cd4906e8c4",
                    "LayerId": "e7a0fe87-7662-4c42-86ee-549a17935a4a"
                }
            ]
        },
        {
            "id": "8069fe81-9394-4aa2-b93b-1403f89077bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4505ade2-1e7f-4031-b45c-ba903e0fe5d4",
            "compositeImage": {
                "id": "c3ba2e79-878a-456c-b497-013744cff18a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8069fe81-9394-4aa2-b93b-1403f89077bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8298778-069e-4d4c-9565-dae35c8ca3c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8069fe81-9394-4aa2-b93b-1403f89077bc",
                    "LayerId": "e7a0fe87-7662-4c42-86ee-549a17935a4a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "e7a0fe87-7662-4c42-86ee-549a17935a4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4505ade2-1e7f-4031-b45c-ba903e0fe5d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 116,
    "xorig": 42,
    "yorig": 69
}