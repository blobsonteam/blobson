{
    "id": "19ceae9b-5923-4bca-bb6a-048e872e1353",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_ledge_getup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 207,
    "bbox_left": 0,
    "bbox_right": 103,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e12eede4-0368-45bf-b88a-76d6c643d6c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19ceae9b-5923-4bca-bb6a-048e872e1353",
            "compositeImage": {
                "id": "329a0aa3-7a5c-4c6e-b82d-f3400894839c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e12eede4-0368-45bf-b88a-76d6c643d6c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7804b52b-bdc5-4bc0-af08-c0c0f7d3a164",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e12eede4-0368-45bf-b88a-76d6c643d6c3",
                    "LayerId": "846c8eae-46a0-4fb9-a38b-6d78156a359a"
                }
            ]
        },
        {
            "id": "c5a47c75-d6e5-4265-a188-11c62dd6cf1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19ceae9b-5923-4bca-bb6a-048e872e1353",
            "compositeImage": {
                "id": "abe34aee-a040-482f-9fe3-04418ff98396",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5a47c75-d6e5-4265-a188-11c62dd6cf1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "667c88de-96bd-4393-830a-c706864e94c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5a47c75-d6e5-4265-a188-11c62dd6cf1c",
                    "LayerId": "846c8eae-46a0-4fb9-a38b-6d78156a359a"
                }
            ]
        },
        {
            "id": "58703c3d-feae-45dc-933e-17423d77912d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19ceae9b-5923-4bca-bb6a-048e872e1353",
            "compositeImage": {
                "id": "59785d4f-557f-48cc-b3ea-e09a3403ff11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58703c3d-feae-45dc-933e-17423d77912d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85c8b8eb-3bec-4699-8841-5339b92ce2dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58703c3d-feae-45dc-933e-17423d77912d",
                    "LayerId": "846c8eae-46a0-4fb9-a38b-6d78156a359a"
                }
            ]
        },
        {
            "id": "4bbd4929-30bc-44de-a153-ab5db17f735a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19ceae9b-5923-4bca-bb6a-048e872e1353",
            "compositeImage": {
                "id": "298c86de-4701-4284-ada1-80972af566af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bbd4929-30bc-44de-a153-ab5db17f735a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2823393-6521-4388-82c1-ba8cd2088d24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bbd4929-30bc-44de-a153-ab5db17f735a",
                    "LayerId": "846c8eae-46a0-4fb9-a38b-6d78156a359a"
                }
            ]
        },
        {
            "id": "a90f9401-83ef-480b-a025-e79f0970a4b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19ceae9b-5923-4bca-bb6a-048e872e1353",
            "compositeImage": {
                "id": "526301e4-c84b-4944-8909-41d40072db62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a90f9401-83ef-480b-a025-e79f0970a4b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f78f21bb-28d9-4fc7-a2f5-da139dfc1b4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a90f9401-83ef-480b-a025-e79f0970a4b1",
                    "LayerId": "846c8eae-46a0-4fb9-a38b-6d78156a359a"
                }
            ]
        },
        {
            "id": "faa28bb7-bb4f-4311-ba08-b1bba12e748e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19ceae9b-5923-4bca-bb6a-048e872e1353",
            "compositeImage": {
                "id": "d13ac8a0-b1a0-47bc-9447-480c23944ba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faa28bb7-bb4f-4311-ba08-b1bba12e748e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f2468d6-ede0-4c08-a1d9-c8a847c572f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faa28bb7-bb4f-4311-ba08-b1bba12e748e",
                    "LayerId": "846c8eae-46a0-4fb9-a38b-6d78156a359a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 208,
    "layers": [
        {
            "id": "846c8eae-46a0-4fb9-a38b-6d78156a359a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19ceae9b-5923-4bca-bb6a-048e872e1353",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 104,
    "xorig": 56,
    "yorig": 207
}