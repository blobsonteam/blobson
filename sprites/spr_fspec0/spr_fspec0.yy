{
    "id": "03a74baf-70f6-4e78-b6ab-74adc0583950",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fspec0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 15,
    "bbox_right": 75,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "889c728a-afb4-467a-998d-054cda52f5c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a74baf-70f6-4e78-b6ab-74adc0583950",
            "compositeImage": {
                "id": "6fd43fa9-00c3-45a0-808c-b662541560db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "889c728a-afb4-467a-998d-054cda52f5c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a417608-fcb1-45bf-9ba5-57391e7d0c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "889c728a-afb4-467a-998d-054cda52f5c4",
                    "LayerId": "68deb719-c637-470d-8b1f-823cac2ddc3e"
                },
                {
                    "id": "1d5510ba-c83c-4a99-8ece-ee52c5e29947",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "889c728a-afb4-467a-998d-054cda52f5c4",
                    "LayerId": "322f07dc-e54f-4cac-ae99-7ac4c28b997e"
                }
            ]
        },
        {
            "id": "08178999-c528-4a64-99f2-e94f8e5d4260",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a74baf-70f6-4e78-b6ab-74adc0583950",
            "compositeImage": {
                "id": "72b9b14f-6347-4dfd-8f3a-163aa957d366",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08178999-c528-4a64-99f2-e94f8e5d4260",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec95d3f9-cedc-4108-96bf-eb4e901eb39f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08178999-c528-4a64-99f2-e94f8e5d4260",
                    "LayerId": "68deb719-c637-470d-8b1f-823cac2ddc3e"
                },
                {
                    "id": "3acbdd37-69c1-46cd-9f7f-8a18bf26a574",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08178999-c528-4a64-99f2-e94f8e5d4260",
                    "LayerId": "322f07dc-e54f-4cac-ae99-7ac4c28b997e"
                }
            ]
        },
        {
            "id": "c49c9346-1527-4260-a0e3-35e1c7f9cd65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a74baf-70f6-4e78-b6ab-74adc0583950",
            "compositeImage": {
                "id": "730dfacf-a40e-44af-a977-1503da342c49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c49c9346-1527-4260-a0e3-35e1c7f9cd65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0564c012-7fe7-4042-af5b-a90a6c2206a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c49c9346-1527-4260-a0e3-35e1c7f9cd65",
                    "LayerId": "68deb719-c637-470d-8b1f-823cac2ddc3e"
                },
                {
                    "id": "0a1cd0cc-2671-4ea0-a26a-5f019640cfe2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c49c9346-1527-4260-a0e3-35e1c7f9cd65",
                    "LayerId": "322f07dc-e54f-4cac-ae99-7ac4c28b997e"
                }
            ]
        },
        {
            "id": "c5ea05f5-140a-4eac-900a-cf5426e527a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a74baf-70f6-4e78-b6ab-74adc0583950",
            "compositeImage": {
                "id": "ce218c1b-5e8d-43f4-ae08-1454ab794e84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5ea05f5-140a-4eac-900a-cf5426e527a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d26c0390-9199-4d63-ab7a-acbe595e8df6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5ea05f5-140a-4eac-900a-cf5426e527a5",
                    "LayerId": "68deb719-c637-470d-8b1f-823cac2ddc3e"
                },
                {
                    "id": "6eaece92-023d-48d3-9e44-468253f34568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5ea05f5-140a-4eac-900a-cf5426e527a5",
                    "LayerId": "322f07dc-e54f-4cac-ae99-7ac4c28b997e"
                }
            ]
        },
        {
            "id": "1e95b7b0-e3f8-47d2-ac38-2bba21044c48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a74baf-70f6-4e78-b6ab-74adc0583950",
            "compositeImage": {
                "id": "e93256d0-00a0-4872-a473-ad0383bfa244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e95b7b0-e3f8-47d2-ac38-2bba21044c48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09f03a87-38bd-4cea-a5d4-2f8d3189bae2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e95b7b0-e3f8-47d2-ac38-2bba21044c48",
                    "LayerId": "68deb719-c637-470d-8b1f-823cac2ddc3e"
                },
                {
                    "id": "4005e253-8c57-464a-beff-650458e8fbfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e95b7b0-e3f8-47d2-ac38-2bba21044c48",
                    "LayerId": "322f07dc-e54f-4cac-ae99-7ac4c28b997e"
                }
            ]
        },
        {
            "id": "b8d6b828-1d0d-4094-9e0b-470cecfefd08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a74baf-70f6-4e78-b6ab-74adc0583950",
            "compositeImage": {
                "id": "70294bcf-111c-4fc2-86c7-000d56e8daa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8d6b828-1d0d-4094-9e0b-470cecfefd08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ebb0329-bca1-4e3e-bb4c-a6eedb6f7cbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8d6b828-1d0d-4094-9e0b-470cecfefd08",
                    "LayerId": "68deb719-c637-470d-8b1f-823cac2ddc3e"
                },
                {
                    "id": "802bbbbf-df8f-4e21-a7c6-6f413e026464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8d6b828-1d0d-4094-9e0b-470cecfefd08",
                    "LayerId": "322f07dc-e54f-4cac-ae99-7ac4c28b997e"
                }
            ]
        },
        {
            "id": "d22dd6f1-7e16-4826-80be-81bc8fe12019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a74baf-70f6-4e78-b6ab-74adc0583950",
            "compositeImage": {
                "id": "631278a8-8156-4a30-840c-8e1791fd050c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d22dd6f1-7e16-4826-80be-81bc8fe12019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efd17581-4336-4698-b987-9b8e0a33f634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d22dd6f1-7e16-4826-80be-81bc8fe12019",
                    "LayerId": "68deb719-c637-470d-8b1f-823cac2ddc3e"
                },
                {
                    "id": "47c1f6ad-5848-42bb-812f-bbbf08c0c57f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d22dd6f1-7e16-4826-80be-81bc8fe12019",
                    "LayerId": "322f07dc-e54f-4cac-ae99-7ac4c28b997e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "68deb719-c637-470d-8b1f-823cac2ddc3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03a74baf-70f6-4e78-b6ab-74adc0583950",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "322f07dc-e54f-4cac-ae99-7ac4c28b997e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03a74baf-70f6-4e78-b6ab-74adc0583950",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 47,
    "yorig": 28
}