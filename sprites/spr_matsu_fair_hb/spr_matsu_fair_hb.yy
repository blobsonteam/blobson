{
    "id": "648038f7-cbf3-47d1-8f24-a5ad4249973b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_fair_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 61,
    "bbox_right": 97,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2220bdc-6258-4d25-a8c9-5f767aafebcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "648038f7-cbf3-47d1-8f24-a5ad4249973b",
            "compositeImage": {
                "id": "acd6097c-a078-4a10-9205-bec5dfa24ee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2220bdc-6258-4d25-a8c9-5f767aafebcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acb4cd23-8822-4483-af17-d48f2fe00782",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2220bdc-6258-4d25-a8c9-5f767aafebcd",
                    "LayerId": "148537e4-0f40-4612-a27f-411ea38dd903"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 121,
    "layers": [
        {
            "id": "148537e4-0f40-4612-a27f-411ea38dd903",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "648038f7-cbf3-47d1-8f24-a5ad4249973b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 37,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 98,
    "xorig": 36,
    "yorig": 114
}