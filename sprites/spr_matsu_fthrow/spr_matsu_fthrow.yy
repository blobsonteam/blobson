{
    "id": "0df563be-cef7-4ce1-88a4-8d0d886cb180",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_fthrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 0,
    "bbox_right": 125,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4d2186d-2d27-48b0-9c5c-5636ee02ba29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df563be-cef7-4ce1-88a4-8d0d886cb180",
            "compositeImage": {
                "id": "b32f5f2f-4556-479f-b55a-f848919d54c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4d2186d-2d27-48b0-9c5c-5636ee02ba29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ac1407d-2c6b-4762-8fbd-004103ea5ed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4d2186d-2d27-48b0-9c5c-5636ee02ba29",
                    "LayerId": "50af2ef5-a677-47ed-a6be-45e10ec99039"
                }
            ]
        },
        {
            "id": "577cbd25-086e-4856-bc1e-b252824f8f43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df563be-cef7-4ce1-88a4-8d0d886cb180",
            "compositeImage": {
                "id": "0eea6990-5219-403c-b75f-4d32c47387d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "577cbd25-086e-4856-bc1e-b252824f8f43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b424c15a-8a77-4cef-9efa-7a6cf6226ffb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "577cbd25-086e-4856-bc1e-b252824f8f43",
                    "LayerId": "50af2ef5-a677-47ed-a6be-45e10ec99039"
                }
            ]
        },
        {
            "id": "9a1a5eb5-3e9a-4d63-8ae1-95ba51947095",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df563be-cef7-4ce1-88a4-8d0d886cb180",
            "compositeImage": {
                "id": "576b47eb-51a9-4b56-bbd3-64618b3b7b52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a1a5eb5-3e9a-4d63-8ae1-95ba51947095",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a1a435c-2792-4bb3-87fe-dbd2018e9c76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a1a5eb5-3e9a-4d63-8ae1-95ba51947095",
                    "LayerId": "50af2ef5-a677-47ed-a6be-45e10ec99039"
                }
            ]
        },
        {
            "id": "f9f98d92-ab10-4708-b8d5-1145a8ef9acc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df563be-cef7-4ce1-88a4-8d0d886cb180",
            "compositeImage": {
                "id": "64f9e78b-da99-4fe6-b974-3b20958e1866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9f98d92-ab10-4708-b8d5-1145a8ef9acc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea47d84a-6d66-4b7f-a607-c8e4ccdcd8cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9f98d92-ab10-4708-b8d5-1145a8ef9acc",
                    "LayerId": "50af2ef5-a677-47ed-a6be-45e10ec99039"
                }
            ]
        },
        {
            "id": "7bf3a96c-0f62-414d-8747-3b1051f5c79f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df563be-cef7-4ce1-88a4-8d0d886cb180",
            "compositeImage": {
                "id": "ed285713-9a53-4231-96be-9a654d7e1746",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bf3a96c-0f62-414d-8747-3b1051f5c79f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e8b4f9b-0b9a-4609-a241-376abc4325cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bf3a96c-0f62-414d-8747-3b1051f5c79f",
                    "LayerId": "50af2ef5-a677-47ed-a6be-45e10ec99039"
                }
            ]
        },
        {
            "id": "b8036cfb-e25f-4bc8-b893-922ff400214d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df563be-cef7-4ce1-88a4-8d0d886cb180",
            "compositeImage": {
                "id": "efdec25d-a0d7-446b-8128-b04651e157d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8036cfb-e25f-4bc8-b893-922ff400214d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26e321ea-dad0-4dc5-a3cf-07491c5b9445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8036cfb-e25f-4bc8-b893-922ff400214d",
                    "LayerId": "50af2ef5-a677-47ed-a6be-45e10ec99039"
                }
            ]
        },
        {
            "id": "6ad2d123-e446-4949-907a-318138db27c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df563be-cef7-4ce1-88a4-8d0d886cb180",
            "compositeImage": {
                "id": "914cf680-7b6d-4c1c-a024-a74a08bdd1d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ad2d123-e446-4949-907a-318138db27c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f936aa2-a348-4a6d-8286-c673239165b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ad2d123-e446-4949-907a-318138db27c2",
                    "LayerId": "50af2ef5-a677-47ed-a6be-45e10ec99039"
                }
            ]
        },
        {
            "id": "6fe40d7e-d49b-41ab-936b-6a391d320a20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df563be-cef7-4ce1-88a4-8d0d886cb180",
            "compositeImage": {
                "id": "da1b4d2e-503f-45dc-9649-1c9c511bfc1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fe40d7e-d49b-41ab-936b-6a391d320a20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ceafd63-143e-4458-bc36-9a1e2a479e49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fe40d7e-d49b-41ab-936b-6a391d320a20",
                    "LayerId": "50af2ef5-a677-47ed-a6be-45e10ec99039"
                }
            ]
        },
        {
            "id": "0b53c8d9-73b0-432e-b72d-c8bcf10f2401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df563be-cef7-4ce1-88a4-8d0d886cb180",
            "compositeImage": {
                "id": "a2d36c6b-80af-48bc-956c-8288541fdbe8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b53c8d9-73b0-432e-b72d-c8bcf10f2401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12ffc189-b9eb-4d95-b4be-03c45064dfff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b53c8d9-73b0-432e-b72d-c8bcf10f2401",
                    "LayerId": "50af2ef5-a677-47ed-a6be-45e10ec99039"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 101,
    "layers": [
        {
            "id": "50af2ef5-a677-47ed-a6be-45e10ec99039",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0df563be-cef7-4ce1-88a4-8d0d886cb180",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 126,
    "xorig": 47,
    "yorig": 100
}