{
    "id": "fa5b5c34-6d57-433a-b45e-8a362fe24da0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_uspecial",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "942ae871-f561-4431-b89e-d3624222c640",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa5b5c34-6d57-433a-b45e-8a362fe24da0",
            "compositeImage": {
                "id": "882b16fb-ccf6-470c-8fde-5076b95ddf87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "942ae871-f561-4431-b89e-d3624222c640",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84d001a2-7971-419c-8955-5b99cbd75837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "942ae871-f561-4431-b89e-d3624222c640",
                    "LayerId": "eaf95800-be01-4067-bcb0-4d3727e1032c"
                }
            ]
        },
        {
            "id": "15fb0b9b-c8be-4068-a70e-c0a1a919755c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa5b5c34-6d57-433a-b45e-8a362fe24da0",
            "compositeImage": {
                "id": "43229d64-2f8a-4c25-8450-f76407f662ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15fb0b9b-c8be-4068-a70e-c0a1a919755c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1606a69e-f608-4d3b-8b30-2aa877a0d4df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15fb0b9b-c8be-4068-a70e-c0a1a919755c",
                    "LayerId": "eaf95800-be01-4067-bcb0-4d3727e1032c"
                }
            ]
        },
        {
            "id": "6322bc27-c4b2-44af-ba6c-2eae09a173fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa5b5c34-6d57-433a-b45e-8a362fe24da0",
            "compositeImage": {
                "id": "c1d07d18-c95a-4f6f-bca9-d8378aee42ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6322bc27-c4b2-44af-ba6c-2eae09a173fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "630dc209-9265-46f7-9f20-0fe602f00f39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6322bc27-c4b2-44af-ba6c-2eae09a173fa",
                    "LayerId": "eaf95800-be01-4067-bcb0-4d3727e1032c"
                }
            ]
        },
        {
            "id": "5bde9e92-04f1-4881-8472-860f2bc91c7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa5b5c34-6d57-433a-b45e-8a362fe24da0",
            "compositeImage": {
                "id": "611ac2d1-ae39-4da4-9ce5-d6ec5fb109b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bde9e92-04f1-4881-8472-860f2bc91c7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68ffec63-ae7d-40cf-8bd2-9b49cee5789e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bde9e92-04f1-4881-8472-860f2bc91c7f",
                    "LayerId": "eaf95800-be01-4067-bcb0-4d3727e1032c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 119,
    "layers": [
        {
            "id": "eaf95800-be01-4067-bcb0-4d3727e1032c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa5b5c34-6d57-433a-b45e-8a362fe24da0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 32,
    "yorig": 118
}