{
    "id": "f9588fe1-6b59-4b54-93b8-e389c27d6d42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stock0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a041400-768d-434d-94d7-eb54465b724b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9588fe1-6b59-4b54-93b8-e389c27d6d42",
            "compositeImage": {
                "id": "4245d361-ed04-4cfa-b14e-10f12f305c16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a041400-768d-434d-94d7-eb54465b724b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0469e083-f9df-4564-b913-e7d48448673b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a041400-768d-434d-94d7-eb54465b724b",
                    "LayerId": "b72c4e5a-45f5-463b-be49-b5cfcbdbae4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "b72c4e5a-45f5-463b-be49-b5cfcbdbae4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9588fe1-6b59-4b54-93b8-e389c27d6d42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}