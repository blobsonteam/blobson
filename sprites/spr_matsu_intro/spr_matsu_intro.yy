{
    "id": "1d392728-fab6-415d-8f5c-2858721fc6c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_intro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 12,
    "bbox_right": 78,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8be4184-26d7-49ad-8af2-a7abefa7931c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "80953062-c0ee-41b7-9995-9f58ed672292",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8be4184-26d7-49ad-8af2-a7abefa7931c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c86eef39-68d1-41fd-bc10-e43fa2843e76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8be4184-26d7-49ad-8af2-a7abefa7931c",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "910ba476-7fe6-4917-b06b-4a0b0cfdc2a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "89ddb12a-a99d-4af8-962b-e4805d6e91df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "910ba476-7fe6-4917-b06b-4a0b0cfdc2a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d975954-f017-4094-9b8b-737b90ced097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "910ba476-7fe6-4917-b06b-4a0b0cfdc2a7",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "a542a3ad-5f91-4884-a362-54c5a35d0820",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "e5b8bf27-0087-4d99-ac14-0268e53f8206",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a542a3ad-5f91-4884-a362-54c5a35d0820",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "815040c0-1e80-41fb-a384-7f284c066334",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a542a3ad-5f91-4884-a362-54c5a35d0820",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "7baf62ac-7292-42da-a4c1-4a0f8d163538",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "98825faa-efd0-4121-b7c9-4810d8b55800",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7baf62ac-7292-42da-a4c1-4a0f8d163538",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cfcfa1f-2d3a-4490-8080-7d48c3fecb11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7baf62ac-7292-42da-a4c1-4a0f8d163538",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "458298f2-18e6-439e-8146-3b11502d526e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "a62d130b-1a5c-4a25-977e-b7847dbce726",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "458298f2-18e6-439e-8146-3b11502d526e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7af13c27-3037-490d-a69e-f2056cc8acf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "458298f2-18e6-439e-8146-3b11502d526e",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "c137df7e-cd99-48fb-a36e-733672cd0bc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "b396a580-95fd-421d-aca6-de68308b234e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c137df7e-cd99-48fb-a36e-733672cd0bc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcc4540b-42a3-4617-a783-879ae86a32b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c137df7e-cd99-48fb-a36e-733672cd0bc4",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "3dfcdb26-cf9d-4dde-846a-87bc8c74b990",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "203cebed-9d70-46c4-811f-efca9f968662",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dfcdb26-cf9d-4dde-846a-87bc8c74b990",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7910fd96-aed7-4b03-b614-98c7c23d2a07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dfcdb26-cf9d-4dde-846a-87bc8c74b990",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "d99c53d1-b7d9-4f73-911b-908fd99539fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "a51d461c-e026-48b7-865a-394169e17fb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d99c53d1-b7d9-4f73-911b-908fd99539fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11308c26-b2f7-4812-acc3-987cbe14ee7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d99c53d1-b7d9-4f73-911b-908fd99539fe",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "641e76af-1e46-4314-b12d-01423951c8e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "99a1e626-69ad-40ff-843a-495cfaa218de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "641e76af-1e46-4314-b12d-01423951c8e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93ba8b6c-4401-4bf6-9ee2-cc9af266f6f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "641e76af-1e46-4314-b12d-01423951c8e3",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "c48cfb8a-5bbe-4675-956e-c2272f90d37c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "9da385d8-c9ba-46e5-8557-19ee3fb69b0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c48cfb8a-5bbe-4675-956e-c2272f90d37c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7e0b0ff-77b2-41cc-a2e6-31987c2ce9a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c48cfb8a-5bbe-4675-956e-c2272f90d37c",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "11a0eb7d-6b8c-419f-9c21-415ac3aea6f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "d3e76ae1-7cbf-462a-885a-aeb5e80b2f1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11a0eb7d-6b8c-419f-9c21-415ac3aea6f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d08214d1-c53f-4f04-af7e-7e960aba4ed5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11a0eb7d-6b8c-419f-9c21-415ac3aea6f0",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "71f9bae2-53f5-4597-bc5d-106a85a7ea93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "dd4ee67f-b6e5-4f64-8cc4-b1498cb7c86c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71f9bae2-53f5-4597-bc5d-106a85a7ea93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e0aabeb-aee4-4c84-bf40-674d6a38d0ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71f9bae2-53f5-4597-bc5d-106a85a7ea93",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "a8e1fdf9-761a-4595-aa1e-f493279ea17c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "2cdfeedc-f453-4756-8264-b0f8f233c638",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8e1fdf9-761a-4595-aa1e-f493279ea17c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76a8403e-e094-4d9a-b6c7-2ad167bc77ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8e1fdf9-761a-4595-aa1e-f493279ea17c",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "ab58c7c7-4632-4bc9-a4cb-b0a846a35a7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "94589495-da47-4f0f-affd-ed2c097fc0c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab58c7c7-4632-4bc9-a4cb-b0a846a35a7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0cb6443-cf1a-4b53-94ec-32a5c85573d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab58c7c7-4632-4bc9-a4cb-b0a846a35a7a",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "5d9bba7b-c1f5-4dc7-850f-cd888d966a34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "37869b18-0d54-4f65-adf2-7105a7479e96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d9bba7b-c1f5-4dc7-850f-cd888d966a34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f6a8192-c01b-4fc4-9cef-71b231825349",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d9bba7b-c1f5-4dc7-850f-cd888d966a34",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "f16b1c9a-6795-45f5-9832-ad10de0b8541",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "596d51db-f086-4b8e-811d-4a5a21ce7a18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f16b1c9a-6795-45f5-9832-ad10de0b8541",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5bc30a2-d64d-45e9-a99a-1780787b9880",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f16b1c9a-6795-45f5-9832-ad10de0b8541",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "750d3c80-beda-47c8-b9e7-4f3b0de8d569",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "4d537c69-6e9f-416c-94bd-fcbdf0b5410f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "750d3c80-beda-47c8-b9e7-4f3b0de8d569",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "807b4c15-a7a6-4744-a6f9-4bfeca0e4e7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "750d3c80-beda-47c8-b9e7-4f3b0de8d569",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "2dbb0546-13de-478d-a57f-1b630fa50e63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "d5ac3728-22f9-4bd8-aad6-679c6e90825d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dbb0546-13de-478d-a57f-1b630fa50e63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cead864e-6e80-4566-8d05-fb3a7cc88066",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dbb0546-13de-478d-a57f-1b630fa50e63",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "29fda802-a7e3-4b0d-89c9-84384dff088e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "1204dc4e-0945-4724-b14b-26baa1e5c547",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29fda802-a7e3-4b0d-89c9-84384dff088e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cae9c1ba-9c0b-4364-bcd8-c095a11007ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29fda802-a7e3-4b0d-89c9-84384dff088e",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "9b946538-f146-4b9d-b54b-80c417ca5d47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "5753abba-9211-4be4-b0ce-9dcc7703e703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b946538-f146-4b9d-b54b-80c417ca5d47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adf5028d-114b-49f6-813c-403ea15a8f7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b946538-f146-4b9d-b54b-80c417ca5d47",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "f072db83-4928-42bc-aff4-ccd911cb3931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "b6eaa5b4-7098-45b7-bf51-ca118e94c173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f072db83-4928-42bc-aff4-ccd911cb3931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a57eee75-2a97-40ad-86ef-d4d0b4cf0d98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f072db83-4928-42bc-aff4-ccd911cb3931",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "a030a96e-07c4-44f2-9945-1566a8eb0028",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "bb4aae66-9df3-456c-bdc0-f0414ee61154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a030a96e-07c4-44f2-9945-1566a8eb0028",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee8b373f-d544-4c22-b724-ede9918997e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a030a96e-07c4-44f2-9945-1566a8eb0028",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "26822f82-3f0f-4529-9dc5-07e237333b7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "9e2465c9-2767-4851-91ed-0d8d80599bf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26822f82-3f0f-4529-9dc5-07e237333b7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d202dea3-7c0c-4a2b-9914-0b8e8f9b180a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26822f82-3f0f-4529-9dc5-07e237333b7f",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "c9081e8b-5840-4984-8fbe-359aa37292a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "b632aa82-b2d4-456c-b9ec-b0d071087deb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9081e8b-5840-4984-8fbe-359aa37292a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfa41561-1a89-466b-8adf-4bb372619a17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9081e8b-5840-4984-8fbe-359aa37292a7",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "fda43ccb-b4eb-43eb-a60c-395e76c3f2fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "4f66485e-9c84-4c04-85f1-36863bd88a71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fda43ccb-b4eb-43eb-a60c-395e76c3f2fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8f31f45-3d22-45f4-99a0-9368cd072233",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fda43ccb-b4eb-43eb-a60c-395e76c3f2fc",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "463a9716-b8d6-40e5-b887-e0eff0ab6a03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "a259af03-32ae-450e-a4d7-4ae5475ba7b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "463a9716-b8d6-40e5-b887-e0eff0ab6a03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac86ad6c-9edf-4717-b4ea-06ef46fca967",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "463a9716-b8d6-40e5-b887-e0eff0ab6a03",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "b9c0382f-e4c9-4524-944a-4be9c605808d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "d5fdc3c3-f1d0-4352-9156-4d46c8e89070",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9c0382f-e4c9-4524-944a-4be9c605808d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1f07bc8-7e25-4cc0-92b4-bccc464c09d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9c0382f-e4c9-4524-944a-4be9c605808d",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "7270521b-5108-49b9-9977-7ce0e508b25d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "dffdf815-36eb-4826-b260-db1bf1de245f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7270521b-5108-49b9-9977-7ce0e508b25d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "005495ab-a413-4735-b0cf-63e49c1d2aac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7270521b-5108-49b9-9977-7ce0e508b25d",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "0e3e5985-1270-4212-a305-338a77c569ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "70d6f612-d517-402d-9dda-6cd94534a06f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e3e5985-1270-4212-a305-338a77c569ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8fccf86-a447-4726-bcf1-dac4b0330753",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e3e5985-1270-4212-a305-338a77c569ec",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "8d26307b-17ae-45d8-b731-25b9f0ef2582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "d828b297-fbdf-4fec-a59e-d34d754f13e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d26307b-17ae-45d8-b731-25b9f0ef2582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2e3797e-733a-4fd7-8543-80da23328757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d26307b-17ae-45d8-b731-25b9f0ef2582",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "cebef4df-553b-43f7-a62c-7e106affb3e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "84ef47d1-5bab-4eca-b853-05627f1a67a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cebef4df-553b-43f7-a62c-7e106affb3e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52f7d99c-9747-431e-957a-5b199de549ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cebef4df-553b-43f7-a62c-7e106affb3e9",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        },
        {
            "id": "86fd881e-bcf5-4a8e-b9f6-7feb18a10272",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "compositeImage": {
                "id": "d01f93d3-9015-422e-91ba-f964fa29ea94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86fd881e-bcf5-4a8e-b9f6-7feb18a10272",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a57a287-52a6-42fe-957d-c31fa726a869",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86fd881e-bcf5-4a8e-b9f6-7feb18a10272",
                    "LayerId": "45606d03-d1c9-4c7b-a4d4-360ae46447c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 102,
    "layers": [
        {
            "id": "45606d03-d1c9-4c7b-a4d4-360ae46447c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d392728-fab6-415d-8f5c-2858721fc6c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 37,
    "yorig": 101
}