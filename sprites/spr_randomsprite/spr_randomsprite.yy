{
    "id": "2569ecc1-9171-4619-9e2f-eacd4298ca35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_randomsprite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 17,
    "bbox_right": 63,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d75bd088-5be0-4640-8977-d7102dc7c9e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2569ecc1-9171-4619-9e2f-eacd4298ca35",
            "compositeImage": {
                "id": "78ae9f08-aadc-4541-8274-f7df58e98edb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d75bd088-5be0-4640-8977-d7102dc7c9e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "927df26f-ed81-422e-bb3e-399062126bb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d75bd088-5be0-4640-8977-d7102dc7c9e6",
                    "LayerId": "78c35a95-b49f-4573-b4f5-d094ebf1b5c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "78c35a95-b49f-4573-b4f5-d094ebf1b5c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2569ecc1-9171-4619-9e2f-eacd4298ca35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}