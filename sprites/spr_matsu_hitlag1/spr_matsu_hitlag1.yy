{
    "id": "73f25380-cd15-4526-ac7b-447efd2b8879",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_hitlag1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f173654f-2fb3-42e4-a077-35f988e3a7cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73f25380-cd15-4526-ac7b-447efd2b8879",
            "compositeImage": {
                "id": "b8e9a55e-a9bf-4d95-8205-471f4e94a9f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f173654f-2fb3-42e4-a077-35f988e3a7cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b597c8e-7e53-4778-ad23-082f88dc80b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f173654f-2fb3-42e4-a077-35f988e3a7cd",
                    "LayerId": "17a461e6-c710-476f-a415-2468bf273203"
                }
            ]
        },
        {
            "id": "f94c629c-333b-4d37-a10e-95aba378a6b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73f25380-cd15-4526-ac7b-447efd2b8879",
            "compositeImage": {
                "id": "2dad67fb-7a51-4ff9-87a8-8aca3e912c38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f94c629c-333b-4d37-a10e-95aba378a6b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63998fcc-0199-4832-a905-bb37a7eb0f08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f94c629c-333b-4d37-a10e-95aba378a6b2",
                    "LayerId": "17a461e6-c710-476f-a415-2468bf273203"
                }
            ]
        },
        {
            "id": "d0315aa2-1e4b-4733-8afb-67fb837baf09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73f25380-cd15-4526-ac7b-447efd2b8879",
            "compositeImage": {
                "id": "e47e61a8-3160-454f-be24-6dcf12191cae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0315aa2-1e4b-4733-8afb-67fb837baf09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a30c298e-b2e5-43dd-b9bf-43850ef743c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0315aa2-1e4b-4733-8afb-67fb837baf09",
                    "LayerId": "17a461e6-c710-476f-a415-2468bf273203"
                }
            ]
        },
        {
            "id": "4b9c0a8d-fbaa-428c-a44b-7ed02c5bd133",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73f25380-cd15-4526-ac7b-447efd2b8879",
            "compositeImage": {
                "id": "fb5a53f8-3121-44f4-88ce-0a34aef923ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b9c0a8d-fbaa-428c-a44b-7ed02c5bd133",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56cbd5f0-66be-40cd-a339-53658d6c376b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b9c0a8d-fbaa-428c-a44b-7ed02c5bd133",
                    "LayerId": "17a461e6-c710-476f-a415-2468bf273203"
                }
            ]
        },
        {
            "id": "d6282298-0953-4872-a76b-e83a0a1f317c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73f25380-cd15-4526-ac7b-447efd2b8879",
            "compositeImage": {
                "id": "2f90f826-9486-46d0-8b2a-1f2e955e0117",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6282298-0953-4872-a76b-e83a0a1f317c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6026cdf9-78a3-4da9-b9fb-5b5fe1ea3251",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6282298-0953-4872-a76b-e83a0a1f317c",
                    "LayerId": "17a461e6-c710-476f-a415-2468bf273203"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "17a461e6-c710-476f-a415-2468bf273203",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73f25380-cd15-4526-ac7b-447efd2b8879",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0.2,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 53,
    "yorig": 86
}