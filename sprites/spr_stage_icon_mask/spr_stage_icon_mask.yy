{
    "id": "b871acda-0026-44b2-969d-16b2828681c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stage_icon_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e56499a6-47fb-40de-ac90-834f62c1f8e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b871acda-0026-44b2-969d-16b2828681c3",
            "compositeImage": {
                "id": "4fcf6877-1dac-4188-8914-66ec04796fd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e56499a6-47fb-40de-ac90-834f62c1f8e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc1c7f9b-e50f-4d36-bc5d-777b642c6b0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e56499a6-47fb-40de-ac90-834f62c1f8e6",
                    "LayerId": "ea14ddf8-54b4-47aa-91b3-20a3ba9afa99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "ea14ddf8-54b4-47aa-91b3-20a3ba9afa99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b871acda-0026-44b2-969d-16b2828681c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}