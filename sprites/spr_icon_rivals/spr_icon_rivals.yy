{
    "id": "6f937609-29e7-428c-b497-949aaa3f4f93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_rivals",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06ce5412-f34d-45c0-82d9-a91c9020eed1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f937609-29e7-428c-b497-949aaa3f4f93",
            "compositeImage": {
                "id": "fefc13c9-98df-4d41-9e1d-10228058b048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06ce5412-f34d-45c0-82d9-a91c9020eed1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fd0210b-77c0-4660-97ba-e9e3fbba1b58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06ce5412-f34d-45c0-82d9-a91c9020eed1",
                    "LayerId": "f7414d0c-6388-46b8-9c44-a8e883180b26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "f7414d0c-6388-46b8-9c44-a8e883180b26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f937609-29e7-428c-b497-949aaa3f4f93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}