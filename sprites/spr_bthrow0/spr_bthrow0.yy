{
    "id": "4ddc5355-82bb-4c94-b20a-73a209db8a02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bthrow0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "edbc12d6-ebcf-4efb-a855-9f838b1bfe5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ddc5355-82bb-4c94-b20a-73a209db8a02",
            "compositeImage": {
                "id": "f6790426-5209-48af-82cd-137edf62e404",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edbc12d6-ebcf-4efb-a855-9f838b1bfe5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d14e67ca-aa2a-4842-95c7-9ede40172539",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edbc12d6-ebcf-4efb-a855-9f838b1bfe5f",
                    "LayerId": "7cb46f57-fe25-4bd4-84ff-8e678b5df856"
                },
                {
                    "id": "2ac6002a-f6a3-4a3f-bddc-776d543962c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edbc12d6-ebcf-4efb-a855-9f838b1bfe5f",
                    "LayerId": "8fe4f2cb-b5b8-4545-a122-2b5a3af20c62"
                }
            ]
        },
        {
            "id": "aa178a91-11f3-463d-8e0a-aab23cd587fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ddc5355-82bb-4c94-b20a-73a209db8a02",
            "compositeImage": {
                "id": "bc9ee27e-6a0d-40d5-a3bc-2b59b44c0058",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa178a91-11f3-463d-8e0a-aab23cd587fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9cc12f1-7adf-4256-b28a-d22942f730c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa178a91-11f3-463d-8e0a-aab23cd587fd",
                    "LayerId": "7cb46f57-fe25-4bd4-84ff-8e678b5df856"
                },
                {
                    "id": "d88a8fdc-ef57-4885-a44f-c9cdc430bd50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa178a91-11f3-463d-8e0a-aab23cd587fd",
                    "LayerId": "8fe4f2cb-b5b8-4545-a122-2b5a3af20c62"
                }
            ]
        },
        {
            "id": "5dd323f8-7daf-4d07-a2bd-f12943a6b55b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ddc5355-82bb-4c94-b20a-73a209db8a02",
            "compositeImage": {
                "id": "d090f4df-53da-478e-849c-80aea48fcfdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dd323f8-7daf-4d07-a2bd-f12943a6b55b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2314b2f6-9620-46de-9a02-f5df3dbd7cea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd323f8-7daf-4d07-a2bd-f12943a6b55b",
                    "LayerId": "7cb46f57-fe25-4bd4-84ff-8e678b5df856"
                },
                {
                    "id": "c6ed2994-71d3-4090-9910-1b1de130ece2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd323f8-7daf-4d07-a2bd-f12943a6b55b",
                    "LayerId": "8fe4f2cb-b5b8-4545-a122-2b5a3af20c62"
                }
            ]
        },
        {
            "id": "15f270ae-3ab2-4dd8-8231-4e71c3fa9eef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ddc5355-82bb-4c94-b20a-73a209db8a02",
            "compositeImage": {
                "id": "c6bf9286-26e7-4cf8-88de-0f86c32435a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15f270ae-3ab2-4dd8-8231-4e71c3fa9eef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7836d231-5323-444c-8b08-1a6aed72eb77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15f270ae-3ab2-4dd8-8231-4e71c3fa9eef",
                    "LayerId": "7cb46f57-fe25-4bd4-84ff-8e678b5df856"
                },
                {
                    "id": "7da8c72d-a33e-46ea-8a91-7caa5dfc33d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15f270ae-3ab2-4dd8-8231-4e71c3fa9eef",
                    "LayerId": "8fe4f2cb-b5b8-4545-a122-2b5a3af20c62"
                }
            ]
        },
        {
            "id": "6a018df9-0d52-41ec-af07-3f90c8026b64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ddc5355-82bb-4c94-b20a-73a209db8a02",
            "compositeImage": {
                "id": "b72375c7-aa7a-49cb-91cf-f99a51bdedc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a018df9-0d52-41ec-af07-3f90c8026b64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46b11ae9-c65d-4a1f-a71d-978f337cb577",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a018df9-0d52-41ec-af07-3f90c8026b64",
                    "LayerId": "7cb46f57-fe25-4bd4-84ff-8e678b5df856"
                },
                {
                    "id": "0a25aa71-58e6-4997-b76e-a57907097288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a018df9-0d52-41ec-af07-3f90c8026b64",
                    "LayerId": "8fe4f2cb-b5b8-4545-a122-2b5a3af20c62"
                }
            ]
        },
        {
            "id": "479719b0-2915-40a2-8e74-72d585b55302",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ddc5355-82bb-4c94-b20a-73a209db8a02",
            "compositeImage": {
                "id": "dcf53ec6-c1a6-4c88-bce0-32e016f1230e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "479719b0-2915-40a2-8e74-72d585b55302",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b57c2cf-c168-4d50-ad0a-c0737c925c35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "479719b0-2915-40a2-8e74-72d585b55302",
                    "LayerId": "7cb46f57-fe25-4bd4-84ff-8e678b5df856"
                },
                {
                    "id": "7486ea71-7008-4ab1-a6ec-3422bf672300",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "479719b0-2915-40a2-8e74-72d585b55302",
                    "LayerId": "8fe4f2cb-b5b8-4545-a122-2b5a3af20c62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8fe4f2cb-b5b8-4545-a122-2b5a3af20c62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ddc5355-82bb-4c94-b20a-73a209db8a02",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "7cb46f57-fe25-4bd4-84ff-8e678b5df856",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ddc5355-82bb-4c94-b20a-73a209db8a02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 20
}