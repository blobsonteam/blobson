{
    "id": "71e0e68a-0168-47c8-a45e-40cc9c29c428",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dtilt0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3e4707e-9b61-49bd-9798-f621fc92dcc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71e0e68a-0168-47c8-a45e-40cc9c29c428",
            "compositeImage": {
                "id": "cf69bfcd-0f7a-4ad4-814f-394ba1615e36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3e4707e-9b61-49bd-9798-f621fc92dcc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a156eae0-726d-46d9-bd01-3af30c3cbe39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3e4707e-9b61-49bd-9798-f621fc92dcc8",
                    "LayerId": "9a892fc8-f3ca-4574-9d65-21a00bca39f9"
                }
            ]
        },
        {
            "id": "5f3c890b-de3b-4658-ad93-03c406e49ce6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71e0e68a-0168-47c8-a45e-40cc9c29c428",
            "compositeImage": {
                "id": "3cbfe8f0-98dd-4761-bd7a-c8176e8f32ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f3c890b-de3b-4658-ad93-03c406e49ce6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff3839a2-d06d-46e6-8522-7057de3cde69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f3c890b-de3b-4658-ad93-03c406e49ce6",
                    "LayerId": "9a892fc8-f3ca-4574-9d65-21a00bca39f9"
                }
            ]
        },
        {
            "id": "ac897fe2-48cf-434e-abbe-26f7dd693b57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71e0e68a-0168-47c8-a45e-40cc9c29c428",
            "compositeImage": {
                "id": "eefc2c28-253f-4a3d-9d50-886cfc38a603",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac897fe2-48cf-434e-abbe-26f7dd693b57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cb6af87-5452-44aa-9345-5aa85c7400cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac897fe2-48cf-434e-abbe-26f7dd693b57",
                    "LayerId": "9a892fc8-f3ca-4574-9d65-21a00bca39f9"
                }
            ]
        },
        {
            "id": "fa1ea698-4e19-4a7a-8f14-22b987535f1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71e0e68a-0168-47c8-a45e-40cc9c29c428",
            "compositeImage": {
                "id": "8d334e04-1601-49bd-97dd-386c9b4140cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa1ea698-4e19-4a7a-8f14-22b987535f1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cff4b45-c80e-46a3-a1cb-b2cf71c30eb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa1ea698-4e19-4a7a-8f14-22b987535f1d",
                    "LayerId": "9a892fc8-f3ca-4574-9d65-21a00bca39f9"
                }
            ]
        },
        {
            "id": "262c966c-29e0-4083-a46c-39de858cc3dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71e0e68a-0168-47c8-a45e-40cc9c29c428",
            "compositeImage": {
                "id": "0dbd095f-c483-4870-956f-402ca7e47c1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "262c966c-29e0-4083-a46c-39de858cc3dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66646eb2-5856-47b4-831a-3bef05573129",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "262c966c-29e0-4083-a46c-39de858cc3dd",
                    "LayerId": "9a892fc8-f3ca-4574-9d65-21a00bca39f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "9a892fc8-f3ca-4574-9d65-21a00bca39f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71e0e68a-0168-47c8-a45e-40cc9c29c428",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 8,
    "yorig": 12
}