{
    "id": "80595a07-06cd-4b5d-806b-ea6e687513eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_fheavy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 0,
    "bbox_right": 143,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df35db96-b0ae-403a-a3b5-f7a942a276c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "8f09260f-232f-452b-af1d-fcf9888ca3e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df35db96-b0ae-403a-a3b5-f7a942a276c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d89c62c6-0fd1-46c7-b56f-f1d647c21b8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df35db96-b0ae-403a-a3b5-f7a942a276c9",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "7481a296-3d03-4cc9-9ab0-254f335badfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "29a9c1e5-8acb-4906-b4e7-b781bc32d156",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7481a296-3d03-4cc9-9ab0-254f335badfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38153368-4131-46d8-a13c-49c2fa854eca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7481a296-3d03-4cc9-9ab0-254f335badfd",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "fd004b9d-99f3-487f-961e-f2752ca1bc79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "11386cbf-2b6d-4934-a9c1-3e7f8aea164b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd004b9d-99f3-487f-961e-f2752ca1bc79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2650183-4829-4f39-a574-d0917644e56b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd004b9d-99f3-487f-961e-f2752ca1bc79",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "81b2579e-ea76-4fbd-882c-1679f9a86334",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "5a3687c9-6bfa-4ed5-87d8-d7eb6bee689b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81b2579e-ea76-4fbd-882c-1679f9a86334",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9af9c502-1460-490a-a486-6b4684a9c9ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81b2579e-ea76-4fbd-882c-1679f9a86334",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "8312e112-3b23-44fc-a2a4-a937b2e25415",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "f1614bc7-42a0-440d-98fd-42796faedde8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8312e112-3b23-44fc-a2a4-a937b2e25415",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a80d76d8-e700-4412-ae99-b07edfadef24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8312e112-3b23-44fc-a2a4-a937b2e25415",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "ab55aa94-d45a-45f5-aaea-0aa399aef8f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "12d09bd1-1b6d-4f7c-9af9-a89afafe660c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab55aa94-d45a-45f5-aaea-0aa399aef8f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcce2ddf-7b14-45e1-baab-3f620ff79840",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab55aa94-d45a-45f5-aaea-0aa399aef8f6",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "61b6fb44-ca68-4b58-a482-a6c11ad1e621",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "0e70e74d-a542-4bce-9b29-c0e3c85d00f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61b6fb44-ca68-4b58-a482-a6c11ad1e621",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8f6ce16-09fe-4423-9a8e-2bc3a35112bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61b6fb44-ca68-4b58-a482-a6c11ad1e621",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "56c46cde-0f56-4026-9b45-a48189785f85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "1faadf96-ff81-459e-843d-b75f037d22a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56c46cde-0f56-4026-9b45-a48189785f85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11645a53-a1b2-4022-bc7b-7707f9f7bc60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c46cde-0f56-4026-9b45-a48189785f85",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "4c8af7ae-ec48-47d8-bb9c-e28f05774754",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "6d7ab257-e8f0-4528-a69d-a66381bb2458",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c8af7ae-ec48-47d8-bb9c-e28f05774754",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97408654-aa2d-4960-af68-a682fe2302b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c8af7ae-ec48-47d8-bb9c-e28f05774754",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "52d9edb2-e54f-403d-aabd-77e8ea157ee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "c390dd41-06b4-4546-bd72-e913a75268de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52d9edb2-e54f-403d-aabd-77e8ea157ee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f36e3a5e-529f-4a9d-ada2-4a9a9a0fcb28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52d9edb2-e54f-403d-aabd-77e8ea157ee2",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "881890c2-d796-4583-a71b-17f2f383f0ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "89b84bba-21b1-4fa6-9a1a-398dce98102f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "881890c2-d796-4583-a71b-17f2f383f0ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1db47f2-c383-4b08-8e56-a194e9f15028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "881890c2-d796-4583-a71b-17f2f383f0ac",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "7150f45a-f1cc-49c6-95ec-fbf42d6a80a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "6a632813-cc16-42c9-8653-a6d4250bfb8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7150f45a-f1cc-49c6-95ec-fbf42d6a80a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8e426b5-30bc-4a5d-ac7f-476c026232f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7150f45a-f1cc-49c6-95ec-fbf42d6a80a2",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "33de96b7-2b5e-4c0c-9366-6183fa4dd176",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "d2a7795d-196f-41fc-9b40-7da2f520b55c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33de96b7-2b5e-4c0c-9366-6183fa4dd176",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a67ada8-1d3b-4fe0-b29b-44cc479a041d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33de96b7-2b5e-4c0c-9366-6183fa4dd176",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "93c04753-441a-43a8-922a-167d7e9f1094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "916d70d3-07c2-4547-a57f-29c1b6f27ab1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93c04753-441a-43a8-922a-167d7e9f1094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6370efc1-7a5c-4b56-b7fa-688ee225e974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93c04753-441a-43a8-922a-167d7e9f1094",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "99097652-2527-42c4-b5a8-268713077e0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "e764478d-9335-4707-9f93-2a987d2b7758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99097652-2527-42c4-b5a8-268713077e0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "339c49ad-de0f-4aad-b2e6-46d11eddd9c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99097652-2527-42c4-b5a8-268713077e0c",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "90b8385b-099d-45aa-ba28-859c0695dda0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "2a772309-aa23-4611-b39b-c40fb156fd4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90b8385b-099d-45aa-ba28-859c0695dda0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b901fe5d-a594-4ba9-a971-7bb8640246bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90b8385b-099d-45aa-ba28-859c0695dda0",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "2844ed52-8071-4d04-b877-a9b4e8a2846c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "34276c0d-d3d3-42d7-98f9-43115f539c8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2844ed52-8071-4d04-b877-a9b4e8a2846c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "554327a1-90ff-4553-9866-c588df562851",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2844ed52-8071-4d04-b877-a9b4e8a2846c",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "5e029919-6358-4247-be0c-4e3ab4af1d3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "ba2dd35a-1fd4-4495-ace3-de5268f31468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e029919-6358-4247-be0c-4e3ab4af1d3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14066e18-47b2-4ec8-86fc-e3d9820bb018",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e029919-6358-4247-be0c-4e3ab4af1d3e",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "fb2b6317-8302-4861-a484-e2cd549ae220",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "95ec4654-df0b-4db5-b610-f1d1957abbdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb2b6317-8302-4861-a484-e2cd549ae220",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4a7ce83-b4bd-48f5-bc89-25440721b237",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb2b6317-8302-4861-a484-e2cd549ae220",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        },
        {
            "id": "b7ea872b-f8c8-4c17-bf86-907b4e93a1f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "compositeImage": {
                "id": "162bd022-1b1f-41fc-b820-65b0671de2cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7ea872b-f8c8-4c17-bf86-907b4e93a1f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3af69a42-2ee2-4620-84a3-38fa7df07470",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7ea872b-f8c8-4c17-bf86-907b4e93a1f7",
                    "LayerId": "94a137f9-34ae-43b5-bda2-176e622d2f5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 98,
    "layers": [
        {
            "id": "94a137f9-34ae-43b5-bda2-176e622d2f5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80595a07-06cd-4b5d-806b-ea6e687513eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 42,
    "yorig": 97
}