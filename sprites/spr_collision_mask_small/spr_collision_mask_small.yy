{
    "id": "646a15f2-4299-428a-afa1-97e93d0a74bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_collision_mask_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f580e7a6-98b6-4ec9-93af-46c52faf734e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "646a15f2-4299-428a-afa1-97e93d0a74bd",
            "compositeImage": {
                "id": "46975c5e-df95-4db3-8b87-f62930c2ad2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f580e7a6-98b6-4ec9-93af-46c52faf734e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8372b13b-9d1d-4057-b96b-cb362245a96c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f580e7a6-98b6-4ec9-93af-46c52faf734e",
                    "LayerId": "17cb12df-ecd3-411c-92a9-ae09339da34c"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 48,
    "layers": [
        {
            "id": "17cb12df-ecd3-411c-92a9-ae09339da34c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "646a15f2-4299-428a-afa1-97e93d0a74bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}