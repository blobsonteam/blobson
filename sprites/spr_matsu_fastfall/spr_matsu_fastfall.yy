{
    "id": "30233203-92fd-4e04-83d3-950ad234205a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_fastfall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 145,
    "bbox_left": 13,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc4bad15-4ddd-4832-b464-46020e5d7bef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30233203-92fd-4e04-83d3-950ad234205a",
            "compositeImage": {
                "id": "65fd909e-342f-40ac-8588-41c49489c80f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc4bad15-4ddd-4832-b464-46020e5d7bef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d02b4ed0-9680-4e28-853a-fd61eac3d76d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc4bad15-4ddd-4832-b464-46020e5d7bef",
                    "LayerId": "c2d13b60-1285-4cbd-af21-37bd629537e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 146,
    "layers": [
        {
            "id": "c2d13b60-1285-4cbd-af21-37bd629537e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30233203-92fd-4e04-83d3-950ad234205a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 33,
    "yorig": 145
}