{
    "id": "f82607d5-52ec-4e34-8770-ede682cf9a9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ledge_jump0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 3,
    "bbox_right": 37,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8419eefe-93eb-4d0c-82dc-4cf64fb7facc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f82607d5-52ec-4e34-8770-ede682cf9a9e",
            "compositeImage": {
                "id": "af90aec7-cb9e-4078-a021-8a13f0ef9da0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8419eefe-93eb-4d0c-82dc-4cf64fb7facc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "523a6910-885d-4474-933a-83501c24ed70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8419eefe-93eb-4d0c-82dc-4cf64fb7facc",
                    "LayerId": "c828e0dd-9e0a-48a5-aa6b-b0dca536771f"
                }
            ]
        },
        {
            "id": "ac16bb57-73c6-4993-ba7a-303d857153c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f82607d5-52ec-4e34-8770-ede682cf9a9e",
            "compositeImage": {
                "id": "8f88f69d-bee0-4289-815e-6679c29f5f29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac16bb57-73c6-4993-ba7a-303d857153c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f0fbdcc-243c-47fe-8c55-fe4de0a666f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac16bb57-73c6-4993-ba7a-303d857153c2",
                    "LayerId": "c828e0dd-9e0a-48a5-aa6b-b0dca536771f"
                }
            ]
        },
        {
            "id": "70e68998-8a6e-4538-832a-915ed1d57da3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f82607d5-52ec-4e34-8770-ede682cf9a9e",
            "compositeImage": {
                "id": "1033e8ff-b99b-49f3-a37d-3cc3069f17fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70e68998-8a6e-4538-832a-915ed1d57da3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac652cbe-ce2a-4abf-8c4f-94ed320d1d2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70e68998-8a6e-4538-832a-915ed1d57da3",
                    "LayerId": "c828e0dd-9e0a-48a5-aa6b-b0dca536771f"
                }
            ]
        },
        {
            "id": "3138a98d-b8d9-4d7c-a649-79bbdd6a57cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f82607d5-52ec-4e34-8770-ede682cf9a9e",
            "compositeImage": {
                "id": "bb61252e-7451-4313-91e5-9142432ad28e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3138a98d-b8d9-4d7c-a649-79bbdd6a57cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c36d89d7-cd2c-4e76-82f1-e20a1a06d01d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3138a98d-b8d9-4d7c-a649-79bbdd6a57cf",
                    "LayerId": "c828e0dd-9e0a-48a5-aa6b-b0dca536771f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c828e0dd-9e0a-48a5-aa6b-b0dca536771f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f82607d5-52ec-4e34-8770-ede682cf9a9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 12,
    "yorig": 48
}