{
    "id": "82bcb783-942a-47ad-96ad-44739e3c1555",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fx_poof1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 0,
    "bbox_right": 96,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb8b894f-dfc3-4316-9de6-bdd7aae9d2da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82bcb783-942a-47ad-96ad-44739e3c1555",
            "compositeImage": {
                "id": "e620dfc5-3812-4578-a604-38a41ca8fd62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb8b894f-dfc3-4316-9de6-bdd7aae9d2da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e85f062-617a-4054-8c14-fd0cf191b25b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb8b894f-dfc3-4316-9de6-bdd7aae9d2da",
                    "LayerId": "4ef73b71-9c52-48ed-b547-9155c85077c5"
                }
            ]
        },
        {
            "id": "4eb854ad-745e-434b-b06a-87d9d6c16f37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82bcb783-942a-47ad-96ad-44739e3c1555",
            "compositeImage": {
                "id": "9d77f64f-c8f7-4407-a6d4-57bb09cd4686",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eb854ad-745e-434b-b06a-87d9d6c16f37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a33ba1a-0980-4747-8e1c-cac858b0cb0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eb854ad-745e-434b-b06a-87d9d6c16f37",
                    "LayerId": "4ef73b71-9c52-48ed-b547-9155c85077c5"
                }
            ]
        },
        {
            "id": "78d3ed9a-13ed-49c9-bd53-1eb5a7378585",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82bcb783-942a-47ad-96ad-44739e3c1555",
            "compositeImage": {
                "id": "306bd72d-cb72-4de0-9bd6-9f436e123e07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78d3ed9a-13ed-49c9-bd53-1eb5a7378585",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "008aaf1c-e10b-4e3b-a5b7-4063b7499932",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78d3ed9a-13ed-49c9-bd53-1eb5a7378585",
                    "LayerId": "4ef73b71-9c52-48ed-b547-9155c85077c5"
                }
            ]
        },
        {
            "id": "3bcb4b7e-86c7-4bfa-8f3d-dea5c5b907e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82bcb783-942a-47ad-96ad-44739e3c1555",
            "compositeImage": {
                "id": "c15d7fdf-41a2-40bc-9b53-27b1d421ddf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bcb4b7e-86c7-4bfa-8f3d-dea5c5b907e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b73378f-b247-4766-a6d6-6f1163c119bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bcb4b7e-86c7-4bfa-8f3d-dea5c5b907e3",
                    "LayerId": "4ef73b71-9c52-48ed-b547-9155c85077c5"
                }
            ]
        },
        {
            "id": "e8dee1b8-a9a9-4109-970a-0bd06723a662",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82bcb783-942a-47ad-96ad-44739e3c1555",
            "compositeImage": {
                "id": "97519b63-0a48-48f5-8502-b97414aacd6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8dee1b8-a9a9-4109-970a-0bd06723a662",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81731e79-19d4-46e9-b196-cfbd06b0004d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8dee1b8-a9a9-4109-970a-0bd06723a662",
                    "LayerId": "4ef73b71-9c52-48ed-b547-9155c85077c5"
                }
            ]
        },
        {
            "id": "6eb756dd-946b-4ef1-9eae-8fd7146bf728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82bcb783-942a-47ad-96ad-44739e3c1555",
            "compositeImage": {
                "id": "9d9c411f-5aff-4936-9091-24b9d135f5e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eb756dd-946b-4ef1-9eae-8fd7146bf728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a816295-71d5-4c9e-abfd-508391bef74c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eb756dd-946b-4ef1-9eae-8fd7146bf728",
                    "LayerId": "4ef73b71-9c52-48ed-b547-9155c85077c5"
                }
            ]
        },
        {
            "id": "17293e79-c01c-44c3-b0d8-2e668e119502",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82bcb783-942a-47ad-96ad-44739e3c1555",
            "compositeImage": {
                "id": "34bb3bff-d048-4907-b693-d1c9fbb0950f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17293e79-c01c-44c3-b0d8-2e668e119502",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32cd6d56-0d5d-4b8f-8b2c-9918405f1468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17293e79-c01c-44c3-b0d8-2e668e119502",
                    "LayerId": "4ef73b71-9c52-48ed-b547-9155c85077c5"
                }
            ]
        },
        {
            "id": "e9433838-3b40-4ae6-a5a4-b6557469e9b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82bcb783-942a-47ad-96ad-44739e3c1555",
            "compositeImage": {
                "id": "2e43972c-46e8-4d26-9a81-bd9155e3daab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9433838-3b40-4ae6-a5a4-b6557469e9b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "194dbc33-a518-4d5f-8a5a-9157f9d002cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9433838-3b40-4ae6-a5a4-b6557469e9b2",
                    "LayerId": "4ef73b71-9c52-48ed-b547-9155c85077c5"
                }
            ]
        },
        {
            "id": "8c465529-1f3d-4128-9982-8ebcde3cf883",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82bcb783-942a-47ad-96ad-44739e3c1555",
            "compositeImage": {
                "id": "761f34f2-4599-479a-b041-51b6bd0411ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c465529-1f3d-4128-9982-8ebcde3cf883",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a433ffc7-60d9-4f01-8cb3-6a1782af9716",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c465529-1f3d-4128-9982-8ebcde3cf883",
                    "LayerId": "4ef73b71-9c52-48ed-b547-9155c85077c5"
                }
            ]
        },
        {
            "id": "aa159e95-154f-4cf4-9e3c-d8b821bac1d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82bcb783-942a-47ad-96ad-44739e3c1555",
            "compositeImage": {
                "id": "3e2638ee-9d1e-4567-9b89-e6127915f53c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa159e95-154f-4cf4-9e3c-d8b821bac1d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e9a9745-498f-492f-af6c-bab833dd1d2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa159e95-154f-4cf4-9e3c-d8b821bac1d1",
                    "LayerId": "4ef73b71-9c52-48ed-b547-9155c85077c5"
                }
            ]
        },
        {
            "id": "bddee8f8-a7e9-413e-9e4f-32232cd2aa1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82bcb783-942a-47ad-96ad-44739e3c1555",
            "compositeImage": {
                "id": "48c0d0d1-cc55-4df1-a768-07686d249396",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bddee8f8-a7e9-413e-9e4f-32232cd2aa1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff523d19-ede2-4d7f-a0ed-37f888a60127",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bddee8f8-a7e9-413e-9e4f-32232cd2aa1c",
                    "LayerId": "4ef73b71-9c52-48ed-b547-9155c85077c5"
                }
            ]
        },
        {
            "id": "2b126475-bedd-49d6-83a8-4e8c7917a394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82bcb783-942a-47ad-96ad-44739e3c1555",
            "compositeImage": {
                "id": "3a4005d9-f31a-4643-b709-b8fc6058fa00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b126475-bedd-49d6-83a8-4e8c7917a394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "010e78d9-1853-473c-bff6-c1f5a2b08143",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b126475-bedd-49d6-83a8-4e8c7917a394",
                    "LayerId": "4ef73b71-9c52-48ed-b547-9155c85077c5"
                }
            ]
        },
        {
            "id": "0b7e0d7c-d86a-45bc-a37b-98c79c87f78d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82bcb783-942a-47ad-96ad-44739e3c1555",
            "compositeImage": {
                "id": "fcf4c645-6c22-4c58-95cb-f577f5c7ea9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b7e0d7c-d86a-45bc-a37b-98c79c87f78d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c10bed0-2a82-4ea9-89b4-9819cc7c74e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b7e0d7c-d86a-45bc-a37b-98c79c87f78d",
                    "LayerId": "4ef73b71-9c52-48ed-b547-9155c85077c5"
                }
            ]
        },
        {
            "id": "26197dd1-b5b8-47d5-bb0e-60d874781116",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82bcb783-942a-47ad-96ad-44739e3c1555",
            "compositeImage": {
                "id": "4bcc62b4-6bbf-4986-a3a5-601af5eca0e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26197dd1-b5b8-47d5-bb0e-60d874781116",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ab6fc5a-1920-4ab2-a57a-77e5a00fd34a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26197dd1-b5b8-47d5-bb0e-60d874781116",
                    "LayerId": "4ef73b71-9c52-48ed-b547-9155c85077c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "4ef73b71-9c52-48ed-b547-9155c85077c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82bcb783-942a-47ad-96ad-44739e3c1555",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 97,
    "xorig": 48,
    "yorig": 86
}