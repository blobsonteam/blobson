{
    "id": "b886bdd7-64e5-48d5-9cae-910eb0fd9d89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite216",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19387d48-cb37-4ce4-9225-155304f71dff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b886bdd7-64e5-48d5-9cae-910eb0fd9d89",
            "compositeImage": {
                "id": "b3e3e8f0-b9e8-464a-be45-1c62fff96234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19387d48-cb37-4ce4-9225-155304f71dff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80b7c5cd-a843-486d-928a-4db188d5d38e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19387d48-cb37-4ce4-9225-155304f71dff",
                    "LayerId": "3dba60a3-860c-4682-8176-b8288c0ba1e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3dba60a3-860c-4682-8176-b8288c0ba1e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b886bdd7-64e5-48d5-9cae-910eb0fd9d89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}