{
    "id": "1eeac186-9495-47f9-86bb-434a96495b05",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ledge_snap0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 2,
    "bbox_right": 23,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba70700b-7f3b-4e4f-83fd-cbd0256213a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1eeac186-9495-47f9-86bb-434a96495b05",
            "compositeImage": {
                "id": "954dccab-80dc-440b-8c8d-4cd0c061678c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba70700b-7f3b-4e4f-83fd-cbd0256213a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77e3ab4a-4ce0-4ae4-a731-49dbea7f2210",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba70700b-7f3b-4e4f-83fd-cbd0256213a3",
                    "LayerId": "534bc4b7-37a7-4024-979a-7b92d37230bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "534bc4b7-37a7-4024-979a-7b92d37230bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1eeac186-9495-47f9-86bb-434a96495b05",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 11,
    "yorig": 17
}