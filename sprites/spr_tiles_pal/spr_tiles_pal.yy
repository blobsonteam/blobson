{
    "id": "8bf69448-a438-4656-937d-075a2b691f1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tiles_pal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 2,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e66919f-c9ee-4300-85ac-789cd19f3753",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bf69448-a438-4656-937d-075a2b691f1d",
            "compositeImage": {
                "id": "49d70665-2631-4482-823b-91982b9287d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e66919f-c9ee-4300-85ac-789cd19f3753",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b87e3f5-f723-4261-ba69-3016b618a42f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e66919f-c9ee-4300-85ac-789cd19f3753",
                    "LayerId": "d9fd568f-11e0-45e6-8269-b4128fc4f61b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d9fd568f-11e0-45e6-8269-b4128fc4f61b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8bf69448-a438-4656-937d-075a2b691f1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3,
    "xorig": 0,
    "yorig": 0
}