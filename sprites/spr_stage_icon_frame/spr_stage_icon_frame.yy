{
    "id": "ce0b1785-6f50-4056-b1b8-0cc9880dc123",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stage_icon_frame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da4d9426-abdf-4eed-aff3-448d54020985",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce0b1785-6f50-4056-b1b8-0cc9880dc123",
            "compositeImage": {
                "id": "c6e15d69-281f-4112-bbef-16b5b9427752",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da4d9426-abdf-4eed-aff3-448d54020985",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a676746-1bb6-4fb5-b6fe-fe1b6832bc13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da4d9426-abdf-4eed-aff3-448d54020985",
                    "LayerId": "6489051d-8818-401f-983b-ce22242d28ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "6489051d-8818-401f-983b-ce22242d28ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce0b1785-6f50-4056-b1b8-0cc9880dc123",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}