{
    "id": "20995b4c-c72f-4cca-816f-8371e38ea32b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_dairb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 105,
    "bbox_left": 5,
    "bbox_right": 111,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "954400f3-2a4d-4727-817b-3af11752cabf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20995b4c-c72f-4cca-816f-8371e38ea32b",
            "compositeImage": {
                "id": "76ed8d7f-3a68-43e1-b657-498259c049e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "954400f3-2a4d-4727-817b-3af11752cabf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "689b0b18-26fe-419c-9238-41971be0edac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "954400f3-2a4d-4727-817b-3af11752cabf",
                    "LayerId": "9c353bfd-98a6-43a6-861c-f2e1d61301bb"
                }
            ]
        },
        {
            "id": "92bd6867-8ee9-4ded-9f01-cbb8f4b30a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20995b4c-c72f-4cca-816f-8371e38ea32b",
            "compositeImage": {
                "id": "2cd7b3a6-cdcf-4c8a-9447-ecb9f41e6a45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92bd6867-8ee9-4ded-9f01-cbb8f4b30a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f1a25c8-fbbb-4675-a102-6cc186074c12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92bd6867-8ee9-4ded-9f01-cbb8f4b30a05",
                    "LayerId": "9c353bfd-98a6-43a6-861c-f2e1d61301bb"
                }
            ]
        },
        {
            "id": "3d97eb7c-5868-4689-a54d-850167c5f744",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20995b4c-c72f-4cca-816f-8371e38ea32b",
            "compositeImage": {
                "id": "9fb6165f-d9b6-4075-8c49-4cc843803d42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d97eb7c-5868-4689-a54d-850167c5f744",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "750d2065-f9c8-4ea3-8c46-44b469da7aeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d97eb7c-5868-4689-a54d-850167c5f744",
                    "LayerId": "9c353bfd-98a6-43a6-861c-f2e1d61301bb"
                }
            ]
        },
        {
            "id": "72675017-6eb1-439c-89ed-b7ffafe32760",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20995b4c-c72f-4cca-816f-8371e38ea32b",
            "compositeImage": {
                "id": "2e1b6e71-7592-4492-8946-5d71ee10ba99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72675017-6eb1-439c-89ed-b7ffafe32760",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c23975be-d20c-4a29-8d00-dbf417ec1935",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72675017-6eb1-439c-89ed-b7ffafe32760",
                    "LayerId": "9c353bfd-98a6-43a6-861c-f2e1d61301bb"
                }
            ]
        },
        {
            "id": "dea4c8df-f409-4a26-aed9-58c08e7c9286",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20995b4c-c72f-4cca-816f-8371e38ea32b",
            "compositeImage": {
                "id": "64b4c6c1-21ea-4b48-9ad5-a41a16e2a84a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dea4c8df-f409-4a26-aed9-58c08e7c9286",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deffe0b2-3887-4b44-8e97-ad7f7b8c2faa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dea4c8df-f409-4a26-aed9-58c08e7c9286",
                    "LayerId": "9c353bfd-98a6-43a6-861c-f2e1d61301bb"
                }
            ]
        },
        {
            "id": "33bde42a-06df-4f2d-b9d5-c066573081c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20995b4c-c72f-4cca-816f-8371e38ea32b",
            "compositeImage": {
                "id": "2a949858-8a63-42f1-b393-6f718689c88e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33bde42a-06df-4f2d-b9d5-c066573081c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "424c874e-0b08-4f75-a8b4-3f704c369996",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33bde42a-06df-4f2d-b9d5-c066573081c6",
                    "LayerId": "9c353bfd-98a6-43a6-861c-f2e1d61301bb"
                }
            ]
        },
        {
            "id": "c572d8ab-b321-47e2-866d-6f164672e5c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20995b4c-c72f-4cca-816f-8371e38ea32b",
            "compositeImage": {
                "id": "9ad6485f-548d-4895-82e1-ad493679f941",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c572d8ab-b321-47e2-866d-6f164672e5c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce543c00-a46c-4558-bd69-78ba5375d76b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c572d8ab-b321-47e2-866d-6f164672e5c2",
                    "LayerId": "9c353bfd-98a6-43a6-861c-f2e1d61301bb"
                }
            ]
        },
        {
            "id": "0e70ab95-aa8b-42ac-8888-7dc79881db9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20995b4c-c72f-4cca-816f-8371e38ea32b",
            "compositeImage": {
                "id": "17066404-18a0-4a4e-9bb0-9c968827560d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e70ab95-aa8b-42ac-8888-7dc79881db9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8394ea37-9c6c-4a27-9495-af167b91801c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e70ab95-aa8b-42ac-8888-7dc79881db9f",
                    "LayerId": "9c353bfd-98a6-43a6-861c-f2e1d61301bb"
                }
            ]
        },
        {
            "id": "39e0536c-915b-432f-8fdb-9f9eb7607759",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20995b4c-c72f-4cca-816f-8371e38ea32b",
            "compositeImage": {
                "id": "3d738bfc-ebce-4902-b817-fbbf06e6486b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39e0536c-915b-432f-8fdb-9f9eb7607759",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0faf6bab-33c6-446f-8692-39792e5156f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39e0536c-915b-432f-8fdb-9f9eb7607759",
                    "LayerId": "9c353bfd-98a6-43a6-861c-f2e1d61301bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "9c353bfd-98a6-43a6-861c-f2e1d61301bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20995b4c-c72f-4cca-816f-8371e38ea32b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 117,
    "xorig": 58,
    "yorig": 105
}