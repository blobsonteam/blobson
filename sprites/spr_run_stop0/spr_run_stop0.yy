{
    "id": "729c751a-eedd-4d96-9ec4-4cad3192fd04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_run_stop0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e66b99f-01b7-4aee-b334-1edaed57e824",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "729c751a-eedd-4d96-9ec4-4cad3192fd04",
            "compositeImage": {
                "id": "d16254cd-5e61-425a-80a7-5d27f671ea62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e66b99f-01b7-4aee-b334-1edaed57e824",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53dc50bf-71fc-4c9d-b6fd-4f44bf31dc58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e66b99f-01b7-4aee-b334-1edaed57e824",
                    "LayerId": "b39f7267-8191-4f36-8e5c-7433c90c8fd2"
                }
            ]
        },
        {
            "id": "cd8ce2aa-b58b-40c7-900c-b001531b34f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "729c751a-eedd-4d96-9ec4-4cad3192fd04",
            "compositeImage": {
                "id": "88ba422f-f471-44de-95e1-099412fa68fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd8ce2aa-b58b-40c7-900c-b001531b34f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d30d6749-0df7-431b-a612-fa7c9edd82b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd8ce2aa-b58b-40c7-900c-b001531b34f8",
                    "LayerId": "b39f7267-8191-4f36-8e5c-7433c90c8fd2"
                }
            ]
        },
        {
            "id": "d46ebeb4-71db-4c2f-844e-8cd6b1c404d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "729c751a-eedd-4d96-9ec4-4cad3192fd04",
            "compositeImage": {
                "id": "048a8a9f-e5be-4eb7-a75f-ad3c19455bff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d46ebeb4-71db-4c2f-844e-8cd6b1c404d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "649c7501-a8d3-4705-b863-9e446241cdb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d46ebeb4-71db-4c2f-844e-8cd6b1c404d5",
                    "LayerId": "b39f7267-8191-4f36-8e5c-7433c90c8fd2"
                }
            ]
        },
        {
            "id": "c0ca2b07-46db-4d4b-98ac-a1b692fbb7c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "729c751a-eedd-4d96-9ec4-4cad3192fd04",
            "compositeImage": {
                "id": "ed4b32c7-4486-48a5-bb67-6d49a50a36a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0ca2b07-46db-4d4b-98ac-a1b692fbb7c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc02ba93-22a5-47cc-9ade-3bf088ec6f55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0ca2b07-46db-4d4b-98ac-a1b692fbb7c6",
                    "LayerId": "b39f7267-8191-4f36-8e5c-7433c90c8fd2"
                }
            ]
        },
        {
            "id": "068ba8e6-3647-4b3b-b0be-ca735be1633c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "729c751a-eedd-4d96-9ec4-4cad3192fd04",
            "compositeImage": {
                "id": "632c45a8-6cde-4f13-8bf0-942a0e38045b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "068ba8e6-3647-4b3b-b0be-ca735be1633c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a142221-7287-4945-aaf0-cf62d71e6425",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "068ba8e6-3647-4b3b-b0be-ca735be1633c",
                    "LayerId": "b39f7267-8191-4f36-8e5c-7433c90c8fd2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "b39f7267-8191-4f36-8e5c-7433c90c8fd2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "729c751a-eedd-4d96-9ec4-4cad3192fd04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}