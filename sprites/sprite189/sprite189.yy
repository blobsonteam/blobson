{
    "id": "6690a3a2-d12e-474b-b597-4c340af1064a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite189",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89aa2b97-1b2b-4d6a-b254-be987fc7503a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6690a3a2-d12e-474b-b597-4c340af1064a",
            "compositeImage": {
                "id": "b1391acd-8ba1-4dc6-9a5e-4dd41cef7253",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89aa2b97-1b2b-4d6a-b254-be987fc7503a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92ceb885-29ba-4fa5-828c-964052759f4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89aa2b97-1b2b-4d6a-b254-be987fc7503a",
                    "LayerId": "795443e9-1f79-463e-8644-7ee9ed79163d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "795443e9-1f79-463e-8644-7ee9ed79163d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6690a3a2-d12e-474b-b597-4c340af1064a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}