{
    "id": "c702020a-ebd7-4252-a0ea-45311eb7226f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_grabbed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f35d63e-e1c3-4896-a17e-859979c5a4da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c702020a-ebd7-4252-a0ea-45311eb7226f",
            "compositeImage": {
                "id": "4c57f956-4a82-4f39-a487-8286118fb683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f35d63e-e1c3-4896-a17e-859979c5a4da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca786a43-bdbc-4e49-a171-49a0f3e6d91e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f35d63e-e1c3-4896-a17e-859979c5a4da",
                    "LayerId": "e4d5bb9c-ba1e-47f7-a98e-c4e698e50281"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "e4d5bb9c-ba1e-47f7-a98e-c4e698e50281",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c702020a-ebd7-4252-a0ea-45311eb7226f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 26,
    "yorig": 84
}