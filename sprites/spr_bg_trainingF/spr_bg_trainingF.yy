{
    "id": "a4c18f89-ba92-477c-aef9-b67de410f18a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_trainingF",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1099,
    "bbox_left": 0,
    "bbox_right": 2047,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e424798-2adc-4ddc-a4ed-6a2d27fa1a33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4c18f89-ba92-477c-aef9-b67de410f18a",
            "compositeImage": {
                "id": "88b71783-eb3b-42e1-adfc-7a0942370df2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e424798-2adc-4ddc-a4ed-6a2d27fa1a33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff6e8083-ad5b-420c-9288-3c5a1c9f3504",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e424798-2adc-4ddc-a4ed-6a2d27fa1a33",
                    "LayerId": "4fdacdbd-4cb9-43c4-8ba5-81fcf3d2862b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1100,
    "layers": [
        {
            "id": "4fdacdbd-4cb9-43c4-8ba5-81fcf3d2862b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4c18f89-ba92-477c-aef9-b67de410f18a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2048,
    "xorig": 1024,
    "yorig": 550
}