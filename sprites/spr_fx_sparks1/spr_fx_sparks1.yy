{
    "id": "3410a82a-d82a-4510-b0f4-512fe701016e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fx_sparks1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 463,
    "bbox_left": 0,
    "bbox_right": 424,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eba47867-13be-4a55-a0b1-64e2c2761f8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "d8faa530-f745-4412-9244-50b73b35bfe0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eba47867-13be-4a55-a0b1-64e2c2761f8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbd4fd90-3405-49c5-b414-d55f333253f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eba47867-13be-4a55-a0b1-64e2c2761f8d",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "31423676-3fbd-4333-a0a4-12502e804fd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "6fb179fc-ee4a-40dc-add3-1b47c48ec23b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31423676-3fbd-4333-a0a4-12502e804fd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dc4720b-4854-418c-bf21-646d917649a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31423676-3fbd-4333-a0a4-12502e804fd6",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "36644c56-47b9-4c63-8fed-011ad0be7802",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "e70c48fa-a481-4a95-986c-ce808a6e28e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36644c56-47b9-4c63-8fed-011ad0be7802",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70dfd7e2-3789-4a93-b54c-5371f9633995",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36644c56-47b9-4c63-8fed-011ad0be7802",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "9eaf50a0-7ee2-4974-aaa7-4ac4861bc599",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "57ff85b3-3499-44dc-899c-67ed4627a09c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eaf50a0-7ee2-4974-aaa7-4ac4861bc599",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54a18720-8ba0-4efe-ab0d-7e5c6fb6c18c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eaf50a0-7ee2-4974-aaa7-4ac4861bc599",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "9dbe47c1-22bd-4da9-a9c6-9a8ff925f32b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "4854292e-8327-475a-945c-47792e93393b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dbe47c1-22bd-4da9-a9c6-9a8ff925f32b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2caf2068-2326-482c-b745-2d16b6dc0934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dbe47c1-22bd-4da9-a9c6-9a8ff925f32b",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "47f5b776-52dd-46ca-8922-e7c7c87af098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "e7dd1250-7db5-45e3-b3ad-6f28b776460f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47f5b776-52dd-46ca-8922-e7c7c87af098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d744d3e8-b2c9-40d0-ac01-9163a48946ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47f5b776-52dd-46ca-8922-e7c7c87af098",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "5c54d03a-53db-4de4-b410-9a498b5e98cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "ed964425-a17f-4be0-b09f-3cf7268e761d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c54d03a-53db-4de4-b410-9a498b5e98cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11f34559-5f4d-4174-a5f7-b0aa3e1e648f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c54d03a-53db-4de4-b410-9a498b5e98cd",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "daf90761-0e10-4ef4-a86e-78911f461297",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "2b624c2d-29d7-4b5e-b5cc-f7fa7bc7c230",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daf90761-0e10-4ef4-a86e-78911f461297",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65de3e86-4844-496a-8c08-96392c583f45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daf90761-0e10-4ef4-a86e-78911f461297",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "e1b64823-3e04-410d-a309-90e7c431e455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "92f43b9d-fe18-41c0-b827-8ddb0407fb94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1b64823-3e04-410d-a309-90e7c431e455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6235935-f9b5-4c67-8722-2501ec049761",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1b64823-3e04-410d-a309-90e7c431e455",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "bd791fa1-aa31-40b2-8b35-18497e22a436",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "df315460-e6a2-4d91-9001-97eee740e20a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd791fa1-aa31-40b2-8b35-18497e22a436",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cdc309f-915c-4022-8d4d-c71a4ff9a8a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd791fa1-aa31-40b2-8b35-18497e22a436",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "c259b011-edfb-43b9-8423-3621d0b025c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "d0022640-3494-4a27-98e2-ad146ee9859c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c259b011-edfb-43b9-8423-3621d0b025c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18ea70b4-48a6-4bd7-aba0-c294604dc34f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c259b011-edfb-43b9-8423-3621d0b025c9",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "ed5a982c-db62-46de-9679-ffc145b2bc0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "d99d95a6-ddf5-48ad-9157-d4dceb42d0c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed5a982c-db62-46de-9679-ffc145b2bc0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00a8a962-45ed-40a4-8202-9d334251fb7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed5a982c-db62-46de-9679-ffc145b2bc0d",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "f39791c9-f5e9-47a7-a28e-4f51548dfbf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "e3f2234e-7adb-4b67-a9de-3722754e116f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f39791c9-f5e9-47a7-a28e-4f51548dfbf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0de6249a-6510-4830-a5e1-e1a1dd8ccb3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f39791c9-f5e9-47a7-a28e-4f51548dfbf8",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "cdbd05e6-13fc-4a06-b6a6-9b82a85fc943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "7a8f2c22-5aa0-4f9c-ab3e-112f384e2209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdbd05e6-13fc-4a06-b6a6-9b82a85fc943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c75b618f-0323-4600-b3bb-6924cf5d1d23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdbd05e6-13fc-4a06-b6a6-9b82a85fc943",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "402a410f-d213-4148-9775-e30084ed5be5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "3d294a12-74f9-4514-a944-f53ce1d6876f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "402a410f-d213-4148-9775-e30084ed5be5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd84b238-cee5-45ce-8f39-7913da174231",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "402a410f-d213-4148-9775-e30084ed5be5",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "0aa2c289-0f81-45fa-a07c-47db87485911",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "7907f057-609c-42b0-8749-8c17deefb7ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0aa2c289-0f81-45fa-a07c-47db87485911",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d86fb3b0-fc48-491d-af63-2f79994c22a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0aa2c289-0f81-45fa-a07c-47db87485911",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "077d284c-b7e2-48d9-868b-2a7e96bd08da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "c74c245c-e5d7-42d4-a2c0-d2bedca1f26c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "077d284c-b7e2-48d9-868b-2a7e96bd08da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce952f1d-61be-4336-9b97-737306fc46e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "077d284c-b7e2-48d9-868b-2a7e96bd08da",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "657e7860-cf81-4c7f-b13b-ab60027f6563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "495201d7-5bac-4860-a2ca-37c349019921",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "657e7860-cf81-4c7f-b13b-ab60027f6563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7d53502-8265-49ef-9278-deb9eac223e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "657e7860-cf81-4c7f-b13b-ab60027f6563",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "75a423b1-d44b-41a0-8378-f3dec37eb54f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "1de0602b-a8c5-4998-94fa-dffb44bec733",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75a423b1-d44b-41a0-8378-f3dec37eb54f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06b10bae-b39d-4d68-a8d0-f6b64bb0877d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75a423b1-d44b-41a0-8378-f3dec37eb54f",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "3bcba6f6-954e-46ed-a049-fe513b6b396d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "94092f73-f8ab-419d-82fa-7d3d00856601",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bcba6f6-954e-46ed-a049-fe513b6b396d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10ef2e53-5e59-40b7-89bb-1f5617f454df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bcba6f6-954e-46ed-a049-fe513b6b396d",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "d84f558f-a353-4985-ac00-e12e36a0b09d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "332448fd-2761-4215-ac07-d32012d17594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d84f558f-a353-4985-ac00-e12e36a0b09d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a59da67a-fe48-4b4a-92ba-48999ad69cf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d84f558f-a353-4985-ac00-e12e36a0b09d",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "617200d4-1bab-4615-a555-9ae3f3743ee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "fc73d05c-69cc-4aa0-be30-461d5b0215ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "617200d4-1bab-4615-a555-9ae3f3743ee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02bc25f3-a950-45d0-8c57-43dfb37abdd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "617200d4-1bab-4615-a555-9ae3f3743ee3",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "b4060700-8394-4643-90fe-5ec8bce678ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "c1d8802a-458a-413f-9c2f-577a5fd3496a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4060700-8394-4643-90fe-5ec8bce678ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c53d0fcf-f00b-4302-ab30-d4f4e98d0552",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4060700-8394-4643-90fe-5ec8bce678ca",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "508a6618-df5f-448e-8371-4e0825d33a17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "0761cf71-7b34-436e-bf48-92f16fd98301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "508a6618-df5f-448e-8371-4e0825d33a17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17fcbfe7-25dc-4345-8029-f21f54d11f3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "508a6618-df5f-448e-8371-4e0825d33a17",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        },
        {
            "id": "382ad504-6927-4995-8fd0-eb34b2a3fe4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "compositeImage": {
                "id": "ac2feb2c-f0bf-409b-819c-c5ff8e6c6101",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "382ad504-6927-4995-8fd0-eb34b2a3fe4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae778ec8-1510-4282-8542-84d3ad05c8e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "382ad504-6927-4995-8fd0-eb34b2a3fe4c",
                    "LayerId": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 464,
    "layers": [
        {
            "id": "c60fc2f5-8916-46fa-b0e3-c69bf48b3ab3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3410a82a-d82a-4510-b0f4-512fe701016e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 425,
    "xorig": 212,
    "yorig": 242
}