{
    "id": "105b64f1-522c-4c83-bdeb-ff92c243e993",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_doublejump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 145,
    "bbox_left": 0,
    "bbox_right": 85,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c0c40cc-3377-494c-a6c3-8b52701e02be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b64f1-522c-4c83-bdeb-ff92c243e993",
            "compositeImage": {
                "id": "2a9de1cd-55b1-4236-84a7-e76d226be6da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c0c40cc-3377-494c-a6c3-8b52701e02be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69febf50-e154-4a8f-bd79-3267dfbde0d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c0c40cc-3377-494c-a6c3-8b52701e02be",
                    "LayerId": "3a5902f6-29f4-4f90-9674-39294450767a"
                }
            ]
        },
        {
            "id": "4ea3461b-368a-4741-b2d2-37abdc35b3e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b64f1-522c-4c83-bdeb-ff92c243e993",
            "compositeImage": {
                "id": "81d26ac8-8c6b-49d6-aadd-2b17e72600b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ea3461b-368a-4741-b2d2-37abdc35b3e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58373bff-012b-462f-a6cd-e2bcee231657",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ea3461b-368a-4741-b2d2-37abdc35b3e3",
                    "LayerId": "3a5902f6-29f4-4f90-9674-39294450767a"
                }
            ]
        },
        {
            "id": "d8348d07-24ac-4243-9c49-c38b57db2214",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b64f1-522c-4c83-bdeb-ff92c243e993",
            "compositeImage": {
                "id": "8902a4cf-394d-4d85-809b-e1f8fc2a200b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8348d07-24ac-4243-9c49-c38b57db2214",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b63eb78-6b30-413d-ae51-eb2a14ebd1e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8348d07-24ac-4243-9c49-c38b57db2214",
                    "LayerId": "3a5902f6-29f4-4f90-9674-39294450767a"
                }
            ]
        },
        {
            "id": "e680a7ff-4172-459b-a603-cc3bcc9f7af2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b64f1-522c-4c83-bdeb-ff92c243e993",
            "compositeImage": {
                "id": "116399ae-dec3-483b-b2ea-72c0caf8bac1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e680a7ff-4172-459b-a603-cc3bcc9f7af2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc6ab72b-1599-4c41-a515-5edf761e39ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e680a7ff-4172-459b-a603-cc3bcc9f7af2",
                    "LayerId": "3a5902f6-29f4-4f90-9674-39294450767a"
                }
            ]
        },
        {
            "id": "b0ded8b1-a8e0-4677-a812-c44824c836e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b64f1-522c-4c83-bdeb-ff92c243e993",
            "compositeImage": {
                "id": "9bad4d73-f1ab-496b-bb80-6cbcf513f078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0ded8b1-a8e0-4677-a812-c44824c836e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9464b40f-bbf4-4e3e-b969-2e61503447cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0ded8b1-a8e0-4677-a812-c44824c836e9",
                    "LayerId": "3a5902f6-29f4-4f90-9674-39294450767a"
                }
            ]
        },
        {
            "id": "48f5791e-12e4-49fd-bd13-40fc07be2ca4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b64f1-522c-4c83-bdeb-ff92c243e993",
            "compositeImage": {
                "id": "fff8c2be-2f5a-4faf-b123-1c01652c23c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48f5791e-12e4-49fd-bd13-40fc07be2ca4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31b8463a-9a17-44d6-aeba-882eb45fca1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48f5791e-12e4-49fd-bd13-40fc07be2ca4",
                    "LayerId": "3a5902f6-29f4-4f90-9674-39294450767a"
                }
            ]
        },
        {
            "id": "f57701a9-90a9-4f05-b575-99c17f66f2ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b64f1-522c-4c83-bdeb-ff92c243e993",
            "compositeImage": {
                "id": "945967a8-45b3-48b0-a30b-3363e752608b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f57701a9-90a9-4f05-b575-99c17f66f2ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03531f0a-a350-48d7-aa52-a7578fa9c95e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f57701a9-90a9-4f05-b575-99c17f66f2ab",
                    "LayerId": "3a5902f6-29f4-4f90-9674-39294450767a"
                }
            ]
        },
        {
            "id": "e2fd9eec-497b-4a31-ade8-d899d70e970c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b64f1-522c-4c83-bdeb-ff92c243e993",
            "compositeImage": {
                "id": "bb07045f-02dd-4b4d-952e-161d9a9a29a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2fd9eec-497b-4a31-ade8-d899d70e970c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9d40116-f530-41b7-9739-ad4df8d9bd4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2fd9eec-497b-4a31-ade8-d899d70e970c",
                    "LayerId": "3a5902f6-29f4-4f90-9674-39294450767a"
                }
            ]
        },
        {
            "id": "567cc82c-a21d-42d7-9951-a175853b21e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b64f1-522c-4c83-bdeb-ff92c243e993",
            "compositeImage": {
                "id": "542bf969-453f-4aa5-b0f2-d01a3475b702",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "567cc82c-a21d-42d7-9951-a175853b21e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "701e5f37-16f6-4fa0-94a5-6a6c54453079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "567cc82c-a21d-42d7-9951-a175853b21e5",
                    "LayerId": "3a5902f6-29f4-4f90-9674-39294450767a"
                }
            ]
        },
        {
            "id": "8d805129-a82a-47d3-a4a0-5ebf74f41606",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b64f1-522c-4c83-bdeb-ff92c243e993",
            "compositeImage": {
                "id": "5b9938ca-9d3c-4b69-ab5c-4d9835e39362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d805129-a82a-47d3-a4a0-5ebf74f41606",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "854a6cff-eacc-4fc2-b86a-e19800d5d316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d805129-a82a-47d3-a4a0-5ebf74f41606",
                    "LayerId": "3a5902f6-29f4-4f90-9674-39294450767a"
                }
            ]
        },
        {
            "id": "12125beb-de7f-49a0-b7d6-c83784d7c47c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b64f1-522c-4c83-bdeb-ff92c243e993",
            "compositeImage": {
                "id": "9399163e-20ae-4cf9-8a44-b7eb017e3c9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12125beb-de7f-49a0-b7d6-c83784d7c47c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b12b68ac-221f-4b7d-bd83-6bda8cdb3962",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12125beb-de7f-49a0-b7d6-c83784d7c47c",
                    "LayerId": "3a5902f6-29f4-4f90-9674-39294450767a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 146,
    "layers": [
        {
            "id": "3a5902f6-29f4-4f90-9674-39294450767a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "105b64f1-522c-4c83-bdeb-ff92c243e993",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 86,
    "xorig": 43,
    "yorig": 145
}