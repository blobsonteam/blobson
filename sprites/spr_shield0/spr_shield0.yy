{
    "id": "69028719-3878-4351-ac9d-cad31d0ef824",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shield0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23d7699a-425c-4bec-b7d9-d31e00f03cf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69028719-3878-4351-ac9d-cad31d0ef824",
            "compositeImage": {
                "id": "fc0fdd81-a504-44b1-ba12-352a24d0aa48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23d7699a-425c-4bec-b7d9-d31e00f03cf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65d6aa35-680e-4187-be8f-67c668408749",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23d7699a-425c-4bec-b7d9-d31e00f03cf9",
                    "LayerId": "0793d56f-cb10-4014-9aab-8f62c6e44583"
                }
            ]
        },
        {
            "id": "4a8f2bb4-ceae-4f99-9faf-e78fb1665d7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69028719-3878-4351-ac9d-cad31d0ef824",
            "compositeImage": {
                "id": "8510c23b-0c0d-4335-9963-cbbf982d6978",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a8f2bb4-ceae-4f99-9faf-e78fb1665d7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e321dd86-28ca-43d9-ae09-ec9189e2b6e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a8f2bb4-ceae-4f99-9faf-e78fb1665d7c",
                    "LayerId": "0793d56f-cb10-4014-9aab-8f62c6e44583"
                }
            ]
        },
        {
            "id": "2ca96935-7502-4ce8-91ff-6016af9e7d49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69028719-3878-4351-ac9d-cad31d0ef824",
            "compositeImage": {
                "id": "49e40f80-f904-4fca-90d0-8569f59bf46c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ca96935-7502-4ce8-91ff-6016af9e7d49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "890d2ab4-5c3a-465f-994a-f97c02db3e66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ca96935-7502-4ce8-91ff-6016af9e7d49",
                    "LayerId": "0793d56f-cb10-4014-9aab-8f62c6e44583"
                }
            ]
        },
        {
            "id": "c0ed35dc-8146-4572-939a-e7abad4bcdaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69028719-3878-4351-ac9d-cad31d0ef824",
            "compositeImage": {
                "id": "618a29fe-54b5-4dc6-b58f-4787fca6d8b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0ed35dc-8146-4572-939a-e7abad4bcdaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a60584aa-01bc-4ccd-8c54-a77fc3325631",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0ed35dc-8146-4572-939a-e7abad4bcdaf",
                    "LayerId": "0793d56f-cb10-4014-9aab-8f62c6e44583"
                }
            ]
        },
        {
            "id": "de064b7a-0259-4181-a320-ad86284d1eeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69028719-3878-4351-ac9d-cad31d0ef824",
            "compositeImage": {
                "id": "2d61fb34-5925-4804-9cb2-e6f07b0fbdef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de064b7a-0259-4181-a320-ad86284d1eeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61012426-06b9-4e53-a703-63f67cdbbc62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de064b7a-0259-4181-a320-ad86284d1eeb",
                    "LayerId": "0793d56f-cb10-4014-9aab-8f62c6e44583"
                }
            ]
        },
        {
            "id": "6c822609-25cf-4c3b-9202-076f7620ddcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69028719-3878-4351-ac9d-cad31d0ef824",
            "compositeImage": {
                "id": "54413e4e-621e-4dec-9c12-edbe3fc9e674",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c822609-25cf-4c3b-9202-076f7620ddcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05467735-544a-4762-9abe-73dc49888795",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c822609-25cf-4c3b-9202-076f7620ddcc",
                    "LayerId": "0793d56f-cb10-4014-9aab-8f62c6e44583"
                }
            ]
        },
        {
            "id": "3e2447c2-f391-4381-ad80-bfa4225e03cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69028719-3878-4351-ac9d-cad31d0ef824",
            "compositeImage": {
                "id": "4a7668de-7235-4cf7-ba85-b26a8464491a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e2447c2-f391-4381-ad80-bfa4225e03cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b655349-9147-49da-aa5c-0db87d86cba8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e2447c2-f391-4381-ad80-bfa4225e03cb",
                    "LayerId": "0793d56f-cb10-4014-9aab-8f62c6e44583"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "0793d56f-cb10-4014-9aab-8f62c6e44583",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69028719-3878-4351-ac9d-cad31d0ef824",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}