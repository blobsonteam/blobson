{
    "id": "73283070-2f2a-4581-ab38-9c6130df0355",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_runstop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 9,
    "bbox_right": 69,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b68524d3-8975-48ff-afe7-566f3fd8fcd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73283070-2f2a-4581-ab38-9c6130df0355",
            "compositeImage": {
                "id": "029d15f3-fe91-4627-b666-dee9973c7790",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b68524d3-8975-48ff-afe7-566f3fd8fcd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51f39e47-a785-4395-bc94-ec46be6f8ea7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b68524d3-8975-48ff-afe7-566f3fd8fcd4",
                    "LayerId": "45e43259-de83-47ae-ad0a-7be800ccc31b"
                }
            ]
        },
        {
            "id": "19597a39-44b8-47a2-a53f-1e672d9f3735",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73283070-2f2a-4581-ab38-9c6130df0355",
            "compositeImage": {
                "id": "956f4b18-4462-4420-beb3-8700ccf8a4c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19597a39-44b8-47a2-a53f-1e672d9f3735",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "817071d0-e1c0-4d55-8939-9fc204079955",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19597a39-44b8-47a2-a53f-1e672d9f3735",
                    "LayerId": "45e43259-de83-47ae-ad0a-7be800ccc31b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "45e43259-de83-47ae-ad0a-7be800ccc31b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73283070-2f2a-4581-ab38-9c6130df0355",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 34,
    "yorig": 95
}