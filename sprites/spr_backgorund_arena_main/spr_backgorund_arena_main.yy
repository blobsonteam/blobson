{
    "id": "5c6f9610-0862-4a25-b078-a52a13445724",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backgorund_arena_main",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 886,
    "bbox_left": 0,
    "bbox_right": 1727,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "101e0e5a-499d-4711-9954-8aaf7991b48a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c6f9610-0862-4a25-b078-a52a13445724",
            "compositeImage": {
                "id": "a231dca5-e5e8-4e2b-b057-260b342d461e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "101e0e5a-499d-4711-9954-8aaf7991b48a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a9d4998-30be-4276-8602-7f69538f3c19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "101e0e5a-499d-4711-9954-8aaf7991b48a",
                    "LayerId": "687d71e5-b979-4aeb-86bc-604a39e11d35"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 887,
    "layers": [
        {
            "id": "687d71e5-b979-4aeb-86bc-604a39e11d35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c6f9610-0862-4a25-b078-a52a13445724",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1728,
    "xorig": 864,
    "yorig": 443
}