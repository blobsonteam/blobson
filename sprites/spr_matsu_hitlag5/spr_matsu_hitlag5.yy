{
    "id": "27a1c34e-5ede-4fd3-b7c7-66a22bf7567d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_hitlag5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 98,
    "bbox_left": 0,
    "bbox_right": 65,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "978429cf-f03f-4271-873d-be55afb423bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a1c34e-5ede-4fd3-b7c7-66a22bf7567d",
            "compositeImage": {
                "id": "2c0420d7-b0f0-4808-aad1-9546c53fcf36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "978429cf-f03f-4271-873d-be55afb423bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f68c679a-8b40-493f-8801-18c89d6f22bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "978429cf-f03f-4271-873d-be55afb423bf",
                    "LayerId": "2f6aff4a-62b2-4de7-894e-9e138f088455"
                }
            ]
        },
        {
            "id": "84cdc6d6-d32a-4131-bf89-863b88204ad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a1c34e-5ede-4fd3-b7c7-66a22bf7567d",
            "compositeImage": {
                "id": "d4164e9d-e178-4449-bdc2-13bee325d3c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84cdc6d6-d32a-4131-bf89-863b88204ad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc4502ed-a4f1-425c-9125-dec815a8b9f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84cdc6d6-d32a-4131-bf89-863b88204ad9",
                    "LayerId": "2f6aff4a-62b2-4de7-894e-9e138f088455"
                }
            ]
        },
        {
            "id": "b512c57f-4bc8-425e-8e15-9171b0447cd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a1c34e-5ede-4fd3-b7c7-66a22bf7567d",
            "compositeImage": {
                "id": "bfd85683-f9fb-486a-a533-9f932c36c17f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b512c57f-4bc8-425e-8e15-9171b0447cd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ba75d18-5dae-41de-b843-0c5c5da7325e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b512c57f-4bc8-425e-8e15-9171b0447cd0",
                    "LayerId": "2f6aff4a-62b2-4de7-894e-9e138f088455"
                }
            ]
        },
        {
            "id": "9cf37b70-3dbe-47e8-85ee-2072df9dc9dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a1c34e-5ede-4fd3-b7c7-66a22bf7567d",
            "compositeImage": {
                "id": "0b35e869-d537-4527-8b6e-580ae54f460c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cf37b70-3dbe-47e8-85ee-2072df9dc9dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bd2936e-4f74-4626-9980-ee1a9c3e9fd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cf37b70-3dbe-47e8-85ee-2072df9dc9dd",
                    "LayerId": "2f6aff4a-62b2-4de7-894e-9e138f088455"
                }
            ]
        },
        {
            "id": "1ebb558a-9248-4b81-900c-c0393301aa39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a1c34e-5ede-4fd3-b7c7-66a22bf7567d",
            "compositeImage": {
                "id": "62d0f25c-a547-4453-8405-472e60fd1aac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ebb558a-9248-4b81-900c-c0393301aa39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2202fd6-57b2-4ae3-9007-08b4ba4249d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ebb558a-9248-4b81-900c-c0393301aa39",
                    "LayerId": "2f6aff4a-62b2-4de7-894e-9e138f088455"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 99,
    "layers": [
        {
            "id": "2f6aff4a-62b2-4de7-894e-9e138f088455",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27a1c34e-5ede-4fd3-b7c7-66a22bf7567d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 29,
    "yorig": 98
}