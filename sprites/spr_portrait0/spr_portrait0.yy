{
    "id": "0480a8aa-832d-4d1c-913c-4b4a98f1f9dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portrait0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a594536-3263-415b-bf4e-f0b53b97cc93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0480a8aa-832d-4d1c-913c-4b4a98f1f9dd",
            "compositeImage": {
                "id": "4ec73228-692d-4b16-8156-287bee739f73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a594536-3263-415b-bf4e-f0b53b97cc93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a24b8851-ed12-48ed-bc2b-0c204b912d64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a594536-3263-415b-bf4e-f0b53b97cc93",
                    "LayerId": "953a87b8-bf75-47f7-8bd1-f1bf2ead54c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "953a87b8-bf75-47f7-8bd1-f1bf2ead54c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0480a8aa-832d-4d1c-913c-4b4a98f1f9dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}