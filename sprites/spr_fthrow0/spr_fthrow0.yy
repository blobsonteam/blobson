{
    "id": "fc8ae0b5-079c-4f58-8d6f-c341a3c33d51",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fthrow0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c84cdad-2ecf-4f07-bde5-59f5666fc240",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc8ae0b5-079c-4f58-8d6f-c341a3c33d51",
            "compositeImage": {
                "id": "cc502bad-cda9-473d-a1f8-749234638c56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c84cdad-2ecf-4f07-bde5-59f5666fc240",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa62bc76-e89c-47c3-ab8d-dbd686b53d12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c84cdad-2ecf-4f07-bde5-59f5666fc240",
                    "LayerId": "07168cd2-7877-4584-a526-9c9dc69878d5"
                },
                {
                    "id": "8dba2166-e33f-4e00-9176-27681fff097e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c84cdad-2ecf-4f07-bde5-59f5666fc240",
                    "LayerId": "805a5f22-7a74-460b-896b-e2bfd2044640"
                }
            ]
        },
        {
            "id": "ecfd0bdf-e2e3-4e8d-966c-3157eec55c1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc8ae0b5-079c-4f58-8d6f-c341a3c33d51",
            "compositeImage": {
                "id": "3e168830-3016-450a-921f-a8a0a8039f5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecfd0bdf-e2e3-4e8d-966c-3157eec55c1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc0c925b-99fc-43ea-b4fd-3b7b2f18d622",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecfd0bdf-e2e3-4e8d-966c-3157eec55c1c",
                    "LayerId": "07168cd2-7877-4584-a526-9c9dc69878d5"
                },
                {
                    "id": "ed00ea9a-0507-4f8c-9eb4-1ea8fa4512b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecfd0bdf-e2e3-4e8d-966c-3157eec55c1c",
                    "LayerId": "805a5f22-7a74-460b-896b-e2bfd2044640"
                }
            ]
        },
        {
            "id": "cde6474f-db7a-4767-a322-3977690fbb86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc8ae0b5-079c-4f58-8d6f-c341a3c33d51",
            "compositeImage": {
                "id": "eeb48960-6726-4d8d-8f83-641678dd3026",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cde6474f-db7a-4767-a322-3977690fbb86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f3bfc97-d104-4f4d-9fd4-b49ddfc86fc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cde6474f-db7a-4767-a322-3977690fbb86",
                    "LayerId": "07168cd2-7877-4584-a526-9c9dc69878d5"
                },
                {
                    "id": "f8020b07-6415-4db4-b666-6a6773e19280",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cde6474f-db7a-4767-a322-3977690fbb86",
                    "LayerId": "805a5f22-7a74-460b-896b-e2bfd2044640"
                }
            ]
        },
        {
            "id": "9f504b11-83f8-4930-826a-62f4c066c945",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc8ae0b5-079c-4f58-8d6f-c341a3c33d51",
            "compositeImage": {
                "id": "bc64fcd5-a59a-49b0-90fa-2c2333de9027",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f504b11-83f8-4930-826a-62f4c066c945",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4eb479e-0146-42cf-8b30-df51b2da36e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f504b11-83f8-4930-826a-62f4c066c945",
                    "LayerId": "07168cd2-7877-4584-a526-9c9dc69878d5"
                },
                {
                    "id": "f48c18ab-6e20-402d-9d0b-f94bce0447f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f504b11-83f8-4930-826a-62f4c066c945",
                    "LayerId": "805a5f22-7a74-460b-896b-e2bfd2044640"
                }
            ]
        },
        {
            "id": "652e543c-0aaa-4b49-9d8e-85f8552c86f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc8ae0b5-079c-4f58-8d6f-c341a3c33d51",
            "compositeImage": {
                "id": "b91be3ca-5ca3-4258-a4a2-cc3f37797504",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "652e543c-0aaa-4b49-9d8e-85f8552c86f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ff90d1e-5c74-4311-8bb9-4b0eeccad717",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "652e543c-0aaa-4b49-9d8e-85f8552c86f4",
                    "LayerId": "07168cd2-7877-4584-a526-9c9dc69878d5"
                },
                {
                    "id": "890c0555-9f30-4b3d-a8c5-27d450c135ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "652e543c-0aaa-4b49-9d8e-85f8552c86f4",
                    "LayerId": "805a5f22-7a74-460b-896b-e2bfd2044640"
                }
            ]
        },
        {
            "id": "9aae5f08-090e-46e1-94a2-dd39c5c11e27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc8ae0b5-079c-4f58-8d6f-c341a3c33d51",
            "compositeImage": {
                "id": "b4c7ae59-a5fb-4b67-b1f6-b3f8d7121e76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aae5f08-090e-46e1-94a2-dd39c5c11e27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "686465d6-37f9-4ca7-96f7-2ec9390fbf9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aae5f08-090e-46e1-94a2-dd39c5c11e27",
                    "LayerId": "07168cd2-7877-4584-a526-9c9dc69878d5"
                },
                {
                    "id": "ab64a24a-97e2-42c8-b6f0-7d3805ca8faf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aae5f08-090e-46e1-94a2-dd39c5c11e27",
                    "LayerId": "805a5f22-7a74-460b-896b-e2bfd2044640"
                }
            ]
        },
        {
            "id": "faf1375b-70d9-46b1-946f-022b513ef394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc8ae0b5-079c-4f58-8d6f-c341a3c33d51",
            "compositeImage": {
                "id": "e2ded835-ffa1-40a9-ad16-45006713eb25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faf1375b-70d9-46b1-946f-022b513ef394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e134ff7d-3c71-47b0-b650-40e1681525f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faf1375b-70d9-46b1-946f-022b513ef394",
                    "LayerId": "07168cd2-7877-4584-a526-9c9dc69878d5"
                },
                {
                    "id": "3b380fd6-fff4-4021-a7c5-1e0a45893ebf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faf1375b-70d9-46b1-946f-022b513ef394",
                    "LayerId": "805a5f22-7a74-460b-896b-e2bfd2044640"
                }
            ]
        },
        {
            "id": "a42ea6cd-fbd4-4222-a5dd-59a9d072c4de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc8ae0b5-079c-4f58-8d6f-c341a3c33d51",
            "compositeImage": {
                "id": "e0ceb2da-4098-409c-80c6-5c7fbcf2673a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a42ea6cd-fbd4-4222-a5dd-59a9d072c4de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86c26d90-a164-4871-a308-2941bb942adc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a42ea6cd-fbd4-4222-a5dd-59a9d072c4de",
                    "LayerId": "07168cd2-7877-4584-a526-9c9dc69878d5"
                },
                {
                    "id": "1488809a-939b-47f6-bd99-775e134c005d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a42ea6cd-fbd4-4222-a5dd-59a9d072c4de",
                    "LayerId": "805a5f22-7a74-460b-896b-e2bfd2044640"
                }
            ]
        },
        {
            "id": "16df5e29-bb8b-49a8-ba85-615437cf5260",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc8ae0b5-079c-4f58-8d6f-c341a3c33d51",
            "compositeImage": {
                "id": "67fa3446-ca91-495a-ab23-5b30e9c69319",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16df5e29-bb8b-49a8-ba85-615437cf5260",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecd4c55e-c2e4-4d59-bcc9-f0a8e1aa1acf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16df5e29-bb8b-49a8-ba85-615437cf5260",
                    "LayerId": "07168cd2-7877-4584-a526-9c9dc69878d5"
                },
                {
                    "id": "064ecbe7-e041-4634-baee-ab990c23d00d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16df5e29-bb8b-49a8-ba85-615437cf5260",
                    "LayerId": "805a5f22-7a74-460b-896b-e2bfd2044640"
                }
            ]
        },
        {
            "id": "8f90fddc-7c15-4bf4-8284-ba4ae8609675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc8ae0b5-079c-4f58-8d6f-c341a3c33d51",
            "compositeImage": {
                "id": "2e92c7b0-04f1-42f3-bf3f-c4044435da0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f90fddc-7c15-4bf4-8284-ba4ae8609675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7761047a-4ce1-4c06-beb3-68848474894a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f90fddc-7c15-4bf4-8284-ba4ae8609675",
                    "LayerId": "07168cd2-7877-4584-a526-9c9dc69878d5"
                },
                {
                    "id": "7e82d429-e20c-4c51-9764-42b6116f7667",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f90fddc-7c15-4bf4-8284-ba4ae8609675",
                    "LayerId": "805a5f22-7a74-460b-896b-e2bfd2044640"
                }
            ]
        },
        {
            "id": "d45b05dc-5b23-49cf-b2c7-1d326e70959c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc8ae0b5-079c-4f58-8d6f-c341a3c33d51",
            "compositeImage": {
                "id": "a4129b1a-f56d-4dbe-b93d-2d9262cf7f70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d45b05dc-5b23-49cf-b2c7-1d326e70959c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c41c0a3d-4b22-4bfd-8c8e-76c2391420b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d45b05dc-5b23-49cf-b2c7-1d326e70959c",
                    "LayerId": "07168cd2-7877-4584-a526-9c9dc69878d5"
                },
                {
                    "id": "c7dc59f6-9ca1-4696-8a3d-2cec6b4b6add",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d45b05dc-5b23-49cf-b2c7-1d326e70959c",
                    "LayerId": "805a5f22-7a74-460b-896b-e2bfd2044640"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "805a5f22-7a74-460b-896b-e2bfd2044640",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc8ae0b5-079c-4f58-8d6f-c341a3c33d51",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "07168cd2-7877-4584-a526-9c9dc69878d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc8ae0b5-079c-4f58-8d6f-c341a3c33d51",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 20
}