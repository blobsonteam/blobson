{
    "id": "14ff0951-7d8a-47c2-9f53-fbc10986df4a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_jab2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 0,
    "bbox_right": 111,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5e07430-f6ca-4e73-bbb1-e98d5427ef56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14ff0951-7d8a-47c2-9f53-fbc10986df4a",
            "compositeImage": {
                "id": "256c6653-2a77-438c-8c14-658fdf5f2808",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5e07430-f6ca-4e73-bbb1-e98d5427ef56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5ddf05b-26f1-4a8a-b011-3d8d28d875e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5e07430-f6ca-4e73-bbb1-e98d5427ef56",
                    "LayerId": "7daa75e4-5a0b-4279-90e3-128f3c61759c"
                }
            ]
        },
        {
            "id": "09118941-e9cf-4f31-9351-5d4c05dfadfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14ff0951-7d8a-47c2-9f53-fbc10986df4a",
            "compositeImage": {
                "id": "663b5f02-56ca-4f40-9d46-998157b92863",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09118941-e9cf-4f31-9351-5d4c05dfadfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "634bf54c-60d3-4656-a7ac-1583b21ab252",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09118941-e9cf-4f31-9351-5d4c05dfadfb",
                    "LayerId": "7daa75e4-5a0b-4279-90e3-128f3c61759c"
                }
            ]
        },
        {
            "id": "4b12d714-e9b1-4892-bdfa-5f30b916ecb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14ff0951-7d8a-47c2-9f53-fbc10986df4a",
            "compositeImage": {
                "id": "3379e180-2539-4634-baf2-e0af68c3c1be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b12d714-e9b1-4892-bdfa-5f30b916ecb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fa2210f-e51a-4983-bb0b-22693962d497",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b12d714-e9b1-4892-bdfa-5f30b916ecb3",
                    "LayerId": "7daa75e4-5a0b-4279-90e3-128f3c61759c"
                }
            ]
        },
        {
            "id": "197e5d3d-080f-42ab-97eb-8878cf117614",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14ff0951-7d8a-47c2-9f53-fbc10986df4a",
            "compositeImage": {
                "id": "596f15a0-ab2f-4891-99dd-816064484f2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "197e5d3d-080f-42ab-97eb-8878cf117614",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "582b9f69-3c9e-46cd-b79a-b9bd79ab7621",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "197e5d3d-080f-42ab-97eb-8878cf117614",
                    "LayerId": "7daa75e4-5a0b-4279-90e3-128f3c61759c"
                }
            ]
        },
        {
            "id": "668bbc1f-c067-4b17-bf84-5eefe20d5023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14ff0951-7d8a-47c2-9f53-fbc10986df4a",
            "compositeImage": {
                "id": "6590a600-2c70-4e85-85d5-b1b1449ed2a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "668bbc1f-c067-4b17-bf84-5eefe20d5023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c5f9667-392f-41b4-9425-6df82f6f9891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "668bbc1f-c067-4b17-bf84-5eefe20d5023",
                    "LayerId": "7daa75e4-5a0b-4279-90e3-128f3c61759c"
                }
            ]
        },
        {
            "id": "e8d32000-bcd3-43d6-a61f-b80840a69b4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14ff0951-7d8a-47c2-9f53-fbc10986df4a",
            "compositeImage": {
                "id": "b80f0c59-4935-4a30-9dbe-2139d2f6cf26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8d32000-bcd3-43d6-a61f-b80840a69b4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bc9a157-81db-4dd5-80f8-717947d1215a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8d32000-bcd3-43d6-a61f-b80840a69b4d",
                    "LayerId": "7daa75e4-5a0b-4279-90e3-128f3c61759c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 98,
    "layers": [
        {
            "id": "7daa75e4-5a0b-4279-90e3-128f3c61759c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14ff0951-7d8a-47c2-9f53-fbc10986df4a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 112,
    "xorig": 54,
    "yorig": 97
}