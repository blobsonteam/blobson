{
    "id": "adacf743-6e6f-4b0f-97ce-deba9156e113",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_lag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 0,
    "bbox_right": 61,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78d5209a-adf1-46fe-ae05-547ff567bf9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adacf743-6e6f-4b0f-97ce-deba9156e113",
            "compositeImage": {
                "id": "a6f50a54-a88b-4f95-bc7e-bb99addee57e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78d5209a-adf1-46fe-ae05-547ff567bf9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48fe5ab9-6c0a-460a-8018-b1284c19d419",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78d5209a-adf1-46fe-ae05-547ff567bf9e",
                    "LayerId": "cae5f99c-aebc-4eaa-9f61-9b86effda974"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 91,
    "layers": [
        {
            "id": "cae5f99c-aebc-4eaa-9f61-9b86effda974",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adacf743-6e6f-4b0f-97ce-deba9156e113",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 62,
    "xorig": 17,
    "yorig": 90
}