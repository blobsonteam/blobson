{
    "id": "97610b6b-03df-4c53-91f0-bee285cccfcd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_portrait",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 74,
    "bbox_right": 161,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fac4cc69-e386-4890-bc6e-40ecd7332d15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97610b6b-03df-4c53-91f0-bee285cccfcd",
            "compositeImage": {
                "id": "358b159b-032e-4f24-9d1e-7f1f739b307b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fac4cc69-e386-4890-bc6e-40ecd7332d15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63516fc4-6a9a-4166-a7a1-fa1c55b6d728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fac4cc69-e386-4890-bc6e-40ecd7332d15",
                    "LayerId": "887e2098-6f84-4a0f-b4e2-d84441719521"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "887e2098-6f84-4a0f-b4e2-d84441719521",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97610b6b-03df-4c53-91f0-bee285cccfcd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}