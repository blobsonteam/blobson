{
    "id": "4c769a3f-fcf1-4c3d-a21e-665ecb1f8188",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nair0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 3,
    "bbox_right": 60,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf24fdd4-48ad-429b-8a8f-6963b8e43920",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c769a3f-fcf1-4c3d-a21e-665ecb1f8188",
            "compositeImage": {
                "id": "959058b5-afbb-42d7-a365-325031d050e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf24fdd4-48ad-429b-8a8f-6963b8e43920",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dc89a83-98f4-4e49-8b46-0c10f47f6784",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf24fdd4-48ad-429b-8a8f-6963b8e43920",
                    "LayerId": "d53b2ca6-d436-4d8d-9c4a-a8521cd5b6a1"
                },
                {
                    "id": "68ac836f-3388-440d-bcdf-8080ba1681b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf24fdd4-48ad-429b-8a8f-6963b8e43920",
                    "LayerId": "0d51fb35-4033-4675-bc91-1717a59980ff"
                }
            ]
        },
        {
            "id": "83f0cdcd-09ca-44d2-98b7-6e37001a59da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c769a3f-fcf1-4c3d-a21e-665ecb1f8188",
            "compositeImage": {
                "id": "469df745-f10d-4099-bad6-61b195eeb7d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83f0cdcd-09ca-44d2-98b7-6e37001a59da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98786bca-9510-4536-b37c-0e1f0eb1d7de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83f0cdcd-09ca-44d2-98b7-6e37001a59da",
                    "LayerId": "d53b2ca6-d436-4d8d-9c4a-a8521cd5b6a1"
                },
                {
                    "id": "5001cf45-8a32-45d9-8b09-835380ab9f18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83f0cdcd-09ca-44d2-98b7-6e37001a59da",
                    "LayerId": "0d51fb35-4033-4675-bc91-1717a59980ff"
                }
            ]
        },
        {
            "id": "0604028a-91a8-446b-a704-c0bc4d528b13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c769a3f-fcf1-4c3d-a21e-665ecb1f8188",
            "compositeImage": {
                "id": "324362cb-418f-403d-9149-84dc99da70bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0604028a-91a8-446b-a704-c0bc4d528b13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01834146-8b3f-4ca3-bba5-add6c31e6ae5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0604028a-91a8-446b-a704-c0bc4d528b13",
                    "LayerId": "d53b2ca6-d436-4d8d-9c4a-a8521cd5b6a1"
                },
                {
                    "id": "1a421122-7d56-48f8-bae0-75bb06b5ed7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0604028a-91a8-446b-a704-c0bc4d528b13",
                    "LayerId": "0d51fb35-4033-4675-bc91-1717a59980ff"
                }
            ]
        },
        {
            "id": "ccf9a79a-cd1a-4ec5-a72e-8c9c6243dc38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c769a3f-fcf1-4c3d-a21e-665ecb1f8188",
            "compositeImage": {
                "id": "540190e6-9a2a-4003-9aae-48c2a74e62ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccf9a79a-cd1a-4ec5-a72e-8c9c6243dc38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb61526e-8040-4da4-830e-13597bfaf087",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccf9a79a-cd1a-4ec5-a72e-8c9c6243dc38",
                    "LayerId": "d53b2ca6-d436-4d8d-9c4a-a8521cd5b6a1"
                },
                {
                    "id": "a2223520-7b65-42bf-90b8-85aebc92cb11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccf9a79a-cd1a-4ec5-a72e-8c9c6243dc38",
                    "LayerId": "0d51fb35-4033-4675-bc91-1717a59980ff"
                }
            ]
        },
        {
            "id": "f9be7317-f55d-42e1-b616-1bc8a5af31ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c769a3f-fcf1-4c3d-a21e-665ecb1f8188",
            "compositeImage": {
                "id": "2cd5b476-e28c-4596-bbca-6bbe19a95333",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9be7317-f55d-42e1-b616-1bc8a5af31ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "264132f0-3a0f-4f43-b89c-9c8fc3633c10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9be7317-f55d-42e1-b616-1bc8a5af31ff",
                    "LayerId": "d53b2ca6-d436-4d8d-9c4a-a8521cd5b6a1"
                },
                {
                    "id": "2063f488-c702-467e-8a30-fc202e5f5afb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9be7317-f55d-42e1-b616-1bc8a5af31ff",
                    "LayerId": "0d51fb35-4033-4675-bc91-1717a59980ff"
                }
            ]
        },
        {
            "id": "bc8f6bdc-2d82-418d-9b48-c19885822aff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c769a3f-fcf1-4c3d-a21e-665ecb1f8188",
            "compositeImage": {
                "id": "9582bc9e-91d8-43f7-8343-578c9b9a550f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc8f6bdc-2d82-418d-9b48-c19885822aff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "468bdec6-9dfc-40b3-b216-c4d06d3fd57d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc8f6bdc-2d82-418d-9b48-c19885822aff",
                    "LayerId": "d53b2ca6-d436-4d8d-9c4a-a8521cd5b6a1"
                },
                {
                    "id": "4ab6ad07-5e96-49f8-98b9-94db997ab3e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc8f6bdc-2d82-418d-9b48-c19885822aff",
                    "LayerId": "0d51fb35-4033-4675-bc91-1717a59980ff"
                }
            ]
        },
        {
            "id": "9642e57f-d185-4d18-ae06-2d9410aac2f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c769a3f-fcf1-4c3d-a21e-665ecb1f8188",
            "compositeImage": {
                "id": "0efa8e72-f48f-44c8-9865-64652d10b055",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9642e57f-d185-4d18-ae06-2d9410aac2f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf4b80c9-abb0-4ca4-ada4-d667be725e1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9642e57f-d185-4d18-ae06-2d9410aac2f6",
                    "LayerId": "d53b2ca6-d436-4d8d-9c4a-a8521cd5b6a1"
                },
                {
                    "id": "3222cfb7-5072-4ef6-9e52-0f7241b990ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9642e57f-d185-4d18-ae06-2d9410aac2f6",
                    "LayerId": "0d51fb35-4033-4675-bc91-1717a59980ff"
                }
            ]
        },
        {
            "id": "e30755c6-a4e1-4fad-8e74-3d159079641f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c769a3f-fcf1-4c3d-a21e-665ecb1f8188",
            "compositeImage": {
                "id": "69b17277-078e-414f-8003-356bcb134a3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e30755c6-a4e1-4fad-8e74-3d159079641f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dc0d63b-febc-4d36-9a9f-bcb7932794a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e30755c6-a4e1-4fad-8e74-3d159079641f",
                    "LayerId": "d53b2ca6-d436-4d8d-9c4a-a8521cd5b6a1"
                },
                {
                    "id": "1aaf50d0-de37-4fc0-bbd7-33ac2de3e434",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e30755c6-a4e1-4fad-8e74-3d159079641f",
                    "LayerId": "0d51fb35-4033-4675-bc91-1717a59980ff"
                }
            ]
        },
        {
            "id": "141041fb-7847-4ea8-af90-59f4e9182ee4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c769a3f-fcf1-4c3d-a21e-665ecb1f8188",
            "compositeImage": {
                "id": "05be4e08-0944-42f3-9f61-ca57146ae69a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "141041fb-7847-4ea8-af90-59f4e9182ee4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5745d70f-88e8-4e48-b26c-9911c13f96b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "141041fb-7847-4ea8-af90-59f4e9182ee4",
                    "LayerId": "d53b2ca6-d436-4d8d-9c4a-a8521cd5b6a1"
                },
                {
                    "id": "200e30c0-84be-458e-844a-8f92cc41f392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "141041fb-7847-4ea8-af90-59f4e9182ee4",
                    "LayerId": "0d51fb35-4033-4675-bc91-1717a59980ff"
                }
            ]
        },
        {
            "id": "92438a60-418a-4388-853f-b53cf028c7be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c769a3f-fcf1-4c3d-a21e-665ecb1f8188",
            "compositeImage": {
                "id": "e9b5edd6-6987-421f-a047-749532b1c144",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92438a60-418a-4388-853f-b53cf028c7be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "522595e8-fc83-456e-96aa-2dc9b9693768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92438a60-418a-4388-853f-b53cf028c7be",
                    "LayerId": "d53b2ca6-d436-4d8d-9c4a-a8521cd5b6a1"
                },
                {
                    "id": "f99fe65f-6361-4f19-a28b-bb31f4030c9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92438a60-418a-4388-853f-b53cf028c7be",
                    "LayerId": "0d51fb35-4033-4675-bc91-1717a59980ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d53b2ca6-d436-4d8d-9c4a-a8521cd5b6a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c769a3f-fcf1-4c3d-a21e-665ecb1f8188",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0d51fb35-4033-4675-bc91-1717a59980ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c769a3f-fcf1-4c3d-a21e-665ecb1f8188",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}