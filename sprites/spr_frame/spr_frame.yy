{
    "id": "c3dd24d8-e41a-460a-9305-52c53863e002",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_frame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27c9ed64-6dfe-4172-9475-42f758327660",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3dd24d8-e41a-460a-9305-52c53863e002",
            "compositeImage": {
                "id": "ed2c58f4-6bae-4847-96bf-fdaea8e7f4ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27c9ed64-6dfe-4172-9475-42f758327660",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c97ec221-6089-4ec6-8752-46d27090141d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27c9ed64-6dfe-4172-9475-42f758327660",
                    "LayerId": "f84fd578-62d4-4423-a2be-de0f7d58d2ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "f84fd578-62d4-4423-a2be-de0f7d58d2ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3dd24d8-e41a-460a-9305-52c53863e002",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}