{
    "id": "6da990f6-a9f9-4dbf-af84-daeef4e03eea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_grabbing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 11,
    "bbox_right": 70,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "857638cb-59ba-4dd8-a550-36ec89bb1f2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da990f6-a9f9-4dbf-af84-daeef4e03eea",
            "compositeImage": {
                "id": "5e7c2ec9-e974-492c-bf04-26b53ae40a48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "857638cb-59ba-4dd8-a550-36ec89bb1f2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eddcd48-251a-41ff-a835-8fb4744c4283",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "857638cb-59ba-4dd8-a550-36ec89bb1f2c",
                    "LayerId": "6f9ddaaa-2ab5-4aeb-a412-2b448e042255"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 98,
    "layers": [
        {
            "id": "6f9ddaaa-2ab5-4aeb-a412-2b448e042255",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6da990f6-a9f9-4dbf-af84-daeef4e03eea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 99,
    "xorig": 38,
    "yorig": 97
}