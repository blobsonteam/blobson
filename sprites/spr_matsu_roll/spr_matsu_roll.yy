{
    "id": "c66a9274-407f-4268-8d3b-7b12002d5f54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_roll",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 0,
    "bbox_right": 101,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e914888b-fbb9-494c-ab82-bf9a9d44ebab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c66a9274-407f-4268-8d3b-7b12002d5f54",
            "compositeImage": {
                "id": "2793f1c2-a07b-4a46-b7c0-013c3529e436",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e914888b-fbb9-494c-ab82-bf9a9d44ebab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f16b58f5-c0d5-4ea9-994f-f4306ecac318",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e914888b-fbb9-494c-ab82-bf9a9d44ebab",
                    "LayerId": "a7365103-5878-40bd-9e1d-7374b26c8f07"
                }
            ]
        },
        {
            "id": "312f6e61-42e9-499b-9509-3a04120fcbba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c66a9274-407f-4268-8d3b-7b12002d5f54",
            "compositeImage": {
                "id": "465a0a64-0f36-4662-b1c8-150fd158c349",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "312f6e61-42e9-499b-9509-3a04120fcbba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f052951-7852-4caa-ae6f-4d6fb48bbe25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "312f6e61-42e9-499b-9509-3a04120fcbba",
                    "LayerId": "a7365103-5878-40bd-9e1d-7374b26c8f07"
                }
            ]
        },
        {
            "id": "8bc12d92-dd36-46aa-9335-d82794f9ecb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c66a9274-407f-4268-8d3b-7b12002d5f54",
            "compositeImage": {
                "id": "5657a25d-8c79-4769-a9cb-74e4280c90f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bc12d92-dd36-46aa-9335-d82794f9ecb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26420221-40a6-4cc6-896c-3f06e006df57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bc12d92-dd36-46aa-9335-d82794f9ecb9",
                    "LayerId": "a7365103-5878-40bd-9e1d-7374b26c8f07"
                }
            ]
        },
        {
            "id": "dec43664-7bc7-4d44-aa0b-4b239f9b1fee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c66a9274-407f-4268-8d3b-7b12002d5f54",
            "compositeImage": {
                "id": "995324ec-7d8e-465a-affb-6e8ec7fee551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dec43664-7bc7-4d44-aa0b-4b239f9b1fee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b25fd514-0496-4a79-b38f-fbcbb5fdc543",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dec43664-7bc7-4d44-aa0b-4b239f9b1fee",
                    "LayerId": "a7365103-5878-40bd-9e1d-7374b26c8f07"
                }
            ]
        },
        {
            "id": "26c07135-ff7f-4349-bc93-d4161d8aedab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c66a9274-407f-4268-8d3b-7b12002d5f54",
            "compositeImage": {
                "id": "c3e9869c-5ae2-407a-b3d0-edc577981950",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26c07135-ff7f-4349-bc93-d4161d8aedab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "841641d5-6193-4673-a935-9818dd36bb60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26c07135-ff7f-4349-bc93-d4161d8aedab",
                    "LayerId": "a7365103-5878-40bd-9e1d-7374b26c8f07"
                }
            ]
        },
        {
            "id": "eeae5d9f-6246-47c8-bdc2-cc0c942d20d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c66a9274-407f-4268-8d3b-7b12002d5f54",
            "compositeImage": {
                "id": "06db186d-204e-41ac-a0b4-3641f496dac7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeae5d9f-6246-47c8-bdc2-cc0c942d20d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89000683-f6c7-41c0-b86b-ed89be80fcae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeae5d9f-6246-47c8-bdc2-cc0c942d20d9",
                    "LayerId": "a7365103-5878-40bd-9e1d-7374b26c8f07"
                }
            ]
        },
        {
            "id": "ae04ae03-b076-4c3b-ac21-e1bff94ae66f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c66a9274-407f-4268-8d3b-7b12002d5f54",
            "compositeImage": {
                "id": "635e3512-f648-475e-8610-499be392a868",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae04ae03-b076-4c3b-ac21-e1bff94ae66f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00e8a0f4-da2c-4bba-b460-b72740491899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae04ae03-b076-4c3b-ac21-e1bff94ae66f",
                    "LayerId": "a7365103-5878-40bd-9e1d-7374b26c8f07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "a7365103-5878-40bd-9e1d-7374b26c8f07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c66a9274-407f-4268-8d3b-7b12002d5f54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 102,
    "xorig": 46,
    "yorig": 86
}