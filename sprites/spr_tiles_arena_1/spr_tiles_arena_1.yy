{
    "id": "36f6f862-6f22-43ba-a40c-1d3e40177572",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tiles_arena_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1300,
    "bbox_left": 66,
    "bbox_right": 259,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e0a6dac-b0e3-430c-b70c-8bfee8017261",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36f6f862-6f22-43ba-a40c-1d3e40177572",
            "compositeImage": {
                "id": "97b45698-a098-4485-a007-e54509e3a121",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e0a6dac-b0e3-430c-b70c-8bfee8017261",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7ccff07-516d-4ae9-a47d-8e0a4af942b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e0a6dac-b0e3-430c-b70c-8bfee8017261",
                    "LayerId": "14fb1cce-155a-45ab-b85a-d28ae2305ae7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1310,
    "layers": [
        {
            "id": "14fb1cce-155a-45ab-b85a-d28ae2305ae7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36f6f862-6f22-43ba-a40c-1d3e40177572",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 261,
    "xorig": 0,
    "yorig": 0
}