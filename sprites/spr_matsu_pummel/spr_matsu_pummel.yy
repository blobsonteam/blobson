{
    "id": "c3a03ec5-263a-4a71-97e1-66ab496e58f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_pummel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 0,
    "bbox_right": 98,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34501707-8d66-4575-873b-7daf6950dd23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3a03ec5-263a-4a71-97e1-66ab496e58f6",
            "compositeImage": {
                "id": "cc5904e5-9ef5-4408-a175-5f3620cbad15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34501707-8d66-4575-873b-7daf6950dd23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13b91971-af15-4192-a2fc-b1cec1d1817d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34501707-8d66-4575-873b-7daf6950dd23",
                    "LayerId": "3455ce28-22a1-41d7-baa4-097868ce6e57"
                }
            ]
        },
        {
            "id": "53819ac0-e8a2-4f8b-93e0-25cf9de93364",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3a03ec5-263a-4a71-97e1-66ab496e58f6",
            "compositeImage": {
                "id": "c987f25c-4eb2-44ad-8112-63132682a7db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53819ac0-e8a2-4f8b-93e0-25cf9de93364",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57ff06fb-4c91-4a65-9467-e3d1218a4632",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53819ac0-e8a2-4f8b-93e0-25cf9de93364",
                    "LayerId": "3455ce28-22a1-41d7-baa4-097868ce6e57"
                }
            ]
        },
        {
            "id": "8ac62fe6-138d-41c5-85ff-bd25e2002b77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3a03ec5-263a-4a71-97e1-66ab496e58f6",
            "compositeImage": {
                "id": "3f4c855d-9eda-400a-9fa5-783521a217f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ac62fe6-138d-41c5-85ff-bd25e2002b77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d326cf56-d12f-47da-a61f-44d9a4b569ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ac62fe6-138d-41c5-85ff-bd25e2002b77",
                    "LayerId": "3455ce28-22a1-41d7-baa4-097868ce6e57"
                }
            ]
        },
        {
            "id": "db3204fb-76b3-4c4e-b0ca-09ba7ef61e85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3a03ec5-263a-4a71-97e1-66ab496e58f6",
            "compositeImage": {
                "id": "a452f81c-42d8-4936-9e82-8b1f9ccafec0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db3204fb-76b3-4c4e-b0ca-09ba7ef61e85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64f9725e-1d03-404c-9786-c6b1070eaa5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db3204fb-76b3-4c4e-b0ca-09ba7ef61e85",
                    "LayerId": "3455ce28-22a1-41d7-baa4-097868ce6e57"
                }
            ]
        },
        {
            "id": "559c45aa-c4a4-4715-b820-cb7dbcbdc74e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3a03ec5-263a-4a71-97e1-66ab496e58f6",
            "compositeImage": {
                "id": "87f87e1c-b7d5-4e31-a031-2a673362f4de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "559c45aa-c4a4-4715-b820-cb7dbcbdc74e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05b42106-a8fe-404d-9782-001fde3b1451",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "559c45aa-c4a4-4715-b820-cb7dbcbdc74e",
                    "LayerId": "3455ce28-22a1-41d7-baa4-097868ce6e57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 98,
    "layers": [
        {
            "id": "3455ce28-22a1-41d7-baa4-097868ce6e57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3a03ec5-263a-4a71-97e1-66ab496e58f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 99,
    "xorig": 38,
    "yorig": 97
}