{
    "id": "f63f0a26-6cae-44d0-9cd3-2cb351dbfc3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon_training",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 148,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9678ab2b-501b-4ec9-9123-a9a34c9a4955",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f63f0a26-6cae-44d0-9cd3-2cb351dbfc3e",
            "compositeImage": {
                "id": "2218ee26-7344-45ca-9487-8c85801e0a06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9678ab2b-501b-4ec9-9123-a9a34c9a4955",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ae133cf-9dc9-440e-8d99-add32a56956d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9678ab2b-501b-4ec9-9123-a9a34c9a4955",
                    "LayerId": "6d6a39ed-317f-4c35-a55e-38cc30554d28"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "6d6a39ed-317f-4c35-a55e-38cc30554d28",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f63f0a26-6cae-44d0-9cd3-2cb351dbfc3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}