{
    "id": "392d9938-821d-4596-9916-ca2972a129de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_hurbox_normal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 0,
    "bbox_right": 42,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2efc3f63-9036-4f7f-979e-a17ac13689b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "392d9938-821d-4596-9916-ca2972a129de",
            "compositeImage": {
                "id": "0f7d3c90-d58b-4ba3-a8cf-e85912bb048a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2efc3f63-9036-4f7f-979e-a17ac13689b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50a8713f-7a41-4bd2-bf56-074a5cb65012",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2efc3f63-9036-4f7f-979e-a17ac13689b4",
                    "LayerId": "5458f7b5-47cf-44c6-b031-219adb1b365c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 91,
    "layers": [
        {
            "id": "5458f7b5-47cf-44c6-b031-219adb1b365c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "392d9938-821d-4596-9916-ca2972a129de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 39,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 0.1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 43,
    "xorig": 21,
    "yorig": 90
}