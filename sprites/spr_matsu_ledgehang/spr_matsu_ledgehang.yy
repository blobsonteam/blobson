{
    "id": "73bc2682-f5c1-4139-a1db-0a1b628c99a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_ledgehang",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 129,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a62aeba7-1bbf-4902-9e27-e8cc3100911d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73bc2682-f5c1-4139-a1db-0a1b628c99a7",
            "compositeImage": {
                "id": "7fe682c2-26e4-48d7-9dcb-b9a73ea5e756",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a62aeba7-1bbf-4902-9e27-e8cc3100911d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b017214c-ebdc-4568-a9e1-00f13767c7a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a62aeba7-1bbf-4902-9e27-e8cc3100911d",
                    "LayerId": "33263179-3f75-4e31-aef1-4660d1e44207"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 130,
    "layers": [
        {
            "id": "33263179-3f75-4e31-aef1-4660d1e44207",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73bc2682-f5c1-4139-a1db-0a1b628c99a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 13,
    "yorig": 129
}