{
    "id": "f27065e6-1a5b-442c-81fd-0590466aa5ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_dthrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 88,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f471a8d-694c-4940-9bd2-219c580c09ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f27065e6-1a5b-442c-81fd-0590466aa5ff",
            "compositeImage": {
                "id": "bb17d813-0dca-481e-a56b-8d2997ab06e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f471a8d-694c-4940-9bd2-219c580c09ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edb433cc-cb63-4ce3-adb4-770b0efd6dc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f471a8d-694c-4940-9bd2-219c580c09ef",
                    "LayerId": "5c5fe68c-2019-4f7b-9405-4b391848a399"
                }
            ]
        },
        {
            "id": "19491210-8131-4c9f-a7bc-12e401d8a666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f27065e6-1a5b-442c-81fd-0590466aa5ff",
            "compositeImage": {
                "id": "c1ccbc75-3622-4e67-8f0a-a0483cabc67c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19491210-8131-4c9f-a7bc-12e401d8a666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbbc1537-e4ef-454e-b944-2f26b79c12e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19491210-8131-4c9f-a7bc-12e401d8a666",
                    "LayerId": "5c5fe68c-2019-4f7b-9405-4b391848a399"
                }
            ]
        },
        {
            "id": "de12b6fa-c712-4091-a938-19634ec954c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f27065e6-1a5b-442c-81fd-0590466aa5ff",
            "compositeImage": {
                "id": "21ac950a-b352-4dd9-b9f8-cf0f64e74b92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de12b6fa-c712-4091-a938-19634ec954c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fe660b3-a858-474f-8cc9-3a347cb372b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de12b6fa-c712-4091-a938-19634ec954c1",
                    "LayerId": "5c5fe68c-2019-4f7b-9405-4b391848a399"
                }
            ]
        },
        {
            "id": "18d5223d-6b1a-4368-b00d-4a377ecf5265",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f27065e6-1a5b-442c-81fd-0590466aa5ff",
            "compositeImage": {
                "id": "4218342b-c6c9-43c2-9f86-eae7dbfb8a80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18d5223d-6b1a-4368-b00d-4a377ecf5265",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e98904c-4632-43fe-b90d-53e02d423ad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18d5223d-6b1a-4368-b00d-4a377ecf5265",
                    "LayerId": "5c5fe68c-2019-4f7b-9405-4b391848a399"
                }
            ]
        },
        {
            "id": "88163ee0-74a0-45dd-a612-2452571be469",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f27065e6-1a5b-442c-81fd-0590466aa5ff",
            "compositeImage": {
                "id": "be0ba1b2-073e-48df-95ee-6ecfc76bdf2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88163ee0-74a0-45dd-a612-2452571be469",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02a37c2b-25f0-43da-951e-dea85b90c5ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88163ee0-74a0-45dd-a612-2452571be469",
                    "LayerId": "5c5fe68c-2019-4f7b-9405-4b391848a399"
                }
            ]
        },
        {
            "id": "a394cd51-545e-420f-8d8b-d3874215c423",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f27065e6-1a5b-442c-81fd-0590466aa5ff",
            "compositeImage": {
                "id": "c6e8972d-3af6-491c-8cd3-070244266d28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a394cd51-545e-420f-8d8b-d3874215c423",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffa50014-d196-41d5-a4e0-5bf39af1417d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a394cd51-545e-420f-8d8b-d3874215c423",
                    "LayerId": "5c5fe68c-2019-4f7b-9405-4b391848a399"
                }
            ]
        },
        {
            "id": "88b8fbe0-0a99-4cbf-a02a-ee91c9c77bc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f27065e6-1a5b-442c-81fd-0590466aa5ff",
            "compositeImage": {
                "id": "41dfd30a-9ca9-4315-b0b0-888c69e90698",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88b8fbe0-0a99-4cbf-a02a-ee91c9c77bc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e63a9a2-418b-49e3-bb59-544b5b2cf76c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88b8fbe0-0a99-4cbf-a02a-ee91c9c77bc9",
                    "LayerId": "5c5fe68c-2019-4f7b-9405-4b391848a399"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 103,
    "layers": [
        {
            "id": "5c5fe68c-2019-4f7b-9405-4b391848a399",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f27065e6-1a5b-442c-81fd-0590466aa5ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 89,
    "xorig": 46,
    "yorig": 102
}