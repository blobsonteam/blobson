{
    "id": "2c8b98e8-699e-4680-9afe-fc8d48d37644",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shine_ledge_grab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6db9188d-b1be-4af3-98d5-a1b6aba2855f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c8b98e8-699e-4680-9afe-fc8d48d37644",
            "compositeImage": {
                "id": "cc95144e-3456-43af-9ac9-f76b75cdaf06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6db9188d-b1be-4af3-98d5-a1b6aba2855f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e67c665-c801-48da-9ffc-3ffd4c52e10e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6db9188d-b1be-4af3-98d5-a1b6aba2855f",
                    "LayerId": "19116752-ca3a-440c-ae47-6b9771da4cb1"
                },
                {
                    "id": "3b751305-8892-4aec-a538-af3feb098e35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6db9188d-b1be-4af3-98d5-a1b6aba2855f",
                    "LayerId": "9f75339c-8d14-4f39-90eb-70e51a25ba9d"
                }
            ]
        },
        {
            "id": "d02514a8-bb87-44af-9bb9-41ef3642d291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c8b98e8-699e-4680-9afe-fc8d48d37644",
            "compositeImage": {
                "id": "2c51e8b5-ce95-4dc4-93c6-5771dda69412",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d02514a8-bb87-44af-9bb9-41ef3642d291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03dcb09c-db83-4572-a03b-50297e902392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d02514a8-bb87-44af-9bb9-41ef3642d291",
                    "LayerId": "19116752-ca3a-440c-ae47-6b9771da4cb1"
                },
                {
                    "id": "dad172e2-eb83-4d1e-85a3-c60f2162dfc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d02514a8-bb87-44af-9bb9-41ef3642d291",
                    "LayerId": "9f75339c-8d14-4f39-90eb-70e51a25ba9d"
                }
            ]
        },
        {
            "id": "3c5b6891-fa66-4efa-884a-bf6f47ce8550",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c8b98e8-699e-4680-9afe-fc8d48d37644",
            "compositeImage": {
                "id": "7b51dcf1-8ea8-423c-a54f-9f195c5c6ae5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c5b6891-fa66-4efa-884a-bf6f47ce8550",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf36d4f4-03bf-430f-b058-c6750f2f76e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c5b6891-fa66-4efa-884a-bf6f47ce8550",
                    "LayerId": "19116752-ca3a-440c-ae47-6b9771da4cb1"
                },
                {
                    "id": "e0ff8a63-7b6a-4cd6-8c87-470f14414971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c5b6891-fa66-4efa-884a-bf6f47ce8550",
                    "LayerId": "9f75339c-8d14-4f39-90eb-70e51a25ba9d"
                }
            ]
        },
        {
            "id": "8fd17358-d64a-4fd5-b1f4-430347d770dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c8b98e8-699e-4680-9afe-fc8d48d37644",
            "compositeImage": {
                "id": "a5941097-76fd-429f-8796-1c343316b765",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fd17358-d64a-4fd5-b1f4-430347d770dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6067653-2cb9-4691-9a98-1a1f27c51b06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fd17358-d64a-4fd5-b1f4-430347d770dd",
                    "LayerId": "19116752-ca3a-440c-ae47-6b9771da4cb1"
                },
                {
                    "id": "070d9b3e-d7bc-4885-997a-e98fa1869e47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fd17358-d64a-4fd5-b1f4-430347d770dd",
                    "LayerId": "9f75339c-8d14-4f39-90eb-70e51a25ba9d"
                }
            ]
        },
        {
            "id": "f74ec1a2-ba39-484e-93a4-2e09c654b34e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c8b98e8-699e-4680-9afe-fc8d48d37644",
            "compositeImage": {
                "id": "0fbc7464-4f15-4e75-b4a3-3fc048b914bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f74ec1a2-ba39-484e-93a4-2e09c654b34e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4da795a2-d3cc-454a-b351-5e99a4843fec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f74ec1a2-ba39-484e-93a4-2e09c654b34e",
                    "LayerId": "19116752-ca3a-440c-ae47-6b9771da4cb1"
                },
                {
                    "id": "8cd904e4-5df3-479c-a61d-a78615920440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f74ec1a2-ba39-484e-93a4-2e09c654b34e",
                    "LayerId": "9f75339c-8d14-4f39-90eb-70e51a25ba9d"
                }
            ]
        },
        {
            "id": "73e61649-c17a-42d2-8d57-9e3a783d4bd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c8b98e8-699e-4680-9afe-fc8d48d37644",
            "compositeImage": {
                "id": "34c2f2cd-d657-4b58-b374-572f3df5f2b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73e61649-c17a-42d2-8d57-9e3a783d4bd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e204e57-3ece-4e9b-b716-ce92ac6ab8ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73e61649-c17a-42d2-8d57-9e3a783d4bd0",
                    "LayerId": "19116752-ca3a-440c-ae47-6b9771da4cb1"
                },
                {
                    "id": "97ae2ac4-1946-48a7-8bc9-24f594dcb5b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73e61649-c17a-42d2-8d57-9e3a783d4bd0",
                    "LayerId": "9f75339c-8d14-4f39-90eb-70e51a25ba9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9f75339c-8d14-4f39-90eb-70e51a25ba9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c8b98e8-699e-4680-9afe-fc8d48d37644",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "19116752-ca3a-440c-ae47-6b9771da4cb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c8b98e8-699e-4680-9afe-fc8d48d37644",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}