{
    "id": "fb770c6b-a177-4cbf-b51e-54e47b25bb67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bair0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 1,
    "bbox_right": 46,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba5416f5-66f8-4d53-a957-c15329c037c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb770c6b-a177-4cbf-b51e-54e47b25bb67",
            "compositeImage": {
                "id": "14413e15-9149-4bc5-ba43-77eb57964e78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba5416f5-66f8-4d53-a957-c15329c037c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec9a21e1-3cea-46d4-a218-37feb6eef04c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba5416f5-66f8-4d53-a957-c15329c037c3",
                    "LayerId": "f26382a9-b2d7-4976-9989-ccf8f37c3b57"
                }
            ]
        },
        {
            "id": "35750054-df80-46f8-8feb-946fa2268567",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb770c6b-a177-4cbf-b51e-54e47b25bb67",
            "compositeImage": {
                "id": "442d258d-7dcf-4961-a18b-7b148639601a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35750054-df80-46f8-8feb-946fa2268567",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80be21ff-4276-4846-9765-7e401a93d168",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35750054-df80-46f8-8feb-946fa2268567",
                    "LayerId": "f26382a9-b2d7-4976-9989-ccf8f37c3b57"
                }
            ]
        },
        {
            "id": "09f49213-7750-4555-a143-c6cd91609d61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb770c6b-a177-4cbf-b51e-54e47b25bb67",
            "compositeImage": {
                "id": "7795e23f-0a2f-49ad-99d8-6352c2d13bf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09f49213-7750-4555-a143-c6cd91609d61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f065a176-057f-47c1-98f6-7c0f2c89f4ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09f49213-7750-4555-a143-c6cd91609d61",
                    "LayerId": "f26382a9-b2d7-4976-9989-ccf8f37c3b57"
                }
            ]
        },
        {
            "id": "db2dfb32-7466-477e-adfe-fd4917a3afe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb770c6b-a177-4cbf-b51e-54e47b25bb67",
            "compositeImage": {
                "id": "3ba7d281-e907-4c39-9d58-e98d0a65931a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db2dfb32-7466-477e-adfe-fd4917a3afe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a298d4f5-3090-4953-adcd-9d859012e30d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db2dfb32-7466-477e-adfe-fd4917a3afe7",
                    "LayerId": "f26382a9-b2d7-4976-9989-ccf8f37c3b57"
                }
            ]
        },
        {
            "id": "aebbf469-0ab7-425f-9c9d-d942fbc25139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb770c6b-a177-4cbf-b51e-54e47b25bb67",
            "compositeImage": {
                "id": "79ea9fce-5810-43bf-b76f-eab9daff8c14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aebbf469-0ab7-425f-9c9d-d942fbc25139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec077767-7ac4-4bd7-a59a-560103f11c54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aebbf469-0ab7-425f-9c9d-d942fbc25139",
                    "LayerId": "f26382a9-b2d7-4976-9989-ccf8f37c3b57"
                }
            ]
        },
        {
            "id": "8d5c275a-4d64-4c81-8b2c-0b0abee02302",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb770c6b-a177-4cbf-b51e-54e47b25bb67",
            "compositeImage": {
                "id": "3e274baa-0ce2-4333-9734-c3b516d3357a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d5c275a-4d64-4c81-8b2c-0b0abee02302",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "481ba41c-0fda-4e49-bebb-044fdfdc8ebf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d5c275a-4d64-4c81-8b2c-0b0abee02302",
                    "LayerId": "f26382a9-b2d7-4976-9989-ccf8f37c3b57"
                }
            ]
        },
        {
            "id": "5d9cbcdd-ffab-45e9-b21f-740653b89600",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb770c6b-a177-4cbf-b51e-54e47b25bb67",
            "compositeImage": {
                "id": "0570ce4d-9237-4b49-b3e1-d3a41f3cd58d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d9cbcdd-ffab-45e9-b21f-740653b89600",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86845179-8ad1-464d-85a4-999b7b8bf51c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d9cbcdd-ffab-45e9-b21f-740653b89600",
                    "LayerId": "f26382a9-b2d7-4976-9989-ccf8f37c3b57"
                }
            ]
        },
        {
            "id": "10ac3ad2-28bb-47b9-a021-de118ea3962e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb770c6b-a177-4cbf-b51e-54e47b25bb67",
            "compositeImage": {
                "id": "e536640e-9595-473a-a758-d41a26c02562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10ac3ad2-28bb-47b9-a021-de118ea3962e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13453c83-074e-4922-b1ac-3a541539582a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10ac3ad2-28bb-47b9-a021-de118ea3962e",
                    "LayerId": "f26382a9-b2d7-4976-9989-ccf8f37c3b57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "f26382a9-b2d7-4976-9989-ccf8f37c3b57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb770c6b-a177-4cbf-b51e-54e47b25bb67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 35,
    "yorig": 24
}