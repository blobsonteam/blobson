{
    "id": "171220ed-33b3-4a3a-a0e2-e27c341885c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_parry_stun0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8ece7ea-8db9-4fb1-8cbf-a569a7ba513a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "171220ed-33b3-4a3a-a0e2-e27c341885c1",
            "compositeImage": {
                "id": "8235f3bf-6eaa-4012-a374-452cdd02c541",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8ece7ea-8db9-4fb1-8cbf-a569a7ba513a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1985bc6-efd0-46d2-bca4-5a0049a9a69b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8ece7ea-8db9-4fb1-8cbf-a569a7ba513a",
                    "LayerId": "0852e518-8d8a-49ef-b8f3-26aedd418247"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "0852e518-8d8a-49ef-b8f3-26aedd418247",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "171220ed-33b3-4a3a-a0e2-e27c341885c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}