{
    "id": "e85e77e7-e4ff-44a2-8792-65c98c53cb3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ledge_tether0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 3,
    "bbox_right": 27,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6007f9d-017b-4c54-93cb-50036e90bdf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e85e77e7-e4ff-44a2-8792-65c98c53cb3b",
            "compositeImage": {
                "id": "482c438e-154f-4a69-9d91-b688e8e9f0de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6007f9d-017b-4c54-93cb-50036e90bdf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7e9f60e-e9d7-48be-9130-bceac68ba0a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6007f9d-017b-4c54-93cb-50036e90bdf9",
                    "LayerId": "ea33e126-c8c9-4e80-accb-1e52a4c63418"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "ea33e126-c8c9-4e80-accb-1e52a4c63418",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e85e77e7-e4ff-44a2-8792-65c98c53cb3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 26,
    "yorig": 3
}