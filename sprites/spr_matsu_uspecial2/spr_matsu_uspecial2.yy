{
    "id": "7d813e9c-a1cc-458d-9c76-b4f76f22b5a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_uspecial2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 0,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c0898ea-b3bf-4db1-95e3-d9725b321fff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d813e9c-a1cc-458d-9c76-b4f76f22b5a5",
            "compositeImage": {
                "id": "62069b95-803f-4095-8f5f-89e0e705e81e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c0898ea-b3bf-4db1-95e3-d9725b321fff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f324a98-8cef-46e8-89bf-7ab1e24a6e27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c0898ea-b3bf-4db1-95e3-d9725b321fff",
                    "LayerId": "c2cfcc26-07ac-4c16-a3ea-47e81efdc219"
                }
            ]
        },
        {
            "id": "ba951483-a780-4393-97a9-6dc5bf1ee303",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d813e9c-a1cc-458d-9c76-b4f76f22b5a5",
            "compositeImage": {
                "id": "0c3d4ee6-f6a7-4c27-b8fd-6550bd977c42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba951483-a780-4393-97a9-6dc5bf1ee303",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f308b19e-4702-4c7d-b912-29a17c8cf5b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba951483-a780-4393-97a9-6dc5bf1ee303",
                    "LayerId": "c2cfcc26-07ac-4c16-a3ea-47e81efdc219"
                }
            ]
        },
        {
            "id": "9c7c1188-8054-46da-a969-dacf8be6ed1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d813e9c-a1cc-458d-9c76-b4f76f22b5a5",
            "compositeImage": {
                "id": "32c7d28e-8031-4f84-974b-28fe944a9b39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c7c1188-8054-46da-a969-dacf8be6ed1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ac27a39-e80c-4907-82f5-772e9d0e9032",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c7c1188-8054-46da-a969-dacf8be6ed1d",
                    "LayerId": "c2cfcc26-07ac-4c16-a3ea-47e81efdc219"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 71,
    "layers": [
        {
            "id": "c2cfcc26-07ac-4c16-a3ea-47e81efdc219",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d813e9c-a1cc-458d-9c76-b4f76f22b5a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 63,
    "xorig": 31,
    "yorig": 70
}