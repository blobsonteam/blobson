{
    "id": "62bffe59-ac7d-4dea-a721-70561ff6a532",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_parried",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 0,
    "bbox_right": 42,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8df39460-d36e-4ecf-81a9-f33ec2d2583d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62bffe59-ac7d-4dea-a721-70561ff6a532",
            "compositeImage": {
                "id": "b794f943-9769-4c2b-a9bd-cdf8cbda3ee8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8df39460-d36e-4ecf-81a9-f33ec2d2583d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcdc4083-8c91-4126-b13a-81b0861a29da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8df39460-d36e-4ecf-81a9-f33ec2d2583d",
                    "LayerId": "58c3a8b2-0c95-4be4-a4c3-2b11ec681486"
                }
            ]
        },
        {
            "id": "cb2efe12-e84f-4827-8be0-542357d21386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62bffe59-ac7d-4dea-a721-70561ff6a532",
            "compositeImage": {
                "id": "ec48304c-38ab-42d4-af9d-65555d76a1c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb2efe12-e84f-4827-8be0-542357d21386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78530f14-afea-4a1f-bc66-aaefc9b94a10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb2efe12-e84f-4827-8be0-542357d21386",
                    "LayerId": "58c3a8b2-0c95-4be4-a4c3-2b11ec681486"
                }
            ]
        },
        {
            "id": "32132647-99bf-4566-839a-83c6673e0cbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62bffe59-ac7d-4dea-a721-70561ff6a532",
            "compositeImage": {
                "id": "dfa9d692-3569-4550-bd54-22b7c637b36f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32132647-99bf-4566-839a-83c6673e0cbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "823d6a66-6d9b-4125-8717-2f477bc8d45c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32132647-99bf-4566-839a-83c6673e0cbf",
                    "LayerId": "58c3a8b2-0c95-4be4-a4c3-2b11ec681486"
                }
            ]
        },
        {
            "id": "3ce071e6-cd05-43dd-a80e-6a33c182dc27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62bffe59-ac7d-4dea-a721-70561ff6a532",
            "compositeImage": {
                "id": "d149c6c3-aee5-48ad-838e-094048c080d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ce071e6-cd05-43dd-a80e-6a33c182dc27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cc18161-7d77-4d34-bf8b-6a04248462ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ce071e6-cd05-43dd-a80e-6a33c182dc27",
                    "LayerId": "58c3a8b2-0c95-4be4-a4c3-2b11ec681486"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "58c3a8b2-0c95-4be4-a4c3-2b11ec681486",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62bffe59-ac7d-4dea-a721-70561ff6a532",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0.3,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 43,
    "xorig": 17,
    "yorig": 86
}