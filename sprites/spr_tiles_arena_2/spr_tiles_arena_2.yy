{
    "id": "21d65e08-08e7-4015-9da2-139dc982c17d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tiles_arena_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 34,
    "bbox_right": 131,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "209d581f-6091-4799-99af-503a2f6f9bc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21d65e08-08e7-4015-9da2-139dc982c17d",
            "compositeImage": {
                "id": "07c9abea-2a11-4e35-bf67-3b257541e2f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "209d581f-6091-4799-99af-503a2f6f9bc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "600b3488-131e-49e8-877a-1840d943cbf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "209d581f-6091-4799-99af-503a2f6f9bc6",
                    "LayerId": "f2255e33-f75a-49a1-ac1b-d0cc7e66bc49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f2255e33-f75a-49a1-ac1b-d0cc7e66bc49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21d65e08-08e7-4015-9da2-139dc982c17d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 133,
    "xorig": 0,
    "yorig": 0
}