{
    "id": "cd618cc8-6811-4f0a-906e-738f761e6eed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p1_select",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d364bad3-4179-450e-a3e7-c404f015ad40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd618cc8-6811-4f0a-906e-738f761e6eed",
            "compositeImage": {
                "id": "f58ac16a-1ae3-4a8c-819c-1f1f889b436a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d364bad3-4179-450e-a3e7-c404f015ad40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f48e802-3cd2-49e7-a256-b9f0d757c340",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d364bad3-4179-450e-a3e7-c404f015ad40",
                    "LayerId": "e5ac4d66-89d0-4e72-afd7-119920033cc0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "e5ac4d66-89d0-4e72-afd7-119920033cc0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd618cc8-6811-4f0a-906e-738f761e6eed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}