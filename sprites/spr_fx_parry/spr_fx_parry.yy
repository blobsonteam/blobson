{
    "id": "6507e6c4-5149-4686-9d5a-2e39335b3974",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fx_parry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 160,
    "bbox_left": 0,
    "bbox_right": 153,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b8d17db-264f-470e-b5b9-c60ae9bb429c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6507e6c4-5149-4686-9d5a-2e39335b3974",
            "compositeImage": {
                "id": "3baf2a77-6226-4fda-adcd-7296947140eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b8d17db-264f-470e-b5b9-c60ae9bb429c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccd584a3-ec67-45e3-b43b-f66780cc4f1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b8d17db-264f-470e-b5b9-c60ae9bb429c",
                    "LayerId": "ed7291c8-be86-416a-8cc0-61da9ef9a7ac"
                }
            ]
        },
        {
            "id": "5ede257d-ae5a-4127-b722-24b39f6dd17c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6507e6c4-5149-4686-9d5a-2e39335b3974",
            "compositeImage": {
                "id": "192fc558-02db-4a82-8fc8-c099cdd64da9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ede257d-ae5a-4127-b722-24b39f6dd17c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19a3f3de-aa97-4009-9dfe-ddf2a5543cd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ede257d-ae5a-4127-b722-24b39f6dd17c",
                    "LayerId": "ed7291c8-be86-416a-8cc0-61da9ef9a7ac"
                }
            ]
        },
        {
            "id": "49bf3acf-18a0-4602-beaf-1a9b3c9d0a6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6507e6c4-5149-4686-9d5a-2e39335b3974",
            "compositeImage": {
                "id": "9334d513-4f93-4d5c-a7a6-61904ebcde93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49bf3acf-18a0-4602-beaf-1a9b3c9d0a6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "596b2e47-ec85-4906-b4b8-ef7f2379e913",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49bf3acf-18a0-4602-beaf-1a9b3c9d0a6b",
                    "LayerId": "ed7291c8-be86-416a-8cc0-61da9ef9a7ac"
                }
            ]
        },
        {
            "id": "ca53874b-2d96-41c5-9781-fdba4c149e27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6507e6c4-5149-4686-9d5a-2e39335b3974",
            "compositeImage": {
                "id": "c8cb610c-e3d9-43fb-914b-cb2dc1ec97f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca53874b-2d96-41c5-9781-fdba4c149e27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5cee828-2d68-4938-be48-a2743fdc1d27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca53874b-2d96-41c5-9781-fdba4c149e27",
                    "LayerId": "ed7291c8-be86-416a-8cc0-61da9ef9a7ac"
                }
            ]
        },
        {
            "id": "637baff3-e208-4a98-993f-c6c00e7eaf33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6507e6c4-5149-4686-9d5a-2e39335b3974",
            "compositeImage": {
                "id": "24f05729-a30f-4d7d-959e-998164311add",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "637baff3-e208-4a98-993f-c6c00e7eaf33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5505127-fc98-4f8e-826a-aff484395c48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "637baff3-e208-4a98-993f-c6c00e7eaf33",
                    "LayerId": "ed7291c8-be86-416a-8cc0-61da9ef9a7ac"
                }
            ]
        },
        {
            "id": "e99a1c07-a85b-40f5-afad-2194f4a5d0e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6507e6c4-5149-4686-9d5a-2e39335b3974",
            "compositeImage": {
                "id": "309b556c-88d7-43c0-8a9c-8fa06b882970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e99a1c07-a85b-40f5-afad-2194f4a5d0e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc8302d6-2bb8-4ac1-8722-ed8294a7d83a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e99a1c07-a85b-40f5-afad-2194f4a5d0e4",
                    "LayerId": "ed7291c8-be86-416a-8cc0-61da9ef9a7ac"
                }
            ]
        },
        {
            "id": "551f7f01-1d4b-4aa8-bba6-9c0653b974fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6507e6c4-5149-4686-9d5a-2e39335b3974",
            "compositeImage": {
                "id": "e0acbc52-9226-4ba3-a733-6f8681d2484b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "551f7f01-1d4b-4aa8-bba6-9c0653b974fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2716176f-4658-43d7-ad7c-a7f91039baa7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "551f7f01-1d4b-4aa8-bba6-9c0653b974fd",
                    "LayerId": "ed7291c8-be86-416a-8cc0-61da9ef9a7ac"
                }
            ]
        },
        {
            "id": "2f7dac23-5dc0-44ba-a4d0-d7f01640368f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6507e6c4-5149-4686-9d5a-2e39335b3974",
            "compositeImage": {
                "id": "85b9ea21-65e7-4346-84cf-1555aa229ac1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f7dac23-5dc0-44ba-a4d0-d7f01640368f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d99a36ea-efc6-4e2b-a7e4-45292e787736",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f7dac23-5dc0-44ba-a4d0-d7f01640368f",
                    "LayerId": "ed7291c8-be86-416a-8cc0-61da9ef9a7ac"
                }
            ]
        },
        {
            "id": "b8fd5ed4-60ca-46f4-87fe-5da74f4063d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6507e6c4-5149-4686-9d5a-2e39335b3974",
            "compositeImage": {
                "id": "c9d3fd35-7d1d-45a8-b5f2-c4679434a14e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8fd5ed4-60ca-46f4-87fe-5da74f4063d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51b42e44-a525-4ef9-ad4a-dcf5a5fd1fa7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8fd5ed4-60ca-46f4-87fe-5da74f4063d4",
                    "LayerId": "ed7291c8-be86-416a-8cc0-61da9ef9a7ac"
                }
            ]
        },
        {
            "id": "a61fcdc8-6d66-4b85-9c9d-22ace3df7607",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6507e6c4-5149-4686-9d5a-2e39335b3974",
            "compositeImage": {
                "id": "c353eeb8-a43c-4b77-b185-6acbe457ef9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a61fcdc8-6d66-4b85-9c9d-22ace3df7607",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad504870-603a-4298-b6e4-a501a7b3d962",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a61fcdc8-6d66-4b85-9c9d-22ace3df7607",
                    "LayerId": "ed7291c8-be86-416a-8cc0-61da9ef9a7ac"
                }
            ]
        },
        {
            "id": "415abb97-d56c-4676-a166-a8a8933eb308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6507e6c4-5149-4686-9d5a-2e39335b3974",
            "compositeImage": {
                "id": "deaeacba-3f04-4f68-b07c-c6f0eb74179b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "415abb97-d56c-4676-a166-a8a8933eb308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35124357-3254-43a4-87ca-92f594797258",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "415abb97-d56c-4676-a166-a8a8933eb308",
                    "LayerId": "ed7291c8-be86-416a-8cc0-61da9ef9a7ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "ed7291c8-be86-416a-8cc0-61da9ef9a7ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6507e6c4-5149-4686-9d5a-2e39335b3974",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 154,
    "xorig": 77,
    "yorig": 80
}