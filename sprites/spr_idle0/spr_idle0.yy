{
    "id": "153fce42-6baa-41c2-8d06-0008cc829b23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_idle0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa93ffad-037b-4383-9f1d-e845e8eb00b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "153fce42-6baa-41c2-8d06-0008cc829b23",
            "compositeImage": {
                "id": "4e0d4e1f-aca0-4e88-8e3d-6cd0e95c769a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa93ffad-037b-4383-9f1d-e845e8eb00b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61956cf6-feb0-4bc9-b8e3-35fa9e31c502",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa93ffad-037b-4383-9f1d-e845e8eb00b1",
                    "LayerId": "a4577c99-7696-4abc-9d73-b64f2db3f37f"
                }
            ]
        },
        {
            "id": "6eaa25bf-4466-483a-b81f-646eb78a276f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "153fce42-6baa-41c2-8d06-0008cc829b23",
            "compositeImage": {
                "id": "44487e61-0c31-4a0a-8ec7-a57217a42d06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eaa25bf-4466-483a-b81f-646eb78a276f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b4848f1-7e0f-4a8a-9a7f-92ba4e910729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eaa25bf-4466-483a-b81f-646eb78a276f",
                    "LayerId": "a4577c99-7696-4abc-9d73-b64f2db3f37f"
                }
            ]
        },
        {
            "id": "f2a4f489-37d4-41a0-8dd9-9f6bf2d0c016",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "153fce42-6baa-41c2-8d06-0008cc829b23",
            "compositeImage": {
                "id": "08898abe-39e9-40fe-940c-e93f0e622641",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2a4f489-37d4-41a0-8dd9-9f6bf2d0c016",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50bf7638-8b3d-41a3-941c-0d4f245af685",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2a4f489-37d4-41a0-8dd9-9f6bf2d0c016",
                    "LayerId": "a4577c99-7696-4abc-9d73-b64f2db3f37f"
                }
            ]
        },
        {
            "id": "8fdd9f32-53d1-4277-833a-09fcfdbd7026",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "153fce42-6baa-41c2-8d06-0008cc829b23",
            "compositeImage": {
                "id": "672e1c8d-8f6a-4ba5-a51b-5ccd89d8c086",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fdd9f32-53d1-4277-833a-09fcfdbd7026",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88dd9d22-1804-410d-826a-242c5573cdb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fdd9f32-53d1-4277-833a-09fcfdbd7026",
                    "LayerId": "a4577c99-7696-4abc-9d73-b64f2db3f37f"
                }
            ]
        },
        {
            "id": "ca7af786-fa6b-4b7a-a1ac-b117b26db49d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "153fce42-6baa-41c2-8d06-0008cc829b23",
            "compositeImage": {
                "id": "e9b361a2-6549-4a82-ac1a-27b0bf98d8a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca7af786-fa6b-4b7a-a1ac-b117b26db49d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35d748aa-0a1d-4cc1-ac42-f8e59404b0d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca7af786-fa6b-4b7a-a1ac-b117b26db49d",
                    "LayerId": "a4577c99-7696-4abc-9d73-b64f2db3f37f"
                }
            ]
        },
        {
            "id": "a341c5e7-b6ec-459a-8c77-8af4f425932f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "153fce42-6baa-41c2-8d06-0008cc829b23",
            "compositeImage": {
                "id": "2dae5078-87c1-4489-93cb-e7424352b015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a341c5e7-b6ec-459a-8c77-8af4f425932f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5627b5d8-09e5-407a-be23-1c2313f76c6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a341c5e7-b6ec-459a-8c77-8af4f425932f",
                    "LayerId": "a4577c99-7696-4abc-9d73-b64f2db3f37f"
                }
            ]
        },
        {
            "id": "a9bfbc90-0ac1-44cc-9a09-9672d09148dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "153fce42-6baa-41c2-8d06-0008cc829b23",
            "compositeImage": {
                "id": "5320816f-39ad-4bc0-a3ce-fef8c7b0db90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9bfbc90-0ac1-44cc-9a09-9672d09148dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b484f75-b7b5-4222-893f-c9f66a027f13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9bfbc90-0ac1-44cc-9a09-9672d09148dc",
                    "LayerId": "a4577c99-7696-4abc-9d73-b64f2db3f37f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "a4577c99-7696-4abc-9d73-b64f2db3f37f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "153fce42-6baa-41c2-8d06-0008cc829b23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}