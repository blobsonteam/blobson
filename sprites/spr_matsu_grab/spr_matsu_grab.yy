{
    "id": "565e98ed-fdc0-4500-8885-904d7d6ba91c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_grab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 0,
    "bbox_right": 94,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5dc526c7-1c8d-4dd3-a2a9-245c6b39d69e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "565e98ed-fdc0-4500-8885-904d7d6ba91c",
            "compositeImage": {
                "id": "9ec6eece-098f-4d66-bc11-09152be9b84e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dc526c7-1c8d-4dd3-a2a9-245c6b39d69e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f42ec388-1103-489c-9537-18dbbb7cd997",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dc526c7-1c8d-4dd3-a2a9-245c6b39d69e",
                    "LayerId": "02aae4e4-072a-4629-9d9c-9dbdd535f5e9"
                }
            ]
        },
        {
            "id": "fb37c870-60a5-4adf-8f9d-37d31aa8c130",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "565e98ed-fdc0-4500-8885-904d7d6ba91c",
            "compositeImage": {
                "id": "c0b089c9-16a4-4e39-9f36-9d815ee5f34b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb37c870-60a5-4adf-8f9d-37d31aa8c130",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "005e0418-8fce-47ac-8dd0-8d381cea9eb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb37c870-60a5-4adf-8f9d-37d31aa8c130",
                    "LayerId": "02aae4e4-072a-4629-9d9c-9dbdd535f5e9"
                }
            ]
        },
        {
            "id": "83d798ee-e4af-4044-bd01-f6d2b626015d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "565e98ed-fdc0-4500-8885-904d7d6ba91c",
            "compositeImage": {
                "id": "d36a58eb-8c45-4386-9989-16bdd61e5360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83d798ee-e4af-4044-bd01-f6d2b626015d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4c05d00-e08d-41a9-94d9-cf28f3f3cd22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83d798ee-e4af-4044-bd01-f6d2b626015d",
                    "LayerId": "02aae4e4-072a-4629-9d9c-9dbdd535f5e9"
                }
            ]
        },
        {
            "id": "b840f2fa-367a-44d5-91dd-4c0a483e981a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "565e98ed-fdc0-4500-8885-904d7d6ba91c",
            "compositeImage": {
                "id": "fb1d3f52-29ba-48d2-aed8-004801f992d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b840f2fa-367a-44d5-91dd-4c0a483e981a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f6ab80d-5305-4050-a433-c967be05c0b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b840f2fa-367a-44d5-91dd-4c0a483e981a",
                    "LayerId": "02aae4e4-072a-4629-9d9c-9dbdd535f5e9"
                }
            ]
        },
        {
            "id": "7940a1f2-1712-4cd9-b243-36952ffb7d1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "565e98ed-fdc0-4500-8885-904d7d6ba91c",
            "compositeImage": {
                "id": "26c7946e-6950-4e66-8036-cf1a131bf9d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7940a1f2-1712-4cd9-b243-36952ffb7d1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a261daf-c3f0-4698-832a-ee3f17c23be1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7940a1f2-1712-4cd9-b243-36952ffb7d1b",
                    "LayerId": "02aae4e4-072a-4629-9d9c-9dbdd535f5e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 94,
    "layers": [
        {
            "id": "02aae4e4-072a-4629-9d9c-9dbdd535f5e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "565e98ed-fdc0-4500-8885-904d7d6ba91c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 95,
    "xorig": 31,
    "yorig": 93
}