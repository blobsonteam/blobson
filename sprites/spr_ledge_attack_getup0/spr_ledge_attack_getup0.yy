{
    "id": "893cf26d-75fb-4579-92bc-e18b32526fcf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ledge_attack_getup0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 3,
    "bbox_right": 38,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2697faf0-82a1-4dcd-bc52-3bbf31360dbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "893cf26d-75fb-4579-92bc-e18b32526fcf",
            "compositeImage": {
                "id": "a72e0aa5-6b7a-4356-b55f-6c3b7c3d2ad3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2697faf0-82a1-4dcd-bc52-3bbf31360dbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16effa56-5a0a-4825-9615-0af0cdd5435b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2697faf0-82a1-4dcd-bc52-3bbf31360dbc",
                    "LayerId": "fc82edb6-70bd-4312-bb50-3f91dfdb1706"
                }
            ]
        },
        {
            "id": "f7f03077-2235-4614-8af9-6779e5d9d2a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "893cf26d-75fb-4579-92bc-e18b32526fcf",
            "compositeImage": {
                "id": "39818e43-4f28-4a48-9e4d-e0680048a7b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f03077-2235-4614-8af9-6779e5d9d2a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81746b86-dd74-4ffc-a828-6ad959065c4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f03077-2235-4614-8af9-6779e5d9d2a9",
                    "LayerId": "fc82edb6-70bd-4312-bb50-3f91dfdb1706"
                }
            ]
        },
        {
            "id": "3b8bb045-8c9f-4d1d-8d87-f41b7f568c5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "893cf26d-75fb-4579-92bc-e18b32526fcf",
            "compositeImage": {
                "id": "531b540b-a038-4b80-bdfb-0bbf1e8ac7f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b8bb045-8c9f-4d1d-8d87-f41b7f568c5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ba626eb-4a22-4324-8918-76f14d399fb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b8bb045-8c9f-4d1d-8d87-f41b7f568c5e",
                    "LayerId": "fc82edb6-70bd-4312-bb50-3f91dfdb1706"
                }
            ]
        },
        {
            "id": "e9b3636f-0f95-4332-b153-a8e872b7d719",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "893cf26d-75fb-4579-92bc-e18b32526fcf",
            "compositeImage": {
                "id": "7e0ea27d-e264-4132-bd34-4b8662f479e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9b3636f-0f95-4332-b153-a8e872b7d719",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "add5d6cf-462f-40a6-a488-fdc8c5fd5726",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9b3636f-0f95-4332-b153-a8e872b7d719",
                    "LayerId": "fc82edb6-70bd-4312-bb50-3f91dfdb1706"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fc82edb6-70bd-4312-bb50-3f91dfdb1706",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "893cf26d-75fb-4579-92bc-e18b32526fcf",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 12,
    "yorig": 48
}