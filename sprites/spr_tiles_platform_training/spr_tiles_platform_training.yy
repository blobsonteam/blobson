{
    "id": "1d32e74b-a238-4e7c-a9ed-af6e18022796",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tiles_platform_training",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 32,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b24335f-b75d-4ee1-8d87-244d1c75c992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d32e74b-a238-4e7c-a9ed-af6e18022796",
            "compositeImage": {
                "id": "834911b7-da39-440c-8778-fca7703c7ea7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b24335f-b75d-4ee1-8d87-244d1c75c992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39ae6515-b927-4ad3-83e9-d49adb7838e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b24335f-b75d-4ee1-8d87-244d1c75c992",
                    "LayerId": "c2cfd947-4efc-4489-8bdf-222bdcb1f998"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c2cfd947-4efc-4489-8bdf-222bdcb1f998",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d32e74b-a238-4e7c-a9ed-af6e18022796",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}