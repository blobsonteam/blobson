{
    "id": "eb2bd189-08ea-463c-9388-f63a8ae54c09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p2_marker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 3,
    "bbox_right": 44,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df618f2e-93c9-4adc-b7e7-2ec16fb9fff5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb2bd189-08ea-463c-9388-f63a8ae54c09",
            "compositeImage": {
                "id": "b7256e7a-8989-4776-bca4-c4d320ca3071",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df618f2e-93c9-4adc-b7e7-2ec16fb9fff5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04890dd6-b90f-426a-9758-c5454dcea892",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df618f2e-93c9-4adc-b7e7-2ec16fb9fff5",
                    "LayerId": "ceef50b5-e6e6-474b-a1af-4611eaf685fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "ceef50b5-e6e6-474b-a1af-4611eaf685fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb2bd189-08ea-463c-9388-f63a8ae54c09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}