{
    "id": "935643b2-b521-456e-8c0d-4caffc59e4a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_uairb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 11,
    "bbox_right": 86,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31b1201f-fa49-4981-8614-41d77c63adb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935643b2-b521-456e-8c0d-4caffc59e4a0",
            "compositeImage": {
                "id": "9a6f3a83-37f9-48e7-8ada-e1fcfc192efb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31b1201f-fa49-4981-8614-41d77c63adb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e1d270b-c577-41cf-844e-714620f3ed1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31b1201f-fa49-4981-8614-41d77c63adb1",
                    "LayerId": "106cf7bf-fdb6-4153-8b32-e23c48731927"
                }
            ]
        },
        {
            "id": "9118fd06-6fea-4089-803a-a5851d816953",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935643b2-b521-456e-8c0d-4caffc59e4a0",
            "compositeImage": {
                "id": "c6393512-7352-473d-8ddc-eb13a6f1e133",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9118fd06-6fea-4089-803a-a5851d816953",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d62a7b48-82c2-4226-860f-f95cfcfd6516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9118fd06-6fea-4089-803a-a5851d816953",
                    "LayerId": "106cf7bf-fdb6-4153-8b32-e23c48731927"
                }
            ]
        },
        {
            "id": "93d2eb4f-ce8c-4c1f-b635-1216e2564a33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935643b2-b521-456e-8c0d-4caffc59e4a0",
            "compositeImage": {
                "id": "66d61148-15bb-4cfb-8a71-d39954d387cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93d2eb4f-ce8c-4c1f-b635-1216e2564a33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10d14e02-5564-47d6-af10-8b4e79d5729b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93d2eb4f-ce8c-4c1f-b635-1216e2564a33",
                    "LayerId": "106cf7bf-fdb6-4153-8b32-e23c48731927"
                }
            ]
        },
        {
            "id": "4aeab610-23a4-4715-8edf-1cacdd77fa89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935643b2-b521-456e-8c0d-4caffc59e4a0",
            "compositeImage": {
                "id": "c1400c4e-f8a6-44e3-b962-566e7beafcd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4aeab610-23a4-4715-8edf-1cacdd77fa89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f17b6063-fa8c-4187-a454-8d97e16331e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4aeab610-23a4-4715-8edf-1cacdd77fa89",
                    "LayerId": "106cf7bf-fdb6-4153-8b32-e23c48731927"
                }
            ]
        },
        {
            "id": "df4530f6-eaba-47a6-8387-7e6bf44eddfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935643b2-b521-456e-8c0d-4caffc59e4a0",
            "compositeImage": {
                "id": "11d2504c-f17e-4481-9edb-a44b298e3fe1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df4530f6-eaba-47a6-8387-7e6bf44eddfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0126a1d4-e862-4772-aa08-8bcde8c59442",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df4530f6-eaba-47a6-8387-7e6bf44eddfb",
                    "LayerId": "106cf7bf-fdb6-4153-8b32-e23c48731927"
                }
            ]
        },
        {
            "id": "4b0df99d-47cd-44b8-b260-7bb71b163f56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935643b2-b521-456e-8c0d-4caffc59e4a0",
            "compositeImage": {
                "id": "ab2ac90f-137a-4c87-b8b7-67ae806b0e44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b0df99d-47cd-44b8-b260-7bb71b163f56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b3bf848-36bb-4dfb-b26a-79c38176ff06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b0df99d-47cd-44b8-b260-7bb71b163f56",
                    "LayerId": "106cf7bf-fdb6-4153-8b32-e23c48731927"
                }
            ]
        },
        {
            "id": "17606ab5-d96d-4f0b-9839-4ec2f3c555c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "935643b2-b521-456e-8c0d-4caffc59e4a0",
            "compositeImage": {
                "id": "87275e38-95a2-4724-82c9-3f3be92e1795",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17606ab5-d96d-4f0b-9839-4ec2f3c555c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "459b6432-e93f-4a2e-b986-4266261ecb2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17606ab5-d96d-4f0b-9839-4ec2f3c555c5",
                    "LayerId": "106cf7bf-fdb6-4153-8b32-e23c48731927"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "106cf7bf-fdb6-4153-8b32-e23c48731927",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "935643b2-b521-456e-8c0d-4caffc59e4a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 95,
    "xorig": 32,
    "yorig": 114
}