{
    "id": "98e7be32-eb46-4596-9873-2fa87dc3a321",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_pal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca92fefb-2f74-470c-b5c9-09a113eef38c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98e7be32-eb46-4596-9873-2fa87dc3a321",
            "compositeImage": {
                "id": "61f4c42f-440c-4fe7-8d9a-f6b94915e9e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca92fefb-2f74-470c-b5c9-09a113eef38c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfdaa67b-74a4-446e-97de-c6f62a04b5d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca92fefb-2f74-470c-b5c9-09a113eef38c",
                    "LayerId": "8d401925-b112-4072-a858-c5319de2cd68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "8d401925-b112-4072-a858-c5319de2cd68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98e7be32-eb46-4596-9873-2fa87dc3a321",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}