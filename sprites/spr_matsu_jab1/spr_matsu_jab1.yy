{
    "id": "118757c2-4d93-47b5-8ef6-80abb988fa86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_jab1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 0,
    "bbox_right": 86,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f12db24-ed65-44d9-9957-76056b22336e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "118757c2-4d93-47b5-8ef6-80abb988fa86",
            "compositeImage": {
                "id": "56d470fc-d6a5-4242-927e-743e60e46984",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f12db24-ed65-44d9-9957-76056b22336e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17e9be05-fab7-4973-aa8d-fa7004925af0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f12db24-ed65-44d9-9957-76056b22336e",
                    "LayerId": "9d2b265d-f2fc-4919-9f88-0d28e09a0bb1"
                }
            ]
        },
        {
            "id": "85981eb2-0ddb-4921-afa9-f59079bcc1d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "118757c2-4d93-47b5-8ef6-80abb988fa86",
            "compositeImage": {
                "id": "2b382fbf-edac-4000-8349-83a954281812",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85981eb2-0ddb-4921-afa9-f59079bcc1d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c02942e0-2df9-4524-9f7d-56415886e8de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85981eb2-0ddb-4921-afa9-f59079bcc1d6",
                    "LayerId": "9d2b265d-f2fc-4919-9f88-0d28e09a0bb1"
                }
            ]
        },
        {
            "id": "a60864e5-b481-4d89-9bec-8165fb6e39df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "118757c2-4d93-47b5-8ef6-80abb988fa86",
            "compositeImage": {
                "id": "91b741be-af3b-40fa-aa6f-a6933220c9b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a60864e5-b481-4d89-9bec-8165fb6e39df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39ffb25a-d746-4db8-a02b-9fd5a0af3515",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a60864e5-b481-4d89-9bec-8165fb6e39df",
                    "LayerId": "9d2b265d-f2fc-4919-9f88-0d28e09a0bb1"
                }
            ]
        },
        {
            "id": "733489a1-42dc-4784-99c9-e489d38b3cb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "118757c2-4d93-47b5-8ef6-80abb988fa86",
            "compositeImage": {
                "id": "7aad5185-2786-4e35-b384-a90a1e2117f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "733489a1-42dc-4784-99c9-e489d38b3cb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb7c6340-11eb-42e8-8064-3963470a11f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "733489a1-42dc-4784-99c9-e489d38b3cb4",
                    "LayerId": "9d2b265d-f2fc-4919-9f88-0d28e09a0bb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 95,
    "layers": [
        {
            "id": "9d2b265d-f2fc-4919-9f88-0d28e09a0bb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "118757c2-4d93-47b5-8ef6-80abb988fa86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 87,
    "xorig": 24,
    "yorig": 94
}