{
    "id": "fde56f70-599a-4c54-9b19-004aec9e5341",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p1_marker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 3,
    "bbox_right": 44,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62f073f4-560c-4518-9b0a-05d146c8e372",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde56f70-599a-4c54-9b19-004aec9e5341",
            "compositeImage": {
                "id": "c029f929-7fe3-45f1-ac0a-9f683ff05c05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62f073f4-560c-4518-9b0a-05d146c8e372",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e6437c7-f1da-4e2b-b4a3-cf44d9036b31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62f073f4-560c-4518-9b0a-05d146c8e372",
                    "LayerId": "7d01bcdd-9d4f-4d3b-84a8-316108c7fa6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "7d01bcdd-9d4f-4d3b-84a8-316108c7fa6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fde56f70-599a-4c54-9b19-004aec9e5341",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}