{
    "id": "5fcba92d-a8a4-4a9c-ba96-a592754b1ec1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 145,
    "bbox_left": 13,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f901278-e40e-46aa-b22c-6b2519f2039c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fcba92d-a8a4-4a9c-ba96-a592754b1ec1",
            "compositeImage": {
                "id": "3d8b6d57-33d4-47a0-93ab-3386399a6316",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f901278-e40e-46aa-b22c-6b2519f2039c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1621fee0-835b-4c27-a089-6b5b699ed14b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f901278-e40e-46aa-b22c-6b2519f2039c",
                    "LayerId": "7d6c953c-86b2-4ad3-a6db-88293dbe319b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 146,
    "layers": [
        {
            "id": "7d6c953c-86b2-4ad3-a6db-88293dbe319b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5fcba92d-a8a4-4a9c-ba96-a592754b1ec1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 33,
    "yorig": 145
}