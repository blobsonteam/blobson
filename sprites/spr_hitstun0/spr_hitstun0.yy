{
    "id": "b1804459-19f2-4edc-b518-e4b1dcd8ccda",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hitstun0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 2,
    "bbox_right": 19,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb8c00a5-db75-4351-b7bd-20897c6f4e8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1804459-19f2-4edc-b518-e4b1dcd8ccda",
            "compositeImage": {
                "id": "3a55ad43-9a27-4c04-a8e0-2990685338c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb8c00a5-db75-4351-b7bd-20897c6f4e8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fd5b012-43f3-4710-acd4-531544f8688a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb8c00a5-db75-4351-b7bd-20897c6f4e8d",
                    "LayerId": "3c123ba8-90f7-458b-8041-b903c3e688fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "3c123ba8-90f7-458b-8041-b903c3e688fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1804459-19f2-4edc-b518-e4b1dcd8ccda",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 18
}