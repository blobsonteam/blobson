{
    "id": "cf0ef83f-c3e7-4e72-85a6-3025b4844fb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sarina_cs_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37ab509c-9855-4061-ae45-25a81a646f20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf0ef83f-c3e7-4e72-85a6-3025b4844fb6",
            "compositeImage": {
                "id": "b714e1c1-0902-4398-ba37-6decb3ad1fb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37ab509c-9855-4061-ae45-25a81a646f20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd13eaf-4e3e-4c56-8b3c-a7bbfac8fffd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37ab509c-9855-4061-ae45-25a81a646f20",
                    "LayerId": "fb1b966e-0656-4c85-a54c-01ffe75c1fc5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "fb1b966e-0656-4c85-a54c-01ffe75c1fc5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf0ef83f-c3e7-4e72-85a6-3025b4844fb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}