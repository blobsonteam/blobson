{
    "id": "37b29b90-9d16-40ae-a558-03292d41e9b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tiles_main_training",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 64,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "79f22a60-d2c4-4e99-83ea-a1a4f6d25657",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b29b90-9d16-40ae-a558-03292d41e9b9",
            "compositeImage": {
                "id": "ed8a725b-171c-48e8-bec7-0224acbfb32c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79f22a60-d2c4-4e99-83ea-a1a4f6d25657",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "738df492-d482-40ec-bb2a-92be3ff23725",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79f22a60-d2c4-4e99-83ea-a1a4f6d25657",
                    "LayerId": "91aee57e-ca98-4436-ba51-41929cb3317a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "91aee57e-ca98-4436-ba51-41929cb3317a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37b29b90-9d16-40ae-a558-03292d41e9b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}