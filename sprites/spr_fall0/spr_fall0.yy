{
    "id": "3d611809-6b43-4202-86b0-314b8eda3d5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fall0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22f1e80e-9a02-43be-89fd-c25b6370daba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d611809-6b43-4202-86b0-314b8eda3d5f",
            "compositeImage": {
                "id": "c64d5009-3297-4c92-9294-b80088782542",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22f1e80e-9a02-43be-89fd-c25b6370daba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cdd76ac-a397-4b01-ab47-f2e164fae97d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22f1e80e-9a02-43be-89fd-c25b6370daba",
                    "LayerId": "de7ca4c3-5daf-47f5-b78a-2d281c627c06"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "de7ca4c3-5daf-47f5-b78a-2d281c627c06",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d611809-6b43-4202-86b0-314b8eda3d5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}