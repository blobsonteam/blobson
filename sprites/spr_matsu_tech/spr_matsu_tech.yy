{
    "id": "543dd210-20d8-4f4c-9a11-ba29e78673bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_tech",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 54,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8022105e-2286-4630-91e7-65c4ed42972b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "543dd210-20d8-4f4c-9a11-ba29e78673bf",
            "compositeImage": {
                "id": "59f6e319-db3b-438a-aec6-fdfdd37960e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8022105e-2286-4630-91e7-65c4ed42972b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1240fa4b-a11a-4091-a372-b3bb906df7da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8022105e-2286-4630-91e7-65c4ed42972b",
                    "LayerId": "66475d09-f023-49c1-b9b1-3d7be4101959"
                }
            ]
        },
        {
            "id": "091bfe24-41df-4676-9030-eeddffb0e3de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "543dd210-20d8-4f4c-9a11-ba29e78673bf",
            "compositeImage": {
                "id": "8c0df5e1-3d25-4381-908d-9ee11fd0dfda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "091bfe24-41df-4676-9030-eeddffb0e3de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7e8deb3-9fd0-488c-bd16-3774f66406dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "091bfe24-41df-4676-9030-eeddffb0e3de",
                    "LayerId": "66475d09-f023-49c1-b9b1-3d7be4101959"
                }
            ]
        },
        {
            "id": "03ca052d-4952-46b2-8de8-ce84368a2e7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "543dd210-20d8-4f4c-9a11-ba29e78673bf",
            "compositeImage": {
                "id": "376da6c3-0e94-4737-85d6-96827a2eeed6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03ca052d-4952-46b2-8de8-ce84368a2e7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84a98b52-475d-4266-93ab-f13f20969a64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03ca052d-4952-46b2-8de8-ce84368a2e7b",
                    "LayerId": "66475d09-f023-49c1-b9b1-3d7be4101959"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "66475d09-f023-49c1-b9b1-3d7be4101959",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "543dd210-20d8-4f4c-9a11-ba29e78673bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 25,
    "yorig": 76
}