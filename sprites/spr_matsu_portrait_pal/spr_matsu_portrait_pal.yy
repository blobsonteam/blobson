{
    "id": "cd90c6c7-1294-4e88-9b57-8a02252f941f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_portrait_pal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d743e14-6223-4a49-b835-c4465cd3ce7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd90c6c7-1294-4e88-9b57-8a02252f941f",
            "compositeImage": {
                "id": "034d1c2f-676b-4c0a-b21f-d45d85433e68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d743e14-6223-4a49-b835-c4465cd3ce7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "832c92aa-561c-4665-abd0-2f1b91bbdbb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d743e14-6223-4a49-b835-c4465cd3ce7c",
                    "LayerId": "a1d1b2f7-044c-43e7-a65b-9eb5d36a28e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "a1d1b2f7-044c-43e7-a65b-9eb5d36a28e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd90c6c7-1294-4e88-9b57-8a02252f941f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}