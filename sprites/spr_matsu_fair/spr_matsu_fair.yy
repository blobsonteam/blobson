{
    "id": "b9f99920-7867-4de3-97e2-697a5ed3df06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_fair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 0,
    "bbox_right": 97,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f958b81b-5bdb-472f-988f-42cd53270988",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9f99920-7867-4de3-97e2-697a5ed3df06",
            "compositeImage": {
                "id": "8fe67620-4dde-4f1f-b87e-c5d9b47fd9e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f958b81b-5bdb-472f-988f-42cd53270988",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeed702c-1187-45dd-808d-ce8fe0e6cab2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f958b81b-5bdb-472f-988f-42cd53270988",
                    "LayerId": "974ae3e7-ec95-4053-a592-b51e5711621b"
                }
            ]
        },
        {
            "id": "83c85f55-950b-40df-ac98-004410ab12a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9f99920-7867-4de3-97e2-697a5ed3df06",
            "compositeImage": {
                "id": "f63a9292-529c-4b0b-aaf0-bbce9cb644fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83c85f55-950b-40df-ac98-004410ab12a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f84db9a5-a9c4-412a-8ebe-750518c1167e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83c85f55-950b-40df-ac98-004410ab12a7",
                    "LayerId": "974ae3e7-ec95-4053-a592-b51e5711621b"
                }
            ]
        },
        {
            "id": "04dbefa0-c606-4fd8-a8ae-7e248d3f97cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9f99920-7867-4de3-97e2-697a5ed3df06",
            "compositeImage": {
                "id": "ad973c5b-4086-46ff-82b1-90ba579c3a94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04dbefa0-c606-4fd8-a8ae-7e248d3f97cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7cc480e-b8fa-4a77-9937-d9f334c7416a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04dbefa0-c606-4fd8-a8ae-7e248d3f97cc",
                    "LayerId": "974ae3e7-ec95-4053-a592-b51e5711621b"
                }
            ]
        },
        {
            "id": "c2a841e1-a1c8-41cf-acf0-cecdaf40ec75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9f99920-7867-4de3-97e2-697a5ed3df06",
            "compositeImage": {
                "id": "573132bd-42be-40b3-a8e3-5e5283c6449a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2a841e1-a1c8-41cf-acf0-cecdaf40ec75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2728f69-23ab-44f9-bef4-4f9c7c4bc6f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2a841e1-a1c8-41cf-acf0-cecdaf40ec75",
                    "LayerId": "974ae3e7-ec95-4053-a592-b51e5711621b"
                }
            ]
        },
        {
            "id": "373eaf3f-c859-478f-be2e-152e62dece9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9f99920-7867-4de3-97e2-697a5ed3df06",
            "compositeImage": {
                "id": "babb1245-bd34-4327-a590-19a8086f513b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "373eaf3f-c859-478f-be2e-152e62dece9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d2ea59b-e9e8-450d-a5ce-cb4d57cc91bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "373eaf3f-c859-478f-be2e-152e62dece9d",
                    "LayerId": "974ae3e7-ec95-4053-a592-b51e5711621b"
                }
            ]
        },
        {
            "id": "6340f2a4-a672-41d9-a4d3-01707e138ca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9f99920-7867-4de3-97e2-697a5ed3df06",
            "compositeImage": {
                "id": "84d99abe-924a-483c-8204-470aebcf68b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6340f2a4-a672-41d9-a4d3-01707e138ca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe153aef-6dd3-4997-91d2-968bbb6d5d14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6340f2a4-a672-41d9-a4d3-01707e138ca9",
                    "LayerId": "974ae3e7-ec95-4053-a592-b51e5711621b"
                }
            ]
        },
        {
            "id": "1a147642-c298-40ec-96a3-40300f783e4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9f99920-7867-4de3-97e2-697a5ed3df06",
            "compositeImage": {
                "id": "01549a4d-2919-411a-88b9-32fce6929dc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a147642-c298-40ec-96a3-40300f783e4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "837c7375-e588-469b-95ee-f7ff287ec21f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a147642-c298-40ec-96a3-40300f783e4d",
                    "LayerId": "974ae3e7-ec95-4053-a592-b51e5711621b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 121,
    "layers": [
        {
            "id": "974ae3e7-ec95-4053-a592-b51e5711621b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9f99920-7867-4de3-97e2-697a5ed3df06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 98,
    "xorig": 42,
    "yorig": 113
}