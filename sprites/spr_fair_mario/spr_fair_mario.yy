{
    "id": "1e450fc0-afd3-4199-8ae8-7270d3b96e40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fair_mario",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 45,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ce9ee05-1bed-45f0-84d3-3097dde406ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e450fc0-afd3-4199-8ae8-7270d3b96e40",
            "compositeImage": {
                "id": "a5d480a7-41f8-4067-8ee8-3a6d0cb28fd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ce9ee05-1bed-45f0-84d3-3097dde406ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "938d7d71-b4eb-4838-80df-6f362ee7449e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ce9ee05-1bed-45f0-84d3-3097dde406ba",
                    "LayerId": "5d66b0b3-ac6d-4a35-961b-a89c81fbd786"
                },
                {
                    "id": "d5d79baf-1e0a-421a-abbc-b39a39fb8699",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ce9ee05-1bed-45f0-84d3-3097dde406ba",
                    "LayerId": "61be0bf8-684c-4645-ba36-1f7c5f9a7b38"
                },
                {
                    "id": "e914886c-d2c7-4cbb-930c-c7549459d9c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ce9ee05-1bed-45f0-84d3-3097dde406ba",
                    "LayerId": "bef42240-4927-45fa-9452-25eea7a0a980"
                }
            ]
        },
        {
            "id": "7762128f-d27f-4500-87db-e24449971f91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e450fc0-afd3-4199-8ae8-7270d3b96e40",
            "compositeImage": {
                "id": "e14dcde1-6b0e-40d4-91a8-fb01586062f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7762128f-d27f-4500-87db-e24449971f91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcd53e8a-b6cc-49de-bcbb-cfed107f4580",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7762128f-d27f-4500-87db-e24449971f91",
                    "LayerId": "5d66b0b3-ac6d-4a35-961b-a89c81fbd786"
                },
                {
                    "id": "d5f695e2-8f99-4320-aa2f-5bf28d1559ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7762128f-d27f-4500-87db-e24449971f91",
                    "LayerId": "61be0bf8-684c-4645-ba36-1f7c5f9a7b38"
                },
                {
                    "id": "4dd45e88-92fa-4673-a0b2-7809500cc0aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7762128f-d27f-4500-87db-e24449971f91",
                    "LayerId": "bef42240-4927-45fa-9452-25eea7a0a980"
                }
            ]
        },
        {
            "id": "658febfb-f0e0-45d7-8f6f-c8607473501e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e450fc0-afd3-4199-8ae8-7270d3b96e40",
            "compositeImage": {
                "id": "9f80d716-f8a3-46de-a0c7-fdae273c6b98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "658febfb-f0e0-45d7-8f6f-c8607473501e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da3e54eb-cc74-40d6-a6bc-17f4d825ec26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "658febfb-f0e0-45d7-8f6f-c8607473501e",
                    "LayerId": "5d66b0b3-ac6d-4a35-961b-a89c81fbd786"
                },
                {
                    "id": "ee5e4881-6102-467e-9847-87f73beb61e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "658febfb-f0e0-45d7-8f6f-c8607473501e",
                    "LayerId": "61be0bf8-684c-4645-ba36-1f7c5f9a7b38"
                },
                {
                    "id": "313bbb17-55fd-47b3-989a-d51216f69ab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "658febfb-f0e0-45d7-8f6f-c8607473501e",
                    "LayerId": "bef42240-4927-45fa-9452-25eea7a0a980"
                }
            ]
        },
        {
            "id": "93aa6601-9cd4-49f7-a25a-813b3a56127e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e450fc0-afd3-4199-8ae8-7270d3b96e40",
            "compositeImage": {
                "id": "34c3cafe-1bb8-4fa9-a13c-563c3ba3727e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93aa6601-9cd4-49f7-a25a-813b3a56127e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a563cf5-cb4c-477f-abdd-4c7b8c49aa97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93aa6601-9cd4-49f7-a25a-813b3a56127e",
                    "LayerId": "61be0bf8-684c-4645-ba36-1f7c5f9a7b38"
                },
                {
                    "id": "852d95f7-eb2e-47be-92ca-4016f5a6c191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93aa6601-9cd4-49f7-a25a-813b3a56127e",
                    "LayerId": "5d66b0b3-ac6d-4a35-961b-a89c81fbd786"
                },
                {
                    "id": "cf456ec1-1890-428a-abc8-76aa0fc5994a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93aa6601-9cd4-49f7-a25a-813b3a56127e",
                    "LayerId": "bef42240-4927-45fa-9452-25eea7a0a980"
                }
            ]
        },
        {
            "id": "1d624b1a-0b3a-4f5e-a452-0c5280e60e4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e450fc0-afd3-4199-8ae8-7270d3b96e40",
            "compositeImage": {
                "id": "4a6d7852-8aaa-423e-92cd-7006f78e73e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d624b1a-0b3a-4f5e-a452-0c5280e60e4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7caacb3-9682-4953-95de-effd8b6d2ebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d624b1a-0b3a-4f5e-a452-0c5280e60e4f",
                    "LayerId": "61be0bf8-684c-4645-ba36-1f7c5f9a7b38"
                },
                {
                    "id": "27297957-aa39-49bd-8394-23f2c91c715e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d624b1a-0b3a-4f5e-a452-0c5280e60e4f",
                    "LayerId": "5d66b0b3-ac6d-4a35-961b-a89c81fbd786"
                },
                {
                    "id": "919b0e96-ab47-4c1b-adf7-e6aa79e2a324",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d624b1a-0b3a-4f5e-a452-0c5280e60e4f",
                    "LayerId": "bef42240-4927-45fa-9452-25eea7a0a980"
                }
            ]
        },
        {
            "id": "92087eb9-2628-44fc-9b60-b29ba7c58ff5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e450fc0-afd3-4199-8ae8-7270d3b96e40",
            "compositeImage": {
                "id": "3b3712b8-fbc4-4a4b-a43a-a3c5734caeef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92087eb9-2628-44fc-9b60-b29ba7c58ff5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f0ea20e-16f7-4197-8536-7da0a6ed9c74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92087eb9-2628-44fc-9b60-b29ba7c58ff5",
                    "LayerId": "61be0bf8-684c-4645-ba36-1f7c5f9a7b38"
                },
                {
                    "id": "5ed75d89-d88f-4f86-8864-18899965e002",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92087eb9-2628-44fc-9b60-b29ba7c58ff5",
                    "LayerId": "5d66b0b3-ac6d-4a35-961b-a89c81fbd786"
                },
                {
                    "id": "4b1c3e3b-42ab-4868-a7ac-bfb50c2a9cdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92087eb9-2628-44fc-9b60-b29ba7c58ff5",
                    "LayerId": "bef42240-4927-45fa-9452-25eea7a0a980"
                }
            ]
        },
        {
            "id": "3e91a46e-09a3-40a7-9788-5e798bba6c5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e450fc0-afd3-4199-8ae8-7270d3b96e40",
            "compositeImage": {
                "id": "9ee8deeb-507d-4d6e-bc21-f1a89f0c7ca6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e91a46e-09a3-40a7-9788-5e798bba6c5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d682687f-a1a0-4a0d-9682-38aa5d70c4ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e91a46e-09a3-40a7-9788-5e798bba6c5b",
                    "LayerId": "61be0bf8-684c-4645-ba36-1f7c5f9a7b38"
                },
                {
                    "id": "130a4fa4-4612-420b-b860-e021d8c1c7e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e91a46e-09a3-40a7-9788-5e798bba6c5b",
                    "LayerId": "5d66b0b3-ac6d-4a35-961b-a89c81fbd786"
                },
                {
                    "id": "93847a20-1f01-47fb-b104-d4caf0a8a58b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e91a46e-09a3-40a7-9788-5e798bba6c5b",
                    "LayerId": "bef42240-4927-45fa-9452-25eea7a0a980"
                }
            ]
        },
        {
            "id": "0f569dfb-3be9-4d85-8275-f7ccce72d1a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e450fc0-afd3-4199-8ae8-7270d3b96e40",
            "compositeImage": {
                "id": "d1de7007-5302-4812-af3e-2b818952a235",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f569dfb-3be9-4d85-8275-f7ccce72d1a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33bdd6e8-0d46-40c4-b709-afbf544b7b52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f569dfb-3be9-4d85-8275-f7ccce72d1a3",
                    "LayerId": "61be0bf8-684c-4645-ba36-1f7c5f9a7b38"
                },
                {
                    "id": "e237b191-cb85-45d5-82f3-26e4ba358d68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f569dfb-3be9-4d85-8275-f7ccce72d1a3",
                    "LayerId": "5d66b0b3-ac6d-4a35-961b-a89c81fbd786"
                },
                {
                    "id": "a8f86fba-fe19-4578-b529-4b9bc5c3e0d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f569dfb-3be9-4d85-8275-f7ccce72d1a3",
                    "LayerId": "bef42240-4927-45fa-9452-25eea7a0a980"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bef42240-4927-45fa-9452-25eea7a0a980",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e450fc0-afd3-4199-8ae8-7270d3b96e40",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "61be0bf8-684c-4645-ba36-1f7c5f9a7b38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e450fc0-afd3-4199-8ae8-7270d3b96e40",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5d66b0b3-ac6d-4a35-961b-a89c81fbd786",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e450fc0-afd3-4199-8ae8-7270d3b96e40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 18
}