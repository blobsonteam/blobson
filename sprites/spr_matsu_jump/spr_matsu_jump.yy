{
    "id": "6205a695-2e8c-42ec-ade2-0a30c59f24ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 145,
    "bbox_left": 10,
    "bbox_right": 62,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28816ed4-2edb-48d3-a526-fcac3df160f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6205a695-2e8c-42ec-ade2-0a30c59f24ba",
            "compositeImage": {
                "id": "acbbbedc-30ff-41ec-8527-7a0d9ee1abd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28816ed4-2edb-48d3-a526-fcac3df160f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fb3cb5d-9c27-4dfd-8cf8-9dced611e778",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28816ed4-2edb-48d3-a526-fcac3df160f6",
                    "LayerId": "11bdc22f-b02d-40d2-8793-b8fa512483dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 146,
    "layers": [
        {
            "id": "11bdc22f-b02d-40d2-8793-b8fa512483dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6205a695-2e8c-42ec-ade2-0a30c59f24ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 33,
    "yorig": 145
}