{
    "id": "b2a5aa3e-d990-4fe7-8348-64aa903a26ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_landlag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 3,
    "bbox_right": 50,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "542fb2c2-7227-45c7-9844-bef079fbacd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2a5aa3e-d990-4fe7-8348-64aa903a26ac",
            "compositeImage": {
                "id": "750a01aa-3fa9-4f8d-aad8-c686a8125589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "542fb2c2-7227-45c7-9844-bef079fbacd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "221f48fa-c7a9-4647-8fe4-c54676595723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "542fb2c2-7227-45c7-9844-bef079fbacd9",
                    "LayerId": "4f8fa0c3-a3fb-428c-a18a-29fd83524e86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "4f8fa0c3-a3fb-428c-a18a-29fd83524e86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2a5aa3e-d990-4fe7-8348-64aa903a26ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 22,
    "yorig": 76
}