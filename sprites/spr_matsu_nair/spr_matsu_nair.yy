{
    "id": "1a6a99d7-9990-4a71-9574-3139c583a06e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_nair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 65,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6aab9ed-90f8-442e-aca1-a8c1beca6ba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a6a99d7-9990-4a71-9574-3139c583a06e",
            "compositeImage": {
                "id": "87e3b654-0d69-42f4-81c7-cb283b7f902f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6aab9ed-90f8-442e-aca1-a8c1beca6ba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a60caced-6f63-47b1-b73b-30bb717bd628",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6aab9ed-90f8-442e-aca1-a8c1beca6ba5",
                    "LayerId": "ed942ef1-1474-42d5-acef-a460157bdce9"
                }
            ]
        },
        {
            "id": "2debcd58-27bf-4ebc-9c13-36aa93994211",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a6a99d7-9990-4a71-9574-3139c583a06e",
            "compositeImage": {
                "id": "5dd7e2d8-fffb-4c26-b2bc-c139ec891274",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2debcd58-27bf-4ebc-9c13-36aa93994211",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f76937ef-231b-4c46-acbe-5a3568ff51dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2debcd58-27bf-4ebc-9c13-36aa93994211",
                    "LayerId": "ed942ef1-1474-42d5-acef-a460157bdce9"
                }
            ]
        },
        {
            "id": "49584ad2-6f0e-4044-a882-6d069b78dfa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a6a99d7-9990-4a71-9574-3139c583a06e",
            "compositeImage": {
                "id": "209e3f7a-0742-4e47-a447-dd1b9707bf5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49584ad2-6f0e-4044-a882-6d069b78dfa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3d9bee7-0143-4561-acfa-921413ecbaa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49584ad2-6f0e-4044-a882-6d069b78dfa8",
                    "LayerId": "ed942ef1-1474-42d5-acef-a460157bdce9"
                }
            ]
        },
        {
            "id": "00aa5077-ce99-4f9d-9736-a26fbcfd2f76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a6a99d7-9990-4a71-9574-3139c583a06e",
            "compositeImage": {
                "id": "87a5243d-6287-442f-8a5e-3b4b60c4911d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00aa5077-ce99-4f9d-9736-a26fbcfd2f76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c6025a4-e98b-495d-8ddc-48d286991033",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00aa5077-ce99-4f9d-9736-a26fbcfd2f76",
                    "LayerId": "ed942ef1-1474-42d5-acef-a460157bdce9"
                }
            ]
        },
        {
            "id": "ff6da7b5-fcec-4868-9995-c34582207341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a6a99d7-9990-4a71-9574-3139c583a06e",
            "compositeImage": {
                "id": "759d0fbd-2753-45af-855a-d599dbf97ceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff6da7b5-fcec-4868-9995-c34582207341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d61c6ba3-96b0-4065-951d-6e39b239cf5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff6da7b5-fcec-4868-9995-c34582207341",
                    "LayerId": "ed942ef1-1474-42d5-acef-a460157bdce9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "ed942ef1-1474-42d5-acef-a460157bdce9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a6a99d7-9990-4a71-9574-3139c583a06e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 34,
    "yorig": 99
}