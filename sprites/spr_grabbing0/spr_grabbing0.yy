{
    "id": "d74b1f84-9531-42d5-8686-ba152f88a88a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grabbing0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c52b2a94-61b4-4da2-8454-e0c245884feb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d74b1f84-9531-42d5-8686-ba152f88a88a",
            "compositeImage": {
                "id": "3699db24-be83-4fca-9164-6451ab85372b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c52b2a94-61b4-4da2-8454-e0c245884feb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d0dd4d3-60f4-43ed-af57-580bbb7ceaa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c52b2a94-61b4-4da2-8454-e0c245884feb",
                    "LayerId": "7b357b96-bb7f-44f8-8a57-097df8eaef94"
                }
            ]
        },
        {
            "id": "2706bd1e-b561-479f-9bbb-9bff34c4ba2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d74b1f84-9531-42d5-8686-ba152f88a88a",
            "compositeImage": {
                "id": "0a4a2332-db3b-42d7-9560-e57d24defef1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2706bd1e-b561-479f-9bbb-9bff34c4ba2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "813c165f-d2e7-4003-942f-6dc2c68186e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2706bd1e-b561-479f-9bbb-9bff34c4ba2b",
                    "LayerId": "7b357b96-bb7f-44f8-8a57-097df8eaef94"
                }
            ]
        },
        {
            "id": "cc1052e5-be46-4a0b-9907-445277554b57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d74b1f84-9531-42d5-8686-ba152f88a88a",
            "compositeImage": {
                "id": "0ac7af36-1a24-4dbe-aff5-7c5c078f785e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc1052e5-be46-4a0b-9907-445277554b57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de3caa98-9966-4e53-a234-87a30d80ba77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc1052e5-be46-4a0b-9907-445277554b57",
                    "LayerId": "7b357b96-bb7f-44f8-8a57-097df8eaef94"
                }
            ]
        },
        {
            "id": "e1f5bf6a-2cc2-4d77-a92e-59eb2a25e701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d74b1f84-9531-42d5-8686-ba152f88a88a",
            "compositeImage": {
                "id": "62130bd3-3ca0-4081-9034-38d3512e1fca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1f5bf6a-2cc2-4d77-a92e-59eb2a25e701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5241bfe-ed5f-4360-8475-df3e1c6b9bf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1f5bf6a-2cc2-4d77-a92e-59eb2a25e701",
                    "LayerId": "7b357b96-bb7f-44f8-8a57-097df8eaef94"
                }
            ]
        },
        {
            "id": "e0473c5e-a371-4977-b145-2ecc3799f0b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d74b1f84-9531-42d5-8686-ba152f88a88a",
            "compositeImage": {
                "id": "bb0e152d-a669-41e5-a11a-61f669ef66b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0473c5e-a371-4977-b145-2ecc3799f0b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a16532eb-b91f-4b86-aef2-0af0069fef24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0473c5e-a371-4977-b145-2ecc3799f0b4",
                    "LayerId": "7b357b96-bb7f-44f8-8a57-097df8eaef94"
                }
            ]
        },
        {
            "id": "12acf041-be34-4b14-b79d-3aeb7e369faf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d74b1f84-9531-42d5-8686-ba152f88a88a",
            "compositeImage": {
                "id": "475a0c42-7fcc-4282-b9af-cb5f0f062189",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12acf041-be34-4b14-b79d-3aeb7e369faf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "023fbb7d-91c9-4414-a5d6-464a8fe3c7db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12acf041-be34-4b14-b79d-3aeb7e369faf",
                    "LayerId": "7b357b96-bb7f-44f8-8a57-097df8eaef94"
                }
            ]
        },
        {
            "id": "1a557b8a-64dd-41a4-b42c-0ddc1039b80d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d74b1f84-9531-42d5-8686-ba152f88a88a",
            "compositeImage": {
                "id": "8b202079-50dd-4c50-81fa-ad667baf92db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a557b8a-64dd-41a4-b42c-0ddc1039b80d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4db72c6-9652-4d6c-a02a-b60e6a80bdfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a557b8a-64dd-41a4-b42c-0ddc1039b80d",
                    "LayerId": "7b357b96-bb7f-44f8-8a57-097df8eaef94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "7b357b96-bb7f-44f8-8a57-097df8eaef94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d74b1f84-9531-42d5-8686-ba152f88a88a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 8,
    "yorig": 12
}