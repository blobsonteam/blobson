{
    "id": "d379671e-ba98-4399-873f-f9e6e133b170",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dthrow0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1cda1f3a-fab2-4af5-932c-a926c4effbee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "compositeImage": {
                "id": "bf9c852b-550c-4317-b695-256db78e3e70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cda1f3a-fab2-4af5-932c-a926c4effbee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daa85957-860f-4849-ae06-2dc6eb07f39a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cda1f3a-fab2-4af5-932c-a926c4effbee",
                    "LayerId": "108a5997-50b2-49b9-a993-f1e16f1fd334"
                },
                {
                    "id": "8d4e76ab-3b03-41e7-b62c-9648abda5703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cda1f3a-fab2-4af5-932c-a926c4effbee",
                    "LayerId": "475a04db-05db-4e8f-bb78-696151cb7be4"
                }
            ]
        },
        {
            "id": "337958f1-f5bc-4921-9e38-1a461f2168c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "compositeImage": {
                "id": "5309f465-6ad2-4c6e-a941-24b5115b45df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "337958f1-f5bc-4921-9e38-1a461f2168c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46403173-b5f8-424a-9955-f3b6a534c45e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "337958f1-f5bc-4921-9e38-1a461f2168c1",
                    "LayerId": "108a5997-50b2-49b9-a993-f1e16f1fd334"
                },
                {
                    "id": "3a29de64-d2b6-4b16-a071-3f1d76c34842",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "337958f1-f5bc-4921-9e38-1a461f2168c1",
                    "LayerId": "475a04db-05db-4e8f-bb78-696151cb7be4"
                }
            ]
        },
        {
            "id": "c423671e-7dc9-4392-ab7f-9090e23c986f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "compositeImage": {
                "id": "5780e29e-af87-4cf0-af11-b20fb8079ebd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c423671e-7dc9-4392-ab7f-9090e23c986f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ab66975-ce16-4b31-a81e-6b281e08a876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c423671e-7dc9-4392-ab7f-9090e23c986f",
                    "LayerId": "108a5997-50b2-49b9-a993-f1e16f1fd334"
                },
                {
                    "id": "9e6e2976-53e1-461c-9e0f-48aac4a1a9f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c423671e-7dc9-4392-ab7f-9090e23c986f",
                    "LayerId": "475a04db-05db-4e8f-bb78-696151cb7be4"
                }
            ]
        },
        {
            "id": "dae2230a-4664-457d-b16e-55c7969e211b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "compositeImage": {
                "id": "9c5360ab-7f19-4ad3-84eb-78764a079471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dae2230a-4664-457d-b16e-55c7969e211b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d32e41a6-3695-488c-8e9a-45d902957645",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dae2230a-4664-457d-b16e-55c7969e211b",
                    "LayerId": "108a5997-50b2-49b9-a993-f1e16f1fd334"
                },
                {
                    "id": "02275077-57cb-4226-aac7-3a2ceaabb65e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dae2230a-4664-457d-b16e-55c7969e211b",
                    "LayerId": "475a04db-05db-4e8f-bb78-696151cb7be4"
                }
            ]
        },
        {
            "id": "a83e5236-4d74-4e2b-ba02-4544ca9d6e02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "compositeImage": {
                "id": "16030b89-87eb-4e3f-ada6-55865458b914",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a83e5236-4d74-4e2b-ba02-4544ca9d6e02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e6eaaf2-0786-4737-a467-0df3bf64b59c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a83e5236-4d74-4e2b-ba02-4544ca9d6e02",
                    "LayerId": "108a5997-50b2-49b9-a993-f1e16f1fd334"
                },
                {
                    "id": "da03b89b-8c88-49f5-ae0b-5fb0c693be35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a83e5236-4d74-4e2b-ba02-4544ca9d6e02",
                    "LayerId": "475a04db-05db-4e8f-bb78-696151cb7be4"
                }
            ]
        },
        {
            "id": "37b354c5-6a6c-42d2-97fe-716f044e838c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "compositeImage": {
                "id": "933c2bd9-8e53-4620-a738-703f71f1e554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37b354c5-6a6c-42d2-97fe-716f044e838c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "453b1ac1-7c09-4da4-bbd0-a1911f5e636c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37b354c5-6a6c-42d2-97fe-716f044e838c",
                    "LayerId": "108a5997-50b2-49b9-a993-f1e16f1fd334"
                },
                {
                    "id": "cbf09615-3173-44d1-9188-daa10fd95929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37b354c5-6a6c-42d2-97fe-716f044e838c",
                    "LayerId": "475a04db-05db-4e8f-bb78-696151cb7be4"
                }
            ]
        },
        {
            "id": "67d7d8cc-3694-4c1c-b99e-6e2dd39e09b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "compositeImage": {
                "id": "99f26f5d-c5ff-4ca7-9220-cd6ec1a8ed2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67d7d8cc-3694-4c1c-b99e-6e2dd39e09b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c391c9c-00d3-4997-bbd4-efba60b90e2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67d7d8cc-3694-4c1c-b99e-6e2dd39e09b8",
                    "LayerId": "108a5997-50b2-49b9-a993-f1e16f1fd334"
                },
                {
                    "id": "93466604-3f2c-48ac-8f8f-03ee3c889b0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67d7d8cc-3694-4c1c-b99e-6e2dd39e09b8",
                    "LayerId": "475a04db-05db-4e8f-bb78-696151cb7be4"
                }
            ]
        },
        {
            "id": "d5efe7d8-81bb-4087-b4c1-072020a7183f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "compositeImage": {
                "id": "e558a1e4-f7ff-4905-bd1d-6313fe0a83ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5efe7d8-81bb-4087-b4c1-072020a7183f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5d06be6-c639-434b-ad37-3aa737bb2ba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5efe7d8-81bb-4087-b4c1-072020a7183f",
                    "LayerId": "108a5997-50b2-49b9-a993-f1e16f1fd334"
                },
                {
                    "id": "0ee038c2-4604-482f-ac39-a32a0bfa4e6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5efe7d8-81bb-4087-b4c1-072020a7183f",
                    "LayerId": "475a04db-05db-4e8f-bb78-696151cb7be4"
                }
            ]
        },
        {
            "id": "10e53dd3-f32d-4e9c-865f-6f7867be7e67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "compositeImage": {
                "id": "3aef32a4-8995-49c1-a535-8d52618014e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10e53dd3-f32d-4e9c-865f-6f7867be7e67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2aaadf8-a9f8-44fb-ad5f-efd27f4ddcab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10e53dd3-f32d-4e9c-865f-6f7867be7e67",
                    "LayerId": "108a5997-50b2-49b9-a993-f1e16f1fd334"
                },
                {
                    "id": "6291e21f-170f-4ee7-8cbe-fb83cf6675da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10e53dd3-f32d-4e9c-865f-6f7867be7e67",
                    "LayerId": "475a04db-05db-4e8f-bb78-696151cb7be4"
                }
            ]
        },
        {
            "id": "e205b91e-6bdd-4451-a384-d6f5d7a27e85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "compositeImage": {
                "id": "75d4c63f-a684-499c-bc5e-dfa3477a785b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e205b91e-6bdd-4451-a384-d6f5d7a27e85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c002edfd-1a50-4446-95ef-b35f3110255f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e205b91e-6bdd-4451-a384-d6f5d7a27e85",
                    "LayerId": "108a5997-50b2-49b9-a993-f1e16f1fd334"
                },
                {
                    "id": "a31d73dc-622e-4fcd-890a-838a3a5878fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e205b91e-6bdd-4451-a384-d6f5d7a27e85",
                    "LayerId": "475a04db-05db-4e8f-bb78-696151cb7be4"
                }
            ]
        },
        {
            "id": "bf9c659f-97c8-4fcf-8403-7cb0d9796669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "compositeImage": {
                "id": "e7239e70-c4f2-4460-84af-8802e4973ecd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf9c659f-97c8-4fcf-8403-7cb0d9796669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1a216c4-4cec-4c1e-8d4a-f25bf30c49ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf9c659f-97c8-4fcf-8403-7cb0d9796669",
                    "LayerId": "108a5997-50b2-49b9-a993-f1e16f1fd334"
                },
                {
                    "id": "52d71f36-7802-44b6-ad12-7b4f5e3bd7d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf9c659f-97c8-4fcf-8403-7cb0d9796669",
                    "LayerId": "475a04db-05db-4e8f-bb78-696151cb7be4"
                }
            ]
        },
        {
            "id": "bfb63ee1-9ff2-4738-a462-f22b9bac64f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "compositeImage": {
                "id": "9f35a506-ab35-41f2-9d95-677e96651e05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfb63ee1-9ff2-4738-a462-f22b9bac64f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cabd2732-f922-45bb-bd97-34edb6f148f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfb63ee1-9ff2-4738-a462-f22b9bac64f4",
                    "LayerId": "108a5997-50b2-49b9-a993-f1e16f1fd334"
                },
                {
                    "id": "13f49aa0-e31d-422d-988f-28e55bc56948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfb63ee1-9ff2-4738-a462-f22b9bac64f4",
                    "LayerId": "475a04db-05db-4e8f-bb78-696151cb7be4"
                }
            ]
        },
        {
            "id": "6ec18624-5e40-49bd-999b-f955ed5ab62d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "compositeImage": {
                "id": "73ad146c-6902-477f-bd5d-5bef91cc9030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ec18624-5e40-49bd-999b-f955ed5ab62d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cf3ca47-fbb6-4c6b-96a2-228fd3957536",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ec18624-5e40-49bd-999b-f955ed5ab62d",
                    "LayerId": "108a5997-50b2-49b9-a993-f1e16f1fd334"
                },
                {
                    "id": "49cd5dcc-288e-40a0-8475-e2b2227240fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ec18624-5e40-49bd-999b-f955ed5ab62d",
                    "LayerId": "475a04db-05db-4e8f-bb78-696151cb7be4"
                }
            ]
        },
        {
            "id": "db1053d1-aad3-4659-a0c5-4d0633d51c2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "compositeImage": {
                "id": "e612a5dd-8700-4aad-8bdf-d3e6ad12c213",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db1053d1-aad3-4659-a0c5-4d0633d51c2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cba9c1a-8311-4ec2-8d6a-9fba438f7248",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db1053d1-aad3-4659-a0c5-4d0633d51c2e",
                    "LayerId": "108a5997-50b2-49b9-a993-f1e16f1fd334"
                },
                {
                    "id": "4f761719-021f-4664-a119-56fb57613622",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db1053d1-aad3-4659-a0c5-4d0633d51c2e",
                    "LayerId": "475a04db-05db-4e8f-bb78-696151cb7be4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "475a04db-05db-4e8f-bb78-696151cb7be4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "108a5997-50b2-49b9-a993-f1e16f1fd334",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d379671e-ba98-4399-873f-f9e6e133b170",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 11,
    "yorig": 19
}