{
    "id": "4ae4eb46-d55b-4ab8-a0c7-f3150eae59ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_hitstun_heavy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 0,
    "bbox_right": 98,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc7cd18e-6378-4d30-af77-39e2f373e8a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ae4eb46-d55b-4ab8-a0c7-f3150eae59ab",
            "compositeImage": {
                "id": "8135b983-91a1-4b6c-805c-a22549bd011e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc7cd18e-6378-4d30-af77-39e2f373e8a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "281fc619-da5f-4fba-b047-e0b29d60163b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc7cd18e-6378-4d30-af77-39e2f373e8a3",
                    "LayerId": "ab7d7a15-5ab2-4bc2-98ab-dee9d8255dad"
                }
            ]
        },
        {
            "id": "9a5c9cde-fef7-4237-835f-225c4a212e74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ae4eb46-d55b-4ab8-a0c7-f3150eae59ab",
            "compositeImage": {
                "id": "26825bbe-fe2a-4105-bca4-671929d31ac7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a5c9cde-fef7-4237-835f-225c4a212e74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bc1c779-30c2-474f-bc88-7194ef47f4e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a5c9cde-fef7-4237-835f-225c4a212e74",
                    "LayerId": "ab7d7a15-5ab2-4bc2-98ab-dee9d8255dad"
                }
            ]
        },
        {
            "id": "a01fc981-4565-42de-9984-73b67f10d727",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ae4eb46-d55b-4ab8-a0c7-f3150eae59ab",
            "compositeImage": {
                "id": "fe0f5fc8-4efb-47b0-a001-bb22b211b786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a01fc981-4565-42de-9984-73b67f10d727",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "726a8064-7e25-414a-9eba-9f66906e503b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a01fc981-4565-42de-9984-73b67f10d727",
                    "LayerId": "ab7d7a15-5ab2-4bc2-98ab-dee9d8255dad"
                }
            ]
        },
        {
            "id": "12f2eaa8-7b8b-4036-8d33-4f0a432f6f7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ae4eb46-d55b-4ab8-a0c7-f3150eae59ab",
            "compositeImage": {
                "id": "a3932028-b5f5-4746-9e6c-b180fb7faa45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12f2eaa8-7b8b-4036-8d33-4f0a432f6f7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a47775d2-4a80-4b0d-91b4-9d7e6b0e5bbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f2eaa8-7b8b-4036-8d33-4f0a432f6f7a",
                    "LayerId": "ab7d7a15-5ab2-4bc2-98ab-dee9d8255dad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 84,
    "layers": [
        {
            "id": "ab7d7a15-5ab2-4bc2-98ab-dee9d8255dad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ae4eb46-d55b-4ab8-a0c7-f3150eae59ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 99,
    "xorig": 49,
    "yorig": 83
}