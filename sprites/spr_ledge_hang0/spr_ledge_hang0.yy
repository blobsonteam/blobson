{
    "id": "6e404e32-0029-4f13-bcca-833f0c7643aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ledge_hang0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 4,
    "bbox_right": 23,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8060b47d-48ef-4954-962c-f07270c538f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e404e32-0029-4f13-bcca-833f0c7643aa",
            "compositeImage": {
                "id": "f56eaab6-2acb-41be-a439-2dcf066123b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8060b47d-48ef-4954-962c-f07270c538f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab14f3f7-2029-44d2-abf2-7a329e39ccc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8060b47d-48ef-4954-962c-f07270c538f0",
                    "LayerId": "6210807e-71fd-417e-a924-541ee5c632c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6210807e-71fd-417e-a924-541ee5c632c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e404e32-0029-4f13-bcca-833f0c7643aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 11,
    "yorig": 14
}