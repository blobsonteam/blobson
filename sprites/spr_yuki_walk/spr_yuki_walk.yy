{
    "id": "34c61fce-4fe6-4769-969e-75b54f113efe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yuki_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 6,
    "bbox_right": 117,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ae8fb3d-8725-4486-bdb4-e317ef5f95c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34c61fce-4fe6-4769-969e-75b54f113efe",
            "compositeImage": {
                "id": "b3d980ee-e5cc-4357-82da-de868cd19b9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ae8fb3d-8725-4486-bdb4-e317ef5f95c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc27ce35-e873-4feb-b5ad-2609d5899b2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ae8fb3d-8725-4486-bdb4-e317ef5f95c9",
                    "LayerId": "df6ecf9b-4996-4a58-8867-f5c17883e667"
                }
            ]
        },
        {
            "id": "f9525e38-daee-4ad1-a503-be2dcea5d88f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34c61fce-4fe6-4769-969e-75b54f113efe",
            "compositeImage": {
                "id": "55e76034-cd0f-434e-8b79-94b8aeedfca3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9525e38-daee-4ad1-a503-be2dcea5d88f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "057c0117-334f-49a5-a11e-2166df9557b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9525e38-daee-4ad1-a503-be2dcea5d88f",
                    "LayerId": "df6ecf9b-4996-4a58-8867-f5c17883e667"
                }
            ]
        },
        {
            "id": "f6d52ccc-5e28-4576-aa2a-9aacdd58e23a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34c61fce-4fe6-4769-969e-75b54f113efe",
            "compositeImage": {
                "id": "da2a81ac-b656-49fe-9e48-b174c580e271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6d52ccc-5e28-4576-aa2a-9aacdd58e23a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e9276c7-9006-4034-b748-1d64d5f342ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6d52ccc-5e28-4576-aa2a-9aacdd58e23a",
                    "LayerId": "df6ecf9b-4996-4a58-8867-f5c17883e667"
                }
            ]
        },
        {
            "id": "c731e079-99a6-4bec-8510-87c0b41d257c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34c61fce-4fe6-4769-969e-75b54f113efe",
            "compositeImage": {
                "id": "0b90d4e7-5459-4aa0-976c-83ae3ff08bb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c731e079-99a6-4bec-8510-87c0b41d257c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58e759c4-4d28-4931-94ee-4eedbd6e940e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c731e079-99a6-4bec-8510-87c0b41d257c",
                    "LayerId": "df6ecf9b-4996-4a58-8867-f5c17883e667"
                }
            ]
        },
        {
            "id": "63136037-ba72-4139-9fd3-2c0590452422",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34c61fce-4fe6-4769-969e-75b54f113efe",
            "compositeImage": {
                "id": "9e0ba50e-262f-4df8-9689-40ccad24953f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63136037-ba72-4139-9fd3-2c0590452422",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03124bef-36be-44dc-8451-65977190276b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63136037-ba72-4139-9fd3-2c0590452422",
                    "LayerId": "df6ecf9b-4996-4a58-8867-f5c17883e667"
                }
            ]
        },
        {
            "id": "c3700639-787d-4c1c-8239-d8a159ce965e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34c61fce-4fe6-4769-969e-75b54f113efe",
            "compositeImage": {
                "id": "c1790c2d-2b35-40cd-b9e7-2e94cc8d4623",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3700639-787d-4c1c-8239-d8a159ce965e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbe4f171-415e-423a-aa22-960bfb721055",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3700639-787d-4c1c-8239-d8a159ce965e",
                    "LayerId": "df6ecf9b-4996-4a58-8867-f5c17883e667"
                }
            ]
        },
        {
            "id": "0a624657-df8a-4f05-bfcd-b5bbec1d9ba4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34c61fce-4fe6-4769-969e-75b54f113efe",
            "compositeImage": {
                "id": "ea8e8732-0255-4ecc-a813-0c7c1d013883",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a624657-df8a-4f05-bfcd-b5bbec1d9ba4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dfc3cb7-64be-4bb2-aa46-b0ff4469c336",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a624657-df8a-4f05-bfcd-b5bbec1d9ba4",
                    "LayerId": "df6ecf9b-4996-4a58-8867-f5c17883e667"
                }
            ]
        },
        {
            "id": "33c10122-dd98-4150-9ec8-161c756bc6df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34c61fce-4fe6-4769-969e-75b54f113efe",
            "compositeImage": {
                "id": "26d4d4dd-fcb6-4fb4-a7dc-8627bd3f09a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33c10122-dd98-4150-9ec8-161c756bc6df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "718635e7-1dd0-4d85-8432-742e838b0953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33c10122-dd98-4150-9ec8-161c756bc6df",
                    "LayerId": "df6ecf9b-4996-4a58-8867-f5c17883e667"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "df6ecf9b-4996-4a58-8867-f5c17883e667",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34c61fce-4fe6-4769-969e-75b54f113efe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 75,
    "yorig": 113
}