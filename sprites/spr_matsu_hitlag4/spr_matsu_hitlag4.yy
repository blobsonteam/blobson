{
    "id": "239a73ee-d26e-4c02-85ce-e3c1009a8cf6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_hitlag4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 0,
    "bbox_right": 83,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a9a6122-7f2a-493d-81fb-6a8fcf100963",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "239a73ee-d26e-4c02-85ce-e3c1009a8cf6",
            "compositeImage": {
                "id": "969f2cda-bdc9-4f31-b496-9ce3a46c0084",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a9a6122-7f2a-493d-81fb-6a8fcf100963",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71d43697-6a5d-4bca-b1e9-9a782e9f45a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a9a6122-7f2a-493d-81fb-6a8fcf100963",
                    "LayerId": "382611d5-d5a3-4729-a836-9adb6e0abf95"
                }
            ]
        },
        {
            "id": "0a998dc9-9053-4665-aedb-4046566f2e58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "239a73ee-d26e-4c02-85ce-e3c1009a8cf6",
            "compositeImage": {
                "id": "0fb542d5-c07a-4740-9acc-87bbc193c202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a998dc9-9053-4665-aedb-4046566f2e58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "424c5837-7387-437d-a27d-ec5e2997b79d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a998dc9-9053-4665-aedb-4046566f2e58",
                    "LayerId": "382611d5-d5a3-4729-a836-9adb6e0abf95"
                }
            ]
        },
        {
            "id": "3e6bc40e-c318-46d1-a4f7-32ab790c18af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "239a73ee-d26e-4c02-85ce-e3c1009a8cf6",
            "compositeImage": {
                "id": "c796e964-da23-4210-b16e-ad3b7a8e8de9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e6bc40e-c318-46d1-a4f7-32ab790c18af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ecf29d6-a605-499f-9dc6-2073653bb062",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e6bc40e-c318-46d1-a4f7-32ab790c18af",
                    "LayerId": "382611d5-d5a3-4729-a836-9adb6e0abf95"
                }
            ]
        },
        {
            "id": "56351c8a-5a94-453b-a844-11fc442e8c4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "239a73ee-d26e-4c02-85ce-e3c1009a8cf6",
            "compositeImage": {
                "id": "5b305d23-bf6a-43e5-8a0c-666ee122c88a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56351c8a-5a94-453b-a844-11fc442e8c4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb97afa2-8eb2-4cf4-aeba-3db0c84072a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56351c8a-5a94-453b-a844-11fc442e8c4e",
                    "LayerId": "382611d5-d5a3-4729-a836-9adb6e0abf95"
                }
            ]
        },
        {
            "id": "c19c5011-d87d-43dc-b8c4-3edb8ab8eb08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "239a73ee-d26e-4c02-85ce-e3c1009a8cf6",
            "compositeImage": {
                "id": "16db6e03-87b0-4ca8-8b60-86a7503acb37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c19c5011-d87d-43dc-b8c4-3edb8ab8eb08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6e6b866-b72d-4087-a4a0-b01fe853f567",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c19c5011-d87d-43dc-b8c4-3edb8ab8eb08",
                    "LayerId": "382611d5-d5a3-4729-a836-9adb6e0abf95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 95,
    "layers": [
        {
            "id": "382611d5-d5a3-4729-a836-9adb6e0abf95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "239a73ee-d26e-4c02-85ce-e3c1009a8cf6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0.2,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 40,
    "yorig": 94
}