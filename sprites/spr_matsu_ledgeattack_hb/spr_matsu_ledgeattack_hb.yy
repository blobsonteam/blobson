{
    "id": "ed0cb398-f046-4c65-9613-ea19bbc462bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_ledgeattack_hb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 0,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86a13e86-d246-4b16-acb6-59b2766ea1ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed0cb398-f046-4c65-9613-ea19bbc462bb",
            "compositeImage": {
                "id": "ab079b01-ebb3-45f0-8b1c-aafacf623b48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86a13e86-d246-4b16-acb6-59b2766ea1ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54d171c9-f0c5-446e-a730-156613284429",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86a13e86-d246-4b16-acb6-59b2766ea1ff",
                    "LayerId": "1f05e72c-c128-47da-a8c4-25c243e56367"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "1f05e72c-c128-47da-a8c4-25c243e56367",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed0cb398-f046-4c65-9613-ea19bbc462bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 61,
    "xorig": 0,
    "yorig": 40
}