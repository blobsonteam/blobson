{
    "id": "69e99446-12f5-4aa1-840b-3097746bc438",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_matsu_bair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 105,
    "bbox_left": 0,
    "bbox_right": 116,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1290e256-62d1-4744-b4df-8cb72f4b637b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e99446-12f5-4aa1-840b-3097746bc438",
            "compositeImage": {
                "id": "de25602d-30be-451b-a758-93f002484065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1290e256-62d1-4744-b4df-8cb72f4b637b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77369cf8-46a1-485c-8430-2b5424b0a1b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1290e256-62d1-4744-b4df-8cb72f4b637b",
                    "LayerId": "956934c3-26d5-4eac-beb8-3d3c43310b26"
                }
            ]
        },
        {
            "id": "479bc42f-5980-4455-9cd1-32216d581646",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e99446-12f5-4aa1-840b-3097746bc438",
            "compositeImage": {
                "id": "0aea2d5a-fd19-4eae-8cf8-108a0d03a63f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "479bc42f-5980-4455-9cd1-32216d581646",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc04cb5e-141e-4d8d-a16d-b65347a78c7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "479bc42f-5980-4455-9cd1-32216d581646",
                    "LayerId": "956934c3-26d5-4eac-beb8-3d3c43310b26"
                }
            ]
        },
        {
            "id": "c9639c80-aedb-49ce-9fbc-5102c52528c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e99446-12f5-4aa1-840b-3097746bc438",
            "compositeImage": {
                "id": "460e10a8-76fe-42c4-b2bf-cf72df91bce2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9639c80-aedb-49ce-9fbc-5102c52528c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92194cd6-4642-4ef3-a284-f3bc555977f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9639c80-aedb-49ce-9fbc-5102c52528c2",
                    "LayerId": "956934c3-26d5-4eac-beb8-3d3c43310b26"
                }
            ]
        },
        {
            "id": "94dd5125-fbe8-478d-a2e7-5de39662cba4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e99446-12f5-4aa1-840b-3097746bc438",
            "compositeImage": {
                "id": "cbca6316-b067-405b-ae0b-430e6dc1d6b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94dd5125-fbe8-478d-a2e7-5de39662cba4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2bf4814-95fb-4565-a9a8-f31a5c1dc7c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94dd5125-fbe8-478d-a2e7-5de39662cba4",
                    "LayerId": "956934c3-26d5-4eac-beb8-3d3c43310b26"
                }
            ]
        },
        {
            "id": "f241a951-1d2e-4a69-84c4-d08c1ce0a05f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e99446-12f5-4aa1-840b-3097746bc438",
            "compositeImage": {
                "id": "f28c4135-4a49-414a-8afe-a1cad2d1673e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f241a951-1d2e-4a69-84c4-d08c1ce0a05f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59fa54f1-94f7-49f0-8943-f77934c215cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f241a951-1d2e-4a69-84c4-d08c1ce0a05f",
                    "LayerId": "956934c3-26d5-4eac-beb8-3d3c43310b26"
                }
            ]
        },
        {
            "id": "fdd43340-f392-4367-bb61-def064d15d50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e99446-12f5-4aa1-840b-3097746bc438",
            "compositeImage": {
                "id": "67c5bef6-704e-4bda-90f2-53576ae6edc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdd43340-f392-4367-bb61-def064d15d50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f5030b9-45df-4587-99c0-5a6024072cac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdd43340-f392-4367-bb61-def064d15d50",
                    "LayerId": "956934c3-26d5-4eac-beb8-3d3c43310b26"
                }
            ]
        },
        {
            "id": "b9d872d2-4413-4849-bd1f-d5eaef7cabc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69e99446-12f5-4aa1-840b-3097746bc438",
            "compositeImage": {
                "id": "d7b042e0-1c69-4f40-b35a-283433fff010",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9d872d2-4413-4849-bd1f-d5eaef7cabc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c6d91d3-3ae8-4ad1-ba26-6e870d8148f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9d872d2-4413-4849-bd1f-d5eaef7cabc9",
                    "LayerId": "956934c3-26d5-4eac-beb8-3d3c43310b26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "956934c3-26d5-4eac-beb8-3d3c43310b26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69e99446-12f5-4aa1-840b-3097746bc438",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 117,
    "xorig": 82,
    "yorig": 105
}