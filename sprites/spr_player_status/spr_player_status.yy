{
    "id": "e0252482-00bf-4937-932d-ee198cfa8d22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_status",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 3,
    "bbox_right": 317,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea1dd982-56e3-49aa-a61d-25925bab98da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0252482-00bf-4937-932d-ee198cfa8d22",
            "compositeImage": {
                "id": "9582771d-6d37-4c2c-9cd1-b186866ad0d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea1dd982-56e3-49aa-a61d-25925bab98da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f8e957c-852b-41fb-b32c-696b264015a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea1dd982-56e3-49aa-a61d-25925bab98da",
                    "LayerId": "0f881c4b-43e8-43bf-903f-c7a6ba876aa3"
                }
            ]
        },
        {
            "id": "e9ec3cd5-d3b5-40f0-bbf6-76681bb2ac55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0252482-00bf-4937-932d-ee198cfa8d22",
            "compositeImage": {
                "id": "ead451e0-6d48-4787-af74-e491e0746de3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9ec3cd5-d3b5-40f0-bbf6-76681bb2ac55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64c2cdc1-22ac-47e9-ac94-04791b9995ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9ec3cd5-d3b5-40f0-bbf6-76681bb2ac55",
                    "LayerId": "0f881c4b-43e8-43bf-903f-c7a6ba876aa3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0f881c4b-43e8-43bf-903f-c7a6ba876aa3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0252482-00bf-4937-932d-ee198cfa8d22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}